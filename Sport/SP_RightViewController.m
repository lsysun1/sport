//
//  SP_RightViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/13.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RightViewController.h"
#import "SP_FocusTeamViewController.h"
#import "SP_AddFriendViewController.h"
#import "SP_AddTeamMateViewController.h"
#import "SP_MyMarkViewController.h"

@interface SP_RightViewController ()

@end

@implementation SP_RightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.view.backgroundColor = [UIColor redColor];
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(- 180, 0, ScreenWidth , ScreenHeight)];
    img.image = [UIImage imageNamed:@"login_bg.jpg"];
    [self.view addSubview:img];
    
    [self setMenuBtn];
    
}



- (void) setMenuBtn
{
    CGFloat btnY = 84;
    for(int i=1;i<=4;i++){
        NSString *imgUrl = [NSString stringWithFormat:@"slider_%d",i];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(40, btnY, 60, 60)];
        imgView.image = [UIImage imageNamed:imgUrl];
        [self.view addSubview:imgView];
        
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, btnY-20, 250, 90)];
        btn.tag = i;
        [btn setBackgroundImage:[UIImage imageNamed:@"slider_sel"] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        if(i <3){
            btnY += 80;
        }else{
            btnY += 200;
        }
    }
}

- (void) btnDidClick:(UIButton *)btn
{
    NSLog(@"DidClick btn.tag -- %ld",btn.tag);
    if(btn.tag == 1){
        SP_FocusTeamViewController *vc = [[SP_FocusTeamViewController alloc]init];
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }else if (btn.tag == 2){
        SP_MyMarkViewController *vc = [[SP_MyMarkViewController alloc]init];
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
    }else if (btn.tag == 3){
        SP_AddTeamMateViewController *vc = [[SP_AddTeamMateViewController alloc]init];
        vc.type = @"1";
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }else{
        SP_AddFriendViewController *vc = [[SP_AddFriendViewController alloc]init];
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
