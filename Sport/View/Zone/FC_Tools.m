//
//  FC_Tools.m
//  Finance
//
//  Created by evan on 14/11/3.
//  Copyright (c) 2014年 inwhoop. All rights reserved.
//

#import "FC_Tools.h"

@implementation FC_Tools

+ (BOOL)isLogin
{
    NSString *userid = GETUSERID;
    if (userid == nil)
    {
        return NO;
    }
    return YES;
}

+ (void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

+ (UILabel *)createLabel:(CGRect)frame andTextColor:(UIColor *)color
                 andText:(NSString *)text andFont:(UIFont *)font
                andAlign:(NSTextAlignment)align Tag:(int)tag
{
    UILabel *nameLabel = [[UILabel alloc] init] ;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = text;
    nameLabel.textColor = color;
    nameLabel.tag = tag;
    nameLabel.font = font;
    nameLabel.frame = frame;
    nameLabel.textAlignment = align;
    return nameLabel;
}

+ (UIView *)createView:(CGRect)frame BackColor:(UIColor *)color Tag:(int)tag
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = color;
    view.tag = tag;
    return view;
}

+ (UIButton *)createButton:(CGRect)frame Title:(NSString *)title
              TitleColor:(UIColor *)color Bgimage:(NSString *)imgname
                     Tag:(int)tag Font:(UIFont *)font
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn.titleLabel setFont:font];
    btn.frame = frame;
    [btn setBackgroundImage:[UIImage imageNamed:imgname] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed",imgname]] forState:UIControlStateHighlighted];
    btn.tag = tag;
    return btn;
}


+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

+ (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline format:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSInteger date = [[NSDate date] timeIntervalSince1970] - [dateline integerValue];
    [formatter setDateFormat:format];
    NSString *dateLoca = [NSString stringWithFormat:@"%@",dateline];
    NSTimeInterval time= [dateLoca doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}

+ (NSString *)dateSince1970:(NSString *)dateline format:(NSString *)format
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = format;
    NSDate *createdDate = [NSDate dateWithTimeIntervalSince1970:[dateline doubleValue]];
    return [fmt stringFromDate:createdDate];
}

#pragma mark --计算字符空间大小
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width
{
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, 1000)//显示的最大容量
                                       options: NSStringDrawingUsesLineFragmentOrigin //描述字符串的附加参数
                                    attributes:@{NSFontAttributeName: font}//描述字符串的参数
                                       context:nil];//上下文
    //返回值
    return rect.size;
}

@end
