//
//  FC_Tools.h
//  Finance
//
//  Created by evan on 14/11/3.
//  Copyright (c) 2014年 inwhoop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FC_Tools : NSObject

/**
 *  是否登录
 *
 *  @return YES-登录 NO-未登录
 */
+ (BOOL)isLogin;

/**
 *  隐藏tableview多余分割线
 *
 *  @param tableView 对象
 */
+ (void)setExtraCellLineHidden: (UITableView *)tableView;

/**
 *  生成UILabel
 *
 *  @param frame label frame
 *  @param color 字体颜色
 *  @param text  文本内容
 *  @param font  字体大小
 *  @param align 对齐方式
 *  @param tag   tag
 *
 *  @return UILabel
 */
+ (UILabel *)createLabel:(CGRect)frame andTextColor:(UIColor *)color
                 andText:(NSString *)text andFont:(UIFont *)font
                andAlign:(NSTextAlignment)align Tag:(int)tag;

/**
 *  生成UIView
 *
 *  @param frame view frame
 *  @param color 背景颜色
 *
 *  @return UIView
 */
+ (UIView *)createView:(CGRect)frame BackColor:(UIColor *)color Tag:(int)tag;

/**
 *  生成UIButton
 *
 *  @param frame   button frame
 *  @param title   标题
 *  @param color   标题颜色
 *  @param imgname 背景图片
 *
 *  @return UIButton
 */
+ (UIButton *)createButton:(CGRect)frame Title:(NSString *)title
              TitleColor:(UIColor *)color Bgimage:(NSString *)imgname
                     Tag:(int)tag Font:(UIFont *)font;


+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;

+ (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline format:(NSString *)format;
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width;
+ (NSString *)dateSince1970:(NSString *)dateline format:(NSString *)format;



@end
