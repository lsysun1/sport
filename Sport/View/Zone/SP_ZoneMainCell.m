//
//  SP_ZoneMainCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZoneMainCell.h"
#import "SP_ZoneContentFrame.h"
#import "SP_ZoneContent.h"
#import "UIImageView+WebCache.h"
#import "SP_CommentBtn.h"
#import "PhotoBroswerVC.h"


@interface SP_ZoneMainCell(){
    
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
    UIView *_imgsView;


    UILabel *_shareNum;
    UILabel *_zanNum;
    UILabel *_commentNum;
    
    

    

}
@end


@implementation SP_ZoneMainCell


+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_ZoneMainCell";
    SP_ZoneMainCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_ZoneMainCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _headImgView = [[UIImageView alloc]init];
        _headImgView.layer.cornerRadius = 25;
        _headImgView.layer.masksToBounds = YES;
        
        self.headButton = [[UIButton alloc]init];
        self.headButton.backgroundColor = [UIColor clearColor];
        
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = UIColorFromRGB(0x696969);
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        _imgsView = [[UIView alloc]init];
        _imgsView.backgroundColor = UIColorFromRGB(0xf3f3f3);
        
        
        
        self.shareBtn = [[UIButton alloc]init];
        [self.shareBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        self.shareBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.shareBtn setTitle:@"分享" forState:UIControlStateNormal];
        [self.shareBtn setImage:[UIImage imageNamed:@"分享"] forState:UIControlStateNormal];
        [self.shareBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 13, 0, 33)];
        
        
        _shareNum = [[UILabel alloc]init];
        _shareNum.textColor = UIColorFromRGB(0x696969);
        _shareNum.font = [UIFont systemFontOfSize:12];
        _shareNum.text = @"(3)";

        
        self.zanBtn = [[SP_CommentBtn alloc]init];
        [self.zanBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        self.zanBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.zanBtn setTitle:@"赞" forState:UIControlStateNormal];
        [self.zanBtn setImage:[UIImage imageNamed:@"赞"] forState:UIControlStateNormal];
        [self.zanBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 13, 0, 33)];

        _zanNum = [[UILabel alloc]init];
        _zanNum.textColor = UIColorFromRGB(0x696969);
        _zanNum.font = [UIFont systemFontOfSize:12];
        _zanNum.text = @"(10)";

        
        self.commentBtn = [[SP_CommentBtn alloc]init];
        [self.commentBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        self.commentBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.commentBtn setTitle:@"评论" forState:UIControlStateNormal];
        [self.commentBtn setImage:[UIImage imageNamed:@"评论"] forState:UIControlStateNormal];
        [self.commentBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 13, 0, 33)];
        
        _commentNum = [[UILabel alloc]init];
        _commentNum.textColor = UIColorFromRGB(0x696969);
        _commentNum.font = [UIFont systemFontOfSize:12];
        _commentNum.text = @"(1)";
    

        self.zanLabel = [UILabel new];
        self.zanLabel.backgroundColor = UIColorFromRGB(0xe5e5e5);;
        self.zanLabel.textColor = UIColorFromRGB(0xfd776c);
        self.zanLabel.font = [UIFont systemFontOfSize:14];
        
        
        [self.contentView addSubview: _headImgView];
        [self.contentView addSubview: _nameLabel];
        [self.contentView addSubview: _timeLabel];
        [self.contentView addSubview: _contentLabel];
        [self.contentView addSubview: _imgsView];
        [self.contentView addSubview: _shareNum];
        [self.contentView addSubview: _zanNum];
        [self.contentView addSubview: _commentNum];
        [self.contentView addSubview: self.zanLabel];
        [self.contentView addSubview: self.headButton];
        [self.contentView addSubview: self.shareBtn];
        [self.contentView addSubview: self.commentBtn];
        [self.contentView addSubview: self.zanBtn];

    }


    return self;
}



- (void) setFrameModel:(SP_ZoneContentFrame *)frameModel
{
    _frameModel = frameModel;
    
    SP_ZoneContent *msg = frameModel.zoneContent;
    
    _commentNum.text = [NSString stringWithFormat:@"(%ld)",frameModel.commentArr.count];
    _shareNum.text = [NSString stringWithFormat:@"(%@)",msg.sharecount];
    _zanNum.text = [NSString stringWithFormat:@"(%@)",msg.zhancount];

    
    _headImgView.frame = frameModel.headImgFrame;
    NSURL *url = [NSURL URLWithString:msg.headimg];
    [_headImgView sd_setImageWithURL:url];
    
    self.headButton.frame = frameModel.headImgFrame;
    
    
    _nameLabel.frame = frameModel.nameFrame;
    _nameLabel.text = msg.nickname;
    
    _timeLabel.frame = frameModel.timeFrame;
    _timeLabel.text = msg.addtime;
    
    _contentLabel.frame = frameModel.contentFrame;
    _contentLabel.text = msg.content;
    
    if(msg.img.count > 0){
        _imgsView.frame = frameModel.imgFrame;
        CGFloat originX = 75;
        CGFloat originY = 3;

        for(int i = 0; i <msg.img.count; i ++){
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(originX, originY, 60, 60)];
            
            NSDictionary *dict = msg.img[i];
            NSString *imgPath = dict[@"img"];
//            NSLog(@"imgPath -- %@",imgPath);
            NSURL *url = [NSURL URLWithString:imgPath];
            
            [imgView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default_small"] options:SDWebImageCacheMemoryOnly];
            [_imgsView addSubview:imgView];
            
            
            UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(originX, originY, 60, 60)];
            btn.tag = i;
            [btn addTarget:self action:@selector(imgBtn:) forControlEvents:UIControlEventTouchUpInside];
            [_imgsView addSubview:btn];

            
            // 计算下个view的尺寸
            originX += 70;

            if((i+1)%3 == 0 && i != 0){
                originX = 0;
                originY += 70;
            }

    
    }
    
    }
    
    
    
    
    
    
    self.commentBtn.frame = frameModel.commentBtnFrame;
    _commentNum.frame = frameModel.commentNumFrame;
    _zanNum.frame = frameModel.zanNumFrame;
    self.zanBtn.frame= frameModel.zanBtnFrame;
    _shareBtn.frame = frameModel.shareBtnFrame;
    _shareNum.frame = frameModel.shareNumFrame;
    
    
    self.zanLabel.frame = frameModel.zanLabelFrame;
    self.zanLabel.attributedText = frameModel.zanAttrSrt;
}


- (void)imgBtn:(UIButton*)btn
{
    SP_ZoneContent *msg  = self.frameModel.zoneContent;
    [self networkImageShow:btn.tag imgArr:msg.img];
}




/*
 *  展示网络图片
 */
-(void)networkImageShow:(NSUInteger)index imgArr:(NSArray *)networkImages
{
    
    [PhotoBroswerVC show:self.vc type:PhotoBroswerVCTypeModal cateListModel:nil index:index photoModelBlock:^NSArray *{
        
        NSMutableArray *modelsM = [NSMutableArray arrayWithCapacity:networkImages.count];
        for (NSUInteger i = 0; i< networkImages.count; i++) {
            
            PhotoModel *pbModel=[[PhotoModel alloc] init];
            pbModel.mid = i + 1;
            
            NSDictionary *dict = networkImages[i];
            NSString *imgPath = dict[@"img"];

            
            pbModel.image_HD_U = imgPath;
            
            
            [modelsM addObject:pbModel];
        }
        
        return modelsM;
    }];
}


/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
