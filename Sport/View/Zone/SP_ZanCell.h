//
//  SP_ZanCell.h
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SP_ZoneContentFrame;
@interface SP_ZanCell : UITableViewCell

@property (nonatomic,strong) SP_ZoneContentFrame *zanCellFrame;
+ (instancetype) cellWithTableView:(UITableView *) tableView;
@end


