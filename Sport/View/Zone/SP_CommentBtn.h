//
//  SP_CommentBtn.h
//  Sport
//
//  Created by 李松玉 on 15/6/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_CommentBtn : UIButton

@property (nonatomic,copy) NSString *pyqid;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,assign) NSIndexPath *index;

@end
