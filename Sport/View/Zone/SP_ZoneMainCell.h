//
//  SP_ZoneMainCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_ZoneContentFrame,SP_CommentBtn,SP_ZoneViewController;
@interface SP_ZoneMainCell : UITableViewCell




@property (strong,nonatomic) UIButton *headButton;
@property (nonatomic,strong) SP_ZoneContentFrame *frameModel;
@property (nonatomic,strong) SP_CommentBtn *commentBtn;
@property (nonatomic,strong) SP_CommentBtn *zanBtn;
@property (nonatomic,assign) NSIndexPath *index;
@property (nonatomic,strong) UILabel *zanLabel;

@property (nonatomic,strong) UIButton *shareBtn;



@property (nonatomic,strong) SP_ZoneViewController *vc;

+ (instancetype) cellWithTableView:(UITableView *) tableView;


@end
