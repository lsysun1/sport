//
//  SP_CommentTextView.h
//  Sport
//
//  Created by 李松玉 on 15/7/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_CommentTextView,SP_CommentTextSpecial;
@protocol SP_CommentTextViewDelegate <NSObject>

- (void)cellNameDidClick:(SP_CommentTextSpecial *)Special;

@end


@interface SP_CommentTextView : UITextView

@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *hfName;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *pyqplid;
@property (nonatomic,strong) NSIndexPath *indexNum;



@property (nonatomic,assign) NSRange nickNameRange;
@property (nonatomic,assign) NSRange huifuNameRange;

@property (nonatomic,strong) NSMutableArray *rangArr;


@property (nonatomic,assign) id<SP_CommentTextViewDelegate> nameDelagate;

@end
