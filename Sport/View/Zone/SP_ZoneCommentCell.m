//
//  SP_ZoneCommentCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZoneCommentCell.h"
#import "SP_ZoneCommentFrame.h"
#import "VerticallyAlignedLabel.h"
#import "SP_ZoneCommentEntiry.h"
#import "SP_CommentTextSpecial.h"
#import "SP_TextModel.h"
#import "RegexKitLite.h"


@interface SP_ZoneCommentCell()
@end

@implementation SP_ZoneCommentCell

+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_ZoneCommentCell";
    SP_ZoneCommentCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_ZoneCommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        
        self.bgImg = [[UIImageView alloc]init];
        
        
        self.commentTextView = [[SP_CommentTextView alloc]init];
        
        
        [self.contentView addSubview:self.bgImg];
        [self.contentView addSubview:self.commentTextView];
    }

    return self;
}


- (void) setFrameModel:(SP_ZoneCommentFrame *)frameModel
{
    _frameModel = frameModel;
    
    
    
    self.commentTextView.frame = frameModel.commentLabelFrame;
    self.commentTextView.indexNum = self.indexNum;
    
    SP_ZoneCommentEntiry *commentEntiry = frameModel.commentEntiry;

    NSString *hfName = commentEntiry.hfnickname;
    NSString *nickName = commentEntiry.nickname;
    NSString *content = commentEntiry.content;
    
    NSString *userSex = commentEntiry.sex;
    NSString *hfUserSex = commentEntiry.hfsex;
    
    
    
    
    
    if(hfName.length == 0){
    
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:",nickName]];
        
        if([userSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(0,nickName.length)];
        }else{
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(0,nickName.length)];
        }
        
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, str.length)];

        

        
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",content]];
        
        // 表情的规则
        NSString *emotionPattern = @"\\[[0-9a-zA-Z\\u4e00-\\u9fa5]+\\]";
        
        // 遍历所有的特殊字符串
        NSMutableArray *parts = [NSMutableArray array];
        [content enumerateStringsMatchedByRegex:emotionPattern usingBlock:^(NSInteger captureCount, NSString *const __unsafe_unretained *capturedStrings, const NSRange *capturedRanges, volatile BOOL *const stop) {
            if ((*capturedRanges).length == 0) return;
            
            SP_TextModel *part = [[SP_TextModel alloc] init];
            part.special = YES;
            part.emotion = YES;
            part.text = *capturedStrings;
            part.range = *capturedRanges;
            [parts addObject:part];
        }];
        
        
        // 遍历所有的非特殊字符
        [content enumerateStringsSeparatedByRegex:emotionPattern usingBlock:^(NSInteger captureCount, NSString *const __unsafe_unretained *capturedStrings, const NSRange *capturedRanges, volatile BOOL *const stop) {
            if ((*capturedRanges).length == 0) return;
            SP_TextModel *part = [[SP_TextModel alloc] init];
            part.special = NO;
            part.text = *capturedStrings;
            part.range = *capturedRanges;
            [parts addObject:part];
        }];
        

        // 排序
        // 系统是按照从小 -> 大的顺序排列对象
        [parts sortUsingComparator:^NSComparisonResult(SP_TextModel *part1, SP_TextModel *part2) {

            if (part1.range.location > part2.range.location) {

                return NSOrderedDescending;
            }

            return NSOrderedAscending;
        }];
        

        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"myemoji" ofType:@"plist"];
        NSDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];

        
        
        UIFont *font = [UIFont systemFontOfSize:15];
        NSMutableArray *specials = [NSMutableArray array];
        // 按顺序拼接每一段文字
        for (SP_TextModel *part in parts) {
            // 等会需要拼接的子串
            NSAttributedString *substr = nil;
            if (part.isEmotion) { // 表情
                NSTextAttachment *attch = [[NSTextAttachment alloc] init];
                NSString *imgstr = data[part.text];
                attch.image = [UIImage imageNamed:imgstr];
                attch.bounds = CGRectMake(0, -3, font.lineHeight, font.lineHeight);
                substr = [NSAttributedString attributedStringWithAttachment:attch];
                
            } else if (part.special) { // 非表情的特殊文字
                substr = [[NSAttributedString alloc] initWithString:part.text attributes:@{
                                                                                           NSForegroundColorAttributeName : [UIColor redColor]
                                                                                           }];
                
                // 创建特殊对象
                SP_TextModel *s = [[SP_TextModel alloc] init];
                s.text = part.text;
                NSUInteger loc = contentStr.length;
                NSUInteger len = part.text.length;
                s.range = NSMakeRange(loc, len);
                [specials addObject:s];
            } else { // 非特殊文字
                substr = [[NSAttributedString alloc] initWithString:part.text];
            }
            [str appendAttributedString:substr];
        }
        
        
        
        
        
        NSRange nickNameRange = NSMakeRange(0,nickName.length);
        self.commentTextView.nickName = commentEntiry.nickname;
        self.commentTextView.pyqplid = commentEntiry.pyqplid;
        self.commentTextView.nickNameRange = nickNameRange;
        
        
        SP_CommentTextSpecial *s = [[SP_CommentTextSpecial alloc]init];
        s.pyqid = commentEntiry.pyqid;
        s.range = nickNameRange;
        s.nickName = commentEntiry.nickname;
        s.pyqplid = commentEntiry.pyqplid;
        s.indexNum = self.indexNum;
        s.uid = commentEntiry.uid;

        NSMutableArray *rangArr = [NSMutableArray array];
        [rangArr addObject:s];
        
        self.commentTextView.rangArr = rangArr;
        
        
        self.commentTextView.attributedText = str;
        
        
    }else{
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@回复%@:",nickName,hfName]];
        
        
        
        if([userSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(0,nickName.length)];
        }else{
            
            
            
            
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(0,nickName.length)];
        }
        
        
        
        
        
        if([hfUserSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(nickName.length + 2,hfName.length)];
        }else{
            
            
            
            
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(nickName.length + 2,hfName.length)];
        }

        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, str.length)];
        
        
        
        
        
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",content]];
        
        // 表情的规则
        NSString *emotionPattern = @"\\[[0-9a-zA-Z\\u4e00-\\u9fa5]+\\]";
        
        // 遍历所有的特殊字符串
        NSMutableArray *parts = [NSMutableArray array];
        [content enumerateStringsMatchedByRegex:emotionPattern usingBlock:^(NSInteger captureCount, NSString *const __unsafe_unretained *capturedStrings, const NSRange *capturedRanges, volatile BOOL *const stop) {
            if ((*capturedRanges).length == 0) return;
            
            SP_TextModel *part = [[SP_TextModel alloc] init];
            part.special = YES;
            part.emotion = YES;
            part.text = *capturedStrings;
            part.range = *capturedRanges;
            [parts addObject:part];
        }];
        
        
        // 遍历所有的非特殊字符
        [content enumerateStringsSeparatedByRegex:emotionPattern usingBlock:^(NSInteger captureCount, NSString *const __unsafe_unretained *capturedStrings, const NSRange *capturedRanges, volatile BOOL *const stop) {
            if ((*capturedRanges).length == 0) return;
            SP_TextModel *part = [[SP_TextModel alloc] init];
            part.special = NO;
            part.text = *capturedStrings;
            part.range = *capturedRanges;
            [parts addObject:part];
        }];
        
        
        // 排序
        // 系统是按照从小 -> 大的顺序排列对象
        [parts sortUsingComparator:^NSComparisonResult(SP_TextModel *part1, SP_TextModel *part2) {
            
            if (part1.range.location > part2.range.location) {
                
                return NSOrderedDescending;
            }
            
            return NSOrderedAscending;
        }];
        
        
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"myemoji" ofType:@"plist"];
        NSDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        
        
        
        UIFont *font = [UIFont systemFontOfSize:15];
        NSMutableArray *specials = [NSMutableArray array];
        // 按顺序拼接每一段文字
        for (SP_TextModel *part in parts) {
            // 等会需要拼接的子串
            NSAttributedString *substr = nil;
            if (part.isEmotion) { // 表情
                NSTextAttachment *attch = [[NSTextAttachment alloc] init];
                NSString *imgstr = data[part.text];
                attch.image = [UIImage imageNamed:imgstr];
                attch.bounds = CGRectMake(0, -3, font.lineHeight, font.lineHeight);
                substr = [NSAttributedString attributedStringWithAttachment:attch];
                
            } else if (part.special) { // 非表情的特殊文字
                substr = [[NSAttributedString alloc] initWithString:part.text attributes:@{
                                                                                           NSForegroundColorAttributeName : [UIColor redColor]
                                                                                           }];
                
                // 创建特殊对象
                SP_TextModel *s = [[SP_TextModel alloc] init];
                s.text = part.text;
                NSUInteger loc = contentStr.length;
                NSUInteger len = part.text.length;
                s.range = NSMakeRange(loc, len);
                [specials addObject:s];
            } else { // 非特殊文字
                substr = [[NSAttributedString alloc] initWithString:part.text];
            }
            [str appendAttributedString:substr];
        }
        
        
        
        
        
        
        
        
        
        self.commentTextView.attributedText = str;
        
        
        NSRange nickNameRange = NSMakeRange(0,nickName.length);
        NSRange huifuNameRange = NSMakeRange(nickName.length + 2,hfName.length);
        
        self.commentTextView.nickName = commentEntiry.nickname;
        self.commentTextView.hfName = commentEntiry.hfnickname;
        self.commentTextView.pyqplid = commentEntiry.pyqplid;
        
        self.commentTextView.nickNameRange = nickNameRange;
        self.commentTextView.huifuNameRange = huifuNameRange;
        
        
        SP_CommentTextSpecial *s1 = [[SP_CommentTextSpecial alloc]init];
        s1.pyqid = commentEntiry.pyqid;
        s1.nickName = commentEntiry.nickname;
        s1.pyqplid = commentEntiry.pyqplid;
        s1.range = nickNameRange;
        s1.indexNum = self.indexNum;
        s1.uid = commentEntiry.uid;

        
        SP_CommentTextSpecial *s2 = [[SP_CommentTextSpecial alloc]init];
        s2.pyqid = commentEntiry.pyqid;
        s2.hfName = commentEntiry.hfnickname;
        s2.pyqplid = commentEntiry.pyqplid;
        s2.range = huifuNameRange;
        s2.indexNum = self.indexNum;
        s2.hfuid = commentEntiry.hfuid;

        
        NSMutableArray *rangArr = [NSMutableArray array];
        [rangArr addObject:s1];
        [rangArr addObject:s2];
        
        self.commentTextView.rangArr = rangArr;

        

    }
    
    
    
    
//    self.bgImg.image = [self resizeWithImageName:@"评论显示背景框"];
    
    
    
    
    
}




//返回一个可拉伸的图片
- (UIImage *)resizeWithImageName:(NSString *)name
{
    UIImage *normal = [UIImage imageNamed:name];
    
    //    CGFloat w = normal.size.width * 0.5f ;
    //    CGFloat h = normal.size.height *0.5f ;
    
    CGFloat w = normal.size.width*0.5;
    CGFloat h = normal.size.height*0.5;
    //传入上下左右不需要拉升的编剧，只拉伸中间部分
    return [normal resizableImageWithCapInsets:UIEdgeInsetsMake(h, w, h, w)];
    
    //    [normal resizableImageWithCapInsets:UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)]
    
    // 1 = width - leftCapWidth  - right
    // 1 = height - topCapHeight  - bottom
    
    //传入上下左右不需要拉升的编剧，只拉伸中间部分，并且传入模式（平铺/拉伸）
    //    [normal :<#(UIEdgeInsets)#> resizingMode:<#(UIImageResizingMode)#>]
    
    //只用传入左边和顶部不需要拉伸的位置，系统会算出右边和底部不需要拉升的位置。并且中间有1X1的点用于拉伸或者平铺
    // 1 = width - leftCapWidth  - right
    // 1 = height - topCapHeight  - bottom
    //    return [normal stretchableImageWithLeftCapWidth:w topCapHeight:h];
}




- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
