//
//  SP_ZoneCommentCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SP_CommentTextView.h"

@class SP_ZoneCommentFrame;
@interface SP_ZoneCommentCell : UITableViewCell


@property (nonatomic,strong) NSDictionary *commentDict;
@property (nonatomic,strong) SP_ZoneCommentFrame *frameModel;
@property (nonatomic,strong) SP_CommentTextView *commentTextView;



@property (nonatomic,strong) UIImageView *bgImg;
@property (nonatomic,strong) NSIndexPath *indexNum;
@property (nonatomic,strong) NSMutableArray *rangArr;

+ (instancetype) cellWithTableView:(UITableView *) tableView;

@end
