//
//  SP_TeamMemberCollectionCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_TeamMemberCollectionCell.h"
#import "SP_TeamMateModel.h"
#import "UIImageView+WebCache.h"

@interface SP_TeamMemberCollectionCell()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *name;


@end


@implementation SP_TeamMemberCollectionCell

- (void)awakeFromNib {
    // Initialization code
    
    self.imgView.layer.cornerRadius = 8;
    self.imgView.layer.masksToBounds = YES;
    
}




- (void) setMateInfo:(SP_TeamMateModel *)mateInfo
{
    _mateInfo = mateInfo;
    
    NSURL *url = [NSURL URLWithString:mateInfo.headimg];
    [self.imgView sd_setImageWithURL:url];
    
    self.name.text = mateInfo.nickname;

}



@end
