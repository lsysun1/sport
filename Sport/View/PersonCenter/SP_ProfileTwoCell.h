//
//  SP_ProfileTwoCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_ProfileTwoCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
