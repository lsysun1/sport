//
//  SP_QuitCell.h
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_QuitCell : UITableViewCell

+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
