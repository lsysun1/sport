//
//  SP_TeamMemberCollectionCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_TeamMateModel;
@interface SP_TeamMemberCollectionCell : UICollectionViewCell
@property (nonatomic, strong) SP_TeamMateModel *mateInfo;
@end
