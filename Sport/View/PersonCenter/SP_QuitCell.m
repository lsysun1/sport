//
//  SP_QuitCell.m
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_QuitCell.h"

@interface SP_QuitCell()
@property (weak, nonatomic) IBOutlet UILabel *quit;

@end


@implementation SP_QuitCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_QuitCell";
    SP_QuitCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_QuitCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xf3f3f3);
    }
    return cell;
}


- (void)awakeFromNib {
    // Initialization code
    
    self.quit.layer.cornerRadius = 8;
    self.quit.layer.masksToBounds = YES;
    self.quit.backgroundColor = UIColorFromRGB(0xe67f22);
    self.quit.textColor = UIColorFromRGB(0xffffff);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
