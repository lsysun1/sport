//
//  SP_TeamCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_TeamCell.h"
#import "SP_TeamModel.h"
#import "SP_TeamMateModel.h"
#import "UIImageView+WebCache.h"
#import "MJExtension.h"

@interface SP_TeamCell()

@property (weak, nonatomic) IBOutlet UIImageView *aLeaderImg;
@property (weak, nonatomic) IBOutlet UILabel *aLeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *aLeaderName;
@property (weak, nonatomic) IBOutlet UILabel *aLeaderPhone;

@property (weak, nonatomic) IBOutlet UIImageView *bLeaderImg;
@property (weak, nonatomic) IBOutlet UILabel *bLeaderTitle;
@property (weak, nonatomic) IBOutlet UILabel *bLeaderName;
@property (weak, nonatomic) IBOutlet UILabel *bLeaderPhone;


@property (weak, nonatomic) IBOutlet UIImageView *teamImg;

@property (weak, nonatomic) IBOutlet UILabel *teamName;

@property (weak, nonatomic) IBOutlet UILabel *teamDes;
@property (weak, nonatomic) IBOutlet UIView *midLine;

@property (weak, nonatomic) IBOutlet UIView *bottomLine;


@end


@implementation SP_TeamCell


+(instancetype) cellWithTableView:(UITableView *)tableView

{
    static NSString *ID = @"SP_TeamCell";
    SP_TeamCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_TeamCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}



- (void)awakeFromNib {
    // Initialization code
    
    self.aLeaderImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.aLeaderImg.layer.cornerRadius = 8;
    self.aLeaderImg.layer.borderWidth = 1;
    self.aLeaderImg.layer.masksToBounds = YES;
    
    self.bLeaderImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.bLeaderImg.layer.cornerRadius = 8;
    self.bLeaderImg.layer.borderWidth = 1;
    self.bLeaderImg.layer.masksToBounds = YES;
    
    self.aLeaderTitle.backgroundColor = UIColorFromRGB(0xe67f22);
    self.aLeaderTitle.layer.cornerRadius = 5;
    self.aLeaderTitle.layer.masksToBounds = YES;
    self.aLeaderTitle.textColor = [UIColor whiteColor];
    
    self.bLeaderTitle.backgroundColor = UIColorFromRGB(0xe67f22);
    self.bLeaderTitle.layer.cornerRadius = 5;
    self.bLeaderTitle.layer.masksToBounds = YES;
    self.bLeaderTitle.textColor = [UIColor whiteColor];
    
    [self.cancelFocus setTitleColor:UIColorFromRGB(0xe67f22) forState:UIControlStateNormal];
    
    self.aLeaderName.textColor = UIColorFromRGB(0x403b3f);
    self.bLeaderName.textColor = UIColorFromRGB(0x403b3f);

    self.aLeaderPhone.textColor = UIColorFromRGB(0x403b3f);
    self.bLeaderPhone.textColor = UIColorFromRGB(0x403b3f);
    
    
    self.midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    self.teamName.textColor = UIColorFromRGB(0x403b3f);
    self.teamDes.textColor = UIColorFromRGB(0x403b3f);
    
    
    self.teamImg.layer.cornerRadius = 5;
    self.teamImg.layer.masksToBounds = YES;
}


- (void) setTeam:(SP_TeamModel *)team
{
    if(team != nil){
        _team = team;
        
        self.teamName.text = team.name;
        self.teamDes.text = team.desc;
        
        NSURL *teamURL = [NSURL URLWithString:team.img];
        [self.teamImg sd_setImageWithURL:teamURL];
        
        
        if(team.playerData.count == 1){
            
            NSDictionary *leaderOneDict = team.playerData[0];
            
            SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
            
            
            self.aLeaderName.text = leaderOne.nickname;
            self.aLeaderPhone.text = leaderOne.tel;
            NSURL *aURL = [NSURL URLWithString:leaderOne.headimg];
            [self.aLeaderImg sd_setImageWithURL:aURL];
            

            self.bLeaderImg.hidden = YES;
            self.bLeaderTitle.hidden = YES;
            self.bLeaderName.hidden = YES;
            self.bLeaderPhone.hidden = YES;
            
        }else{
        
        NSDictionary *leaderOneDict = team.playerData[0];
        NSDictionary *leaderTwoDict = team.playerData[1];
        
        SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
        SP_TeamMateModel *leaderTwo = [SP_TeamMateModel objectWithKeyValues:leaderTwoDict];
        
        
        self.aLeaderName.text = leaderOne.nickname;
        self.aLeaderPhone.text = leaderOne.tel;
        NSURL *aURL = [NSURL URLWithString:leaderOne.headimg];
        [self.aLeaderImg sd_setImageWithURL:aURL];
        
        

        
        self.bLeaderName.text = leaderTwo.nickname;
        self.bLeaderPhone.text = leaderTwo.tel;
        NSURL *bURL = [NSURL URLWithString:leaderTwo.headimg];
        [self.bLeaderImg sd_setImageWithURL:bURL];
            
        }

    }
}





- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
