//
//  SP_ProfileThreeCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_ProfileThreeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *desLabel;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImg;

@property (weak, nonatomic) IBOutlet UILabel *msgNum;

+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
