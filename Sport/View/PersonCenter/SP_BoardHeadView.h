//
//  SP_BoardHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>


@class SP_BoardListEntity;
@interface SP_BoardHeadView : UITableViewHeaderFooterView
@property (nonatomic,strong) SP_BoardListEntity *entity;
@end
