//
//  SP_BoardListCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BoardListCell.h"
#import "SP_BoardCellFrame.h"
#import "UIImageView+WebCache.h"
#import "SP_BoardListEntity.h"

@interface SP_BoardListCell()
{
    UIImageView *_headImg;
    UILabel *_titleLabel;
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UIImageView *_msgIconView;
    UILabel *_msgNumLabel;
    UILabel *_midLine;
    UILabel *_bottomLine;
    UILabel *_contentLable;
}
@end

@implementation SP_BoardListCell

+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_BoardListCell";
    SP_BoardListCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_BoardListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _headImg = [UIImageView new];
        _headImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
        _headImg.layer.cornerRadius = 8;
        _headImg.layer.borderWidth = 1;
        _headImg.layer.masksToBounds = YES;
        [self.contentView addSubview:_headImg];
        
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = UIColorFromRGB(0xe67f22);
        _titleLabel.layer.cornerRadius = 5;
        _titleLabel.layer.masksToBounds = YES;
        _titleLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview:_titleLabel];
        
        _nameLabel = [UILabel new];
        _nameLabel.font = [UIFont systemFontOfSize:13];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        [self.contentView addSubview:_nameLabel];
        
        
        _msgIconView = [UIImageView new];
        _msgIconView.image = [UIImage imageNamed:@"comment_icon.png"];
        [self.contentView addSubview:_msgIconView];
        
        _msgNumLabel = [UILabel new];
        _msgNumLabel.textColor = UIColorFromRGB(0x696969);
        _msgNumLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:_msgNumLabel];
        
        _timeLabel = [UILabel new];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = UIColorFromRGB(0x696969);
        _timeLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_timeLabel];

        
        _midLine = [UILabel new];
        _midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [self.contentView addSubview:_midLine];
        
        _bottomLine = [UILabel new];
        _bottomLine.backgroundColor = UIColorFromRGB(0xfafafa);
        [self.contentView addSubview:_bottomLine];
        
        _contentLable = [UILabel new];
        _contentLable.font = [UIFont systemFontOfSize:14];
        _contentLable.numberOfLines = 0;
        [self.contentView addSubview:_contentLable];
        
        
    }
    return self;
}


- (void) setCellFrame:(SP_BoardCellFrame *)cellFrame
{
    _cellFrame = cellFrame;
    SP_BoardListEntity *entity = cellFrame.boardEntity;
    
    _headImg.frame = cellFrame.headImgFrame;
    NSURL *imgURL = [NSURL URLWithString:entity.headimg];
    [_headImg sd_setImageWithURL:imgURL];
    
    _titleLabel.frame = cellFrame.titleFrame;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont systemFontOfSize:13];
    
    if([entity.role isEqualToString:@"2"]){
        _titleLabel.text = @"队长";
    }else{
        _titleLabel.text = @"副";
    }
    
    _nameLabel.frame = cellFrame.nameFrame;
    _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",entity.nickname,entity.playernum];
    
    
    _msgNumLabel.frame = cellFrame.msgNumFrame;
    _msgNumLabel.text = entity.commentnum;
    _msgNumLabel.textColor = UIColorFromRGB(0xfd5204);
    
    _timeLabel.frame = cellFrame.timeFrame;
    _timeLabel.text = [self dateWithTimeIntervalSince1970:entity.addtime];

    
    _msgIconView.frame = cellFrame.msgIconFrame;
    
    _midLine.frame = cellFrame.midLineFrame;
    
    _contentLable.frame = cellFrame.contentFrame;
    _contentLable.text = entity.content;
    
    _bottomLine.frame = cellFrame.bottomFrame;


}



/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
