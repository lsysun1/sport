//
//  SP_BoardListCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_BoardCellFrame;

@interface SP_BoardListCell : UITableViewCell

@property (nonatomic,strong) SP_BoardCellFrame *cellFrame;

+ (instancetype) cellWithTableView:(UITableView *) tableView;


@end
