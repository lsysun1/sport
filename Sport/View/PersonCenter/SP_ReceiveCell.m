//
//  SP_ReceiveCell.m
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ReceiveCell.h"
#import "SP_SendBattleEntity.h"
#import "UIImageView+WebCache.h"
#import "SP_TeamMateModel.h"
#import "MJExtension.h"


@interface SP_ReceiveCell()
@property (weak, nonatomic) IBOutlet UIImageView *teamImg;
@property (weak, nonatomic) IBOutlet UILabel *teamName;
@property (weak, nonatomic) IBOutlet UILabel *teamDes;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UIView *lineOne;
@property (weak, nonatomic) IBOutlet UIView *lineTwo;
@property (weak, nonatomic) IBOutlet UIView *lineThree;
@property (weak, nonatomic) IBOutlet UIView *lineFour;
@property (weak, nonatomic) IBOutlet UIView *lineFive;
@property (weak, nonatomic) IBOutlet UIView *lineSix;


@property (weak, nonatomic) IBOutlet UIImageView *aImgView;
@property (weak, nonatomic) IBOutlet UILabel *aTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *aName;
@property (weak, nonatomic) IBOutlet UILabel *aTel;

@property (weak, nonatomic) IBOutlet UIImageView *bImgView;
@property (weak, nonatomic) IBOutlet UILabel *bTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bName;
@property (weak, nonatomic) IBOutlet UILabel *bTel;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *msg;



@end


@implementation SP_ReceiveCell

+(instancetype) cellWithTableView:(UITableView *)tableView

{
    static NSString *ID = @"SP_ReceiveCell";
    SP_ReceiveCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_ReceiveCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}



- (void)awakeFromNib {
    // Initialization code
    
    self.teamImg.layer.cornerRadius = 8;
    self.teamImg.layer.borderWidth = 1;
    self.teamImg.layer.masksToBounds = YES;
    
    
    
    self.aImgView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.aImgView.layer.cornerRadius = 8;
    self.aImgView.layer.borderWidth = 1;
    self.aImgView.layer.masksToBounds = YES;
    
    self.bImgView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.bImgView.layer.cornerRadius = 8;
    self.bImgView.layer.borderWidth = 1;
    self.bImgView.layer.masksToBounds = YES;
    
    self.aTitleLabel.backgroundColor = UIColorFromRGB(0xe67f22);
    self.aTitleLabel.layer.cornerRadius = 5;
    self.aTitleLabel.layer.masksToBounds = YES;
    self.aTitleLabel.textColor = [UIColor whiteColor];
    
    self.bTitleLabel.backgroundColor = UIColorFromRGB(0xe67f22);
    self.bTitleLabel.layer.cornerRadius = 5;
    self.bTitleLabel.layer.masksToBounds = YES;
    self.bTitleLabel.textColor = [UIColor whiteColor];
    
    self.aName.textColor = UIColorFromRGB(0x403b3f);
    self.bName.textColor = UIColorFromRGB(0x403b3f);
    
    self.aTel.textColor = UIColorFromRGB(0x403b3f);
    self.bTel.textColor = UIColorFromRGB(0x403b3f);
    
    
    self.lineOne.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.lineTwo.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.lineThree.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.lineFour.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.lineFive.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.lineSix.backgroundColor = UIColorFromRGB(0xfafafa);

    self.teamName.textColor = UIColorFromRGB(0x403b3f);
    self.teamDes.textColor = UIColorFromRGB(0x403b3f);
    
    self.address.textColor = UIColorFromRGB(0x403b3f);
    self.time.textColor = UIColorFromRGB(0x403b3f);
    self.msg.textColor = UIColorFromRGB(0x403b3f);
    
    
    [self.yesBtn setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
    self.yesBtn.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    self.yesBtn.layer.borderWidth = 1;
    self.yesBtn.layer.cornerRadius = 5;
    
    [self.noBtn setTitleColor:UIColorFromRGB(0x403b3f) forState:UIControlStateNormal];
    self.noBtn.layer.borderColor = UIColorFromRGB(0x403b3f).CGColor;
    self.noBtn.layer.borderWidth = 1;
    self.noBtn.layer.cornerRadius = 5;

    
    
}


- (void) setEntity:(SP_SendBattleEntity *)entity
{
    NSURL *teamURL = [NSURL URLWithString:entity.img];
    [self.teamImg sd_setImageWithURL:teamURL];
    
    self.teamName.text = entity.name;
    self.teamDes.text = entity.desc;
    
    if([entity.isthrough isEqualToString:@"1"]){
        self.stateLabel.text = @"同意应战";
        self.stateLabel.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    }else if ([entity.isthrough isEqualToString:@"0"]){
        self.stateLabel.text = @"等待应战";
        self.stateLabel.textColor = UIColorFromRGB(0xe67f22);
    }else if ([entity.isthrough isEqualToString:@"2"]){
        self.stateLabel.text = @"拒绝应战";
        self.stateLabel.textColor = UIColorFromRGB(0x403b3f);
    }
    
    
    
    
    if(entity.playerdata.count == 1){
        NSDictionary *aDict  = entity.playerdata[0];
        SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:aDict];
        
        self.aName.text = leaderOne.nickname;
        self.aTel.text = leaderOne.tel;
        NSURL *aURL = [NSURL URLWithString:leaderOne.headimg];
        [self.aImgView sd_setImageWithURL:aURL];
        
        self.bImgView.hidden = YES;
        self.bTitleLabel.hidden = YES;
        self.bName.hidden = YES;
        self.bTel.hidden = YES;
        
    }else if (entity.playerdata.count == 2){
        NSDictionary *leaderOneDict = entity.playerdata[0];
        NSDictionary *leaderTwoDict = entity.playerdata[1];
        
        SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
        SP_TeamMateModel *leaderTwo = [SP_TeamMateModel objectWithKeyValues:leaderTwoDict];
        
        self.aName.text = leaderOne.nickname;
        self.aTel.text = leaderOne.tel;
        NSURL *aURL = [NSURL URLWithString:leaderOne.headimg];
        [self.aImgView sd_setImageWithURL:aURL];
        
        self.bName.text = leaderTwo.nickname;
        self.bTel.text = leaderTwo.tel;
        NSURL *bURL = [NSURL URLWithString:leaderTwo.headimg];
        [self.bImgView sd_setImageWithURL:bURL];
    }
    
    
    self.address.text = [NSString stringWithFormat:@"对战地点 : %@",entity.cdname];
    self.time.text = [NSString stringWithFormat:@"对战时间 : %@",[self dateWithTimeIntervalSince1970:entity.matchdate]];
    self.msg.text = [NSString stringWithFormat:@"留言 : %@",entity.othernote];
    
    if(![entity.isthrough isEqualToString:@"0"]){
        self.yesBtn.hidden = YES;
        self.noBtn.hidden = YES;
        self.lineSix.hidden = NO;
    }else{
        self.yesBtn.hidden = NO;
        self.noBtn.hidden = NO;
        self.lineSix.hidden = YES;
    }
    
    
}

/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
