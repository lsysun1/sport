//
//  SP_JifenCell.m
//  Sport
//
//  Created by 李松玉 on 15/8/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_JifenCell.h"

@interface SP_JifenCell()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *jifen;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end


@implementation SP_JifenCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_JifenCell";
    SP_JifenCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_JifenCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;

}


//"jfid": "10",
//"uid": "11",
//"jf": "2",
//"type": "1",
//"addtime": "1438091930",
//"msg": "评论"

- (void) setJifenDict:(NSDictionary *)jifenDict
{
    NSString *name = jifenDict[@"msg"];
    NSString *jf = jifenDict[@"jf"];
    
    self.name.text = name;
    self.jifen.text = jf;


}


- (void)awakeFromNib {
    // Initialization code
    self.jifen.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    self.bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
