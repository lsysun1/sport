//
//  SP_TeamCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_TeamModel;
@interface SP_TeamCell : UITableViewCell

@property (strong,nonatomic) SP_TeamModel *team;

+(instancetype) cellWithTableView:(UITableView *)tableView;

@property (weak, nonatomic) IBOutlet UIButton *leaderOneBtn;
@property (weak, nonatomic) IBOutlet UIButton *leaderTwoBtn;

@property (weak, nonatomic) IBOutlet UIButton *cancelFocus;


@end
