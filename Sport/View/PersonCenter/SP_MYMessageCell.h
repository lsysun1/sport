//
//  SP_MYMessageCell.h
//  Sport
//
//  Created by WT_lyy on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_MYMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headimv;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labTime;
@property (weak, nonatomic) IBOutlet UILabel *labMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnReply;

+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
