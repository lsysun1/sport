//
//  SP_JoinTeamListCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/29.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_JoinTeamListEntity;
@interface SP_JoinTeamListCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *titleView;
@property (weak, nonatomic) IBOutlet UILabel *inTitle;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *notAgreeBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UILabel *topLine;

@property (nonatomic,strong) SP_JoinTeamListEntity *entity;


+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
