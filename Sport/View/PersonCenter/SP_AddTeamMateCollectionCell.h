//
//  SP_AddTeamMateCollectionCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_FriendModel,SP_TeamMateModel;
@interface SP_AddTeamMateCollectionCell : UICollectionViewCell
@property (nonatomic,strong) SP_FriendModel *model;

@property (nonatomic,strong) SP_TeamMateModel *teamMate;


@property (weak, nonatomic) IBOutlet UITextField *numTextField;

@property (weak, nonatomic) IBOutlet UIButton *delMateBtn;


@end
