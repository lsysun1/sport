//
//  addteamfoucsCell.h
//  Sport
//
//  Created by 李松玉 on 15/8/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_TeamModel;
@interface addteamfoucsCell : UITableViewCell
@property (nonatomic,strong)SP_TeamModel *team;
+(instancetype) cellWithTableView:(UITableView *)tableView;
@property (weak, nonatomic) IBOutlet UIButton *foucsbtn;

@end
