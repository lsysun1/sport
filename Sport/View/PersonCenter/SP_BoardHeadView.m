//
//  SP_BoardHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BoardHeadView.h"
#import "SP_BoardListEntity.h"
#import "UIImageView+WebCache.h"

@interface SP_BoardHeadView()
{
    UIImageView *_headImg;
    UILabel *_titleLabel;
    UILabel *_nameLabel;
    UILabel *_midLine;
    UILabel *_bottomLine;
    UILabel *_contentLable;
}
@end

@implementation SP_BoardHeadView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        _headImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        _headImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
        _headImg.layer.cornerRadius = 8;
        _headImg.layer.borderWidth = 1;
        _headImg.layer.masksToBounds = YES;
        [self addSubview:_headImg];

        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15 + 37, 17, 30, 15)];
        _titleLabel.backgroundColor = UIColorFromRGB(0xe67f22);
        _titleLabel.layer.cornerRadius = 5;
        _titleLabel.layer.masksToBounds = YES;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_titleLabel];

        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headImg.frame) + 10,34, 100, 13)];
        _nameLabel.font = [UIFont systemFontOfSize:13];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        [self addSubview:_nameLabel];

        _midLine = [[UILabel alloc]initWithFrame: CGRectMake(10, CGRectGetMaxY(_headImg.frame) + 15, ScreenWidth - 20, 1)];
        _midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [self addSubview:_midLine];
        
        _contentLable = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_midLine.frame) + 5, ScreenWidth - 30, 30)];
        _contentLable.font = [UIFont systemFontOfSize:14];
        [self addSubview:_contentLable];
        
        _bottomLine = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_contentLable.frame) + 5, ScreenWidth, 1)];
        _bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [self addSubview:_bottomLine];
        
        
        NSLog(@"maxYYYYY-- %f",CGRectGetMaxY(_bottomLine.frame));
    
    }
    
    
    return self;
}


- (void) setEntity:(SP_BoardListEntity *)entity
{
    _entity = entity;
    
    NSURL *imgURL = [NSURL URLWithString:entity.headimg];
    [_headImg sd_setImageWithURL:imgURL];
    
    if([entity.role isEqualToString:@"2"]){
        _titleLabel.text = @"队长";
    }else{
        _titleLabel.text = @"副";
    }
    
    _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",entity.nickname,entity.playernum];
    
    _contentLable.text = entity.content;

}



@end
