//
//  addteamfoucsCell.m
//  Sport
//
//  Created by 李松玉 on 15/8/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "addteamfoucsCell.h"
#import "UIImageView+WebCache.h"
#import "SP_TeamModel.h"
@interface addteamfoucsCell()

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UILabel *midline;
@property (weak, nonatomic) IBOutlet UILabel *bottomline;

@property (weak, nonatomic) IBOutlet UILabel *name;



@end


@implementation addteamfoucsCell
+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"addteamfoucsCell";
    addteamfoucsCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"addteamfoucsCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}



- (void)awakeFromNib {
    // Initialization code
    
    self.midline.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomline.backgroundColor = UIColorFromRGB(0xf3f3f3);
    self.name.textColor = UIColorFromRGB(0x696969);
    self.des.textColor = UIColorFromRGB(0x696969);

}


- (void)setTeam:(SP_TeamModel *)team
{
    NSURL *url = [NSURL URLWithString:team.img];
    [self.img sd_setImageWithURL:url];
    
    self.name.text = team.name;
    
    
    self.des.text = [NSString stringWithFormat:@"简介:%@",team.desc];
    
    
    if([team.isattention isEqualToString:@"no"]){
        self.foucsbtn.selected = NO;
    }else{
        self.foucsbtn.selected = YES;
    }
    

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
