//
//  SP_SignCellTableViewCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/5.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_SignCellTableViewCell.h"
#import "SP_TagModel.h"

@interface SP_SignCellTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;

@end



@implementation SP_SignCellTableViewCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_SignCellTableViewCell";
    SP_SignCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_SignCellTableViewCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void) setSignArr:(NSMutableArray *)signArr
{
    if(signArr.count != 0){
    
        NSString *singStr = [[NSString alloc]init];
        
        for(int i = 0;i < signArr.count;i++){
            SP_TagModel *model = signArr[i];
            
            NSString *temp = [NSString stringWithFormat:@"%@(%@) ",model.tag,model.zhan];
            
            singStr = [singStr stringByAppendingString:temp];
        }
        
        self.signLabel.text = singStr;
    }
    self.signLabel.textColor = UIColorFromRGB(0xc83d55);

}

- (void)awakeFromNib {
    // Initialization code
    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    self.signLabel.textColor = UIColorFromRGB(0xc83d55);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
