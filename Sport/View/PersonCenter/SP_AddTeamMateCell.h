//
//  SP_AddTeamMateCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/2.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_FriendModel;
@interface SP_AddTeamMateCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;
@property (nonatomic,strong) SP_FriendModel *friendModel;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIButton *headbtn;

@property (nonatomic,copy)NSString *type;

@end
