//
//  SP_JoinTeamListCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/29.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_JoinTeamListCell.h"
#import "SP_JoinTeamListEntity.h"


@implementation SP_JoinTeamListCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_JoinTeamListCell";
    SP_JoinTeamListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_JoinTeamListCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void)awakeFromNib {
    // Initialization code
    
    
    self.topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    self.agreeBtn.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    self.agreeBtn.layer.borderWidth = 1;
    self.agreeBtn.layer.cornerRadius = 5;
    self.agreeBtn.layer.masksToBounds = YES;
    [self.agreeBtn setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
    
    
    
    self.notAgreeBtn.layer.borderColor = UIColorFromRGB(0x696969).CGColor;
    self.notAgreeBtn.layer.borderWidth = 1;
    self.notAgreeBtn.layer.cornerRadius = 5;
    self.notAgreeBtn.layer.masksToBounds = YES;
    [self.notAgreeBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];

    self.inTitle.textColor = UIColorFromRGB(0x696969);
    
    
    
}

- (void) setEntity:(SP_JoinTeamListEntity *)entity
{
    _entity = entity;
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@邀请你加入%@",entity.nickname,entity.teamname]];
    
    if([entity.sex isEqualToString:@"0"]){
        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff5949) range:NSMakeRange(0, entity.nickname.length)];
    }else{
        [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x57afff) range:NSMakeRange(0, entity.nickname.length)];
    }
    
    [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x696969) range:NSMakeRange(entity.nickname.length, 5 )];

    self.titleView.attributedText = str;
    
    if([entity.isthrough isEqualToString:@"0"]){
        self.inTitle.hidden = YES;
        self.agreeBtn.hidden = NO;
        self.notAgreeBtn.hidden = NO;
    }else if([entity.isthrough isEqualToString:@"1"]){
        self.inTitle.hidden = NO;
        self.agreeBtn.hidden = YES;
        self.notAgreeBtn.hidden = YES;
    }else if ([entity.isthrough isEqualToString:@"2"]){
        self.inTitle.text = @"已拒绝";
        self.inTitle.hidden = NO;
        self.agreeBtn.hidden = YES;
        self.notAgreeBtn.hidden = YES;
    }

}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
