//
//  SP_SendBattleCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_SendBattleEntity;
@interface SP_SendBattleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *aBtn;

@property (weak, nonatomic) IBOutlet UIButton *bBtn;

@property (nonatomic,strong) SP_SendBattleEntity *entity;


+(instancetype) cellWithTableView:(UITableView *)tableView;
@end
