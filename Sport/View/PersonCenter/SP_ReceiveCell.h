//
//  SP_ReceiveCell.h
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SP_BattleBtn.h"
@class SP_SendBattleEntity,SP_BattleBtn;
@interface SP_ReceiveCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *aBtn;

@property (weak, nonatomic) IBOutlet UIButton *bBtn;

@property (weak, nonatomic) IBOutlet SP_BattleBtn *yesBtn;
@property (weak, nonatomic) IBOutlet SP_BattleBtn *noBtn;

@property (nonatomic,strong) SP_SendBattleEntity *entity;
+(instancetype) cellWithTableView:(UITableView *)tableView;
@end
