//
//  SP_ProfileTwoCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ProfileTwoCell.h"

@interface SP_ProfileTwoCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *raceLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *line;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end


@implementation SP_ProfileTwoCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_ProfileTwoCell";
    SP_ProfileTwoCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_ProfileTwoCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void)awakeFromNib {
    // Initialization code
    self.titleLabel.textColor = UIColorFromRGB(0x403b3f);
    self.raceLabel.textColor = UIColorFromRGB(0x403b3f);
    self.timeLabel.textColor = UIColorFromRGB(0x403b3f);

    self.line.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
