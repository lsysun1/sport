//
//  SP_MYMessageCell.m
//  Sport
//
//  Created by WT_lyy on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MYMessageCell.h"

@implementation SP_MYMessageCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_MYMessageCell";
    SP_MYMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_MYMessageCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
