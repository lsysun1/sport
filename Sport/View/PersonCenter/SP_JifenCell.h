//
//  SP_JifenCell.h
//  Sport
//
//  Created by 李松玉 on 15/8/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_JifenCell : UITableViewCell

@property (nonatomic,strong) NSDictionary *jifenDict;


+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
