//
//  SP_ProfileOne_Cell.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_UserInfo;
@interface SP_ProfileOne_Cell : UITableViewCell
@property (strong,nonatomic) SP_UserInfo *userInfoMsg;

@property (nonatomic,copy) NSString *type;  // 1 是自己的   2 是别人的
@property (nonatomic,copy) NSMutableArray *signArr;

+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
