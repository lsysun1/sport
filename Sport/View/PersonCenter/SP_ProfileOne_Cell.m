//
//  SP_ProfileOne_Cell.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ProfileOne_Cell.h"
#import "SP_UserInfo.h"
#import "VerticallyAlignedLabel.h"
#import "SP_TagModel.h"


@interface SP_ProfileOne_Cell()
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreNum;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;
@property (weak, nonatomic) IBOutlet UILabel *idolLabel;
@property (weak, nonatomic) IBOutlet UILabel *idolName;
@property (weak, nonatomic) IBOutlet UILabel *schoolLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolName;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;
@property (weak, nonatomic) IBOutlet UILabel *signText;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet VerticallyAlignedLabel *tagContent;

@end


@implementation SP_ProfileOne_Cell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_ProfileOne_Cell";
    SP_ProfileOne_Cell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_ProfileOne_Cell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void)awakeFromNib {
    // Initialization code
    self.scoreLabel.textColor = UIColorFromRGB(0x403b3f);
    self.scoreNum.textColor = UIColorFromRGB(0x403b3f);
    self.tagLabel.textColor = UIColorFromRGB(0x403b3f);
    self.idolLabel.textColor = UIColorFromRGB(0x403b3f);
    self.idolName.textColor = UIColorFromRGB(0x403b3f);
    self.schoolLabel.textColor = UIColorFromRGB(0x403b3f);
    self.schoolName.textColor = UIColorFromRGB(0x403b3f);
    self.signLabel.textColor = UIColorFromRGB(0x403b3f);
    self.signText.textColor = UIColorFromRGB(0x403b3f);
    self.line.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    [self.signText setContentMode:UIViewContentModeTop];
    [self.signText sizeToFit];

}


- (void) setUserInfoMsg:(SP_UserInfo *)userInfoMsg
{
    _userInfoMsg = userInfoMsg;
    
    
    self.scoreNum.text = [NSString stringWithFormat:@"%@分",userInfoMsg.jifen];
    self.idolName.text = [NSString stringWithFormat:@"%@",userInfoMsg.idol];
    self.signText.text = [NSString stringWithFormat:@"%@",userInfoMsg.signature];
    
    if([userInfoMsg.school isEqualToString:@""]){
        self.schoolLabel.text = @"公司:";
        self.schoolName.text = userInfoMsg.company;
    }else{
        self.schoolLabel.text = @"学校:";
        self.schoolName.text = userInfoMsg.school;

    }
}


- (void) setType:(NSString *)type
{
    if([type isEqualToString:@"2"]){
        self.tagLabel.hidden = YES;
        self.tagContent.hidden = YES;
    }

}




- (void) setSignArr:(NSMutableArray *)signArr
{
    if(signArr.count != 0){
        
        NSString *singStr = [[NSString alloc]init];
        
        for(int i = 0;i < signArr.count;i++){
            SP_TagModel *model = signArr[i];
            
            NSString *temp = [NSString stringWithFormat:@"%@,",model.tag];
            
            singStr = [singStr stringByAppendingString:temp];
        }
        
        self.tagContent.text = singStr;
    }
    self.tagContent.textColor = UIColorFromRGB(0xc83d55);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
