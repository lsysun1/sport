//
//  SP_AddTeamMateCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/2.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddTeamMateCell.h"
#import "SP_FriendModel.h"
#import "UIImageView+WebCache.h"

@interface SP_AddTeamMateCell()

@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end


@implementation SP_AddTeamMateCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_AddTeamMateCell";
    SP_AddTeamMateCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_AddTeamMateCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void) setFriendModel:(SP_FriendModel *)friendModel
{
    self.nameLabel.text = friendModel.nickname;
    
    NSURL *url = [NSURL URLWithString:friendModel.headimg];
    [self.headImg sd_setImageWithURL:url];
    
    
    
    

    
}


- (void)awakeFromNib {
    // Initialization code
    self.headImg.layer.borderWidth = 1;
    self.headImg.layer.cornerRadius = 8;
    self.headImg.layer.masksToBounds = YES;
    self.headImg.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];

    self.cancel.hidden = YES;
    
}

- (void)setType:(NSString *)type{
    if([type isEqualToString:@"1"]){
        [self.cancel setTitleColor:UIColorFromRGB(0xe67f22) forState:UIControlStateNormal];
        self.cancel.hidden = NO;
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
