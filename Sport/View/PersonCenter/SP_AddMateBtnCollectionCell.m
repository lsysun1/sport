//
//  SP_AddMateBtnCollectionCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddMateBtnCollectionCell.h"

@interface SP_AddMateBtnCollectionCell()



@end


@implementation SP_AddMateBtnCollectionCell

- (void)awakeFromNib {
    // Initialization code
    self.addBtn.layer.cornerRadius = 8;
    self.addBtn.layer.masksToBounds = YES;
    self.addBtn.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
}

@end
