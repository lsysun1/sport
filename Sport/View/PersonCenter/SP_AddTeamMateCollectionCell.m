//
//  SP_AddTeamMateCollectionCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddTeamMateCollectionCell.h"
#import "SP_FriendModel.h"
#import "UIImageView+WebCache.h"
#import "SP_TeamMateModel.h"

@interface SP_AddTeamMateCollectionCell()

@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end


@implementation SP_AddTeamMateCollectionCell

- (void)awakeFromNib {
    // Initialization code
    
    
    
}

- (void) setModel:(SP_FriendModel *)model
{
    _model = model;

    NSURL *url = [NSURL URLWithString:model.headimg];
    [self.headImg sd_setImageWithURL:url];
    self.headImg.layer.cornerRadius = 8;
    self.headImg.layer.masksToBounds = YES;
    

    self.userName.text = model.nickname;

    self.numTextField.layer.borderColor = UIColorFromRGB(0x696969).CGColor;
    self.numTextField.layer.borderWidth = 1;
    self.numTextField.textColor = UIColorFromRGB(0x696969);
    
    _model.playernum = self.numTextField.text;


}


- (void) setTeamMate:(SP_TeamMateModel *)teamMate
{
    _teamMate = teamMate;
    
    NSURL *url = [NSURL URLWithString:teamMate.headimg];
    [self.headImg sd_setImageWithURL:url];
    self.headImg.layer.cornerRadius = 8;
    self.headImg.layer.masksToBounds = YES;
    
    
    self.userName.text = teamMate.nickname;
    
    self.numTextField.layer.borderColor = UIColorFromRGB(0x696969).CGColor;
    self.numTextField.layer.borderWidth = 1;
    self.numTextField.textColor = UIColorFromRGB(0x696969);
    
    teamMate.playernum = self.numTextField.text;
    

}



@end
