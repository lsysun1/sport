//
//  SP_ProfileThreeCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ProfileThreeCell.h"




@implementation SP_ProfileThreeCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_ProfileThreeCell";
    SP_ProfileThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_ProfileThreeCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}

//@property (weak, nonatomic) IBOutlet UIImageView *imgView;
//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UILabel *desLabel;
//@property (weak, nonatomic) IBOutlet UIView *line;
//@property (weak, nonatomic) IBOutlet UIView *bottomView;

- (void)awakeFromNib {
    // Initialization code
    
    self.titleLabel.textColor = UIColorFromRGB(0x403b3f);
    self.desLabel.textColor = UIColorFromRGB(0x403b3f);

    self.line.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    self.msgNum.backgroundColor = UIColorFromRGB(0xe94c3d);
    self.msgNum.layer.cornerRadius = 12.5;
    self.msgNum.layer.masksToBounds = YES;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
