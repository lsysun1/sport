//
//  SP_SignCellTableViewCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/5.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_SignCellTableViewCell : UITableViewCell

@property (nonatomic,copy) NSMutableArray *signArr;


+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
