//
//  SP_YhbHfBtn.h
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_YhbHfBtn : UIButton

@property (nonatomic,copy) NSString *yhbplid;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSIndexPath *index;

@property (nonatomic,copy) NSString *zsbplid;




@end
