//
//  SP_CompanyCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CompanyCell.h"
#import "SP_YhbList.h"
#import "UIImageView+WebCache.h"

@interface SP_CompanyCell()
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@property (strong, nonatomic) IBOutlet UIImageView *openImg;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *activeTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@property (strong, nonatomic) IBOutlet UILabel *inNumLabel;
@property (strong, nonatomic) IBOutlet UIView *lineView;
@end


@implementation SP_CompanyCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_CompanyCell";
    SP_CompanyCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_CompanyCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = UIColorFromRGB(0xfafafa);
    }
    return cell;
}




- (void)awakeFromNib {
    // Initialization code
    self.bgView.backgroundColor = UIColorFromRGB(0xfafafa);
    self.bgView.layer.borderWidth = 1;
    self.bgView.layer.borderColor = [UIColorFromRGB(0x209b29)CGColor];
    
    self.headImg.layer.borderWidth = 2;
    self.headImg.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    self.headImg.layer.cornerRadius = 31.5;
    self.headImg.layer.masksToBounds = YES;
    
    self.inBtn.layer.borderColor = [UIColorFromRGB(0xff4e00) CGColor];
    self.inBtn.layer.borderWidth = 1;
    self.inBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.inBtn setTitle:@"已响应" forState:UIControlStateSelected];
    [self.inBtn setTitle:@"未响应" forState:UIControlStateNormal];
    [self.inBtn setTitleColor:UIColorFromRGB(0xff4e00) forState:UIControlStateNormal];
    [self.inBtn setTitleColor:UIColorFromRGB(0xff4e00) forState:UIControlStateSelected];
    
    self.nameLabel.textColor = UIColorFromRGB(0x696969);
    self.ageLabel.textColor = UIColorFromRGB(0x696969);
    self.contentLabel.textColor = UIColorFromRGB(0x696969);
    self.activeTimeLabel.textColor = UIColorFromRGB(0x696969);
    self.commentLabel.textColor = UIColorFromRGB(0x696969);
    self.inNumLabel.textColor = UIColorFromRGB(0x696969);
    self.timeLabel.textColor = UIColorFromRGB(0x696969);
    
    self.lineView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
}

- (void) setYhbList:(SP_YhbList *)yhbList
{
    _yhbList = yhbList;
    
    NSURL *url =[NSURL URLWithString:yhbList.headimg];
    [self.headImg sd_setImageWithURL:url];
    
    if ([yhbList.sex isEqualToString:@"0"]) {
        self.headImg.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        self.headImg.layer.borderColor = [UIColor blueColor].CGColor;
    }
    
    
    self.nameLabel.text = yhbList.nickname;
    
    
    
    
    if([yhbList.type isEqualToString:@"1"]){
        self.contentLabel.text = [NSString stringWithFormat:@"我想在%@踢足球",yhbList.location];
    }else if([yhbList.type isEqualToString:@"2"]){
        self.contentLabel.text = [NSString stringWithFormat:@"我想在%@打篮球",yhbList.location];
    }else if([yhbList.type isEqualToString:@"3"]){
        self.contentLabel.text = [NSString stringWithFormat:@"我想在%@跑步",yhbList.location];
    }else{
        self.contentLabel.text = [NSString stringWithFormat:@"我想在%@运动",yhbList.location];
    }

    
    
    self.timeLabel.text = [NSString stringWithFormat:@"(%@发布)",[self dateWithTimeIntervalSince1970:yhbList.addtime]];
    self.commentLabel.text = [NSString stringWithFormat:@"%@条评论",yhbList.commentcount];
    self.inNumLabel.text = [NSString stringWithFormat:@"%@人响应",yhbList.responscount];

    NSDate *dateTime = [self convertDateFromString:yhbList.date];
    NSString *week  = [self weekdayStringFromDate:dateTime];
    
    self.activeTimeLabel.text = [NSString stringWithFormat:@"活动时间:%@ %@",yhbList.date,week];
    

    
    
    if([yhbList.uid isEqualToString:GETUSERID]){
        self.inBtn.hidden = YES;
    }
    
    
    NSDate *date = [self convertDateFromString2:yhbList.date];

    NSDate *noew = [NSDate date];
    
    NSDate *earlierDate = [date earlierDate:noew];
    
    
    
    
    if(![earlierDate isEqualToDate:date]){
        self.openImg.image = [UIImage imageNamed:@"已开启"];
        self.inBtn.hidden = NO;
    }else{
        
        NSString *astr = [self changeDateToDateString:date];
        NSString *bstr = [self changeDateToDateString:noew];
        
        if([astr isEqualToString:bstr]){
            self.openImg.image = [UIImage imageNamed:@"已开启"];
            self.inBtn.hidden = NO;
            return;
        
        }
        
        
        self.openImg.image = [UIImage imageNamed:@"已结束"];
        
        if([yhbList.isrespons isEqualToString:@"no"]){
            self.inBtn.selected = NO;
        }else{
            self.inBtn.selected = YES;
        }
    }
            self.inBtn.hidden = YES;

}


/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}

- (NSString *)dateWithTimeIntervalSince19702:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (NSString *) changeDateToDateString :(NSDate *) date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSLocale *locale = [NSLocale currentLocale];
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"YYYY-MM-dd" options:0 locale:locale];
    [dateFormatter setDateFormat:dateFormat];
    [dateFormatter setLocale:locale];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

/**
 *  NSString转NSDate
 */
- (NSDate *) convertDateFromString:(NSString*)uiDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init] ;
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    NSDate *date = [formatter dateFromString:uiDate];
    return date;
}



static NSString *GLOBAL_TIMEFORMAT = @"yyyy-MM-dd";

- (NSDate *) convertDateFromString2:(NSString*)uiDate
{
    NSTimeZone* localzone = [NSTimeZone localTimeZone];
    NSTimeZone* GTMzone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:GLOBAL_TIMEFORMAT];
    [dateFormatter setTimeZone:GTMzone];
    NSDate *bdate = [[NSDate alloc]init];
    bdate = [dateFormatter dateFromString:uiDate];
    
    NSDate *day = [NSDate dateWithTimeInterval:3600 sinceDate:bdate];
    
    [dateFormatter setTimeZone:localzone];
    
    return bdate;
}


/**
 *  判断星期几
 */
- (NSString*)weekdayStringFromDate:(NSDate*)inputDate {
    
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"星期日", @"星期一", @"星期二", @"星期三", @"星期四", @"星期五", @"星期六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSCalendarUnitWeekday;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
