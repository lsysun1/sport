//
//  SP_ScrollHeadBtn.h
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_ScrollHeadBtn : UIButton


@property (nonatomic,strong) NSDictionary *userInfoDict;

@property(nonatomic,copy)  NSString *uid;
@property(nonatomic,copy)  NSString *issignal;
@property(nonatomic,copy)  NSString *nickname;
@property(nonatomic,copy)  NSString *headimg;
@property(nonatomic,copy)  NSString *sex;



@end





//"uid": "13",
//"issignal": "0",
//"nickname": "haha",
//"headimg": "http://121.40.220.245/Public/upload/header/143306389068358.jpg",
//"sex": "1"