//
//  SP_CompanyHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CompanyHeadView.h"
#import "SP_YhbList.h"
#import "UIImageView+WebCache.h"
#import "SP_YhbInfoEntity.h"
#import "SP_ScrollHeadBtn.h"

@interface SP_CompanyHeadView()
{
    UIView *_topView;
    UIImageView *_headImgView;
    
    
    UILabel *_nameLabel;
    UILabel *_ageLabel;
    UILabel *_themeLabel;
    UILabel *_timeLabel;
    UIButton *_signBtn;
    
    UILabel *_inNum;
    UILabel *_inNumBottomLine;
    
    UILabel *_signNum;
    UILabel *_signNumLine;
    
    UIScrollView *_responsScrollView;
    UIScrollView *_signScrollView;

    
    UIView *_bottomLine;
    UIView *_bottomLineTwo;
    
    UILabel *_des;
    
}
@end


@implementation SP_CompanyHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 142 + 20)];
        _topView.backgroundColor = UIColorFromRGB(0xf4f4f4);
        
        _headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(18, 18, 63, 63)];
        _headImgView.layer.borderWidth = 2;
        _headImgView.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
        _headImgView.layer.cornerRadius = 31.5;
        _headImgView.layer.masksToBounds = YES;
        
        
        self.headBtn = [[UIButton alloc]init];
        self.headBtn.frame = _headImgView.frame;
        
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headImgView.frame) + 10, 35, 100, 14)];
        _nameLabel.font = [UIFont systemFontOfSize:13];
        _nameLabel.text = @"这里是名字";
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        
        _ageLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headImgView.frame) + 10, CGRectGetMaxY(_nameLabel.frame) + 5, 100, 14)];
        _ageLabel.font = [UIFont systemFontOfSize:13];
        _ageLabel.text = @"99岁";
        _ageLabel.textColor = UIColorFromRGB(0x696969);
        
        _themeLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(_headImgView.frame) + 10, 200, 14)];
        _themeLabel.font = [UIFont systemFontOfSize:13];
        _themeLabel.text = @"主题:武侯区约踢足球";
        _themeLabel.textColor = UIColorFromRGB(0x696969);
        
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(_themeLabel.frame) + 10, 200, 14)];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.text = @"活动时间:大约在冬季";
        _timeLabel.textColor = UIColorFromRGB(0x696969);
        
        _des = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(_timeLabel.frame) + 10, 200, 14)];
        _des.font = [UIFont systemFontOfSize:13];
        _des.text = @"活动内容:hahaha";
        _des.textColor = UIColorFromRGB(0x696969);
        
        
        
        _signBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 100 , 20, 70, 30)];
        _signBtn.hidden = YES;
        _signBtn.layer.borderColor = [UIColorFromRGB(0xff4e00) CGColor];
        _signBtn.layer.borderWidth = 1;
        _signBtn.backgroundColor = [UIColor whiteColor];
        _signBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        [_signBtn setTitle:@"已签到" forState:UIControlStateSelected];
        [_signBtn setTitle:@"未签到" forState:UIControlStateNormal];
        [_signBtn setTitleColor:UIColorFromRGB(0xff4e00) forState:UIControlStateNormal];

        
        self.inBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 100 , CGRectGetMaxY(_signBtn.frame) + 15, 70, 30)];
        self.inBtn.layer.borderColor = [UIColorFromRGB(0xff4e00) CGColor];
        self.inBtn.layer.borderWidth = 1;
        self.inBtn.backgroundColor = [UIColor whiteColor];
        self.inBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        self.inBtn.layer.cornerRadius = 3;
        [self.inBtn setTitle:@"已响应" forState:UIControlStateSelected];
        [self.inBtn setTitle:@"未响应" forState:UIControlStateNormal];
        [self.inBtn setTitleColor:UIColorFromRGB(0xff4e00) forState:UIControlStateSelected];
        [self.inBtn setTitleColor:UIColorFromRGB(0xff4e00) forState:UIControlStateNormal];
        
        self.markBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 80 , CGRectGetMaxY(_inBtn.frame) + 8, 30, 30)];
        [self.markBtn setBackgroundImage:[UIImage imageNamed:@"收藏图标1"] forState:UIControlStateNormal];
        [self.markBtn setBackgroundImage:[UIImage imageNamed:@"收藏图标2"] forState:UIControlStateSelected];

        _inNum = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_des.frame) + 20, ScreenWidth, 25)];
        _inNum.font = [UIFont systemFontOfSize:13];
        _inNum.text = @"5人响应";
        _inNum.textColor = UIColorFromRGB(0x696969);
        
        
        _inNumBottomLine = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_inNum.frame) + 3, ScreenWidth - 18, 1)];
        _inNumBottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        _responsScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_inNumBottomLine.frame) + 5, ScreenWidth - 18, 60)];
        _responsScrollView.showsHorizontalScrollIndicator = NO;
        

        
        _bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_responsScrollView.frame), ScreenWidth, 6)];
        _bottomLine.backgroundColor = UIColorFromRGB(0xf4f4f4);
        
        
        
        _signNum = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_bottomLine.frame) + 5, ScreenWidth, 25)];
        _signNum.font = [UIFont systemFontOfSize:13];
        _signNum.text = @"5人响应";
        _signNum.textColor = UIColorFromRGB(0x696969);
        
        _signNumLine = [[UILabel alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_signNum.frame) + 3, ScreenWidth - 18, 1)];
        _signNumLine.backgroundColor = UIColorFromRGB(0xdcdcdc);


        _signScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(18, CGRectGetMaxY(_signNumLine.frame) + 5, ScreenWidth - 18, 60)];
        _signScrollView.showsHorizontalScrollIndicator = NO;

        _bottomLineTwo = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_signScrollView.frame), ScreenWidth, 6)];
        _bottomLineTwo.backgroundColor = UIColorFromRGB(0xf4f4f4);

        
        
        
        
        
        [self addSubview:_topView];
        [self addSubview:_headImgView];
        [self addSubview:_nameLabel];
        [self addSubview:_ageLabel];
        [self addSubview:_themeLabel];
        [self addSubview:_timeLabel];
        [self addSubview:_signBtn];
        [self addSubview:_inBtn];
        [self addSubview:_des];
        [self addSubview:self.markBtn];
        [self addSubview:_inNum];
        [self addSubview:_inNumBottomLine];
        [self addSubview:_responsScrollView];
        [self addSubview:_bottomLine];
        [self addSubview:_signScrollView];
        [self addSubview:_bottomLineTwo];
        [self addSubview:_signNum];
        [self addSubview:_signNumLine];
        [self addSubview:self.headBtn];



    }

    return self;
}

- (void)responseHead:(NSArray *)array{
    

    CGFloat btnX = 0;
    for(int i = 0;i<array.count;i++){
        
        SP_ScrollHeadBtn *btn = [[SP_ScrollHeadBtn alloc]initWithFrame:CGRectMake(btnX, 5, 50, 50)];
        
        NSDictionary *dict = array[i];
        btn.userInfoDict = dict;
        
        
        [_responsScrollView addSubview:btn];
        btnX += 60;
    }
    
    _responsScrollView.contentSize = CGSizeMake(50 * array.count + 10 * (array.count-1), 60);
}


- (void)sighnHead:(NSArray *)array{
    CGFloat btnX = 0;
    for(int i = 0;i<array.count;i++){
        
        SP_ScrollHeadBtn *btn = [[SP_ScrollHeadBtn alloc]initWithFrame:CGRectMake(btnX, 5, 50, 50)];
        
        NSDictionary *dict = array[i];
        btn.userInfoDict = dict;
        
        
        [_signScrollView addSubview:btn];
        btnX += 60;
    }
    
    _signScrollView.contentSize = CGSizeMake(50 * array.count + 10 * (array.count-1), 60);
}

- (void) setYhbList:(SP_YhbList *)yhbList
{
    _yhbList = yhbList;
    
    if([GETUSERID isEqualToString:yhbList.uid]){
        self.inBtn.hidden = YES;
    }
    
    if([yhbList.isrespons isEqualToString:@"no"]){
        self.inBtn.selected = NO;
    }else{
        self.inBtn.selected = YES;
    }
    
    
    _nameLabel.text = yhbList.nickname;
    
    NSURL *imgURL = [NSURL URLWithString:yhbList.headimg];
    [_headImgView sd_setImageWithURL:imgURL];
    
    if ([yhbList.sex isEqualToString:@"0"]) {
        _headImgView.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        _headImgView.layer.borderColor = [UIColorFromRGB(0x599fff)CGColor];
    }
    
    
    _themeLabel.text = [NSString stringWithFormat:@"主题:%@",yhbList.desc];
    
    if([yhbList.type isEqualToString:@"1"]){
        _themeLabel.text = [NSString stringWithFormat:@"%@约足球",GETGROUNDZONE];
    }else if ([yhbList.type isEqualToString:@"2"]){
        _themeLabel.text = [NSString stringWithFormat:@"%@约篮球",GETGROUNDZONE];
    }else if ([yhbList.type isEqualToString:@"3"]){
        _themeLabel.text = [NSString stringWithFormat:@"%@约跑步",GETGROUNDZONE];
    }else{
        _themeLabel.text = [NSString stringWithFormat:@"%@约运动",GETGROUNDZONE];
    }
    
    
    
    _des.text = [NSString stringWithFormat:@"主题:%@",yhbList.desc];

    NSDate *dateTime = [self convertDateFromString:yhbList.date];
    NSString *week  = [GT_Tool weekdayStringFromDate:dateTime];
    
    _timeLabel.text = [NSString stringWithFormat:@"活动时间:%@",[NSString stringWithFormat:@"%@ %@",yhbList.date,week]];

    _inNum.text = [NSString stringWithFormat:@"%@人响应",yhbList.responscount];
    
    
    

}

- (void)setInfoEntity:(SP_YhbInfoEntity *)infoEntity
{
    _infoEntity = infoEntity;
    
    if([infoEntity.collect isEqualToString:@"yes"]){
        self.markBtn.selected = YES;
    }else{
        self.markBtn.selected = NO;
    }
    
    [self responseHead:infoEntity.responsdata];
    [self sighnHead:infoEntity.signaldata];
    
    _signNum.text = [NSString stringWithFormat:@"已签到(%ld人)",infoEntity.signaldata.count];

}


/**
 *  NSString转NSDate
 */
- (NSDate*) convertDateFromString:(NSString*)uiDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date=[formatter dateFromString:uiDate];
    return date;
}



@end
