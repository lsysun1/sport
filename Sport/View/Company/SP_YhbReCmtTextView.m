//
//  SP_YhbReCmtTextView.m
//  Sport
//
//  Created by 李松玉 on 15/7/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_YhbReCmtTextView.h"
#import "SP_CommentTextSpecial.h"




@implementation SP_YhbReCmtTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.editable = NO;
        self.textContainerInset = UIEdgeInsetsMake( + 5, + 5, 0, -5);
        // 禁止滚动, 让文字完全显示出来
        self.scrollEnabled = NO;
    }
    return self;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 触摸对象
    UITouch *touch = [touches anyObject];
    
    // 触摸点
    CGPoint point = [touch locationInView:self];
    
    BOOL contains = NO;
    
    NSMutableArray *specials = self.rangArr;
    
    for (SP_CommentTextSpecial *special in specials) {
        
        self.selectedRange = special.range;
        // self.selectedRange --影响--> self.selectedTextRange
        // 获得选中范围的矩形框
        NSArray *rects = [self selectionRectsForRange:self.selectedTextRange];
        // 清空选中范围
        self.selectedRange = NSMakeRange(0, 0);
        
        for (UITextSelectionRect *selectionRect in rects) {
            CGRect rect = selectionRect.rect;
            if (rect.size.width == 0 || rect.size.height == 0) continue;
            
            if (CGRectContainsPoint(rect, point)) { // 点中了某个特殊字符串
                contains = YES;
                break;
            }
        }
        
        if (contains) {
            for (UITextSelectionRect *selectionRect in rects) {
                CGRect rect = selectionRect.rect;
                if (rect.size.width == 0 || rect.size.height == 0) continue;
                
                UIView *cover = [[UIView alloc] init];
                cover.backgroundColor = [UIColor grayColor];
                cover.frame = rect;
                cover.tag = 999;
                cover.layer.cornerRadius = 5;
                [self insertSubview:cover atIndex:0];
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    for (UIView *child in self.subviews) {
                        if (child.tag == 999) [child removeFromSuperview];
                    }
                });
                
                if(self.sdelegate && [self.sdelegate respondsToSelector:@selector(getSpecialBack:)]){
                    [self.sdelegate getSpecialBack:special];
                }
                
                
            }
            
            break;
        }
    }
    
}






- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self touchesCancelled:touches withEvent:event];
        });
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    // 去掉特殊字符串后面的高亮背景
    for (UIView *child in self.subviews) {
        if (child.tag == 999) [child removeFromSuperview];
    }
}


@end
