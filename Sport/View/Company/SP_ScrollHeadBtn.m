//
//  SP_ScrollHeadBtn.m
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ScrollHeadBtn.h"
#import "UIButton+WebCache.h"


@implementation SP_ScrollHeadBtn

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.layer.cornerRadius = 25;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 1;
    }
    return self;
}


- (void) setUserInfoDict:(NSDictionary *)userInfoDict
{
    _userInfoDict = userInfoDict;
    
    //"uid": "13",
    //"issignal": "0",
    //"nickname": "haha",
    //"headimg": "http://121.40.220.245/Public/upload/header/143306389068358.jpg",
    //"sex": "1"
    
    self.uid = userInfoDict[@"uid"];
    self.issignal = userInfoDict[@"issignal"];
    self.nickname = userInfoDict[@"nickname"];
    self.headimg = userInfoDict[@"headimg"];
    self.sex = userInfoDict[@"sex"];
    
    
    if ([self.sex isEqualToString:@"0"]) {
        self.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        self.layer.borderColor = [UIColorFromRGB(0x599ffff)CGColor];
    }
    
    NSURL *url = [NSURL URLWithString:self.headimg];
    
    [self sd_setBackgroundImageWithURL:url forState:UIControlStateNormal];
    
    
    
}



@end
