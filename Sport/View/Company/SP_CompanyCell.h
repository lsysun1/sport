//
//  SP_CompanyCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_YhbList;
@interface SP_CompanyCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;
@property (nonatomic,strong) SP_YhbList *yhbList;
@property(nonatomic,copy)NSString *zoneName;
@property (strong, nonatomic) IBOutlet UIButton *inBtn;

@end
