//
//  SP_CompanyHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_YhbList,SP_YhbInfoEntity;
@interface SP_CompanyHeadView : UITableViewHeaderFooterView
@property (nonatomic,strong) SP_YhbList *yhbList;
@property (nonatomic,strong) SP_YhbInfoEntity *infoEntity;
@property (nonatomic,strong) UIButton *inBtn;
@property (nonatomic,strong) UIButton *markBtn;
@property (nonatomic,strong) UIButton *headBtn;

- (void)responseHead:(NSArray *)array;//响应人数
- (void)sighnHead:(NSArray *)array;//签到人数

@end
