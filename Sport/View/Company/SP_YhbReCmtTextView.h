//
//  SP_YhbReCmtTextView.h
//  Sport
//
//  Created by 李松玉 on 15/7/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_CommentTextSpecial;

@protocol SP_YhbReCmtTextViewDelegate <NSObject>

- (void) getSpecialBack:(SP_CommentTextSpecial *)special;

@end


@interface SP_YhbReCmtTextView : UITextView


@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *hfName;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *yhbplid;
@property (nonatomic,copy) NSString *zsbplid;
@property (nonatomic,strong) NSIndexPath *indexNum;



@property (nonatomic,assign) NSRange nickNameRange;
@property (nonatomic,assign) NSRange huifuNameRange;

@property (nonatomic,strong) NSMutableArray *rangArr;

@property (nonatomic,assign) id<SP_YhbReCmtTextViewDelegate> sdelegate;

@end
