//
//  SP_YhbCmtCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_YhbCmtFrame,SP_YhbHfBtn;

@interface SP_YhbCmtCell : UITableViewCell


@property (nonatomic,strong) UIButton *headButton;
@property (nonatomic,strong) SP_YhbHfBtn *hfButton;


@property (nonatomic,strong) SP_YhbCmtFrame *cmtFrame;



+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
