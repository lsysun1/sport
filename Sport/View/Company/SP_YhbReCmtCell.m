//
//  SP_YhbReCmtCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_YhbReCmtCell.h"
#import "SP_YhbReCmtFrame.h"
#import "SP_YhbReCmtEntity.h"
#import "SP_YhbReCmtTextView.h"
#import "SP_CommentTextSpecial.h"


@implementation SP_YhbReCmtCell

+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_YhbReCmtCell";
    SP_YhbReCmtCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_YhbReCmtCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        
        
        
        self.commentTextView = [[SP_YhbReCmtTextView alloc]init];
        self.commentTextView.backgroundColor = UIColorFromRGB(0xe5e5e5);
        
        [self.contentView addSubview:self.commentTextView];
    }
    
    return self;
}

- (void) setFrameModel:(SP_YhbReCmtFrame *)frameModel
{
    
    
    _frameModel = frameModel;
    
    
    
    self.commentTextView.frame = frameModel.commentLabelFrame;
//    self.commentTextView.indexNum = self.indexNum;
    
    SP_YhbReCmtEntity *commentEntiry = frameModel.reCmtEntity;
    
    NSString *hfName = commentEntiry.hfnickname;
    NSString *nickName = commentEntiry.nickname;
    NSString *content = commentEntiry.content;
    NSString *userSex = commentEntiry.sex;
    NSString *hfUserSex = commentEntiry.hfsex;

    
    if(hfName.length == 0){
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@:%@",nickName,content]];
        
        if([userSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(0,nickName.length)];
        }else{
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(0,nickName.length)];
        }
        
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, str.length)];
        
        
        NSRange nickNameRange = NSMakeRange(0,nickName.length);
        self.commentTextView.nickName = commentEntiry.nickname;
        self.commentTextView.yhbplid = commentEntiry.yhbplid;
        self.commentTextView.nickNameRange = nickNameRange;
        self.commentTextView.zsbplid = commentEntiry.zsbplid;
        
        
        SP_CommentTextSpecial *s = [[SP_CommentTextSpecial alloc]init];
        s.range = nickNameRange;
        s.nickName = commentEntiry.nickname;
        s.yhbplid = commentEntiry.yhbplid;
        s.zsbplid = commentEntiry.zsbplid;
        s.usermsgid = commentEntiry.usermsgid;
        s.indexNum = self.indexNum;
        s.uid = commentEntiry.uid;
        
        NSMutableArray *rangArr = [NSMutableArray array];
        [rangArr addObject:s];
        
        self.commentTextView.rangArr = rangArr;
        
        
        self.commentTextView.attributedText = str;
        
        
    }else{
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@回复%@:%@",nickName,hfName,content]];
        
        if([userSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(0,nickName.length)];
        }else{
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(0,nickName.length)];
        }
        
        if([hfUserSex isEqualToString:@"0"]){
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xff64a5) range:NSMakeRange(nickName.length + 2,hfName.length)];
        }else{
            [str addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x51a8ef) range:NSMakeRange(nickName.length + 2,hfName.length)];
        }
        
        
        
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, str.length)];
        self.commentTextView.attributedText = str;
        
        
        NSRange nickNameRange = NSMakeRange(0,nickName.length);
        NSRange huifuNameRange = NSMakeRange(nickName.length + 2,hfName.length);
        
        self.commentTextView.nickName = commentEntiry.nickname;
        self.commentTextView.hfName = commentEntiry.hfnickname;
        self.commentTextView.yhbplid = commentEntiry.yhbplid;
        self.commentTextView.zsbplid = commentEntiry.zsbplid;
        
        self.commentTextView.nickNameRange = nickNameRange;
        self.commentTextView.huifuNameRange = huifuNameRange;
        
        
        SP_CommentTextSpecial *s1 = [[SP_CommentTextSpecial alloc]init];
        s1.yhbplid = commentEntiry.yhbplid;
        s1.zsbplid = commentEntiry.zsbplid;
        s1.usermsgid = commentEntiry.usermsgid;
        s1.nickName = commentEntiry.nickname;
        s1.range = nickNameRange;
        s1.indexNum = self.indexNum;
        s1.uid = commentEntiry.uid;
        
        
        SP_CommentTextSpecial *s2 = [[SP_CommentTextSpecial alloc]init];
        s2.yhbplid = commentEntiry.yhbplid;
        s2.zsbplid = commentEntiry.zsbplid;
        s2.usermsgid = commentEntiry.usermsgid;
        s2.hfName = commentEntiry.hfnickname;
        s2.range = huifuNameRange;
        s2.indexNum = self.indexNum;
        s2.hfuid = commentEntiry.hfuid;
        
        
        NSMutableArray *rangArr = [NSMutableArray array];
        [rangArr addObject:s1];
        [rangArr addObject:s2];
        
        self.commentTextView.rangArr = rangArr;
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
