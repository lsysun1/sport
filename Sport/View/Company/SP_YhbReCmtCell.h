//
//  SP_YhbReCmtCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_YhbReCmtTextView,SP_YhbReCmtFrame;
@interface SP_YhbReCmtCell : UITableViewCell

@property (nonatomic,strong) SP_YhbReCmtFrame    *frameModel;
@property (nonatomic,strong) SP_YhbReCmtTextView *commentTextView;

@property (nonatomic,strong) NSIndexPath *indexNum;


+ (instancetype) cellWithTableView:(UITableView *) tableView;
@end
