//
//  SP_YhbCmtCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_YhbCmtCell.h"
#import "SP_YhbCmtFrame.h"
#import "SP_YhbCommentEntity.h"
#import "UIImageView+WebCache.h"
#import "SP_YhbHfBtn.h"

@interface SP_YhbCmtCell()

{
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
}

@end


@implementation SP_YhbCmtCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_YhbCmtCell";
    SP_YhbCmtCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_YhbCmtCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _headImgView = [[UIImageView alloc]init];
        _headImgView.layer.cornerRadius = 25;
        _headImgView.layer.masksToBounds = YES;
        _headImgView.layer.borderWidth = 1;
        
        self.headButton = [[UIButton alloc]init];
        self.headButton.backgroundColor = [UIColor clearColor];
        
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = UIColorFromRGB(0x696969);
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        self.hfButton = [[SP_YhbHfBtn alloc]init];
        self.hfButton.backgroundColor = UIColorFromRGB(0xe5e5e5);
        self.hfButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.hfButton setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        [self.hfButton setTitle:@"回复" forState:UIControlStateNormal];
        
        
        
        [self.contentView addSubview: _headImgView];
        [self.contentView addSubview:self.headButton];
        [self.contentView addSubview: _nameLabel];
        [self.contentView addSubview: _timeLabel];
        [self.contentView addSubview: _contentLabel];
        [self.contentView addSubview: self.hfButton];
        
    }
    return self;
}


- (void) setCmtFrame:(SP_YhbCmtFrame *)cmtFrame
{
    _cmtFrame = cmtFrame;
    
    SP_YhbCommentEntity *cmtEntity = cmtFrame.cmtEntity;
    
    self.hfButton.zsbplid = cmtEntity.zsbplid;
    self.hfButton.yhbplid = cmtEntity.yhbplid;
    self.hfButton.hfuid = cmtEntity.uid;
    self.hfButton.name = cmtEntity.nickname;
    self.hfButton.index = cmtFrame.index;
    
    _headImgView.frame = cmtFrame.headImgFrame;
    self.headButton.frame = cmtFrame.headImgFrame;
    _nameLabel.frame = cmtFrame.nameFrame;
    _contentLabel.frame = cmtFrame.contentFrame;
    _timeLabel.frame = cmtFrame.timeFrame;
    self.hfButton.frame = cmtFrame.hfBtnFrame;
    
    
    NSURL *imgUrl = [NSURL URLWithString:cmtEntity.headimg];
    [_headImgView sd_setImageWithURL:imgUrl];
    
    if ([cmtEntity.sex isEqualToString:@"0"]) {
        _headImgView.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        _headImgView.layer.borderColor = [UIColorFromRGB(0x599ffff)CGColor];
    }
    
    
    
    _nameLabel.text = cmtEntity.nickname;
    _timeLabel.text = [self dateWithTimeIntervalSince1970:cmtEntity.addtime];
    if(cmtEntity.content.length != 0){
        _contentLabel.text = cmtEntity.content;
    }else{
        _contentLabel.text = cmtEntity.plcontent;
    }

}

/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
