//
//  SP_RaceDetailsBeforeHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/7/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceDetailsBeforeHeadView.h"
#import "SP_MatchInfo_TopData.h"
#import "SP_MatchInfo_MvpData.h"
#import "UIImageView+WebCache.h"
#import "SP_MatchLeagueList.h"


@interface SP_RaceDetailsBeforeHeadView()
{
    UIView *_topView;
    UIImageView *_topBgImgView;
    UIImageView *_aTeamImgView;
    UIImageView *_bTeamImgView;
    UILabel *_aTeamNameLabel;
    UILabel *_bTeamNameLabel;
    UILabel *_scoreLabel;
    UILabel *_timeLabel;
    UIButton *_aTeamBtn;
    UIButton *_bTeamBtn;
    UIView *_aTeamSupport;
    UILabel *_aTeamSupportNum;
    UIView *_bTeamSupport;
    UILabel *_bTeamSupportNum;
    
    UIView *_locationView;
    UILabel *_locationLabel;
    UIView *_raceNameView;
    UILabel *_raceNameLabel;

}
@end

@implementation SP_RaceDetailsBeforeHeadView

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140)];
        
        _topBgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140)];
        _topBgImgView.image = [UIImage imageNamed:@"赛事详情-背景图.jpg"];
        
        _aTeamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 60, 60)];
        _aTeamImgView.layer.cornerRadius = 10;
        _aTeamImgView.layer.masksToBounds = YES;
        
        
        _aTeamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_aTeamImgView.frame) + 5, 80, 15)];
        _aTeamNameLabel.font = [UIFont systemFontOfSize:14];
        _aTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _aTeamNameLabel.textColor = [UIColor whiteColor];
        
        
        _aTeamBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(_aTeamNameLabel.frame) + 5, 37, 37)];
        _aTeamBtn.tag = 11;
        [_aTeamBtn setBackgroundImage:[UIImage imageNamed:@"支持红队"] forState:UIControlStateNormal];
        [_aTeamBtn addTarget:self action:@selector(supportBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        _aTeamSupport = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, 100, 2)];
        _aTeamSupport.backgroundColor = UIColorFromRGB(0xe84c4d);
        
        _aTeamSupportNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamSupport.frame) + 3, 100, 14)];
        _aTeamSupportNum.font = [UIFont systemFontOfSize:14];
        _aTeamSupportNum.textColor = UIColorFromRGB(0xe84c4d);
        
        
        _bTeamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 80, 10, 60, 60)];
        _bTeamImgView.layer.cornerRadius = 10;
        _bTeamImgView.layer.masksToBounds = YES;

        
        
        _bTeamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 90, CGRectGetMaxY(_aTeamImgView.frame) + 5, 80, 15)];
        _bTeamNameLabel.font = [UIFont systemFontOfSize:14];
        _bTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _bTeamNameLabel.textColor = [UIColor whiteColor];
        
        
        _bTeamBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, CGRectGetMaxY(_aTeamNameLabel.frame) + 5, 37, 37)];
        _bTeamBtn.tag = 22;
        [_bTeamBtn setBackgroundImage:[UIImage imageNamed:@"支持蓝队"] forState:UIControlStateNormal];
        [_bTeamBtn addTarget:self action:@selector(supportBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        CGFloat bBtnW = CGRectGetMinX(_bTeamBtn.frame) - 5 - CGRectGetMaxX(_aTeamSupport.frame)  - 5;
        _bTeamSupport = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamSupport.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, bBtnW, 2)];
        _bTeamSupport.backgroundColor = UIColorFromRGB(0x599fff);
        
        _bTeamSupportNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_bTeamBtn.frame) - 70, CGRectGetMaxY(_aTeamSupport.frame) + 3, 65, 14)];
        _bTeamSupportNum.font = [UIFont systemFontOfSize:14];
        _bTeamSupportNum.textColor = UIColorFromRGB(0x599fff);
        _bTeamSupportNum.textAlignment = NSTextAlignmentRight;
        
        
        _scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, 15, 200, 40)];
        _scoreLabel.font = [UIFont systemFontOfSize:40];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.textColor = [UIColor whiteColor];
        
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_scoreLabel.frame) + 10, 200, 15)];
        _timeLabel.font = [UIFont systemFontOfSize:15];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor whiteColor];
        
        
        
        
        _raceNameView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame), ScreenWidth, 40)];
        
        UIImageView *telImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10.3, 15, 16.5)];
        telImgView.image = [UIImage imageNamed:@"奖杯图标"];
        [_raceNameView addSubview:telImgView];
        
        _raceNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 40, 40)];
        _raceNameLabel.font = [UIFont systemFontOfSize:14];
        _raceNameLabel.textColor = [UIColor blackColor];
        _raceNameLabel.text = @"比赛名称";
        [_raceNameView addSubview:_raceNameLabel];
        
        UIView *sepViewTwo = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
        sepViewTwo.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [_raceNameView addSubview:sepViewTwo];
        
        
        
        _locationView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_raceNameView.frame), ScreenWidth, 40)];
        [self addSubview:_locationView];
        
        UIImageView *locationImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10.3, 15, 16.5)];
        locationImgView.image = [UIImage imageNamed:@"位置_灰"];
        [_locationView addSubview:locationImgView];
        
        _locationLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 40, 40)];
        _locationLabel.font = [UIFont systemFontOfSize:14];
        _locationLabel.textColor = [UIColor blackColor];
        _locationLabel.text = @"成都";
        [_locationView addSubview:_locationLabel];
        
        UIView *sepViewOne = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
        sepViewOne.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [_locationView addSubview:sepViewOne];
        
        
        
        
        
        
        [self addSubview:_topView];
        [_topView addSubview:_topBgImgView];
        [_topView addSubview:_aTeamImgView];
        [_topView addSubview:_bTeamImgView];
        [_topView addSubview:_aTeamNameLabel];
        [_topView addSubview:_bTeamNameLabel];
        [_topView addSubview:_scoreLabel];
        [_topView addSubview:_timeLabel];
        [_topView addSubview:_aTeamBtn];
        [_topView addSubview:_bTeamBtn];
        [_topView addSubview:_aTeamSupport];
        [_topView addSubview:_aTeamSupportNum];
        [_topView addSubview:_bTeamSupport];
        [_topView addSubview:_bTeamSupportNum];
        [self addSubview:_raceNameView];

    }
    return self;
}



- (void) setTopData:(SP_MatchInfo_TopData *)topData
{
    if(topData){
    _topData = topData;
    _timeLabel.text = [self dateWithTimeIntervalSince1970:topData.date];
    
    
    if([topData.ateamscore isEqualToString:@"-"]){
        _scoreLabel.text = @"VS";
    }else{
        _scoreLabel.text = [NSString stringWithFormat:@"%@ - %@",topData.ateamscore,topData.bteamscore];
    }
    
    
    _aTeamNameLabel.text = topData.ateamname;
    NSURL *aURL = [NSURL URLWithString:topData.ateamimg];
    [_aTeamImgView sd_setImageWithURL:aURL];
    
    _bTeamNameLabel.text = topData.bteamname;
    NSURL *bURL = [NSURL URLWithString:topData.bteamimg];
    [_bTeamImgView sd_setImageWithURL:bURL];
    
    
        
        float aSupport = [topData.ateamsupport floatValue];
        float bSupport = [topData.bteamsupport floatValue];
        
        if(aSupport !=0.0f || bSupport != 0.0f){
            
            float aRatio = aSupport / (aSupport + bSupport);
            
            
            
            
            CGFloat aBtnW = (CGRectGetMinX(_bTeamBtn.frame) - CGRectGetMaxX(_aTeamBtn.frame))* aRatio;
            _aTeamSupport.frame = CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, aBtnW, 2);
            
            CGFloat bBtnW = CGRectGetMinX(_bTeamBtn.frame) - 5 - CGRectGetMaxX(_aTeamSupport.frame)  - 5;
            _bTeamSupport.frame = CGRectMake(CGRectGetMaxX(_aTeamSupport.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, bBtnW, 2);
            NSLog(@"aRatio --- %f",aRatio);
            
        }
        
        _aTeamSupportNum.text = topData.ateamsupport;
        _bTeamSupportNum.text = topData.bteamsupport;
    }
    
}

- (void) setListModel:(SP_MatchLeagueList *)listModel
{
    if(listModel){
        _listModel = listModel;
        _raceNameLabel.text = listModel.name;
    }
}



- (void) supportBtnDidClick:(UIButton *)btn
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(headViewSupportBtnDidClick:)]){
        [self.delegate headViewSupportBtnDidClick:btn];
    }
    
}



/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


@end
