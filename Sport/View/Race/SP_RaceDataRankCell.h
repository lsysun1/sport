//
//  SP_RaceDataRankCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_RaceDataRankCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;
@property (nonatomic,strong) NSDictionary *model;
@property (nonatomic,assign) NSInteger number;
@property (nonatomic,assign) int ballType;

@end


//{
//    "teamid": 11,
//    "name": "屌丝队",
//    "img": "http://121.40.220.245/Public/upload/teamheader/143243686979895.jpg",
//    "shen": 1,
//    "ping": 1,
//    "fu": 0,
//    "jsq": 3,
//    "jifen": 4
//},