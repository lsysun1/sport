//
//  SP_RaceListCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_MatchLeagueList;
@interface SP_RaceListCell : UITableViewCell

@property (nonatomic, strong) SP_MatchLeagueList *listModel;

@property (weak, nonatomic) IBOutlet UIButton *markBtn;


+(instancetype) cellWithTableView:(UITableView *)tableView;



@end
