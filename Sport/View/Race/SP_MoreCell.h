//
//  SP_MoreCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_MoreCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
