//
//  SP_CommentCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CommentCell.h"
#import "SP_CommentFrame.h"
#import "SP_CommentModel.h"
#import "UIImageView+WebCache.h"

@interface SP_CommentCell()
{
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
    
    UILabel *_praiseNum;
    
    UILabel *_bottomLine;
    
}
@end


@implementation SP_CommentCell


+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_CommentCell";
    SP_CommentCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_CommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _headImgView = [[UIImageView alloc]init];
        _headImgView.layer.cornerRadius = 25;
        _headImgView.layer.masksToBounds = YES;
        
        self.headButton = [[UIButton alloc]init];
        self.headButton.backgroundColor = [UIColor clearColor];
        
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = UIColorFromRGB(0x696969);


        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = UIColorFromRGB(0x696969);
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;

        
        _praiseNum = [[UILabel alloc]init];
        _praiseNum.font = [UIFont systemFontOfSize:14];
        _praiseNum.textColor = UIColorFromRGB(0x696969);
        
        self.praiseBtn = [[UIButton alloc]init];
        [self.praiseBtn setBackgroundImage:[UIImage imageNamed:@"已赞"] forState:UIControlStateSelected];
        [self.praiseBtn setBackgroundImage:[UIImage imageNamed:@"未赞"] forState:UIControlStateNormal];
        [self.praiseBtn addTarget:self action:@selector(praiseBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
        

        
        _bottomLine = [[UILabel alloc]init];
        _bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        
        
        
        
        [self.contentView addSubview: _headImgView];
        [self.contentView addSubview:self.headButton];
        [self.contentView addSubview: _nameLabel];
        [self.contentView addSubview: _timeLabel];
        [self.contentView addSubview: _contentLabel];
        [self.contentView addSubview: _praiseNum];
        [self.contentView addSubview: self.praiseBtn];
        [self.contentView addSubview: _bottomLine];

    }
    return self;
}




- (void) setCommentFrame:(SP_CommentFrame *)commentFrame
{
    _commentFrame = commentFrame;
    SP_CommentModel *msg = commentFrame.msgModel;
    
    
    _headImgView.frame = commentFrame.headImgFrame;
    NSURL *url = [NSURL URLWithString:msg.headimg];
    [_headImgView sd_setImageWithURL:url];
    
    self.headButton.frame = commentFrame.headImgFrame;
    
    
    _nameLabel.frame = commentFrame.nameFrame;
    _nameLabel.text = msg.nickname;
    
    _timeLabel.frame = commentFrame.timeFrame;
    _timeLabel.text = msg.addtime;
    
    _contentLabel.frame = commentFrame.contentFrame;
    _contentLabel.text = msg.msg;
    
    _praiseNum.frame = commentFrame.praiseNumFrame;
    _praiseNum.text = msg.praiseCount;

    self.praiseBtn.frame = commentFrame.praiseBtnFrame;
    if([msg.isPraise isEqualToString:@"no"]){
        self.praiseBtn.selected = NO;
    }else{
        self.praiseBtn.selected = YES;
    }
    
    _bottomLine.frame = commentFrame.bottomLineFrame;

}




- (void) praiseBtnDidClick
{
    SP_CommentModel *msg = self.commentFrame.msgModel;
    NSString *msgID = msg.matchmsgid;
    
    _praiseBtn.selected = YES;
    
    int praise = [msg.praiseCount intValue] + 1;
    
    if([msg.isPraise isEqualToString:@"no"]){
        _praiseNum.text = [NSString stringWithFormat:@"%d",praise];
    }

    
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(praiseBtnIsClick:)]){
        [self.delegate praiseBtnIsClick:msgID];
    }
}




- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
