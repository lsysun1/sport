//
//  SP_RaceListCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceListCell.h"
#import "SP_MatchLeagueList.h"

@interface SP_RaceListCell()
{
    IBOutlet UIView *topLine;
    IBOutlet UIImageView *iconImg;
    IBOutlet UILabel *title;
    IBOutlet UILabel *content;
    IBOutlet UIView *bottomLine;
    

}
@end


@implementation SP_RaceListCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_RaceListCell";
    SP_RaceListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_RaceListCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



- (void)awakeFromNib {
    // Initialization code
    
    topLine.frame = CGRectMake(0, 0, ScreenWidth, 7);
    topLine.backgroundColor = UIColorFromRGB(0xfafafa);
    
    bottomLine.frame = CGRectMake(0, 104, ScreenWidth, 1);
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    self.markBtn.frame = CGRectMake(ScreenWidth - 50, 37, 34, 34);
    [self.markBtn setBackgroundImage:[UIImage imageNamed:@"icon_mark_sel"] forState:UIControlStateSelected];
//    [self.markBtn addTarget:self action:@selector(markBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat imgMaxX = CGRectGetMaxX(iconImg.frame);
    CGFloat markBtnMinX = CGRectGetMinX(self.markBtn.frame);
    
    title.frame = CGRectMake(imgMaxX + 10, 26, markBtnMinX - imgMaxX - 20, 21);
    title.textColor = [UIColor blackColor];
    
    content.frame = CGRectMake(imgMaxX + 10, 48, markBtnMinX - imgMaxX - 20, 42);
    content.textColor = UIColorFromRGB(0x6a6a6a);
    
    
}


//- (void) markBtnDidClick
//{
//    if(self.markBtn.selected == YES){
//        self.markBtn.selected = NO;
//    }else{
//        self.markBtn.selected = YES;
//    }
//
//}



- (void) setListModel:(SP_MatchLeagueList *)listModel
{
    title.text = listModel.name;
    content.text = listModel.desc;
    
    NSLog(@"listModel.collect --- %@",listModel.collect);
    NSLog(@"listModel.matchleagueid --- %@",listModel.matchleagueid);

    if([listModel.collect isEqualToString:@"no"]){
        self.markBtn.selected = NO;
    }else{
        self.markBtn.selected = YES;
    }

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
