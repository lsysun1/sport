//
//  SP_RaceDetailsCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_MatchList;
@protocol SP_RaceDetailsCellDelegate <NSObject>

- (void)buttonOneActionForItemText:(NSString *)itemText;
- (void)buttonTwoActionForItemText:(NSString *)itemText;
- (void)cellDidOpen:(UITableViewCell *)cell;
- (void)cellDidClose:(UITableViewCell *)cell;

@end



@interface SP_RaceDetailsCell : UITableViewCell

@property (nonatomic,strong) SP_MatchList *matchList;
@property (nonatomic, assign) id <SP_RaceDetailsCellDelegate> delegate;

+(instancetype) cellWithTableView:(UITableView *)tableView;
- (void)openCell;


@end
