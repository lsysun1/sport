//
//  SP_Race_Rank_ZuQiu.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Race_Rank_ZuQiu.h"


@interface SP_Race_Rank_ZuQiu(){
    UILabel *_group;

}

@end


@implementation SP_Race_Rank_ZuQiu

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 40)];
        title.font = [UIFont systemFontOfSize:13];
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        title.text = @"积分榜";
        [self addSubview:title];
        
        UILabel *state = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 55, 0, 28, 40)];
        state.font = [UIFont systemFontOfSize:13];
        state.textColor = UIColorFromRGB(0x696969);
        state.text = @"积分";
        [self addSubview:state];
        
        UILabel *lose = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(state.frame) - 55, 0, 41, 40)];
        lose.font = [UIFont systemFontOfSize:13];
        lose.textColor = UIColorFromRGB(0x696969);
        lose.text = @"净胜球";
        [self addSubview:lose];
        
        UILabel *win = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(lose.frame) - 56, 0, 54, 40)];
        win.font = [UIFont systemFontOfSize:13];
        win.textColor = UIColorFromRGB(0x696969);
        win.text = @"胜/平/负";
        [self addSubview:win];
        
        UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, 20)];
        [self addSubview:bottomView];
        
        _group= [[UILabel alloc]initWithFrame:CGRectMake(20, 0, ScreenWidth, 20)];
        _group.textColor = UIColorFromRGB(0x696969);
        _group.font = [UIFont systemFontOfSize:13];
        _group.text = [NSString stringWithFormat:@"%@组",self.name];

        [bottomView addSubview:_group];
        
    }
    return self;
}

- (void)setName:(NSString *)name
{
    if(name.length != 0){
        _group.text = [NSString stringWithFormat:@"%@组",name];
    }

}

@end
