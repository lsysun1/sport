//
//  SP_RaceDetailsHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceDetailsHeadView.h"
#import "SP_MatchInfo_TopData.h"
#import "SP_MatchInfo_MvpData.h"
#import "UIImageView+WebCache.h"
#import "SP_MvpViewController.h"
#import "SP_MatchList.h"
#import "SP_MatchLeagueList.h"
#import "SP_TeamInfoViewController.h"


@interface SP_RaceDetailsHeadView()
{
    UIView *_topView;
    UIImageView *_topBgImgView;
    UIImageView *_aTeamImgView;
    UIImageView *_bTeamImgView;
    UILabel *_aTeamNameLabel;
    UILabel *_bTeamNameLabel;
    UILabel *_scoreLabel;
    UILabel *_timeLabel;
    UIButton *_aTeamBtn;
    UIButton *_bTeamBtn;
    UIView *_aTeamSupport;
    UILabel *_aTeamSupportNum;
    UIView *_bTeamSupport;
    UILabel *_bTeamSupportNum;

    
    
    UIView *_mvpView;
    UIButton *_mvpBtn;
    UIImageView *_headIconImg;
    UILabel *_mvpLabel;
    UILabel *_mvpName;
    UILabel *_mvpPosition;
    UILabel *_mvpEndLabel;
    UIImageView *_mvpArrowImg;
    
    UIView *_bottomLine;
    UIView *_bottomView;
    
    UIView *_countView;
    UILabel *_countLabel;
    UIView *_countLine;
    
    
    
    UIButton *_aTeamImgBtn;
    UIButton *_bTeamImgBtn;
    
    
    UILabel *_noMvpLabel;

    
    
    

}
@end

@implementation SP_RaceDetailsHeadView


- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140)];
        
        _topBgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140)];
        _topBgImgView.image = [UIImage imageNamed:@"赛事详情-背景图.jpg"];
        
        _aTeamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 60, 60)];
        _aTeamImgView.layer.cornerRadius = 10;
        _aTeamImgView.layer.masksToBounds = YES;
        
        _aTeamImgBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 10, 60, 60)];
        
        
        
        _aTeamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_aTeamImgView.frame) + 5, 80, 15)];
        _aTeamNameLabel.font = [UIFont systemFontOfSize:14];
        _aTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _aTeamNameLabel.textColor = [UIColor whiteColor];
        
        _aTeamBtn = [[UIButton alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(_aTeamNameLabel.frame) + 5, 37, 37)];
        _aTeamBtn.tag = 11;
        [_aTeamBtn setBackgroundImage:[UIImage imageNamed:@"支持红队"] forState:UIControlStateNormal];
        [_aTeamBtn addTarget:self action:@selector(supportBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        _aTeamSupport = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, 100, 2)];
        _aTeamSupport.backgroundColor = UIColorFromRGB(0xe84c4d);
        
        _aTeamSupportNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamSupport.frame) + 3, 100, 14)];
        _aTeamSupportNum.font = [UIFont systemFontOfSize:14];
        _aTeamSupportNum.textColor = UIColorFromRGB(0xe84c4d);
        
        
        _bTeamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 80, 10, 60, 60)];
        _bTeamImgView.layer.cornerRadius = 10;
        _bTeamImgView.layer.masksToBounds = YES;
        
        _bTeamImgBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 80, 10, 60, 60)];

        
        
        _bTeamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 90, CGRectGetMaxY(_aTeamImgView.frame) + 5, 80, 15)];
        _bTeamNameLabel.font = [UIFont systemFontOfSize:14];
        _bTeamNameLabel.textAlignment = NSTextAlignmentCenter;
        _bTeamNameLabel.textColor = [UIColor whiteColor];
        
        
        _bTeamBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, CGRectGetMaxY(_aTeamNameLabel.frame) + 5, 37, 37)];
        _bTeamBtn.tag = 22;
        [_bTeamBtn setBackgroundImage:[UIImage imageNamed:@"支持蓝队"] forState:UIControlStateNormal];
        [_bTeamBtn addTarget:self action:@selector(supportBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

        
        CGFloat bBtnW = CGRectGetMinX(_bTeamBtn.frame) - 5 - CGRectGetMaxX(_aTeamSupport.frame)  - 5;
        _bTeamSupport = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_aTeamSupport.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, bBtnW, 2)];
        _bTeamSupport.backgroundColor = UIColorFromRGB(0x599fff);
        
        _bTeamSupportNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_bTeamBtn.frame) - 70, CGRectGetMaxY(_aTeamSupport.frame) + 3, 65, 14)];
        _bTeamSupportNum.font = [UIFont systemFontOfSize:14];
        _bTeamSupportNum.textColor = UIColorFromRGB(0x599fff);
        _bTeamSupportNum.textAlignment = NSTextAlignmentRight;
        
        
        _scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, 15, 200, 40)];
        _scoreLabel.font = [UIFont systemFontOfSize:40];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.textColor = [UIColor whiteColor];
        
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_scoreLabel.frame) + 10, 200, 15)];
        _timeLabel.font = [UIFont systemFontOfSize:15];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor whiteColor];
        
        
        
        _mvpView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame), ScreenWidth, 97)];
        _mvpView.backgroundColor = [UIColor whiteColor];
        
        _mvpBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame), ScreenWidth, 97)];
        _mvpBtn.backgroundColor = [UIColor clearColor];
        
        _headIconImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 60, 60)];
        _headIconImg.backgroundColor = [UIColor purpleColor];
        _headIconImg.layer.cornerRadius = 10;
        _headIconImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
        _headIconImg.layer.borderWidth = 2;
        _headIconImg.layer.masksToBounds = YES;
        
        
        _noMvpLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 30, 100, 60)];
        _noMvpLabel.text = @"还没有人投票哦~";
        _noMvpLabel.font = [UIFont boldSystemFontOfSize:15];
        
        _mvpLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 19, 35, 14)];
        _mvpLabel.backgroundColor = UIColorFromRGB(0xe74c38);
        _mvpLabel.font = [UIFont systemFontOfSize:15];
        _mvpLabel.text = @"MVP";
        _mvpLabel.textColor = [UIColor whiteColor];
        _mvpLabel.layer.cornerRadius = 4;
        _mvpLabel.layer.masksToBounds = YES;
        
        _mvpName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headIconImg.frame) + 5, 40, 100, 15)];
        _mvpName.font = [UIFont systemFontOfSize:15];
        _mvpName.textColor = UIColorFromRGB(0x696969);

        _mvpPosition = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_headIconImg.frame) + 5, 58, 100, 13)];
        _mvpPosition.font = [UIFont systemFontOfSize:13];
        _mvpPosition.textColor = UIColorFromRGB(0x696969);
        
        
        
        _mvpArrowImg = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 40, 36, 9, 18)];
        _mvpArrowImg.image = [UIImage imageNamed:@"箭头"];
        
        _mvpEndLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 145, 37, 96, 16)];
        _mvpEndLabel.font = [UIFont systemFontOfSize:16];
        _mvpEndLabel.text = @"投票已经结束";
        _mvpEndLabel.textAlignment = NSTextAlignmentRight;
        _mvpEndLabel.textColor = UIColorFromRGB(0x696969);
        
        
        
        
        
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 90, ScreenWidth, 7)];
        _bottomView.backgroundColor = UIColorFromRGB(0xfafafa);
        
        _bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 89, ScreenWidth, 1)];
        _bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);


        _countView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_mvpView.frame), ScreenWidth, 30)];
        _countView.backgroundColor = UIColorFromRGB(0xffffff);
        
        _countLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 7.5, 100, 15)];
        _countLabel.font = [UIFont systemFontOfSize:15];
        _countLabel.text = @"比赛统计";
        _countLabel.textColor = UIColorFromRGB(0x696969);
        
        _countLine = [[UIView alloc]initWithFrame:CGRectMake(0, 29, ScreenWidth, 1)];
        _countLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        
        
        
        
        [self addSubview:_topView];
        [_topView addSubview:_topBgImgView];
        [_topView addSubview:_aTeamImgView];
        [_topView addSubview:_bTeamImgView];
        [_topView addSubview:_aTeamNameLabel];
        [_topView addSubview:_bTeamNameLabel];
        [_topView addSubview:_scoreLabel];
        [_topView addSubview:_timeLabel];
        [_topView addSubview:_aTeamBtn];
        [_topView addSubview:_bTeamBtn];
        [_topView addSubview:_aTeamSupport];
        [_topView addSubview:_aTeamSupportNum];
        [_topView addSubview:_bTeamSupport];
        [_topView addSubview:_bTeamSupportNum];

        [self addSubview:_mvpView];
        [_mvpView addSubview:_headIconImg];
        [_mvpView addSubview:_mvpLabel];
        [_mvpView addSubview:_mvpName];
        [_mvpView addSubview:_mvpPosition];
        [_mvpView addSubview:_bottomView];
        [_mvpView addSubview:_bottomLine];
        [_mvpView addSubview:_mvpArrowImg];
        [_mvpView addSubview:_mvpEndLabel];
        [_mvpView addSubview:_noMvpLabel];
        
        [self addSubview:_mvpBtn];
        
        [self addSubview:_countView];
        [_countView addSubview:_countLabel];
        [_countView addSubview:_countLine];
        
        [self addSubview:_aTeamImgBtn];
        [self addSubview:_bTeamImgBtn];


        
    
    }
    return self;
}



- (void) setTopData:(SP_MatchInfo_TopData *)topData
{
    _topData = topData;
    _timeLabel.text = [self dateWithTimeIntervalSince1970:topData.date];
    _scoreLabel.text = [NSString stringWithFormat:@"%@ - %@",topData.ateamscore,topData.bteamscore];
    
    _aTeamNameLabel.text = topData.ateamname;
    NSURL *aURL = [NSURL URLWithString:topData.ateamimg];
    [_aTeamImgView sd_setImageWithURL:aURL];
    
    _bTeamNameLabel.text = topData.bteamname;
    NSURL *bURL = [NSURL URLWithString:topData.bteamimg];
    [_bTeamImgView sd_setImageWithURL:bURL];
    
    
    _aTeamImgBtn.tag = 1;
    _bTeamImgBtn.tag = 2;
    
    [_aTeamImgBtn addTarget:self action:@selector(teamBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_bTeamImgBtn addTarget:self action:@selector(teamBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    
    
    float aSupport = [topData.ateamsupport floatValue];
    float bSupport = [topData.bteamsupport floatValue];
    
    if(aSupport !=0.0f || bSupport != 0.0f){
    
    float aRatio = aSupport / (aSupport + bSupport);
    
    
    
    
        CGFloat aBtnW = (CGRectGetMinX(_bTeamBtn.frame) - CGRectGetMaxX(_aTeamBtn.frame))* aRatio;
        _aTeamSupport.frame = CGRectMake(CGRectGetMaxX(_aTeamBtn.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, aBtnW, 2);

        CGFloat bBtnW = CGRectGetMinX(_bTeamBtn.frame) - 5 - CGRectGetMaxX(_aTeamSupport.frame)  - 5;
        _bTeamSupport.frame = CGRectMake(CGRectGetMaxX(_aTeamSupport.frame) + 5, CGRectGetMaxY(_aTeamNameLabel.frame) + 12, bBtnW, 2);
        NSLog(@"aRatio --- %f",aRatio);

    }
    
    _aTeamSupportNum.text = topData.ateamsupport;
    _bTeamSupportNum.text = topData.bteamsupport;

    
    
    
    
    

}



- (void) teamBtnDidClick:(UIButton *)btn
{
    SP_TeamInfoViewController *vc = [[SP_TeamInfoViewController alloc]init];

    if(btn.tag == 1)
    {
        vc.teamName = self.topData.ateamname;
        vc.teamID = self.topData.ateamid;
    }else{
        vc.teamName = self.topData.bteamname;
        vc.teamID = self.topData.bteamid;
    }
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}


- (void) setMvpData:(SP_MatchInfo_MvpData *)mvpData
{
    if(mvpData.uid.length != 0){
        _mvpName.text = mvpData.nickname;
        NSURL *url = [NSURL URLWithString:mvpData.headimg];
        [_headIconImg sd_setImageWithURL:url];
        
        _mvpPosition.text = mvpData.postion;
        
        _mvpName.hidden = NO;
        _headIconImg.hidden = NO;
        _mvpPosition.hidden = NO;
        _mvpLabel.hidden = NO;
        _noMvpLabel.hidden = YES;
        
    }else{
        _mvpName.hidden = YES;
        _headIconImg.hidden = YES;
        _mvpPosition.hidden = YES;
        _mvpLabel.hidden = YES;
        _noMvpLabel.hidden = NO;
        
    }


}




- (void) setMatchList:(SP_MatchList *)matchList
{
    _matchList = matchList;
    
    if([matchList.mvpsupportoff isEqualToString:@"0"]){
        _mvpArrowImg.image = [UIImage imageNamed:@"arrow_attenteam.png"];
        _mvpEndLabel.text = @"投票进行中";
        _mvpEndLabel.textColor = [UIColor redColor];
        [_mvpBtn addTarget:self action:@selector(mvpBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    }else{
        _mvpArrowImg.image = [UIImage imageNamed:@"箭头"];
        _mvpEndLabel.text = @"投票已经结束";
        _mvpEndLabel.textColor = UIColorFromRGB(0x696969);
    }
}


- (void) supportBtnDidClick:(UIButton *)btn
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(headViewSupportBtnDidClick:)]){
        [self.delegate headViewSupportBtnDidClick:btn];
    }
    
    
    
    
    
    
    
}



- (void) mvpBtnDidClick
{
    SP_MvpViewController *mvpVC = [[SP_MvpViewController alloc]init];
    mvpVC.topData = self.topData;
    mvpVC.listModel = self.listModel;
    [[AppDelegate sharedAppDelegate].nav pushViewController:mvpVC animated:YES];
}



/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


@end
