//
//  SP_Race_Rank.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Race_Rank.h"

@interface SP_Race_Rank()
{
    UILabel *_group;
}

@end

@implementation SP_Race_Rank


- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 40)];
        title.font = [UIFont systemFontOfSize:13];
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        title.text = @"积分榜";
        [self addSubview:title];
        
        UILabel *state = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 55, 0, 28, 40)];
        state.font = [UIFont systemFontOfSize:13];
        state.textColor = UIColorFromRGB(0x696969);
        state.text = @"状态";
        [self addSubview:state];
        
        UILabel *lose = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(state.frame) - 55, 0, 28, 40)];
        lose.font = [UIFont systemFontOfSize:13];
        lose.textColor = UIColorFromRGB(0x696969);
        lose.text = @"负场";
        [self addSubview:lose];
        
        UILabel *win = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(lose.frame) - 50, 0, 28, 40)];
        win.font = [UIFont systemFontOfSize:13];
        win.textColor = UIColorFromRGB(0x696969);
        win.text = @"胜场";
        [self addSubview:win];
        
        
        UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, 20)];
        [self addSubview:bottomView];
        
        _group= [[UILabel alloc]initWithFrame:CGRectMake(20, 0, ScreenWidth, 20)];
        _group.textColor = UIColorFromRGB(0x696969);
        _group.font = [UIFont systemFontOfSize:13];
        _group.text = [NSString stringWithFormat:@"%@组",self.name];
        
        [bottomView addSubview:_group];
        
    }
    return self;
}

- (void)setName:(NSString *)name
{
    if(name.length != 0){
        _group.text = [NSString stringWithFormat:@"%@组",name];
    }
    
}

@end
