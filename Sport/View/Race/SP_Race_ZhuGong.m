//
//  SP_Race_ZhuGong.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Race_ZhuGong.h"

@implementation SP_Race_ZhuGong

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = UIColorFromRGB(0xf0f0f0);
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 40)];
        title.font = [UIFont systemFontOfSize:13];
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        title.text = @"总助攻榜";
        [self addSubview:title];
        
        
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 51, 10, 24, 20)];
        img.image = [UIImage imageNamed:@"篮球-助攻"];
        [self addSubview:img];
        
        
        
    }
    return self;
}
@end
