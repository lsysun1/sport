//
//  SP_MvpCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MvpCell.h"
#import "UIImageView+WebCache.h"
#import "SP_MvpModel.h"

@interface SP_MvpCell()

@property (weak, nonatomic) IBOutlet UIImageView *aPlayerImg;
@property (weak, nonatomic) IBOutlet UILabel *aPlayerPosition;
@property (weak, nonatomic) IBOutlet UILabel *aPlayerNum;
@property (weak, nonatomic) IBOutlet UILabel *aPlayerPnum;

@property (weak, nonatomic) IBOutlet UIView *midLine;

@property (weak, nonatomic) IBOutlet UIImageView *bPlayerImg;
@property (weak, nonatomic) IBOutlet UILabel *bPlayerNum;
@property (weak, nonatomic) IBOutlet UILabel *bPlayerPostion;
@property (weak, nonatomic) IBOutlet UILabel *bPlayerPnum;





@end


@implementation SP_MvpCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_MvpCell";
    SP_MvpCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_MvpCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}




- (void)awakeFromNib {
    // Initialization code
    
    self.midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    self.aPlayerImg.layer.cornerRadius = 6;
    self.aPlayerImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.aPlayerImg.layer.borderWidth = 2;
    self.aPlayerImg.layer.masksToBounds = YES;
    
    self.bPlayerImg.layer.cornerRadius = 6;
    self.bPlayerImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.bPlayerImg.layer.borderWidth = 2;
    self.bPlayerImg.layer.masksToBounds = YES;
    
    
    [self.aPlayerBtn setBackgroundImage:[UIImage imageNamed:@"投票1"] forState:UIControlStateNormal];
    [self.aPlayerBtn setBackgroundImage:[UIImage imageNamed:@"投票2"] forState:UIControlStateSelected];

    [self.bPlayerBtn setBackgroundImage:[UIImage imageNamed:@"投票1"] forState:UIControlStateNormal];
    [self.bPlayerBtn setBackgroundImage:[UIImage imageNamed:@"投票2"] forState:UIControlStateSelected];

    
    
    
    self.aPlayerPnum.textColor = UIColorFromRGB(0xe84c3d);
    
    self.bPlayerPnum.textColor = UIColorFromRGB(0xe84c3d);


}



- (void) setATeamMvp:(SP_MvpModel *)aTeamMvp
{
    if(aTeamMvp != nil){
        self.aPlayerNum.text = [NSString stringWithFormat:@"%@号",aTeamMvp.number];
        NSURL *imgURL = [NSURL URLWithString:aTeamMvp.headimg];
        [self.aPlayerImg sd_setImageWithURL:imgURL];
        self.aPlayerPosition.text = aTeamMvp.postion;
        self.aPlayerPnum.text = aTeamMvp.count;
        
        if([aTeamMvp.isSupport isEqualToString:@"no"]){
            self.aPlayerBtn.selected = NO;
        }else{
            self.aPlayerBtn.selected = YES;
        }

    }else{
        self.aPlayerBtn.hidden = YES;
        self.aPlayerImg.hidden = YES;
        self.aPlayerNum.hidden = YES;
        self.aPlayerPosition.hidden = YES;
    }
}



- (void) setBTeamMvp:(SP_MvpModel *)bTeamMvp
{
    if(bTeamMvp != nil){
        self.bPlayerNum.text = [NSString stringWithFormat:@"%@号",bTeamMvp.number];
        NSURL *imgURL = [NSURL URLWithString:bTeamMvp.headimg];
        [self.bPlayerImg sd_setImageWithURL:imgURL];
        self.bPlayerPostion.text = bTeamMvp.postion;
        self.bPlayerPnum.text = bTeamMvp.count;

        
        if([bTeamMvp.isSupport isEqualToString:@"no"]){
            self.bPlayerBtn.selected = NO;
        }else{
            self.bPlayerBtn.selected = YES;
        }
        
    }else{
        self.bPlayerBtn.hidden = YES;
        self.bPlayerImg.hidden = YES;
        self.bPlayerNum.hidden = YES;
        self.bPlayerPostion.hidden = YES;
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
