//
//  SP_CommentCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_CommentFrame;

@protocol SP_CommentCellDelegate <NSObject>
@optional
- (void) praiseBtnIsClick:(NSString *)msgID;

@end


@interface SP_CommentCell : UITableViewCell
+ (instancetype) cellWithTableView:(UITableView *) tableView;

@property (strong,nonatomic) SP_CommentFrame *commentFrame;
@property (strong,nonatomic) id<SP_CommentCellDelegate> delegate;
@property (strong,nonatomic) UIButton *praiseBtn;
@property (strong,nonatomic) UIButton *headButton;


@end
