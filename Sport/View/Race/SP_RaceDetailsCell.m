//
//  SP_RaceDetailsCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceDetailsCell.h"
#import "SP_MatchList.h"
#import "UIImageView+WebCache.h"


@interface SP_RaceDetailsCell() <UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UIButton *remindBtn;
@property (nonatomic, weak) IBOutlet UIView *mySliderView;
@property (nonatomic, weak) IBOutlet UIView *myContentView;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) CGPoint panStartPoint;
@property (nonatomic, assign) CGFloat startingRightLayoutConstraintConstant;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewRightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contentViewLeftConstraint;


@property (weak, nonatomic) IBOutlet UILabel *aTeamName;
@property (weak, nonatomic) IBOutlet UIImageView *aTeamImg;
@property (weak, nonatomic) IBOutlet UILabel *time;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bTeamImg;

@property (weak, nonatomic) IBOutlet UILabel *bTeamName;



@end

@implementation SP_RaceDetailsCell

static CGFloat const kBounceValue = 10.0f;

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_RaceDetailsCell";
    SP_RaceDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_RaceDetailsCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.mySliderView.backgroundColor = UIColorFromRGB(0xccccccc);
    
    self.aTeamImg.layer.cornerRadius = 5;
    self.aTeamImg.layer.masksToBounds = YES;
    self.bTeamImg.layer.cornerRadius = 5;
    self.bTeamImg.layer.masksToBounds = YES;

    
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panThisCell:)];
    self.panRecognizer.delegate = self;
    [self.myContentView addGestureRecognizer:self.panRecognizer];
    
}



- (void) setMatchList:(SP_MatchList *)matchList
{
    _matchList = matchList;
    
    self.aTeamName.text = matchList.ateamname;
    self.bTeamName.text = matchList.bteamname;
    
    NSURL *aImg = [NSURL URLWithString:matchList.ateamimg];
    NSURL *bImg = [NSURL URLWithString:matchList.bteamimg];

    [self.aTeamImg sd_setImageWithURL:aImg];
    [self.bTeamImg sd_setImageWithURL:bImg];
    
    
    if([self.matchList.ateamscore isEqualToString:@"-"]){
        self.scoreLabel.text = @"VS";
    }else{
        self.scoreLabel.text = [NSString stringWithFormat:@"%@ - %@",matchList.ateamscore,matchList.bteamscore];
    }
    self.time.text = [self dateWithTimeIntervalSince1970:matchList.date];
    

}





#pragma mark - 设置左滑
- (void)prepareForReuse
{
    [super prepareForReuse];
    [self resetConstraintContstantsToZero:NO notifyDelegateDidClose:NO];
}

- (void)openCell
{
    [self setConstraintsToShowAllButtons:NO notifyDelegateDidOpen:NO];
}




- (CGFloat)buttonTotalWidth
{
    return CGRectGetWidth(self.frame) - CGRectGetMinX(self.mySliderView.frame);
}

- (void)panThisCell:(UIPanGestureRecognizer *)recognizer
{
//    switch (recognizer.state) {
//        case UIGestureRecognizerStateBegan:
//            self.panStartPoint = [recognizer translationInView:self.myContentView];
//            self.startingRightLayoutConstraintConstant = self.contentViewRightConstraint.constant;
//            break;
//            
//        case UIGestureRecognizerStateChanged: {
//            CGPoint currentPoint = [recognizer translationInView:self.myContentView];
//            CGFloat deltaX = currentPoint.x - self.panStartPoint.x;
//            BOOL panningLeft = NO;
//            if (currentPoint.x < self.panStartPoint.x) {  //1
//                panningLeft = YES;
//            }
//            
//            if (self.startingRightLayoutConstraintConstant == 0) { //2
//                //The cell was closed and is now opening
//                if (!panningLeft) {
//                    CGFloat constant = MAX(-deltaX, 0); //3
//                    if (constant == 0) { //4
//                        [self resetConstraintContstantsToZero:YES notifyDelegateDidClose:NO]; //5
//                    } else {
//                        self.contentViewRightConstraint.constant = constant; //6
//                    }
//                } else {
//                    CGFloat constant = MIN(-deltaX, [self buttonTotalWidth]); //7
//                    if (constant == [self buttonTotalWidth]) { //8
//                        [self setConstraintsToShowAllButtons:YES notifyDelegateDidOpen:NO]; //9
//                    } else {
//                        self.contentViewRightConstraint.constant = constant; //10
//                    }
//                }
//            }else {
//                //The cell was at least partially open.
//                CGFloat adjustment = self.startingRightLayoutConstraintConstant - deltaX; //11
//                if (!panningLeft) {
//                    CGFloat constant = MAX(adjustment, 0); //12
//                    if (constant == 0) { //13
//                        [self resetConstraintContstantsToZero:YES notifyDelegateDidClose:NO]; //14
//                    } else {
//                        self.contentViewRightConstraint.constant = constant; //15
//                    }
//                } else {
//                    CGFloat constant = MIN(adjustment, [self buttonTotalWidth]); //16
//                    if (constant == [self buttonTotalWidth]) { //17
//                        [self setConstraintsToShowAllButtons:YES notifyDelegateDidOpen:NO]; //18
//                    } else {
//                        self.contentViewRightConstraint.constant = constant;//19
//                    }
//                }
//            }
//            
//            self.contentViewLeftConstraint.constant = -self.contentViewRightConstraint.constant; //20
//        }
//            break;
//            
//        case UIGestureRecognizerStateEnded:
//            if (self.startingRightLayoutConstraintConstant == 0) { //1
//                //We were opening
//                CGFloat halfOfButtonOne = CGRectGetWidth(self.mySliderView.frame) / 2; //2
//                if (self.contentViewRightConstraint.constant >= halfOfButtonOne) { //3
//                    //Open all the way
//                    [self setConstraintsToShowAllButtons:YES notifyDelegateDidOpen:YES];
//                } else {
//                    //Re-close
//                    [self resetConstraintContstantsToZero:YES notifyDelegateDidClose:YES];
//                }
//                
//            } else {
//                //We were closing
//                CGFloat buttonOnePlusHalfOfButton2 = CGRectGetWidth(self.mySliderView.frame); //4
//                if (self.contentViewRightConstraint.constant >= buttonOnePlusHalfOfButton2) { //5
//                    //Re-open all the way
//                    [self setConstraintsToShowAllButtons:YES notifyDelegateDidOpen:YES];
//                } else {
//                    //Close
//                    [self resetConstraintContstantsToZero:YES notifyDelegateDidClose:YES];
//                }
//            }
//            break;
//            
//        case UIGestureRecognizerStateCancelled:
//            if (self.startingRightLayoutConstraintConstant == 0) {
//                //We were closed - reset everything to 0
//                [self resetConstraintContstantsToZero:YES notifyDelegateDidClose:YES];
//            } else {
//                //We were open - reset to the open state
//                [self setConstraintsToShowAllButtons:YES notifyDelegateDidOpen:YES];
//            }
//            break;
//            
//        default:
//            break;
//    }
}

- (void)updateConstraintsIfNeeded:(BOOL)animated completion:(void (^)(BOOL finished))completion;
{
    float duration = 0;
    if (animated) {
//        NSLog(@"Animated!");
        duration = 0.1;
    }
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self layoutIfNeeded];
    } completion:completion];
}


- (void)resetConstraintContstantsToZero:(BOOL)animated notifyDelegateDidClose:(BOOL)notifyDelegate
{
    if (notifyDelegate) {
        [self.delegate cellDidClose:self];
    }
    
    if (self.startingRightLayoutConstraintConstant == 0 &&
        self.contentViewRightConstraint.constant == 0) {
        //Already all the way closed, no bounce necessary
        return;
    }
    
    self.contentViewRightConstraint.constant = -kBounceValue;
    self.contentViewLeftConstraint.constant = kBounceValue;
    
    [self updateConstraintsIfNeeded:animated completion:^(BOOL finished) {
        self.contentViewRightConstraint.constant = 0;
        self.contentViewLeftConstraint.constant = 0;
        
        [self updateConstraintsIfNeeded:animated completion:^(BOOL finished) {
            self.startingRightLayoutConstraintConstant = self.contentViewRightConstraint.constant;
        }];
    }];
}


- (void)setConstraintsToShowAllButtons:(BOOL)animated notifyDelegateDidOpen:(BOOL)notifyDelegate
{
    if (notifyDelegate) {
        [self.delegate cellDidOpen:self];
    }
    
    //1
    if (self.startingRightLayoutConstraintConstant == [self buttonTotalWidth] &&
        self.contentViewRightConstraint.constant == [self buttonTotalWidth]) {
        return;
    }
    //2
    self.contentViewLeftConstraint.constant = -[self buttonTotalWidth] - kBounceValue;
    self.contentViewRightConstraint.constant = [self buttonTotalWidth] + kBounceValue;
    
    [self updateConstraintsIfNeeded:animated completion:^(BOOL finished) {
        //3
        self.contentViewLeftConstraint.constant = -[self buttonTotalWidth];
        self.contentViewRightConstraint.constant = [self buttonTotalWidth];
        
        [self updateConstraintsIfNeeded:animated completion:^(BOOL finished) {
            //4
            self.startingRightLayoutConstraintConstant = self.contentViewRightConstraint.constant;
        }];
    }];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


@end
