//
//  SP_RaceDataRankCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceDataRankCell.h"
#import "UIImageView+WebCache.h"


@interface SP_RaceDataRankCell()

@property (strong, nonatomic) IBOutlet UILabel *rankNum;
@property (strong, nonatomic) IBOutlet UILabel *winNum;
@property (strong, nonatomic) IBOutlet UILabel *loseNum;
@property (strong, nonatomic) IBOutlet UILabel *nLoseNum;
@property (strong, nonatomic) IBOutlet UILabel *teamName;
@property (weak, nonatomic) IBOutlet UIImageView *teamImg;

@end


@implementation SP_RaceDataRankCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_RaceDataRankCell";
    SP_RaceDataRankCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_RaceDataRankCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



- (void)awakeFromNib {
    // Initialization code
    self.rankNum.textColor = UIColorFromRGB(0x696969);
    self.winNum.textColor = UIColorFromRGB(0x696969);
    self.loseNum.textColor = UIColorFromRGB(0x696969);
    self.nLoseNum.textColor = UIColorFromRGB(0x696969);
    self.teamName.textColor = UIColorFromRGB(0x696969);
    
    self.teamImg.layer.cornerRadius = 8;
    self.teamImg.layer.masksToBounds = YES;
    

    
}

- (void) setModel:(NSDictionary *)model
{
    _model = model;
    self.teamName.text = model[@"name"];
    NSURL *imgURL = [NSURL URLWithString:model[@"img"]];
    [self.teamImg sd_setImageWithURL:imgURL];
    
    self.rankNum.text = [NSString stringWithFormat:@"%ld",self.number];
    
    
    NSLog(@"self.ballType -- %d",self.ballType);
    
    if(_ballType == 1){
        NSString *winNum = [NSString stringWithFormat:@"%d/%d/%d",[model[@"shen"] intValue],[model[@"ping"] intValue],[model[@"fu"] intValue]];
        self.winNum.text = winNum;
        
        NSLog(@"净胜球--- %d",[model[@"jsq"] intValue]);
        
        self.loseNum.text = [NSString stringWithFormat:@"%d",[model[@"jsq"] intValue]];
        self.nLoseNum.text = [NSString stringWithFormat:@"%d",[model[@"jifen"] intValue]];
    }else{
        self.winNum.text = [NSString stringWithFormat:@"%d",[model[@"shen"] intValue]];;
        self.loseNum.text = [NSString stringWithFormat:@"%d",[model[@"fu"] intValue]];
        
        NSString *liansheng =[NSString stringWithFormat:@"%@",model[@"lianshen"]];
        if(![liansheng isEqualToString:@"0"]){
            self.nLoseNum.text = [NSString stringWithFormat:@"%d连胜",[model[@"lianshen"] intValue]];
        }else{
            self.nLoseNum.text = [NSString stringWithFormat:@"%d连负",[model[@"lianshen"] intValue]];
        }
        
    }
    
}

- (void)setBallType:(int)ballType
{
    _ballType = ballType;

}


- (void) setNumber:(NSInteger)number
{
    _number = number;
    
    self.rankNum.text = [NSString stringWithFormat:@"%ld",number];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//  足球
//    "teamid": 11,
//    "name": "屌丝队",
//    "img": "http://121.40.220.245/Public/upload/teamheader/143243686979895.jpg",
//    "shen": 1,
//    "ping": 1,
//    "fu": 0,
//    "jsq": 3,
//    "jifen": 4
//},


//  篮球
//{
//    "teamid": 14,
//    "name": "黄蜂队",
//    "img": "http://121.40.220.245/Public/upload/teamheader/143322638234593.jpg",
//    "shen": 0,
//    "fu": 1,
//    "lianshen": 0
//}

@end
