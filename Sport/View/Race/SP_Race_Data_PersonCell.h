//
//  SP_Race_Data_PersonCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_Race_Data_PersonCell : UITableViewCell
+(instancetype) cellWithTableView:(UITableView *)tableView;
@property (nonatomic,strong) NSDictionary *model;
@property (nonatomic,assign) NSInteger number;
@property (nonatomic,copy) NSString *cellType;


@end
