//
//  SP_PlayerCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_MatchInfo_PersonData;
@interface SP_PlayerCell : UITableViewCell
@property (nonatomic, strong) SP_MatchInfo_PersonData *person;

@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UIButton *headBtn;

+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
