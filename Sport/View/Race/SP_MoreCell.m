//
//  SP_MoreCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MoreCell.h"

@interface SP_MoreCell()

@property (strong, nonatomic) IBOutlet UILabel *moreLabel;

@end

@implementation SP_MoreCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_MoreCell";
    SP_MoreCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_MoreCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (void)awakeFromNib {
    // Initialization code
    self.moreLabel.textColor = UIColorFromRGB(0xe94d41);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
