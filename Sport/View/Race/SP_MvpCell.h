//
//  SP_MvpCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_MvpModel;
@interface SP_MvpCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *aPlayerBtn;
@property (weak, nonatomic) IBOutlet UIButton *bPlayerBtn;


@property (weak, nonatomic) IBOutlet UIButton *goAPlayer;

@property (weak, nonatomic) IBOutlet UIButton *goBPlayer;


+(instancetype) cellWithTableView:(UITableView *)tableView;

@property (nonatomic,copy) SP_MvpModel *aTeamMvp;
@property (nonatomic,copy) SP_MvpModel *bTeamMvp;


@end
