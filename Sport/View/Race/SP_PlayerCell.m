//
//  SP_PlayerCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_PlayerCell.h"
#import "SP_MatchInfo_PersonData.h"
#import "UIImageView+WebCache.h"

@interface SP_PlayerCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *assLabel;

@end


@implementation SP_PlayerCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_PlayerCell";
    SP_PlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_PlayerCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}




- (void)awakeFromNib {

    self.headImgView.layer.cornerRadius = 8;
    self.headImgView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    self.headImgView.layer.borderWidth = 2;
    self.headImgView.layer.masksToBounds = YES;
    
    
    
    
}

- (void) setPerson:(SP_MatchInfo_PersonData *)person
{
    NSURL *url = [NSURL URLWithString:person.headimg];
    [self.headImgView sd_setImageWithURL:url];
    
    self.nameLabel.text = person.nickname;
    self.scoreLabel.text = person.goal;
    self.assLabel.text = person.assists;
    self.positionLabel.text = person.postion;

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
