//
//  SP_RaceDetailsHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_MatchInfo_TopData,SP_MatchInfo_MvpData,SP_MatchList,SP_MatchLeagueList;

@protocol SP_RaceDetailsHeadViewDelegate <NSObject>

@optional
- (void) headViewSupportBtnDidClick:(UIButton *)btn;

@end


@interface SP_RaceDetailsHeadView : UITableViewHeaderFooterView

@property (nonatomic,strong) SP_MatchInfo_TopData *topData;
@property (nonatomic,strong) SP_MatchInfo_MvpData *mvpData;
@property (nonatomic,strong) SP_MatchList *matchList;
@property (nonatomic,strong) SP_MatchLeagueList *listModel;


@property (nonatomic,assign) id<SP_RaceDetailsHeadViewDelegate> delegate;

@end
