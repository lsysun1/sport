//
//  SP_Race_Data_PersonCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Race_Data_PersonCell.h"
#import "UIImageView+WebCache.h"

@interface SP_Race_Data_PersonCell()
@property (strong, nonatomic) IBOutlet UILabel *leftNum;
@property (strong, nonatomic) IBOutlet UILabel *teamName;
@property (strong, nonatomic) IBOutlet UIImageView *headImg;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *position;
@property (strong, nonatomic) IBOutlet UILabel *rightNum;

@end


@implementation SP_Race_Data_PersonCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_RaceDataRankCell";
    SP_Race_Data_PersonCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_Race_Data_PersonCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (void)awakeFromNib {
    // Initialization code
    
    self.leftNum.textColor = UIColorFromRGB(0x696969);
    self.teamName.textColor = UIColorFromRGB(0x696969);
    self.name.textColor = UIColorFromRGB(0x696969);
    self.position.textColor = UIColorFromRGB(0x696969);
    self.rightNum.textColor = UIColorFromRGB(0x696969);

    self.headImg.layer.borderWidth = 1;
    self.headImg.layer.cornerRadius = 8;
    self.headImg.layer.masksToBounds = YES;
    
    
    
}

- (void) setModel:(NSDictionary *)model
{
    _model = model;
    
    NSURL *imgURL = [NSURL URLWithString:model[@"headimg"]];
    [self.headImg sd_setImageWithURL:imgURL];
    if([model[@"sex"] isEqualToString:@"0"]){
        self.headImg.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        self.headImg.layer.borderColor = [UIColorFromRGB(0x599ffff)CGColor];
    }
    
    self.leftNum.text = [NSString stringWithFormat:@"%ld",self.number];
    self.teamName.text = [NSString stringWithFormat:@"%@",model[@"teamname"]];
    self.position.text = [NSString stringWithFormat:@"%@",model[@"postion"]];
    self.name.text = [NSString stringWithFormat:@"%@",model[@"nickname"]];
    
    if([self.cellType isEqualToString:@"mvp"]){
        self.rightNum.text = [NSString stringWithFormat:@"%ld",[model[@"mvpcount"]integerValue]];
    }else if ([self.cellType isEqualToString:@"score"]){
        self.rightNum.text = [NSString stringWithFormat:@"%ld",[model[@"totalscore"]integerValue]];
    }else if ([self.cellType isEqualToString:@"ass"]){
        self.rightNum.text = [NSString stringWithFormat:@"%ld",[model[@"totalassists"]integerValue]];
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//  MVP
//"uid": 14,
//"nickname": "匿名小鲜肉",
//"headimg": "http://121.40.220.245/Public/upload/header/143625012956574.jpg",
//"sex": "0",
//"postion": "守门员",
//"teamname": "马刺队",
//"role": "2",
//"mvpcount": 1



//  得分
//"uid": "14",
//"teamid": "14",
//"totalscore": "33",
//"teamname": "黄蜂队",
//"nickname": "匿名小鲜肉",
//"headimg": "http://121.40.220.245/Public/upload/header/143625012956574.jpg",
//"sex": "0",
//"postion": "守门员",
//"role": "1"


// 助攻
//"uid": "15",
//"teamid": "14",
//"totalassists": "5",
//"teamname": "黄蜂队",
//"nickname": "小骚羊",
//"headimg": "http://121.40.220.245/Public/upload/header/143243789229676.jpg",
//"sex": "1",
//"postion": "后卫",
//"role": "2"


@end
