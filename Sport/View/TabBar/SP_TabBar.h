//
//  SP_TabBar.h
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SP_TabBarDelegate <NSObject>
/**
 *  @param from 从哪个视图(视图索引)
 *  @param to   到哪个视图(视图索引)
 */
- (void)tabBarDidSelectBtnFrom:(NSInteger)from to:(NSInteger)to;

@end


@interface SP_TabBar : UIView
@property (nonatomic, weak) id<SP_TabBarDelegate> delegate;

/**
 *  提供给外界创建按钮
 *
 *  @param norName 默认状态的图片
 *  @param disName 高亮状态的图片
 */
- (void)addTabBarButtonWithNormalImageName:(NSString *)norName andDisableImageName:(NSString *)disName;


@end
