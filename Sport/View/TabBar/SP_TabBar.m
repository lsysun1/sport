//
//  SP_TabBar.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_TabBar.h"
#import "SP_TabBarButton.h"

@interface SP_TabBar ()

// 定义变量记录当前选中的按钮
@property (nonatomic, weak) UIButton *selectBtn;

@end


@implementation SP_TabBar

- (void)addTabBarButtonWithNormalImageName:(NSString *)norName andDisableImageName:(NSString *)disName
{
    SP_TabBarButton *btn = [[SP_TabBarButton alloc] init];
    
    [btn setImage:[UIImage imageNamed:norName] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:disName] forState:UIControlStateDisabled];
    
    [self addSubview:btn];
    
    [btn addTarget:self action:@selector(btnOnClick:) forControlEvents:UIControlEventTouchDown];
    
    if (1 == self.subviews.count) {
        [self btnOnClick:btn];
    }
    
    btn.adjustsImageWhenHighlighted = NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundColor = [UIColor whiteColor];
    
    for (int i = 0; i < self.subviews.count ; i++) {
        
        UIButton *btn = self.subviews[i];
        
        CGFloat btnY = 0;
        CGFloat btnW = self.frame.size.width / self.subviews.count;
        CGFloat btnH = self.frame.size.height;
        CGFloat btnX = i * btnW;
        btn.frame = CGRectMake(btnX, btnY, btnW, btnH);
        
        btn.tag = i;
        
        UILabel *topLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, btnW, 1)];
        topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [btn addSubview:topLine];
    }
    

    
}

- (void)btnOnClick:(UIButton *)btn
{
    
    if ([self.delegate respondsToSelector:@selector(tabBarDidSelectBtnFrom:to:)]) {
        [self.delegate tabBarDidSelectBtnFrom:self.selectBtn.tag to:btn.tag];
    }
    
    self.selectBtn.enabled = YES;
    btn.enabled = NO;
    self.selectBtn = btn;
    
}
@end
