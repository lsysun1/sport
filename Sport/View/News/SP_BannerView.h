//
//  SP_BannerView.h
//  Sport
//
//  Created by 李松玉 on 15/7/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_BannerEntity;
@interface SP_BannerView : UIView

@property (nonatomic, strong) SP_BannerEntity *bannerEntity;

@end
