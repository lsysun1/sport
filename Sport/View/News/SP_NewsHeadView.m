//
//  SP_NewsHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/6/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsHeadView.h"
#import "AppDelegate.h"
#import "SP_BannerView.h"

//const int ImageCount = 2;

@interface SP_NewsHeadView()<UIScrollViewDelegate>
{
    UIScrollView *_myScrollView;
    UIView *_typeView;
    UIPageControl *_myPageController;
    NSTimer *_timer;
    int _ImageCount;
}

@end

@implementation SP_NewsHeadView

- (id)initWithFrame:(CGRect)frame
{
    if(self == [super initWithFrame:frame]){
        
        _myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
        [self addSubview:_myScrollView];
        
        _myPageController = [[UIPageControl alloc]init];
        [self addSubview:_myPageController];

        
        
        
        _typeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_myScrollView.frame), ScreenWidth, 40)];
        _typeView.backgroundColor = UIColorFromRGB(0xfafafa);
        
        self.footBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth/2, 40)];
        self.footBtn.selected = YES;
        self.footBtn.tag = 1;
        self.footBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.footBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        [self.footBtn setTitleColor:UIColorFromRGB(0xfd4d01) forState:UIControlStateSelected];
        [self.footBtn setTitle:@"足球" forState:UIControlStateNormal];
        [self.footBtn setImage:[UIImage imageNamed:@"新闻_足球"] forState:UIControlStateNormal];
        [self.footBtn setImage:[UIImage imageNamed:@"新闻_足球点"] forState:UIControlStateSelected];
        
        [self.footBtn setImageEdgeInsets:UIEdgeInsetsMake(10, ScreenWidth/4 , 10, ScreenWidth/4 + 20)];
        
        
        self.baskBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 40)];
        self.baskBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        self.baskBtn.tag = 0;
        [self.baskBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
        [self.baskBtn setTitleColor:UIColorFromRGB(0xfd4d01) forState:UIControlStateSelected];
        [self.baskBtn setTitle:@"篮球" forState:UIControlStateNormal];
        [self.baskBtn setImage:[UIImage imageNamed:@"新闻_篮球"] forState:UIControlStateNormal];
        [self.baskBtn setImage:[UIImage imageNamed:@"新闻_篮球点"] forState:UIControlStateSelected];
        [self.baskBtn setImageEdgeInsets:UIEdgeInsetsMake(10, ScreenWidth/4 , 10, ScreenWidth/4 + 20)];


    
        UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 5, 1, 30)];
        midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth, 1)];
        bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        
        [self addSubview:_typeView];
        [_typeView addSubview:self.footBtn];
        [_typeView addSubview:self.baskBtn];
        [_typeView addSubview:midLine];
        [_typeView addSubview:bottomLine];
        
    }
    return self;
}




- (void) setBannerArr:(NSMutableArray *)bannerArr
{
    _ImageCount = (int)bannerArr.count;
    
    _myScrollView.delegate = self;
    _myScrollView.bounces = NO;
    _myScrollView.showsHorizontalScrollIndicator = NO;
    _myScrollView.showsVerticalScrollIndicator = NO;
    _myScrollView.pagingEnabled = YES;
    _myScrollView.contentSize = CGSizeMake(_ImageCount * _myScrollView.bounds.size.width, 0);

    _myPageController.numberOfPages = _ImageCount;
    
    // 控件尺寸
    CGSize size = [_myPageController sizeForNumberOfPages:_ImageCount];
    
    _myPageController.bounds = CGRectMake(0, 10, size.width, size.height);
    _myPageController.center = CGPointMake(self.center.x, 200);
    
    
    [_myPageController addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];
    
    // 添加图片
    
    for (int index = 1; index <= bannerArr.count; index++) {
        
        SP_BannerView *bannerView = [[SP_BannerView alloc]initWithFrame:_myScrollView.bounds];
        bannerView.bannerEntity = bannerArr[index - 1];
        
        

        
        [_myScrollView addSubview:bannerView];

        
        
    }
    

    
    // 计算imageView的位置
    [_myScrollView.subviews enumerateObjectsUsingBlock:^(SP_BannerView *bannerView, NSUInteger idx, BOOL *stop) {
        
        // 调整x => origin => frame
        CGRect frame = bannerView.frame;
        frame.origin.x = idx * frame.size.width;
        
        bannerView.frame = frame;
    }];

    
    // 分页初始页数为0
    _myPageController.currentPage = 0;
    
    [self startTimer];


}















// 分页控件的监听方法
- (void)pageChanged:(UIPageControl *)page
{
    //    NSLog(@"%ld", (long)page.currentPage);
    
    // 根据页数，调整滚动视图中的图片位置 contentOffset
    CGFloat x = page.currentPage * _myScrollView.bounds.size.width;
    [_myScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)startTimer
{
    _timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    // 添加到运行循环
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)updateTimer
{
    // 页号发生变化
    // (当前的页数 + 1) % 总页数
    int page = (_myPageController.currentPage + 1) % _ImageCount;
    _myPageController.currentPage = page;
    
    //    NSLog(@"%ld", (long)myPageController.currentPage);
    // 调用监听方法，让滚动视图滚动
    [self pageChanged:_myPageController];
}

#pragma mark - ScrollView的代理方法
// 滚动视图停下来，修改页面控件的小点（页数）
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 停下来的当前页数
    //    NSLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
    
    // 计算页数
    int page = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    _myPageController.currentPage = page;
}

/**
 修改时钟所在的运行循环的模式后，抓不住图片
 
 解决方法：抓住图片时，停止时钟，送售后，开启时钟
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //    NSLog(@"%s", __func__);
    // 停止时钟，停止之后就不能再使用，如果要启用时钟，需要重新实例化
    [_timer invalidate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self startTimer];
}

@end
