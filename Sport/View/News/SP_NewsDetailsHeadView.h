//
//  SP_NewsDetailsHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_NewsList;


@protocol SP_NewsDetailsHeadViewDelegate <NSObject>
@optional
- (void)webviewDidFinished:(CGFloat)height;
@end

@interface SP_NewsDetailsHeadView : UIView

@property (nonatomic,copy) NSString *newscontentStr;
@property (nonatomic,copy) SP_NewsList *newsListModel;
@property (nonatomic,assign) CGFloat ViewHeight;
@property (nonatomic, assign) id<SP_NewsDetailsHeadViewDelegate> delegate;


@end
