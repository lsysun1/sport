//
//  SP_NewsCommentCell.h
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_NewsCommentFrame;
@protocol SP_NewsCommentCellDelegate <NSObject>
@optional
- (void) praiseBtnIsClick:(NSString *)msgID;

@end


@interface SP_NewsCommentCell : UITableViewCell
+ (instancetype) cellWithTableView:(UITableView *) tableView;

@property (strong,nonatomic) SP_NewsCommentFrame *commentFrame;
@property (strong,nonatomic) id<SP_NewsCommentCellDelegate> delegate;

@property (strong,nonatomic) UIButton *praiseBtn;
@property (strong,nonatomic) UIButton *headButton;

@end
