//
//  SP_NewsListCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsListCell.h"
#import "UIImageView+WebCache.h"
#import "SP_NewsList.h"

@interface SP_NewsListCell()
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *content;
@property (strong, nonatomic) IBOutlet UILabel *location;
@property (strong, nonatomic) IBOutlet UILabel *commentNum;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UIView *bottomLine;

@end



@implementation SP_NewsListCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"SP_NewsListCell";
    SP_NewsListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SP_NewsListCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;

}

- (void)awakeFromNib {
    // Initialization code
    
    self.content.textColor = UIColorFromRGB(0x696969);
    
    self.location.backgroundColor = UIColorFromRGB(0x47b650);
    self.location.textColor = [UIColor whiteColor];

    self.date.textColor = UIColorFromRGB(0x696969);
    
    self.commentNum.textColor = UIColorFromRGB(0xfd5204);

    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
}

- (void) setModel:(SP_NewsList *)model
{
    _model = model;
    
    NSURL *url = [NSURL URLWithString:model.newsimg];
    [self.imgView sd_setImageWithURL:url];
    
    self.title.text = model.newstitle;
    self.content.text = model.newsdesc;
    self.location.text = model.newstag;
    self.commentNum.text = model.commentcount;
    self.location.text = model.newstag;
    self.date.text = [self dateWithTimeIntervalSince1970:model.addtime];
    
}


/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
