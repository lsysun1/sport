//
//  SP_NewsDetailsHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsDetailsHeadView.h"
#import "SP_NewsList.h"

@interface SP_NewsDetailsHeadView()<UIWebViewDelegate>
{
    UILabel *_titleLabel;
    UILabel *_tagLabel;
    UILabel *_dateLabel;
    UILabel *_topSepLinel;
    UIWebView *_contentWebView;
    CGFloat _orginY;
}
@end



@implementation SP_NewsDetailsHeadView


- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 30)];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        
        _tagLabel = [[UILabel alloc]init];
        _tagLabel.font = [UIFont systemFontOfSize:12];
        _tagLabel.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        _tagLabel.layer.cornerRadius = 4;
        _tagLabel.layer.masksToBounds = YES;
        _tagLabel.textColor = [UIColor whiteColor];
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        
        _dateLabel = [[UILabel alloc]init];
        _dateLabel.font = [UIFont systemFontOfSize:13];
        _dateLabel.textAlignment = NSTextAlignmentRight;
        _dateLabel.textColor = UIColorFromRGB(0x696969);
        
        _topSepLinel = [[UILabel alloc]init];
        _topSepLinel.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
    
        _contentWebView = [[UIWebView alloc]init];
        _contentWebView.scrollView.scrollEnabled = NO;
        _contentWebView.delegate = self;
        
        
    
    
        
        [self addSubview:_titleLabel];
        [self addSubview:_tagLabel];
        [self addSubview:_dateLabel];
        [self addSubview:_topSepLinel];
        [self addSubview:_contentWebView];
        
    }
    return self;
}



- (void) setNewsListModel:(SP_NewsList *)newsListModel
{
    _newsListModel = newsListModel;
    
    _titleLabel.text = newsListModel.newstitle;
    CGSize size = [_titleLabel sizeThatFits:CGSizeMake(_titleLabel.frame.size.width, MAXFLOAT)];
    _titleLabel.frame = CGRectMake(15, 5,size.width, 30);
    
    if(newsListModel.newstag){
        _tagLabel.frame = CGRectMake(CGRectGetMaxX(_titleLabel.frame) + 10, 12, 30, 16);
        _tagLabel.text = newsListModel.newstag;
    }
    _dateLabel.frame = CGRectMake(ScreenWidth - 200, CGRectGetMaxY(_titleLabel.frame) + 5, 180, 13);
    _dateLabel.text = [self dateWithTimeIntervalSince1970:newsListModel.addtime];
    
    _topSepLinel.frame = CGRectMake(15, CGRectGetMaxY(_dateLabel.frame) + 5, ScreenWidth - 30, 1);
    

    


}


- (void) setNewscontentStr:(NSString *)newscontentStr
{
    
    if(newscontentStr){
        NSString *webString = [NSString stringWithFormat:@"<html> \n"
                               "<head> \n"
                               "<style type=\"text/css\"> \n"
                               "body {font-family: \"%@\"; font-size: %f; color: %@;}\n"
                               "</style> \n"
                               "</head>"
                               "<body>%@</body>"
                               "</html>", @"宋体", 14.0,UIColorFromRGB(0x363636),newscontentStr];
        [_contentWebView loadHTMLString:webString baseURL:nil];
    }
    

    
    
    _contentWebView.frame = CGRectMake(15, CGRectGetMaxY(_topSepLinel.frame) + 10, ScreenWidth - 30, 200);
    


}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{

    
    [webView stringByEvaluatingJavaScriptFromString:
     @"var script = document.createElement('script');"
     "script.type = 'text/javascript';"
     "script.text = \"function ResizeImages() { "
     "var myimg,oldwidth;"
     "var maxwidth = 300.0;" // UIWebView中显示的图片宽度
     "for(i=0;i <document.images.length;i++){"
     "myimg = document.images[i];"
     "if(myimg.width > maxwidth){"
     "oldwidth = myimg.width;"
     "myimg.width = maxwidth;"
     "}"
     "}"
     "}\";"
     "document.getElementsByTagName('head')[0].appendChild(script);"];
    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];
    
    CGRect frame = webView.frame;
    NSString *fitHeight = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    frame.size.height = [fitHeight floatValue];
    webView.frame = frame;
    
    _orginY = webView.frame.origin.y + webView.frame.size.height;
    self.ViewHeight = CGRectGetMaxY(webView.frame);
    if (_delegate && [_delegate respondsToSelector:@selector(webviewDidFinished:)])
    {
        [_delegate webviewDidFinished:self.ViewHeight];
    }
}






/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


@end
