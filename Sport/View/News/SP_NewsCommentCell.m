//
//  SP_NewsCommentCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsCommentCell.h"
#import "SP_NewsCommentFrame.h"
#import "SP_NewsCommentModel.h"
#import "UIImageView+WebCache.h"

@interface SP_NewsCommentCell()
{
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_timeLabel;
    UILabel *_contentLabel;
    
    
    
    UILabel *_praiseNum;
    
    UILabel *_bottomLine;
    
}
@end


@implementation SP_NewsCommentCell


+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"SP_NewsCommentCell";
    SP_NewsCommentCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[SP_NewsCommentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        
        _headImgView = [[UIImageView alloc]init];
        _headImgView.layer.cornerRadius = 25;
        _headImgView.layer.masksToBounds = YES;
        
        
        self.headButton = [[UIButton alloc]init];
        self.headButton.backgroundColor = [UIColor clearColor];
        
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textColor = UIColorFromRGB(0x696969);
        
        
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = UIColorFromRGB(0x696969);
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        _praiseNum = [[UILabel alloc]init];
        _praiseNum.font = [UIFont systemFontOfSize:14];
        _praiseNum.textColor = UIColorFromRGB(0x696969);
        
        self.praiseBtn = [[UIButton alloc]init];
        [self.praiseBtn setBackgroundImage:[UIImage imageNamed:@"已赞"] forState:UIControlStateSelected];
        [self.praiseBtn setBackgroundImage:[UIImage imageNamed:@"未赞"] forState:UIControlStateNormal];
        [self.praiseBtn addTarget:self action:@selector(praiseBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        _bottomLine = [[UILabel alloc]init];
        _bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        
        
        
        
        
        [self.contentView addSubview: _headImgView];
        [self.contentView addSubview:self.headButton];
        [self.contentView addSubview: _nameLabel];
        [self.contentView addSubview: _timeLabel];
        [self.contentView addSubview: _contentLabel];
        [self.contentView addSubview: _praiseNum];
        [self.contentView addSubview: self.praiseBtn];
        [self.contentView addSubview: _bottomLine];
        
    }
    return self;
}




- (void) setCommentFrame:(SP_NewsCommentFrame *)commentFrame
{
    _commentFrame = commentFrame;
    SP_NewsCommentModel *msg = commentFrame.msgModel;
    
    
    _headImgView.frame = commentFrame.headImgFrame;
    NSURL *url = [NSURL URLWithString:msg.headimg];
    [_headImgView sd_setImageWithURL:url];
    
    self.headButton.frame = commentFrame.headImgFrame;
    
    
    _nameLabel.frame = commentFrame.nameFrame;
    _nameLabel.text = msg.nickname;
    
    _timeLabel.frame = commentFrame.timeFrame;
    _timeLabel.text = msg.addtime;
    
    _contentLabel.frame = commentFrame.contentFrame;
    _contentLabel.text = msg.content;
    
    _praiseNum.frame = commentFrame.praiseNumFrame;
    _praiseNum.text = msg.praisecount;
    
    self.praiseBtn.frame = commentFrame.praiseBtnFrame;
    if([msg.ispraised isEqualToString:@"no"]){
        self.praiseBtn.selected = NO;
    }else{
        self.praiseBtn.selected = YES;
    }
    
    _bottomLine.frame = commentFrame.bottomLineFrame;
    
}




- (void) praiseBtnDidClick
{
    SP_NewsCommentModel *msg = self.commentFrame.msgModel;
    NSString *newscommentid = msg.newscommentid;
    
    _praiseBtn.selected = YES;
    
    int praise = [msg.praisecount intValue] + 1;
    
    if([msg.ispraised isEqualToString:@"no"]){
        _praiseNum.text = [NSString stringWithFormat:@"%d",praise];
    }

    
    if(self.delegate && [self.delegate respondsToSelector:@selector(praiseBtnIsClick:)]){
        [self.delegate praiseBtnIsClick:newscommentid];
    }
}

/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
