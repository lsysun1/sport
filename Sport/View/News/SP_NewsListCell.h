//
//  SP_NewsListCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_NewsList;
@interface SP_NewsListCell : UITableViewCell
@property (nonatomic, strong) SP_NewsList *model;


+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
