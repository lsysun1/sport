//
//  SP_BannerView.m
//  Sport
//
//  Created by 李松玉 on 15/7/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BannerView.h"
#import "UIImageView+WebCache.h"
#import "SP_BannerEntity.h"
#import "SP_NewsDetailsViewController.h"
#import "SP_NewsList.h"

@interface SP_BannerView()
{
    UIImageView *_imgView;
    UIButton *_btn;
    UILabel *_title;
}
@end


@implementation SP_BannerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        _imgView = [[UIImageView alloc]initWithFrame:self.bounds];
        [self addSubview:_imgView];
        
        
        _btn =[[UIButton alloc]initWithFrame:self.bounds];
        [_btn addTarget:self action:@selector(bannberBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn];
        
        _title = [[UILabel alloc]initWithFrame:CGRectMake(10 , 165, self.bounds.size.width - 10, 20)];
        _title.textColor = [UIColor whiteColor];
        _title.font = [UIFont boldSystemFontOfSize:17];
        [self addSubview:_title];
        
    
        
    }
    return self;
}

- (void) setBannerEntity:(SP_BannerEntity *)bannerEntity
{
    _bannerEntity = bannerEntity;
    
    NSURL *imgURL = [NSURL URLWithString:bannerEntity.img];
    [_imgView sd_setImageWithURL:imgURL];
    
    _title.text = bannerEntity.title;
    
}

- (void) bannberBtnDidClick:(UIButton *)btn
{
    if([_bannerEntity.type isEqualToString:@"1"]){
        
        SP_NewsList *newsList = [[SP_NewsList alloc]init];
        
        newsList.newsid = _bannerEntity.linkid;
        newsList.newstitle = _bannerEntity.title;
        newsList.addtime = _bannerEntity.addtime;
        
        
        SP_NewsDetailsViewController *vc = [[SP_NewsDetailsViewController alloc]init];
        vc.newsList = newsList;
        
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
        NSLog(@"新闻");
    }else{
        NSLog(@"广告");
    }
    
}




@end
