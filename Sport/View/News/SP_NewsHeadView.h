//
//  SP_NewsHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/6/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SP_NewsHeadView : UIView


@property (nonatomic,strong) UIButton *footBtn;
@property (nonatomic,strong) UIButton *baskBtn;
@property (nonatomic,strong) NSMutableArray *bannerArr;

@end
