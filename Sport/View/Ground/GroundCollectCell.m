//
//  GroundCollectCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundCollectCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"
#import "UIButton+WebCache.h"
#import "SP_UerHomeViewController.h"

@interface GroundCollectCell()
{
    UIScrollView *_scrollView;
    UILabel *_collectNum;
    UIView *_partView;
    UIImage *_headImg;

}
@end


@implementation GroundCollectCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(5, 0, ScreenWidth - 10, 60)];
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:_scrollView];
        

        
        
        _partView = [[UIView alloc]init];
        [self.contentView addSubview:_partView];
        
        UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(5, 0, ScreenWidth, 1)];
        midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [_partView addSubview:midLine];

        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(10,10.3, 15, 16.5)];
        imgView.image = [UIImage imageNamed:@"图标3"];
        [_partView addSubview:imgView];
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 75, 40)];
        title.font = [UIFont systemFontOfSize:14];
        title.text = @"发布的活动";
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [_partView addSubview:title];
        
        _collectNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(title.frame) + 2, 0, 100, 40)];
        _collectNum.font = [UIFont systemFontOfSize:14];
        _collectNum.textColor = [UIColor blackColor];
        _collectNum.text = @"(20)";
        [_partView addSubview:_collectNum];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(5, 37, ScreenWidth - 10, 3)];
        line.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [_partView addSubview:line];
        
        
//        [_partView addSubview:_partView];
        
    }
    return self;
}

+ (instancetype) cellWithTableView:(UITableView *) tableView
{
    static NSString *ID = @"GroundCollectCell";
    GroundCollectCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:ID];
    if(cell == nil){
        cell = [[GroundCollectCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void) setCollectArray:(NSArray *)collectArray
{
    _collectArray = collectArray;
    if(collectArray.count == 0){
        _scrollView.hidden = YES;
        _partView.frame = CGRectMake(0, 0, ScreenWidth, 40);
    }else{
        _scrollView.hidden = NO;
        _partView.frame = CGRectMake(0, 60, ScreenWidth, 40);

        
        
        CGFloat btnX = 0;
        CGFloat btnY = 6;
        CGFloat btnW = 45;
        CGFloat btnH = 45;
        
        for (int i = 0; i<collectArray.count; i++) {
            
            NSDictionary *dict = collectArray[i];
        
            CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
            
            NSString *tagStr = dict[@"uid"];
            NSInteger btnTag = [tagStr integerValue];
            
            NSURL *imgURL = [NSURL URLWithString:dict[@"headimg"]];
            
            UIButton *btn = [[UIButton alloc]initWithFrame:btnFrame];
            btn.tag = btnTag;
            
            UIImage *btnBg = [GT_Tool createImageWithColor:UIColorFromRGB(0xffffff)];
            
            btn.titleLabel.font = [UIFont systemFontOfSize:15];

            [btn setBackgroundImage:btnBg forState:UIControlStateNormal];
            [btn sd_setBackgroundImageWithURL:imgURL forState:UIControlStateNormal];
            
            btn.layer.cornerRadius = 22.5;
            btn.layer.borderWidth = 1;
            btn.layer.borderColor = UIColorFromRGB(0x6a6a6a).CGColor;
            btn.layer.masksToBounds = YES;
            
            
            if([dict[@"sex"] isEqualToString:@"0"]){
                btn.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
            }else{
                btn.layer.borderColor = UIColorFromRGB(0x599ffff).CGColor;
            }

            
            [btn addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [_scrollView addSubview:btn];
            
            
            
            
            btnX += 61;
            _scrollView.contentSize = CGSizeMake(btnX + 3, 45);
            
        }
    
    }
}

- (void) btnDidClick:(UIButton *)btn
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
