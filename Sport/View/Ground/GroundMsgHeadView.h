//
//  GroundMsgHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/7/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroundMsgHeadView : UIView

@property (nonatomic,strong) UILabel *msgNum;

@end
