//
//  GroundRaceCell.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroundRaceCell : UITableViewCell

+(instancetype) cellWithTableView:(UITableView *)tableView;


@end
