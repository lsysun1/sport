//
//  GroundRaceCell.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundRaceCell.h"

@interface GroundRaceCell()
@property (weak, nonatomic) IBOutlet UILabel *aTeamName;
@property (weak, nonatomic) IBOutlet UIImageView *aTeamImg;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bTeamImg;
@property (weak, nonatomic) IBOutlet UILabel *bTeamName;



@end


@implementation GroundRaceCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"GroundRaceCell";
    GroundRaceCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GroundRaceCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
