//
//  GroundCommentHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundCommentHeadView.h"

@implementation GroundCommentHeadView

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(10,10.3, 15, 16.5)];
        imgView.image = [UIImage imageNamed:@"图标5"];
        [self addSubview:imgView];
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth, 40)];
        title.font = [UIFont systemFontOfSize:14];
        title.text = @"留言板";
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [self addSubview:title];
        
        self.collectNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(title.frame) + 2, 0, 100, 40)];
        self.collectNum.font = [UIFont systemFontOfSize:14];
        self.collectNum.text = @"(14)";
        [self addSubview:self.collectNum];
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(5, 37, ScreenWidth - 10, 3)];
        line.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [self addSubview:line];
    }
    return self;
}



@end
