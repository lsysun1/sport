//
//  MyTextView.m
//  MyWeiBo
//
//  Created by 李松玉 on 9/4/14.
//  Copyright (c) 2014 Sun1. All rights reserved.
//

#import "MyTextView.h"


@interface MyTextView()
{
    UILabel *placeLabel;
}
@end

@implementation MyTextView

- (id)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        
        placeLabel = [[UILabel alloc]init];
        placeLabel.textColor = [UIColor lightGrayColor];
        placeLabel.hidden = NO;
        placeLabel.numberOfLines = 2;
        placeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self insertSubview:placeLabel atIndex:0];
        placeLabel.font = [UIFont systemFontOfSize:14];
        
        
        //监听textView文字的改变的通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self];
    }

    return self;
}


- (void) setPlacehoder:(NSString *)placehoder
{
    _placehoder = [placehoder copy];

    placeLabel.text = placehoder;

    

    if(placehoder.length){
        placeLabel.hidden = NO;
        //计算placeLabel的Frame
        CGFloat placeholderX = 4;
        CGFloat placeholderY = 0;
        CGFloat maxW = self.frame.size.width - 2 * placeholderX;
        CGFloat maxH = self.frame.size.height - 2 * placeholderY;
        CGSize placeholderSize = [placehoder sizeWithFont:placeLabel.font constrainedToSize:CGSizeMake(maxW, maxH)];
        placeLabel.frame = CGRectMake(placeholderX, placeholderY, placeholderSize.width, placeholderSize.height + 20);

    }else{
        placeLabel.hidden = YES;
    }
}

- (void) textDidChange
{
    placeLabel.hidden = (self.text.length != 0);
}




@end
