//
//  GroundRaceHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundRaceHeadView.h"


@interface GroundRaceHeadView()
{
    UILabel *_notice;
}
@end

@implementation GroundRaceHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10.3, 15, 16.5)];
        imgView.image = [UIImage imageNamed:@"图标1"];
        [self addSubview:imgView];
        
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, 90, 40)];
        title.font = [UIFont systemFontOfSize:14];
        title.text = @"进行中的比赛";
        title.textColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [self addSubview:title];
        
        _notice = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(title.frame), 0, 120, 40)];
        _notice.font = [UIFont systemFontOfSize:14];
        _notice.text = @"(暂无进行中的比赛)";
        _notice.textColor = [UIColor redColor];
        [self addSubview:_notice];

        
        
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(5, 37, ScreenWidth - 10, 3)];
        line.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
        [self addSubview:line];
        
        
        
        
    }
    return self;
}

- (void) setNoRace:(BOOL)noRace
{
    _noRace = noRace;
    
    if(_noRace == YES){
        _notice.hidden = YES;
    }else{
        _notice.hidden = NO;
    }
    
    
}


@end
