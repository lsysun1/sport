//
//  GroundHeadView.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_GroundEntity;
@interface GroundHeadView : UITableViewHeaderFooterView

@property (nonatomic,strong) SP_GroundEntity *entity;

@end
