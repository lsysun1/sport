//
//  GroundListCell.h
//  Sport
//
//  Created by 李松玉 on 15/5/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SP_GroundEntity;
@interface GroundListCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *markBtn;
@property (weak, nonatomic) IBOutlet UIButton *readActiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *postActiveBtn;
@property (strong, nonatomic) SP_GroundEntity *entity;


+(instancetype) cellWithTableView:(UITableView *)tableView;

@end
