//
//  GroundListCell.m
//  Sport
//
//  Created by 李松玉 on 15/5/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundListCell.h"
#import "SP_GroundEntity.h"
#import "UIImageView+WebCache.h"

@interface GroundListCell()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;

@property (weak, nonatomic) IBOutlet UILabel *midLine;
@property (weak, nonatomic) IBOutlet UILabel *bottomLine;
@property (weak, nonatomic) IBOutlet UILabel *bottomView;



@end




@implementation GroundListCell


+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"GroundListCell";
    GroundListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GroundListCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
    
}


- (void)awakeFromNib {
    // Initialization code
    self.tagLabel.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    self.tagLabel.textColor = [UIColor whiteColor];
    self.tagLabel.layer.cornerRadius = 5;
    self.tagLabel.layer.masksToBounds = YES;
    
    self.address.textColor = UIColorFromRGB(0x696969);
    self.phone.textColor = UIColorFromRGB(0x696969);
    [self.readActiveBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [self.postActiveBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    
    self.midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    self.bottomView.backgroundColor = UIColorFromRGB(0xfafafa);
    
//    [self.markBtn setImage:[UIImage imageNamed:@"icon_mark_sel.png"] forState:UIControlStateSelected];
    [self.markBtn setBackgroundImage:[UIImage imageNamed:@"icon_mark_sel.png"] forState:UIControlStateSelected];
    
    
    
    
}

- (void) setEntity:(SP_GroundEntity *)entity
{
    _entity = entity;
    
    self.nameLabel.text = entity.name;
    self.address.text = entity.location;
    self.phone.text = entity.tel;
    
    NSURL *imgUrl = [NSURL URLWithString:entity.img[0]];
    [self.imgView sd_setImageWithURL:imgUrl];
    
    if([entity.collect isEqualToString:@"no"]){
        self.markBtn.selected = NO;
    }else{
        self.markBtn.selected = YES;
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
