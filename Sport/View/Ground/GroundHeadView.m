//
//  GroundHeadView.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "GroundHeadView.h"
#import "SP_GroundEntity.h"
#import "UIImageView+WebCache.h"

@interface GroundHeadView()<UIScrollViewDelegate>
{
    UIScrollView *_myScrollView;
    UIPageControl *_myPageController;
    UIView *_groundNameView;
    UILabel *_groundNameLabel;
    UIView *_locationView;
    UILabel *_locationLabel;
    UIView *_telView;
    UILabel *_telLabel;
    NSTimer *_timer;
    int _imgCount;
}
@end

@implementation GroundHeadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        
        
        

    
    }
    return self;
}


- (void) setEntity:(SP_GroundEntity *)entity
{
    _entity = entity;
    _imgCount = (int)self.entity.img.count;
    
    _myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
    _myScrollView.delegate = self;
    _myScrollView.bounces = NO;
    _myScrollView.showsHorizontalScrollIndicator = NO;
    _myScrollView.showsVerticalScrollIndicator = NO;
    _myScrollView.pagingEnabled = YES;
    _myScrollView.contentSize = CGSizeMake(self.entity.img.count * _myScrollView.bounds.size.width, 0);
    [self addSubview:_myScrollView];
    
    
    _groundNameView = [[UIView alloc]initWithFrame:CGRectMake(0, 170, ScreenWidth, 30)];
    _groundNameView.backgroundColor = [UIColor blackColor];
    _groundNameView.alpha = 0.7;
    [self addSubview:_groundNameView];
    
    _groundNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 170, 200, 30)];
    _groundNameLabel.font = [UIFont systemFontOfSize:16];
    _groundNameLabel.textColor = [UIColor whiteColor];
    _groundNameLabel.text = self.entity.name;
    [self addSubview:_groundNameLabel];
    
    
    
    _myPageController = [[UIPageControl alloc]init];
    _myPageController.numberOfPages = _imgCount;
    
    // 控件尺寸
    CGSize size = [_myPageController sizeForNumberOfPages:_imgCount];
    
    _myPageController.bounds = CGRectMake(0, 10, size.width, size.height);
    _myPageController.center = CGPointMake(self.center.x /2 * 3 , 200);
    
    
    [_myPageController addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:_myPageController];
    
    // 添加图片
    
    for (int index = 1; index <= _imgCount; index++) {
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:_myScrollView.bounds];
        
        NSURL *imgURL = [NSURL URLWithString:self.entity.img[index -1]];
        [imageView sd_setImageWithURL:imgURL];
        
//        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"img_0%d.jpg", index]];
        
        [_myScrollView addSubview:imageView];
    }
    
    
    // 计算imageView的位置
    [_myScrollView.subviews enumerateObjectsUsingBlock:^(UIImageView *imageView, NSUInteger idx, BOOL *stop) {
        
        // 调整x => origin => frame
        CGRect frame = imageView.frame;
        frame.origin.x = idx * frame.size.width;
        
        imageView.frame = frame;
    }];
    //    NSLog(@"%@", self.scrollView.subviews);
    
    // 分页初始页数为0
    _myPageController.currentPage = 0;
    
    [self startTimer];
    
    
    _locationView = [[UIView alloc]initWithFrame:CGRectMake(0, 200, ScreenWidth, 40)];
    [self addSubview:_locationView];
    
    UIImageView *locationImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10.3, 15, 16.5)];
    locationImgView.image = [UIImage imageNamed:@"位置_灰"];
    [_locationView addSubview:locationImgView];
    
    _locationLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 40, 40)];
    _locationLabel.font = [UIFont systemFontOfSize:14];
    _locationLabel.textColor = [UIColor blackColor];
    _locationLabel.text = self.entity.location;
    [_locationView addSubview:_locationLabel];
    
    UIView *sepViewOne = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
    sepViewOne.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_locationView addSubview:sepViewOne];
    
    
    _telView = [[UIView alloc]initWithFrame:CGRectMake(0, 240, ScreenWidth, 40)];
    [self addSubview:_telView];
    
    UIImageView *telImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10.3, 15, 16.5)];
    telImgView.image = [UIImage imageNamed:@"电话"];
    [_telView addSubview:telImgView];
    
    _telLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 0, ScreenWidth - 40, 40)];
    _telLabel.font = [UIFont systemFontOfSize:14];
    _telLabel.textColor = [UIColor blackColor];
    _telLabel.text = self.entity.tel;
    [_telView addSubview:_telLabel];
    
    UIView *sepViewTwo = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
    sepViewTwo.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_telView addSubview:sepViewTwo];
    
    
    
}



// 分页控件的监听方法
- (void)pageChanged:(UIPageControl *)page
{
    //    NSLog(@"%ld", (long)page.currentPage);
    
    // 根据页数，调整滚动视图中的图片位置 contentOffset
    CGFloat x = page.currentPage * _myScrollView.bounds.size.width;
    [_myScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)startTimer
{
    _timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    // 添加到运行循环
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)updateTimer
{
    // 页号发生变化
    // (当前的页数 + 1) % 总页数
    int page = (_myPageController.currentPage + 1) % _imgCount;
    _myPageController.currentPage = page;
    
    //    NSLog(@"%ld", (long)myPageController.currentPage);
    // 调用监听方法，让滚动视图滚动
    [self pageChanged:_myPageController];
}

#pragma mark - ScrollView的代理方法
// 滚动视图停下来，修改页面控件的小点（页数）
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 停下来的当前页数
    //    NSLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
    
    // 计算页数
    int page = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    _myPageController.currentPage = page;
}

/**
 修改时钟所在的运行循环的模式后，抓不住图片
 
 解决方法：抓住图片时，停止时钟，送售后，开启时钟
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //    NSLog(@"%s", __func__);
    // 停止时钟，停止之后就不能再使用，如果要启用时钟，需要重新实例化
    [_timer invalidate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self startTimer];
}
@end
