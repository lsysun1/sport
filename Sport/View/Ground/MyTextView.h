//
//  MyTextView.h
//  MyWeiBo
//
//  Created by 李松玉 on 9/4/14.
//  Copyright (c) 2014 Sun1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTextView : UITextView
@property (nonatomic, copy) NSString *placehoder;
@end
