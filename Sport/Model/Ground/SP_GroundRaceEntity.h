//
//  SP_GroundRaceEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_GroundRaceEntity : NSObject

@property (nonatomic,copy) NSString *teammatchid;
@property (nonatomic,copy) NSString *ateamid;
@property (nonatomic,copy) NSString *bteamid;
@property (nonatomic,copy) NSString *isthrough;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *matchdate;
@property (nonatomic,copy) NSString *ycdid;
@property (nonatomic,copy) NSString *othernote;
@property (nonatomic,copy) NSString *ateamname;
@property (nonatomic,copy) NSString *ateamimg;
@property (nonatomic,copy) NSString *bteamname;
@property (nonatomic,copy) NSString *bteamimg;



@end

//{
//    "teammatchid": "1",
//    "ateamid": "10",
//    "bteamid": "10",
//    "isthrough": "1",
//    "addtime": "1434597477",
//    "matchdate": "1475891598",
//    "ycdid": "1",
//    "othernote": "",
//    "ateamname": "无敌队",
//    "ateamimg": "http://127.0.0.1/yundong/Public/upload/teamheader/143210643937576.jpg",
//    "bteamname": "无敌队",
//    "bteamimg": "http://127.0.0.1/yundong/Public/upload/teamheader/143210643937576.jpg"
//},