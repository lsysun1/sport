//
//  SP_BannerEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_BannerEntity : NSObject

@property (nonatomic,copy) NSString *bannerid;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *linkid;
@property (nonatomic,copy) NSString *sort;
@property (nonatomic,copy) NSString *addtime;


@end




//"bannerid": "5",
//"title": "广告banner测试1111",
//"img": "http: //121.40.220.245/Public/upload/newsimg/14364309994005.jpg",
//"type": "0",
//"linkid": "5",
//"sort": "2",
//"addtime": "1436430999"
//type  0广告  1新闻
//linkid  type=0时为广告id ，type=1时为新闻id

