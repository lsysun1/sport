//
//  SP_NewsCommentModel.h
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_NewsCommentModel : NSObject

@property (nonatomic,copy) NSString *newscommentid;
@property (nonatomic,copy) NSString *newsid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *ispraised;
@property (nonatomic,copy) NSString *praisecount;

@property (nonatomic,copy) NSString *ycdmsgid;
@property (nonatomic,copy) NSString *ycdid;


@end



//"newscommentid": "42",
//"newsid": "9",
//"uid": "11",
//"content": "买抄手了",
//"addtime": "07-28 21:58",
//"location": "成都",
//"nickname": "囧里个囧",
//"headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//"sex": "1",
//"ispraised": "no",
//"praisecount": "2"


//{
//    "newscommentid": "3",
//    "newsid": "4",
//    "uid": "10",
//    "content": "yyyyyy",
//    "addtime": "1434010113",
//    "nickname": "小仙子",
//    "headimg": "http://121.40.220.245/Public/upload/header/143193822533250.jpg",
//    "sex": "0"
//},



/**
 *  场地留言模型
 */
//"msg": [
//        {
//            "ycdmsgid": "6",
//            "ycdid": "10",
//            "uid": "12",
//            "content": "毛主席万岁。",
//            "addtime": "今天 00:03",
//            "nickname": "夏敏洁",
//            "headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//            "sex": "0"
//        }
//        ]
//}