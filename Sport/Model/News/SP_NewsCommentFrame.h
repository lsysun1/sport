//
//  SP_NewsCommentFrame.h
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>


@class SP_NewsCommentModel;
@interface SP_NewsCommentFrame : NSObject

@property (nonatomic, assign, readonly) CGRect headImgFrame;
@property (nonatomic, assign, readonly) CGRect nameFrame;
@property (nonatomic, assign, readonly) CGRect timeFrame;
@property (nonatomic, assign, readonly) CGRect contentFrame;
@property (nonatomic, assign, readonly) CGRect praiseNumFrame;
@property (nonatomic, assign, readonly) CGRect praiseBtnFrame;
@property (nonatomic, assign, readonly) CGRect bottomLineFrame;


@property (nonatomic, assign, readonly) CGFloat cellHeight;


@property (nonatomic,strong) SP_NewsCommentModel *msgModel;



@end
