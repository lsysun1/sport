//
//  SP_NewsList.h
//  Sport
//
//  Created by 李松玉 on 15/6/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_NewsList : NSObject

@property (nonatomic,copy) NSString *newsid;
@property (nonatomic,copy) NSString *newstitle;
@property (nonatomic,copy) NSString *newsdesc;
@property (nonatomic,copy) NSString *newsimg;
@property (nonatomic,copy) NSString *newstag;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *commentcount;
@property (nonatomic,copy) NSString *shareurl;

@end


//"newsid": "2",
//"newstitle": "房贷首付但是",
//"newsdesc": "士大夫但是反对法",
//"newsimg": "http://121.40.220.245/Public/upload/newsimg/14340005082252.jpg",
//"newstag": "似的",
//"addtime": "0",
//"commentcount": "0"