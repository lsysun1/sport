//
//  SP_YhbCommentEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_YhbCommentEntity : NSObject

@property (nonatomic,copy) NSString *yhbplid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *yhbid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSArray  *hfdata;


@property (nonatomic,copy) NSString *usermsgid;
@property (nonatomic,copy) NSString *touid;


@property (nonatomic,copy) NSString *zsbplid;
@property (nonatomic,copy) NSString *zsbid;
@property (nonatomic,copy) NSString *plcontent;
@property (nonatomic,copy) NSString *playernum;


@end


//"yhbplid": "10",
//"uid": "14",
//"yhbid": "13",
//"content": "不见不散哦[可爱]",
//"addtime": "1433056461",
//"nickname": "匿名小鲜肉",
//"headimg": "http://121.40.220.245/Public/upload/header/143625012956574.jpg",
//"sex": "0",
//"hfdata": [
//           {
//               "hfplid": "11",
//               "yhbplid": "10",
//               "hfuid": "14",
//               "uid": "12",
//               "content": "nihao",
//               "addtime": "1433250099",
//               "nickname": "夏敏洁",
//               "sex": "0",
//               "hfnickname": "匿名小鲜肉",
//               "hfsex": "0"
//           },