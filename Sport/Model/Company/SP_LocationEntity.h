//
//  SP_LocationEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_LocationEntity : NSObject

@property (nonatomic,copy) NSString *location;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *zone;
@property (nonatomic,copy) NSString *lat;
@property (nonatomic,copy) NSString *lng;


@end
