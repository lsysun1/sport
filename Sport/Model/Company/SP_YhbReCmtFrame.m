//
//  SP_YhbReCmtFrame.m
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_YhbReCmtFrame.h"
#import "SP_YhbReCmtEntity.h"

@implementation SP_YhbReCmtFrame




- (void) setReCmtEntity:(SP_YhbReCmtEntity *)reCmtEntity
{
    _reCmtEntity = reCmtEntity;
    
    NSString *allStr = [NSString stringWithFormat:@"%@回复:%@",reCmtEntity.nickname,reCmtEntity.content];
//    NSLog(@"allstr -- %@",allStr);
    
    CGFloat contentWidth = ScreenWidth - 85;
    
    
    
    CGSize labelSize = [self labelAutoCalculateRectWith:allStr FontSize:14 MaxSize:CGSizeMake(contentWidth, MAXFLOAT)];
    
    _commentLabelFrame = CGRectMake(75, 0, contentWidth, labelSize.height + 5);
    
    _cellHeight = labelSize.height + 5;


}


- (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize

{
    
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByCharWrapping;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    
    
    labelSize.height=ceil(labelSize.height);
    
    labelSize.width=ceil(labelSize.width);
    
    return labelSize;
}




@end
