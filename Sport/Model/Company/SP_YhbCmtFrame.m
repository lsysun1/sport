//
//  SP_YhbCmtFrame.m
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_YhbCmtFrame.h"
#import "SP_YhbCommentEntity.h"

@implementation SP_YhbCmtFrame


- (id)init
{
    self = [super init];
    if(self){
        self.reCmtArr = [NSMutableArray array];
    }
    return self;
}


- (void) setCmtEntity:(SP_YhbCommentEntity *)cmtEntity
{
    _cmtEntity = cmtEntity;
    
    //头像
    _headImgFrame = CGRectMake(15, 15, 50, 50);
    
    //名字
    _nameFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, 20, 200, 14);
    
    //时间
    _timeFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_nameFrame) + 8, 200, 13);
    
    //正文
    CGFloat contentWidth = ScreenWidth - 100;
    if(cmtEntity.content.length != 0){
        CGSize labelSize = [cmtEntity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                        constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                            lineBreakMode:NSLineBreakByWordWrapping];
        
        _contentFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_timeFrame) + 20 ,labelSize.width, labelSize.height);
    }else{
        CGSize labelSize = [cmtEntity.plcontent sizeWithFont:[UIFont systemFontOfSize:14]
                                         constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                             lineBreakMode:NSLineBreakByWordWrapping];
        
        _contentFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_timeFrame) + 20 ,labelSize.width, labelSize.height);

    
    }
    //回复按钮
    _hfBtnFrame = CGRectMake(ScreenWidth - 50, 20, 35, 17);

    _cellHeight = CGRectGetMaxY(_contentFrame) + 5;

}


@end
