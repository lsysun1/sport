//
//  SP_YhbCmtFrame.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SP_YhbCommentEntity;
@interface SP_YhbCmtFrame : NSObject

@property (nonatomic, assign, readonly) CGRect headImgFrame;
@property (nonatomic, assign, readonly) CGRect nameFrame;
@property (nonatomic, assign, readonly) CGRect timeFrame;
@property (nonatomic, assign, readonly) CGRect contentFrame;
@property (nonatomic, assign, readonly) CGRect hfBtnFrame;


@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, strong) NSIndexPath *index;

@property (nonatomic,strong) SP_YhbCommentEntity *cmtEntity;


@property (nonatomic,strong) NSMutableArray *reCmtArr;

@end


//UIImageView *_headImgView;
//UILabel *_nameLabel;
//UILabel *_timeLabel;
//UILabel *_contentLabel;
//UIButton *_hfBtn;