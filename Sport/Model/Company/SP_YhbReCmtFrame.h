//
//  SP_YhbReCmtFrame.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SP_YhbReCmtEntity;
@interface SP_YhbReCmtFrame : NSObject

@property (nonatomic, strong) SP_YhbReCmtEntity *reCmtEntity;
@property (nonatomic, assign, readonly) CGRect commentLabelFrame;
@property (nonatomic, assign, readonly) CGFloat cellHeight;


@end
