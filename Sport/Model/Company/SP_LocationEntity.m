//
//  SP_LocationEntity.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_LocationEntity.h"

@implementation SP_LocationEntity



- (NSString *)description
{
    return [NSString stringWithFormat:@"location -- %@\n city - %@\n zone - %@\n lat - %@\n lng - %@\n",_location,_city,_zone,_lat,_lng];
}


@end
