//
//  SP_YhbInfoEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_YhbInfoEntity : NSObject

@property (nonatomic,copy) NSString *yhbid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *location;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *lat;
@property (nonatomic,copy) NSString *lng;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *issignaloff;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *isrespons;
@property (nonatomic,copy) NSString *issignal;
@property (nonatomic,copy) NSString *collect;
@property (nonatomic,copy) NSString *responscount;
@property (nonatomic,copy) NSArray  *responsdata;
@property (nonatomic,copy) NSArray  *signaldata;



@end


//{
//    "code": 200,
//    "msg": {
//        "yhbid": "20",
//        "uid": "12",
//        "location": "zoneName",
//        "type": "1",
//        "sex": "0",
//        "date": "2015-07-24",
//        "lat": "",
//        "lng": "",
//        "desc": "朋友你好，这里是北京，",
//        "addtime": "1437737649",
//        "issignaloff": "0",
//        "ycdid": "0",
//        "nickname": "夏敏洁",
//        "headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//        "isrespons": "no",
//        "issignal": "no",
//        "collect": "yes",
//        "responscount": 1,
//        "responsdata": [
//                        {
//                            "uid": "13",
//                            "issignal": "0",
//                            "nickname": "haha",
//                            "headimg": "http://121.40.220.245/Public/upload/header/143306389068358.jpg",
//                            "sex": "1"
//                        }
//                        ],
//        "signaldata": []
//    }
//}