//
//  SP_YhbReCmtEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_YhbReCmtEntity : NSObject

@property (nonatomic,copy) NSString *hfplid;
@property (nonatomic,copy) NSString *yhbplid;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *hfnickname;
@property (nonatomic,copy) NSString *hfsex;


@property (nonatomic,copy) NSString *usermsghfid;

@property (nonatomic,copy) NSString *usermsgid;



@property (nonatomic,copy) NSString *zsbplhf;
@property (nonatomic,copy) NSString *zsbplid;



@end


