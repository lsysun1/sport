//
//  SP_GroundEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_GroundEntity : NSObject


@property (nonatomic,copy) NSString *ycdid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSArray *img;
@property (nonatomic,copy) NSString *location;
@property (nonatomic,copy) NSString *tel;
@property (nonatomic,copy) NSString *shi;
@property (nonatomic,copy) NSString *xian;
@property (nonatomic,copy) NSString *lat;
@property (nonatomic,copy) NSString *lng;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *collect;
@property (nonatomic,copy) NSString *yhbcount;
@property (nonatomic,copy) NSString *msgcount;


@end


//"ycdid": "9",
//"name": "北京工人体育馆",
//"img": [
//        "http://121.40.220.245/Public/upload/ycdimg/14371166415653/img1.jpg"
//        ],
//"location": "北京市朝阳区三里屯酒吧一条街",
//"tel": "18602828361",
//"shi": "北京市",
//"xian": "朝阳区",
//"lat": "39.940536",
//"lng": "39.940536",
//"type": "1",
//"addtime": "1437116641",
//"isdelete": "0",
//"collect": "no",
//"yhbcount": "0",
//"msgcount": "1",
//"latlng": {
//    "lat": "39.940536",
//    "lng": "39.940536"
//}
//}