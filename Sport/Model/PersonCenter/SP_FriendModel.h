//
//  SP_FriendModel.h
//  Sport
//
//  Created by 李松玉 on 15/5/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_FriendModel : NSObject

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *borndate;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *isattention;
@property (nonatomic,copy) NSString *tel;
@property (nonatomic,copy) NSString *touid;
@property (nonatomic,copy) NSString *pinYin;
@property (nonatomic,copy) NSString *num;
@property (nonatomic,copy) NSString *role;

@property (nonatomic,copy) NSString *playernum;
@property (nonatomic,copy) NSString *isthrough;


//"uid": "15",
//"nickname": "小骚羊",
//"headimg": "http://121.40.220.245/Public/upload/header/143243789229676.jpg",
//"borndate": "",
//"sex": "1",
//"isattention": "yes"



//@property (nonatomic,copy) NSString *uid;
//@property (nonatomic,copy) NSString *nickname;
//@property (nonatomic,copy) NSString *headimg;
//@property (nonatomic,copy) NSString *role;
//@property (nonatomic,copy) NSString *tel;
//@property (nonatomic,copy) NSString *playernum;
//@property (nonatomic,copy) NSString *isthrough;



@end
