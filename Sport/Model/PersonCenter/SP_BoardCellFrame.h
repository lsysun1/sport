//
//  SP_BoardCellFrame.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SP_BoardListEntity;
@interface SP_BoardCellFrame : NSObject

@property (nonatomic,strong) SP_BoardListEntity *boardEntity;

@property (nonatomic,assign,readonly) CGRect headImgFrame;
@property (nonatomic,assign,readonly) CGRect titleFrame;
@property (nonatomic,assign,readonly) CGRect nameFrame;
@property (nonatomic,assign,readonly) CGRect timeFrame;
@property (nonatomic,assign,readonly) CGRect msgIconFrame;
@property (nonatomic,assign,readonly) CGRect msgNumFrame;
@property (nonatomic,assign,readonly) CGRect midLineFrame;
@property (nonatomic,assign,readonly) CGRect bottomFrame;
@property (nonatomic,assign,readonly) CGRect contentFrame;


@property (nonatomic, assign, readonly) CGFloat cellHeight;









@end
