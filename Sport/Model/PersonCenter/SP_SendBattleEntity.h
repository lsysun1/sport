//
//  SP_SendBattleEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_SendBattleEntity : NSObject

@property (nonatomic,copy) NSString *teamid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *teammatchid;
@property (nonatomic,copy) NSString *matchdate;
@property (nonatomic,copy) NSString *ycdid;
@property (nonatomic,copy) NSString *othernote;
@property (nonatomic,copy) NSString *isthrough;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSArray  *playerdata;
@property (nonatomic,copy) NSString *cdname;


@end



//"msg": [
//        {
//            "teamid": "11",
//            "name": "屌丝队",
//            "img": "http://121.40.220.245/Public/upload/teamheader/143243686979895.jpg",
//            "addtime": "1437274146",
//            "teammatchid": "8",
//            "matchdate": "1437445800",
//            "ycdid": "6",
//            "othernote": "默默哦OK极趣",
//            "isthrough": "0",           等待 同意  拒绝    0  1 2
//            "desc": "一群屌丝组建的球队！！",
//            "playerdata": [
//                           {
//                               "sex": "1",
//                               "uid": "10",
//                               "nickname": "你大爷",
//                               "headimg": "http://121.40.220.245/Public/upload/header/143541568286634.jpg",
//                               "role": "2",
//                               "tel": "18302820052"
//                           }
//                           ],
//            "cdname": "省体育馆"
//        }
//        ]
//}