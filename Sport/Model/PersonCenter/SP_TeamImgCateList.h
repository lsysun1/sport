//
//  SP_TeamImgCateList.h
//  
//
//  Created by 李松玉 on 15/6/10.
//
//

#import <Foundation/Foundation.h>

@interface SP_TeamImgCateList : NSObject


@property (nonatomic,copy) NSString *teamimgcateid;
@property (nonatomic,copy) NSString *teamid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *cover;




@end


//{
//    "code": 200,
//    "msg": [
//            {
//                "teamimgcateid": "2",
//                "teamid": "10",
//                "name": "赛诗会",
//                "date": "2015-05-21",
//                "addtime": "0",
//                "cover": "http://121.40.220.245/Public/upload/teamimg/14328925733814/1432892607779/WP_20150508_16_27_06_Pro.jpg"
//            }
//            ]
//}