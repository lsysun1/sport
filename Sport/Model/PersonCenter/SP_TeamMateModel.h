//
//  SP_TeamMateModel.h
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_TeamMateModel : NSObject

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *role;
@property (nonatomic,copy) NSString *tel;
@property (nonatomic,copy) NSString *playernum;
@property (nonatomic,copy) NSString *isthrough;


@end


//"uid": "12",
//"nickname": "夏敏洁",
//"headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//"role": "2",
//"playernum": "10",
//"isthrough": "1",
//"tel": "15828101225"