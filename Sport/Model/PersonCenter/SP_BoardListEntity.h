//
//  SP_BoardListEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_BoardListEntity : NSObject

@property (nonatomic,copy) NSString *zsbid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *teamid;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *playernum;
@property (nonatomic,copy) NSString *commentnum;
@property (nonatomic,copy) NSString *role;


@end


//
//{
//    "code": 200,
//    "msg": [
//            {
//                "zsbid": "4",
//                "uid": "12",
//                "content": "我们要向湖人队发起挑战！！大家说说！！",
//                "teamid": "10",
//                "addtime": "1435462228",
//                "nickname": "夏敏洁",
//                "headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//                "sex": "0",
//                "playernum": "10",
//                "commentnum": "2"
//            }
//            ]
//}