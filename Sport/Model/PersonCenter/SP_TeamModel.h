//
//  SP_TeamModel.h
//  Sport
//
//  Created by 李松玉 on 15/6/4.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_TeamModel : NSObject

@property (nonatomic,copy) NSString *teamid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *company;
@property (nonatomic,copy) NSString *school;

@property (nonatomic,copy) NSString *isattention;
@property (nonatomic,copy) NSArray  *playerData;




@end


//{
//    "teamid": "10",
//    "name": "无敌队",
//    "img": "http://121.40.220.245/Public/upload/teamheader/143210643937576.jpg",
//    "desc": "青春无敌！！！",
//    "isattention": "0",
//    "playerData": [
//                   {
//                       "uid": "12",
//                       "nickname": "夏敏洁",
//                       "headimg": "http://121.40.220.245/Public/upload/header/143374880189120.jpg",
//                       "role": "2",
//                       "tel": "15828101225"
//                   }
//                   ]
//},


//"teamid": "10",
//"name": "无敌队",
//"img": "http://121.40.220.245/Public/upload/teamheader/143210643937576.jpg",
//"desc": "青春无敌！！！",
//"type": "1",
//"addtime": "1432106439",
//"company": "",
//"school": "四川大学"