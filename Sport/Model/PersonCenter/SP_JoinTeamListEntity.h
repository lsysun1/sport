//
//  SP_JoinTeamListEntity.h
//  Sport
//
//  Created by 李松玉 on 15/7/29.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_JoinTeamListEntity : NSObject


@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *teamname;
@property (nonatomic,copy) NSString *teamid;
@property (nonatomic,copy) NSString *isthrough;





@end


//"nickname": "匿名小鲜肉",
//"uid": "14",
//"sex": "0",
//"teamname": "马刺队",
//"teamid": "15",
//"isthrough": "1"