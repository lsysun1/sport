//
//  SP_BoardCellFrame.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BoardCellFrame.h"
#import "SP_BoardListEntity.h"


@implementation SP_BoardCellFrame


- (void) setBoardEntity:(SP_BoardListEntity *)boardEntity
{
    if(boardEntity != nil){
        
        _boardEntity = boardEntity;
        
        //头像
        _headImgFrame = CGRectMake(15, 15, 50, 50);

        //队长标志
        _titleFrame = CGRectMake(15 + 37, 17, 30, 15);
        
        //名字
        _nameFrame = CGRectMake(CGRectGetMaxX(_headImgFrame) + 10,34, 100, 13);
        
        //评论数
        _msgNumFrame = CGRectMake(ScreenWidth - 50, 65, 40, 13);
        
        //评论图标
        _msgIconFrame = CGRectMake(CGRectGetMinX(_msgNumFrame) - 18, 65, 14, 14);
        
        //时间
        _timeFrame = CGRectMake(CGRectGetMinX(_msgIconFrame) - 208, 65, 200, 13);
        
        //中间的下划线
        _midLineFrame = CGRectMake(10, CGRectGetMaxY(_timeFrame) + 5, ScreenWidth - 20, 1);
        
        //内容
        CGFloat contentWidth = ScreenWidth - 30;
        CGSize labelSize = [boardEntity.content sizeWithFont:[UIFont systemFontOfSize:14]
                                        constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                            lineBreakMode:NSLineBreakByWordWrapping];

        _contentFrame = CGRectMake(15, CGRectGetMaxY(_midLineFrame) + 5, labelSize.width, labelSize.height);
        
        //底部的空白View
        _bottomFrame = CGRectMake(0, CGRectGetMaxY(_contentFrame) + 5, ScreenWidth , 8);
        
        
        _cellHeight = CGRectGetMaxY(_bottomFrame);
        
    }
}


@end
