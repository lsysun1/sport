//
//  SP_UserInfo.h
//  Sport
//
//  Created by 李松玉 on 15/5/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_UserInfo : NSObject

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *idol;
@property (nonatomic,copy) NSString *signature;
@property (nonatomic,copy) NSString *school;
@property (nonatomic,copy) NSString *company;
@property (nonatomic,copy) NSString *postion;
@property (nonatomic,copy) NSString *borndate;
@property (nonatomic,copy) NSString *tel;
@property (nonatomic,copy) NSString *fansCount;
@property (nonatomic,copy) NSString *isattention;
@property (nonatomic,copy) NSString *jifen;
@property (nonatomic,copy) NSString *attentionCount;



@end


