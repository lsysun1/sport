//
//  SP_ZanCellFrame.h
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_ZanCellFrame : NSObject

@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, copy) NSArray *zanArr;

@end
