//
//  SP_ZanCellFrame.m
//  Sport
//
//  Created by 李松玉 on 15/8/1.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZanCellFrame.h"

@implementation SP_ZanCellFrame


- (void) setZanArr:(NSArray *)zanArr
{
    NSString *allStr = [[NSString alloc]init];
    
//    for(NSDictionary *dict in zanArr){
////        all = [allStr stringByAppendingString:@"%@,",dict[@"nickname"]];
//    }
//    
    

//    NSLog(@"allstr -- %@",allStr);
    
    CGFloat contentWidth = ScreenWidth - 85;
    
    
    
    CGSize labelSize = [self labelAutoCalculateRectWith:allStr FontSize:14 MaxSize:CGSizeMake(contentWidth, MAXFLOAT)];

}


- (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize

{
    
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByCharWrapping;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    
    
    labelSize.height=ceil(labelSize.height);
    
    labelSize.width=ceil(labelSize.width);
    
    return labelSize;
}


@end
