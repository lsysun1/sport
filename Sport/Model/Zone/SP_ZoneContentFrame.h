//
//  SP_ZoneContentFrame.h
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SP_ZoneContent;
@interface SP_ZoneContentFrame : NSObject

@property (nonatomic, assign, readonly) CGRect headImgFrame;
@property (nonatomic, assign, readonly) CGRect nameFrame;
@property (nonatomic, assign, readonly) CGRect timeFrame;
@property (nonatomic, assign, readonly) CGRect contentFrame;
@property (nonatomic, assign, readonly) CGRect imgFrame;
@property (nonatomic, assign, readonly) CGRect shareBtnFrame;
@property (nonatomic, assign, readonly) CGRect shareNumFrame;
@property (nonatomic, assign, readonly) CGRect zanBtnFrame;
@property (nonatomic, assign, readonly) CGRect zanNumFrame;
@property (nonatomic, assign, readonly) CGRect commentBtnFrame;
@property (nonatomic, assign, readonly) CGRect commentNumFrame;
@property (nonatomic, assign, readonly) CGRect bottomLineFrame;
@property (nonatomic, assign, readonly) CGRect commentTableViewFrame;
@property (nonatomic, assign, readonly) CGRect bottomViewFrame;
@property (nonatomic, assign, readonly) CGRect zanLabelFrame;

@property (nonatomic, copy) NSMutableAttributedString *zanAttrSrt;

@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, assign, readonly) CGFloat zanCellHeight;

@property (nonatomic, assign) int  indexRow;
@property (nonatomic, assign) int  hasRowNum;

@property (nonatomic, assign,readonly) BOOL allHaveZanAndCmt;
@property (nonatomic, assign,readonly) BOOL hasZanList;
@property (nonatomic, assign,readonly) BOOL hasCommentList;
@property (nonatomic, assign,readonly) BOOL noZanAndCmt;

@property (nonatomic,strong) SP_ZoneContent *zoneContent;
@property (nonatomic,strong) NSMutableArray *commentArr;

@end


