//
//  SP_CommentTextSpecial.h
//  Sport
//
//  Created by 李松玉 on 15/7/22.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_CommentTextSpecial : NSObject

/** 这段特殊文字的内容 */
@property (nonatomic, copy) NSString *text;
/** 这段特殊文字的范围 */
@property (nonatomic, assign) NSRange range;

@property (nonatomic,copy) NSString *pyqid;
@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *hfName;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *pyqplid;
@property (nonatomic,strong)NSIndexPath *indexNum;


@property (nonatomic,copy) NSString *yhbplid;


@property (nonatomic,copy) NSString *usermsgid;

@property (nonatomic,copy) NSString *zsbplid;


@end
