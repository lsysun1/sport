//
//  SP_ZoneCommentFrame.h
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SP_ZoneCommentEntiry;
@interface SP_ZoneCommentFrame : NSObject

@property (nonatomic, strong) SP_ZoneCommentEntiry *commentEntiry;
@property (nonatomic, assign, readonly) CGRect commentLabelFrame;
@property (nonatomic, assign, readonly) CGFloat cellHeight;
@property (nonatomic, strong) NSIndexPath *index;


@end
