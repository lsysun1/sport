//
//  SP_ZoneCommentFrame.m
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZoneCommentFrame.h"
#import "SP_ZoneCommentEntiry.h"

@implementation SP_ZoneCommentFrame


- (void) setCommentDict:(NSDictionary *)commentDict
{

}

- (void) setCommentEntiry:(SP_ZoneCommentEntiry *)commentEntiry
{
    _commentEntiry = commentEntiry;
    
    NSString *allStr = [NSString stringWithFormat:@"%@回复:%@",_commentEntiry.nickname,_commentEntiry.content];
//    NSLog(@"allstr -- %@",allStr);
    
    CGFloat contentWidth = ScreenWidth - 85;
    
    
    
    CGSize labelSize = [self labelAutoCalculateRectWith:allStr FontSize:14 MaxSize:CGSizeMake(contentWidth, MAXFLOAT)];
    
    _commentLabelFrame = CGRectMake(75, 0, contentWidth, labelSize.height + 5);
    
    
    
//    NSLog(@"_cellHeight --- %f",_cellHeight);
    _cellHeight = labelSize.height + 5;


}

- (CGSize)labelAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize

{
    
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineBreakMode=NSLineBreakByCharWrapping;
    
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
    
    
    labelSize.height=ceil(labelSize.height);
    
    labelSize.width=ceil(labelSize.width);
    
    return labelSize;
}


@end
