//
//  SP_ZoneContentFrame.m
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZoneContentFrame.h"
#import "SP_ZoneContent.h"

@implementation SP_ZoneContentFrame


- (void) setZoneContent:(SP_ZoneContent *)zoneContent
{
    
    _zoneContent = zoneContent;
    
    //头像
    _headImgFrame = CGRectMake(15, 15, 50, 50);
    
    //名字
    _nameFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, 20, 200, 14);
    
    //时间
    _timeFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_nameFrame) + 8, 200, 13);
    
    //正文
    CGFloat contentWidth = ScreenWidth - 100;
    CGSize labelSize = [zoneContent.content sizeWithFont:[UIFont systemFontOfSize:14]
                                    constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                        lineBreakMode:NSLineBreakByWordWrapping];
    
    _contentFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_timeFrame) + 20 ,labelSize.width, labelSize.height);

    
    //图片
    if(zoneContent.img.count > 0 ){
        long picsLines = (zoneContent.img.count-1) / 3;
        CGFloat picsX = 0;
        CGFloat picsY = CGRectGetMaxY(_contentFrame) + 8;
        CGFloat picsW = ScreenWidth;
        CGFloat picsH = 70 * (1 +picsLines);
        _imgFrame = CGRectMake(picsX, picsY, picsW, picsH);
    }
    
    if(CGRectGetMaxY(_imgFrame)){
        _commentNumFrame = CGRectMake(ScreenWidth - 40, CGRectGetMaxY(_imgFrame) + 10, 30, 14);
    }else{
        _commentNumFrame = CGRectMake(ScreenWidth - 40, CGRectGetMaxY(_contentFrame) + 10, 30, 14);
    }
    
    _commentBtnFrame = CGRectMake(CGRectGetMinX(_commentNumFrame) - 60, CGRectGetMinY(_commentNumFrame), 60, 14);
    _zanNumFrame = CGRectMake(CGRectGetMinX(_commentBtnFrame) - 40, CGRectGetMinY(_commentNumFrame), 30, 14);
    _zanBtnFrame = CGRectMake(CGRectGetMinX(_zanNumFrame)-60, CGRectGetMinY(_commentNumFrame), 60, 14);
    _shareNumFrame = CGRectMake(CGRectGetMinX(_zanBtnFrame) - 40, CGRectGetMinY(_commentNumFrame), 30, 13);
    _shareBtnFrame = CGRectMake(CGRectGetMinX(_shareNumFrame) - 60, CGRectGetMinY(_commentNumFrame), 60, 14);
    
    
    if(zoneContent.zhanlist.count > 0){
        
        NSString *zanStr = [[NSString alloc]init];
        for(NSDictionary *dict in zoneContent.zhanlist){
            zanStr = [zanStr stringByAppendingString:[NSString stringWithFormat:@"%@,",dict[@"nickname"]]];
        }
        
        zanStr = [zanStr substringWithRange:NSMakeRange(0, zanStr.length - 1)];
        
        //        NSLog(@"赞 Str -- %@",zanStr);
        CGFloat zanWidth = ScreenWidth - 85;

        CGSize zanCellSize = [zanStr sizeWithFont:[UIFont systemFontOfSize:14]
                                constrainedToSize:CGSizeMake(zanWidth, MAXFLOAT)
                                    lineBreakMode:NSLineBreakByWordWrapping];
        _zanLabelFrame = CGRectMake(75, CGRectGetMaxY(_commentNumFrame) + 10, zanWidth, zanCellSize.height);
        
        
        
        UIFont *font = [UIFont systemFontOfSize:12];

        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"  %@",zanStr]];

        NSTextAttachment *imgAttch = [[NSTextAttachment alloc]init];
        imgAttch.image = [UIImage imageNamed:@"赞"];
        imgAttch.bounds = CGRectMake(4, -3, font.lineHeight, font.lineHeight);

        NSAttributedString *imgStr = [NSAttributedString attributedStringWithAttachment:imgAttch];
        
        [str insertAttributedString:imgStr atIndex:0];
        
        self.zanAttrSrt = str;
        
    
        //cell高度
        _cellHeight = CGRectGetMaxY(_zanLabelFrame);

    }else{
        //cell高度
        _cellHeight = CGRectGetMaxY(_commentNumFrame) + 10;

    }
    
    
    
    
    


    
    
    _allHaveZanAndCmt = NO;
    _hasCommentList = NO;
    _hasZanList = NO;
    _noZanAndCmt = NO;
    _hasRowNum = 1;
    
    int plCount = [zoneContent.plcount intValue];
    int zanCount = [zoneContent.zhancount intValue];
    
    
    if(zanCount > 0 && plCount > 0 ){
        _hasZanList = YES;
        _hasCommentList = YES;
        _hasRowNum = 3;
    }else if (zanCount > 0 && plCount <= 0){
        _hasZanList = YES;
        _hasCommentList = NO;
        _hasRowNum = 2;
    }else if (zanCount <=0 && plCount > 0){
        _hasCommentList = YES;
        _hasZanList = NO;
        _hasRowNum = 2;
    }else if (zanCount <=0 && plCount <= 0){
        _hasZanList = NO;
        _hasCommentList = NO;
        _hasRowNum = 1;
    }


//    NSLog(@"_hasRowNum --- %d",_hasRowNum);
//    NSLog(@"_hasCommentList -- %d",_hasCommentList);
//    NSLog(@"_hasZanList -- %d",_hasZanList);
//    NSLog(@"\n");
    
    

}

//"plcount": "1",
//"zhancount": "0",




@end
