//
//  SP_ZoneCommentEntiry.h
//  Sport
//
//  Created by 李松玉 on 15/7/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_ZoneCommentEntiry : NSObject

@property (nonatomic,copy) NSString *pyqplid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *pyqid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,copy) NSArray *hfdata;

//回复数据里面特有的模型
@property (nonatomic,copy) NSString *pyqplhfid;
@property (nonatomic,copy) NSString *hfnickname;
@property (nonatomic,copy) NSString *hfsex;
@property (nonatomic,copy) NSString *hfuid;



@end



//"pyqplid": "1",
//"uid": "10",
//"pyqid": "6",
//"content": "你是个大傻逼！",
//"addtime": "06-15 17:04",
//"nickname": "小仙子",
//"headimg": "http://121.40.220.245/Public/upload/header/143541568286634.jpg",
//"sex": "1",
//"hfdata": [
//           {
//               "pyqplhfid": "3",
//               "uid": "15",
//               "hfuid": "15",
//               "content": "asdfasdfsaf",
//               "pyqplid": "1",
//               "nickname": "小骚羊",
//               "sex": "1",
//               "hfnickname": "小骚羊",
//               "hfsex": "1"
//           },