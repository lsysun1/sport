//
//  SP_ZoneContent.h
//  Sport
//
//  Created by 李松玉 on 15/6/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_ZoneContent : NSObject

@property (nonatomic,copy) NSString *pyqid;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *sharecount;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *sex;
@property (nonatomic,strong) NSArray *img;
@property (nonatomic,copy) NSString *plcount;
@property (nonatomic,copy) NSString *zhancount;
@property (nonatomic,strong) NSArray *zhanlist;
@property (nonatomic,copy) NSString *isZhan;
@property (nonatomic,copy) NSString *pyqplhfid;
@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *hfsex;
@property (nonatomic,strong) NSArray *hfdata;
@property (nonatomic,strong) NSMutableArray *commentArr;

//"pyqplhfid": "14",
//"uid": "11",
//"hfuid": "10",
//"content": "来快乐旅途",
//"pyqplid": "12",
//"nickname": "囧你个囧",
//"sex": "1",
//"hfnickname": "小仙子",
//"hfsex": "1"






@end





//{
//    "code": 200,
//    "msg": [
//            {
//                "pyqid": "6",
//                "uid": "11",
//                "content": "今天是个好日子，心想的事儿都能成~~",
//                "sharecount": "0",
//                "addtime": "06-14 21:27",
//                "nickname": "囧你个囧",
//                "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//                "sex": "1",
//                "img": [
//                        {
//                            "img": "http://121.40.220.245/Public/upload/pyqimg/14342884617190/123445/14207802872832.jpg"
//                        },
//                        {
//                            "img": "http://121.40.220.245/Public/upload/pyqimg/14342884617190/123445/14207803309102.jpg"
//                        },
//                        {
//                            "img": "http://121.40.220.245/Public/upload/pyqimg/14342884617190/123445/14207803749710.jpg"
//                        }
//                        ],
//                "plcount": "1",
//                "zhancount": "1",
//                "zhanlist": [
//                             {
//                                 "uid": "10",
//                                 "nickname": "小仙子",
//                                 "sex": "0"
//                             }
//                             ],
//                "isZhan": "0"
//            }
//            ]
//}