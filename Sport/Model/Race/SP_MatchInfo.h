//
//  SP_MatchInfo.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_MatchInfo : NSObject

@property (nonatomic,strong) NSDictionary *topData;
@property (nonatomic,strong) NSDictionary *mvpData;
@property (nonatomic,strong) NSDictionary *bottomData;



@end

//{
//    "code": 200,
//    "msg": {
//        "topData": {
//            "matchid": "1",
//            "date": "1422226541",
//            "ateamid": "10",
//            "bteamid": "11",
//            "ateamscore": "-",
//            "bteamscore": "-",
//            "ateamsupport": "3",
//            "bteamsupport": "2",
//            "ateamname": "无敌队",
//            "ateamimg": "http://121.40.220.245/Public/upload/teamheader/143210643937576.jpg",
//            "bteamname": null,
//            "bteamimg": ""
//        },
//        "mvpData": {
//            "uid": "11",
//            "nickname": "囧你个囧",
//            "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//            "postion": "后卫"
//        },
//        "bottomData": {
//            "ateam": [
//                      {
//                          "uid": "11",
//                          "goal": "3",
//                          "assists": "0",
//                          "nickname": "囧你个囧",
//                          "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//                          "postion": "后卫"
//                      },
//                      {
//                          "uid": "12",
//                          "goal": "0",
//                          "assists": "5",
//                          "nickname": "夏敏洁",
//                          "headimg": "http://121.40.220.245/Public/upload/header/143193147716697.jpg",
//                          "postion": "前锋"
//                      }
//                      ],
//            "bteam": [
//                      {
//                          "uid": "10",
//                          "goal": "5",
//                          "assists": "1",
//                          "nickname": "小仙子",
//                          "headimg": "http://121.40.220.245/Public/upload/header/143193822533250.jpg",
//                          "postion": "守门员"
//                      }
//                      ]
//        }
//    }
//}