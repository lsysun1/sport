//
//  SP_MatchInfo_TopData.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_MatchInfo_TopData : NSObject

@property (nonatomic,copy) NSString *matchid;
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *ateamid;
@property (nonatomic,copy) NSString *bteamid;
@property (nonatomic,copy) NSString *ateamscore;
@property (nonatomic,copy) NSString *bteamscore;
@property (nonatomic,copy) NSString *ateamsupport;
@property (nonatomic,copy) NSString *bteamsupport;
@property (nonatomic,copy) NSString *ateamname;
@property (nonatomic,copy) NSString *ateamimg;
@property (nonatomic,copy) NSString *bteamname;
@property (nonatomic,copy) NSString *bteamimg;

@end


//"matchid": "1",
//"date": "1422226541",
//"ateamid": "10",
//"bteamid": "11",
//"ateamscore": "-",
//"bteamscore": "-",
//"ateamsupport": "3",
//"bteamsupport": "2",
//"ateamname": "无敌队",
//"ateamimg": "http://121.40.220.245/Public/upload/teamheader/143210643937576.jpg",
//"bteamname": null,
//"bteamimg": ""