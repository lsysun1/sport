//
//  SP_MatchInfo_MvpData.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_MatchInfo_MvpData : NSObject

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *postion;



@end



//"uid": "11",
//"nickname": "囧你个囧",
//"headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//"postion": "后卫"