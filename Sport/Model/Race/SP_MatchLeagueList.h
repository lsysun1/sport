//
//  SP_MatchLeagueList.h
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_MatchLeagueList : NSObject

@property (nonatomic,copy) NSString *matchleagueid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,copy) NSString *img;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *location;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *collect;


@end


//{
//    "code": 200,
//    "msg": [
//            {
//                "matchleagueid": "1",
//                "name": "aaaa",
//                "desc": "aaaa",
//                "img": "aaaa.jpg",
//                "type": "0",
//                "location": "成都",
//                "addtime": "11111111",
//                "collect": "no"
//            }
//            ]
//}