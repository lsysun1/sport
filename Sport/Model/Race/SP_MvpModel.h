//
//  SP_MvpModel.h
//  Sport
//
//  Created by 李松玉 on 15/5/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_MvpModel : NSObject
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *number;
@property (nonatomic,copy) NSString *postion;
@property (nonatomic,copy) NSString *count;
@property (nonatomic,copy) NSString *isSupport;


@end


//"uid": "11",
//"nickname": "囧你个囧",
//"headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//"number": "7",
//"postion": "后卫",
//"count": "1",
//"isSupport": "no"