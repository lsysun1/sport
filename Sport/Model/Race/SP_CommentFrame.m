//
//  SP_CommentFrame.m
//  Sport
//
//  Created by 李松玉 on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CommentFrame.h"
#import "SP_CommentModel.h"

@implementation SP_CommentFrame


- (void) setMsgModel:(SP_CommentModel *)msgModel
{
    _msgModel = msgModel;
    
    //头像
    _headImgFrame = CGRectMake(15, 15, 50, 50);
    
    //名字
    _nameFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, 20, 200, 14);
    
    //时间
    _timeFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_nameFrame) + 8, 200, 13);
    
    //正文
    CGFloat contentWidth = ScreenWidth - 100;
    CGSize labelSize = [msgModel.msg sizeWithFont:[UIFont systemFontOfSize:14]
                                               constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                                    lineBreakMode:NSLineBreakByWordWrapping];
    
    _contentFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_timeFrame) + 20 ,labelSize.width, labelSize.height);
    
    //赞数
    _praiseNumFrame = CGRectMake(ScreenWidth - 40, CGRectGetMaxY(_contentFrame) + 10, 30, 14);
    
    //赞按钮
    _praiseBtnFrame = CGRectMake(CGRectGetMinX(_praiseNumFrame) - 18.0,  CGRectGetMaxY(_contentFrame) + 10, 14, 14);
    
    
    
    //底线
    _bottomLineFrame = CGRectMake(CGRectGetMaxX(_headImgFrame)+10, CGRectGetMaxY(_praiseNumFrame) +10, ScreenWidth - CGRectGetMaxX(_headImgFrame)+10, 1);
    
    //cell高度
    _cellHeight = CGRectGetMaxY(_bottomLineFrame) ;

}





@end
