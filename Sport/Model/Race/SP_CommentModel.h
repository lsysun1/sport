//
//  SP_CommentModel.h
//  Sport
//
//  Created by 李松玉 on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SP_CommentModel : NSObject

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *headimg;
@property (nonatomic,copy) NSString *msg;
@property (nonatomic,copy) NSString *addtime;
@property (nonatomic,copy) NSString *matchmsgid;
@property (nonatomic,copy) NSString *praiseCount;
@property (nonatomic,copy) NSString *isPraise;
@property (nonatomic,copy) NSString *teammsgid;
@end


//"uid": "10",
//"nickname": "小仙子",
//"headimg": "http://121.40.220.245/Public/upload/header/143193822533250.jpg",
//"msg": "what fuck a day",
//"addtime": "05-21 10:32",
//"id": "4",
//"praiseCount": "0",
//"isPraise": "no"


//"uid": "11",
//"nickname": "囧你个囧",
//"headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//"msg": "旅进旅退",
//"addtime": "05-26 15:09",
//"teammsgid": "11",
//"praiseCount": "0",
//"isPraise": "no"


//{
//    "newscommentid": "3",
//    "newsid": "4",
//    "uid": "10",
//    "content": "yyyyyy",
//    "addtime": "1434010113",
//    "nickname": "小仙子",
//    "headimg": "http://121.40.220.245/Public/upload/header/143193822533250.jpg",
//    "sex": "0"
//},