//
//  AppDelegate.h
//  Sport
//
//  Created by pisen_lyy on 15-4-10.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MMDrawerController * drawerController;
@property (strong, nonatomic) UINavigationController *nav;
+(AppDelegate *)sharedAppDelegate;

@end

