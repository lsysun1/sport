//
//  ChineseString.h
//  ChineseSort
//
//  Created by Bill on 12-8-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChineseString : NSObject

@property(nonatomic, copy) NSString *string;
@property(nonatomic, copy) NSString *pinYin;
@property(nonatomic, copy) NSString *touid;
@property(nonatomic, copy) NSString *headimg;
@property(nonatomic, copy) NSString *tel;




@end

//"touid": "10",
//"nickname": "小仙子",
//"headimg": "http://121.40.220.245/Public/upload/header/143193822533250.jpg",
//"tel": "18302820052"