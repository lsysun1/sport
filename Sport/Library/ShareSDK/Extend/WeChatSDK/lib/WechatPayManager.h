//
//  WechatPayManager.h
//  SDKSample
//
//  Created by 李松玉 on 15/8/25.
//
//

#import <Foundation/Foundation.h>

#import "WXUtil.h"
#import "ApiXml.h"
#import "WXApi.h"

// 账号帐户资料
// 更改商户把相关参数后可测试
#define APP_ID          @"wx879857579f5871d1"                   //APPID
#define APP_SECRET      @"9ccc7336741814c058dc0b9ed84c3431"     //appsecret,看起来好像没用
//商户号，填写商户对应参数
#define MCH_ID          @"1241884802"
//商户API密钥，填写相应参数
#define PARTNER_ID      @"9aWxRywn0moaILvJESWXSgUBtpAtHNNW"
//支付结果回调页面
#define NOTIFY_URL      @"http://wxpay.weixin.qq.com/pub_v2/pay/notify.v2.php"

//获取服务器端支付数据地址（商户自定义）(在小吉这里，签名算法直接放在APP端，故不需要自定义)
#define SP_URL          @"http://wxpay.weixin.qq.com/pub_v2/app/app_pay.php"


@interface WechatPayManager : NSObject
{
    
}


//预支付网关url地址
@property (nonatomic,strong) NSString* payUrl;

//debug信息
@property (nonatomic,strong) NSMutableString *debugInfo;
@property (nonatomic,assign) NSInteger lastErrCode;//返回的错误码

//商户关键信息
@property (nonatomic,strong) NSString *appId,*mchId,*spKey;


//初始化函数
-(id)initWithAppID:(NSString*)appID
             mchID:(NSString*)mchID
             spKey:(NSString*)key;

//获取当前的debug信息
-(NSString *) getDebugInfo;

//获取预支付订单信息（核心是一个prepayID）
- (NSMutableDictionary*)getPrepay:(NSMutableDictionary *)dict;

@end
