//
//  AppDelegate.m
//  Sport
//
//  Created by pisen_lyy on 15-4-10.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "AppDelegate.h"
#import "SP_LoginViewController.h"
#import "SP_TabBarViewController.h"
#import "SP_RightViewController.h"
#import <MAMapKit/MAMapKit.h>
#import "CLLocation+YCLocation.h"
#import <AMapSearchKit/AMapSearchAPI.h>
#import "JSONKit.h"


#import <ShareSDK/ShareSDK.h>
#import "WeiboSDK.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WXApi.h"

#import "XGPush.h"
#import "XGSetting.h"
#import "IntroViewController.h"


#define _IPHONE80_ 80000


@interface AppDelegate ()<CLLocationManagerDelegate,AMapSearchDelegate>
{
    CLLocationManager *_locationmanager;
    CLLocation *_earthLocation;
    AMapSearchAPI *_search;
}

@end

@implementation AppDelegate

+(AppDelegate *)sharedAppDelegate
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    return appDelegate;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    

    [XGPush startApp:2200125929 appKey:@"I4AWE341Y5ZQ"];
    
    //注销之后需要再次注册前的准备
    void (^successCallback)(void) = ^(void){
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(sysVer < 8){
                [self registerPush];
            }
            else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
    };
    [XGPush initForReregister:successCallback];
    
    //推送反馈回调版本示例
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]handleLaunching's successBlock");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]handleLaunching's errorBlock");
    };
    
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //清除所有通知(包含本地通知)
    //[[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [XGPush handleLaunching:launchOptions successCallback:successBlock errorCallback:errorBlock];
    
    
    
    
    [ShareSDK registerApp:@"93dbb1d78a8d"];
    
    //     连接新浪微博开放平台应用以使用相关功能，此应用需要引用SinaWeiboConnection.framework
    //     http://open.weibo.com上注册新浪微博开放平台应用，并将相关信息填写到以下字段
    //     **/
    [ShareSDK connectSinaWeiboWithAppKey:@"3249991664"
                               appSecret:@"f6e427e8d884fd444d544aa37497ba50"
                             redirectUri:@"http://sns.whalecloud.com/sina2/callback"
                             weiboSDKCls:[WeiboSDK class]];
    
    //添加QQ空间应用
    [ShareSDK connectQZoneWithAppKey:@"1104576785"
                           appSecret:@"5ciFJaJQUCL9Trj9"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    [ShareSDK connectQQWithQZoneAppKey:@"1104576785"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //添加QQ应用  注册网址   http://mobile.qq.com/api/
    [ShareSDK connectQQWithQZoneAppKey:@"100371282"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    [ShareSDK connectTencentWeiboWithAppKey:@"1104576785"
                                  appSecret:@"5ciFJaJQUCL9Trj9"
                                redirectUri:@"http://www.baidu.com"];

    //微信登陆的时候需要初始化
    [ShareSDK connectWeChatWithAppId:@"wx2c074653e826a6c9"
                           appSecret:@"2594a235c7d0e8041697142dd36487e8"
                           wechatCls:[WXApi class]];

    
    
    [MAMapServices sharedServices].apiKey = MAP_KEY;
    
    SP_TabBarViewController *tabBar = [[SP_TabBarViewController alloc]init];
    self.nav = [[UINavigationController alloc]initWithRootViewController:tabBar];
    SP_RightViewController * rightVC = [[SP_RightViewController alloc] init];

    self.drawerController = [[MMDrawerController alloc]
                                             initWithCenterViewController:self.nav
                                             leftDrawerViewController:rightVC
                                             rightDrawerViewController:rightVC];
    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:self.nav
                             leftDrawerViewController:nil
                             rightDrawerViewController:rightVC];
    [self.drawerController setShowsShadow:YES];
    [self.drawerController setRestorationIdentifier:@"MMDrawer"];
    [self.drawerController setMaximumRightDrawerWidth:130.0];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [self.drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         UIViewController * sideDrawerViewController;
         if(drawerSide == MMDrawerSideLeft){
             sideDrawerViewController = drawerController.leftDrawerViewController;
         }
         else if(drawerSide == MMDrawerSideRight){
             sideDrawerViewController = drawerController.rightDrawerViewController;
         }
         [sideDrawerViewController.view setAlpha:percentVisible];
     }];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        _locationmanager = [[CLLocationManager alloc] init];
        [_locationmanager requestAlwaysAuthorization];        //NSLocationAlwaysUsageDescription
        [_locationmanager requestWhenInUseAuthorization];     //NSLocationWhenInUseDescription
        _locationmanager.delegate = self;
    }
    
    
    [self getCurPosition];
    
    
    //判断是不是第一次启动应用
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        NSLog(@"第一次启动");
        //如果是第一次启动的话,使用UserGuideViewController (用户引导页面) 作为根视图
        
        IntroViewController *userGuideViewController = [[IntroViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:userGuideViewController];
        self.window.rootViewController = nav;
    }
    else
    {
        NSLog(@"不是第一次启动");
        self.window.rootViewController = self.drawerController;
    }
    
//    self.window.rootViewController = self.drawerController;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}




#pragma mark 定位
//获得自己的当前的位置信息
- (void) getCurPosition
{
    //开始探测自己的位置
    if (_locationmanager==nil)
    {
        _locationmanager =[[CLLocationManager alloc] init];
    }
    
    
    if ([CLLocationManager locationServicesEnabled])
    {
        _locationmanager.delegate=self;
        _locationmanager.desiredAccuracy=kCLLocationAccuracyBest;
        _locationmanager.distanceFilter=10.0f;
        [_locationmanager startUpdatingLocation];
    }
}


- (void) changeWithLat:(CGFloat)lat Lng:(CGFloat)lng
{
    _search = [[AMapSearchAPI alloc] initWithSearchKey:MAPKEY_GD Delegate:self];
    
    //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
    AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
    regeoRequest.searchType = AMapSearchType_ReGeocode;
    
    regeoRequest.location = [AMapGeoPoint locationWithLatitude:lat longitude:lng];
    regeoRequest.radius = 10000;
    regeoRequest.requireExtension = YES;
    
    //发起逆地理编码
    [_search AMapReGoecodeSearch: regeoRequest];
}

//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        AMapReGeocode *result = response.regeocode;
        
        NSString *city = [[NSString alloc]init];
        
        if(result.addressComponent.city.length == 0){
            city = result.addressComponent.province;
        }else{
            city = result.addressComponent.city;
        }
        
        NSString *zone = result.addressComponent.district;
        
        
        
        
        
        [[NSUserDefaults standardUserDefaults] setObject:city forKey:CITYNAME];
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:ZONENAME];
        
        if([GETGROUNDZONE isEqualToString:@""]){
            [[NSUserDefaults standardUserDefaults] setObject:zone forKey:GROUNDZONE];
        }

        
        NSLog(@"开启地位:%@",GETZONENAM);

    }
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
            fromLocation:(CLLocation *)oldLocation
{
//    NSLog(@"newLocation:%@",[newLocation description]);
    
    
    _earthLocation = [newLocation locationMarsFromEarth];
    NSLog(@"earthLocation:%@",[_earthLocation description]);
    

    [self changeWithLat:_earthLocation.coordinate.latitude Lng:_earthLocation.coordinate.longitude];

}


- (BOOL)application: (UIApplication *)application  handleOpenURL: (NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
    
}
            
- (BOOL)application: (UIApplication *)application
                           openURL: (NSURL *)url
                 sourceApplication: (NSString *)sourceApplication
                        annotation: (id)annotation
{
        return [ShareSDK handleOpenURL: url
                     sourceApplication:sourceApplication
                            annotation: annotation
                            wxDelegate: self];
}









- (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}

- (void)registerPushForIOS8{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = @"Accept";
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    //NSString * deviceTokenStr = [XGPush registerDevice:deviceToken];
    
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]register successBlock");
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]register errorBlock");
    };
    
    //注册设备
    if(![GETUSERTEL isEqualToString:@"0"]){
        [XGPush setAccount:GETUSERTEL];
        NSString * deviceTokenStr = [XGPush registerDevice:deviceToken successCallback:successBlock errorCallback:errorBlock];
////        //如果不需要回调
//        [XGPush registerDevice:deviceToken];
        
        //打印获取的deviceToken的字符串
        NSLog(@"deviceTokenStr is %@",deviceTokenStr);

    }
    
    
    
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    //notification是发送推送时传入的字典信息
    [XGPush localNotificationAtFrontEnd:notification userInfoKey:@"clockID" userInfoValue:@"myid"];
    
    //删除推送列表中的这一条
    [XGPush delLocalNotification:notification];
    //[XGPush delLocalNotification:@"clockID" userInfoValue:@"myid"];
    
    //清空推送列表
    //[XGPush clearLocalNotifications];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_

//注册UserNotification成功的回调
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
//    //用户已经允许接收以下类型的推送
//    UIUserNotificationType allowedTypes = [notificationSettings types];
    
}

//按钮点击事件回调
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler{
    if([identifier isEqualToString:@"ACCEPT_IDENTIFIER"]){
        NSLog(@"ACCEPT_IDENTIFIER is clicked");
    }
    
    completionHandler();
}

#endif
//如果deviceToken获取不到会进入此事件
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@",err];
    
    NSLog(@"%@",str);
    
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    //推送反馈(app运行时)
//    [XGPush handleReceiveNotification:userInfo];
    
    
    //回调版本示例
   
     void (^successBlock)(void) = ^(void){
     //成功之后的处理
     NSLog(@"[XGPush]handleReceiveNotification successBlock");
     };
     
     void (^errorBlock)(void) = ^(void){
     //失败之后的处理
     NSLog(@"[XGPush]handleReceiveNotification errorBlock");
     };
     
     void (^completion)(void) = ^(void){
     //失败之后的处理
     NSLog(@"[xg push completion]userInfo is %@",userInfo);
     };
    
//    NSDictionary *aps = userInfo[@"aps"];
//    NSString *haha = aps[@"alert"];
    
     [XGPush handleReceiveNotification:userInfo successCallback:successBlock errorCallback:errorBlock completion:completion];
   
}


@end
