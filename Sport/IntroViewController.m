//
//  IntroViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "IntroViewController.h"
//主界面
#import "MMDrawerController.h"
#import "SP_TabBarViewController.h"
#import "SP_RightViewController.h"

#import "EAIntroPage.h"
#import "EAIntroView.h"
#import "EARestrictedScrollView.h"



@interface IntroViewController ()<EAIntroDelegate>

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.bgImage = [UIImage imageNamed:@"引导页面1.jpg"];
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title1"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.bgImage = [UIImage imageNamed:@"引导页面2.jpg"];
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title2"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.bgImage = [UIImage imageNamed:@"引导页面3.jpg"];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title3"]];
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.bgImage = [UIImage imageNamed:@"引导页面4.jpg"];
    page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4]];
    
    [intro setDelegate:self];
    [intro showInView:self.view animateDuration:0.3];
    
    
}


#pragma mark - EAIntroView delegate

- (void)introDidFinish:(EAIntroView *)introView {
    NSLog(@"introDidFinish callback");
    [self pushToHomeViewController];
}





/**
 *  跳转到主界面
 */
- (void) pushToHomeViewController
{
    SP_TabBarViewController *tabBar = [[SP_TabBarViewController alloc]init];
    [AppDelegate sharedAppDelegate].nav = [[UINavigationController alloc]initWithRootViewController:tabBar];
    SP_RightViewController * rightVC = [[SP_RightViewController alloc] init];
    
    
    [AppDelegate sharedAppDelegate].drawerController = [[MMDrawerController alloc]
                                                        initWithCenterViewController:[AppDelegate sharedAppDelegate].nav
                                                        leftDrawerViewController:nil
                                                        rightDrawerViewController:rightVC];
    [[AppDelegate sharedAppDelegate].drawerController setShowsShadow:YES];
    [[AppDelegate sharedAppDelegate].drawerController setRestorationIdentifier:@"MMDrawer"];
    [[AppDelegate sharedAppDelegate].drawerController setMaximumRightDrawerWidth:130.0];
    [[AppDelegate sharedAppDelegate].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [[AppDelegate sharedAppDelegate].drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [[AppDelegate sharedAppDelegate].drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         UIViewController * sideDrawerViewController;
         if(drawerSide == MMDrawerSideLeft){
             sideDrawerViewController = drawerController.leftDrawerViewController;
         }
         else if(drawerSide == MMDrawerSideRight){
             sideDrawerViewController = drawerController.rightDrawerViewController;
         }
         [sideDrawerViewController.view setAlpha:percentVisible];
     }];
    
    [self.navigationController pushViewController:[AppDelegate sharedAppDelegate].drawerController animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
