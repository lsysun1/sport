//
//  ImageCompression.h
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCompression : NSObject
+ (UIImage *)compressedImage:(UIImage *)image;

@end
