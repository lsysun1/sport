//
//  UIUtils.m
//  WXTime
//
//  Created by wei.chen on 12-7-22.
//  Copyright (c) 2012年 www.iphonetrain.com 无限互联ios开发培训中心 All rights reserved.
//

#import "UIUtils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation UIUtils

static NSDateFormatter * _formatter = nil;

+ (NSString *)getDocumentsPath:(NSString *)fileName {
    
    //两种获取document路径的方式
//    NSString *documents = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documents = [paths objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:fileName];
    
    return path;
}

+ (NSString*) stringFromFomate:(NSDate*) date formate:(NSString*)formate
{
    if (nil == _formatter)
    {
        _formatter = [[NSDateFormatter alloc] init];
                NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
                [_formatter setLocale:enUS];
    }
	[_formatter setDateFormat:formate];
	NSString *str1 = [_formatter stringFromDate:date];
    NSString *str = [NSString stringWithFormat:@"%@",str1];
	return str;
}

+ (NSDate *)dateFromFomate:(NSString *)datestring formate:(NSString*)formate
{
    if (nil == _formatter)
    {
        _formatter = [[NSDateFormatter alloc] init];
        NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
        [_formatter setLocale:enUS];
    }
    [_formatter setDateFormat:formate];
    NSDate *date = [_formatter dateFromString:datestring];
    return date;
}

//Sat Jan 12 11:50:16 +0800 2013
+ (NSString *)fomateString:(NSString *)datestring
{
    NSString *formate = @"E MMM d HH:mm:ss Z yyyy";
    NSDate *createDate = [UIUtils dateFromFomate:datestring formate:formate];
    NSString *text = [UIUtils stringFromFomate:createDate formate:@"MM-dd HH:mm"];
    return text;
}

//+ (NSMutableArray *)parserToArrayFromXMLString:(NSString *)data
//{
//    NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
//    
//    //装载xml对象
//    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:nil];
//    
//    //获取xml根节点
//    GDataXMLElement *rootElement = [xmlDoc rootElement];
//    
//    //获取根节点下的子节点数量
//    NSArray *childArray = [rootElement children];
//    
//    for (int i = 0;i < [childArray count];i++)
//    {
//        NSMutableDictionary *dicItem = [[NSMutableDictionary alloc] init];
//        GDataXMLElement *element = [childArray objectAtIndex:i];
//        for (int k = 0; k < [element childCount]; k++)
//        {
//            GDataXMLNode *subItem = [element childAtIndex:k];
//            if ([subItem.name isEqualToString:@"user"])
//            {
//                GDataXMLNode *elements = [element childAtIndex:k];
//                NSMutableDictionary *dicContact = [[NSMutableDictionary alloc] init];
//                //处理contacts数组
//                for (int j = 0; j < [elements.children count]; j++)
//                {
//                    GDataXMLNode *node = elements.children[j];
//                    [dicContact setObject:node.stringValue forKey:node.name];
//                }
//                
//                [dicItem setObject:dicContact forKey:@"user"];
//            }
//            else
//            {
//                if ([[subItem stringValue] isEqualToString:@"--"])
//                {
//                    [dicItem setObject:@"" forKey:subItem.name];
//                }
//                else
//                {
//                    [dicItem setObject:subItem.stringValue forKey:subItem.name];
//                }
//            }
//        }
//        [array addObject:dicItem];
//        [dicItem release];
//    }
//    [xmlDoc release];
//    return array;
//}
//
//+ (NSMutableDictionary *)parserToDictionaryFromXMLString:(NSString *)data
//{
//    NSMutableDictionary *dictionary = [[[NSMutableDictionary alloc] init] autorelease];
//    
//    //装载xml对象
//    GDataXMLDocument *xmlDoc = [[GDataXMLDocument alloc] initWithXMLString:data options:0 error:nil];
//    
//    //获取xml根节点
//    GDataXMLElement *rootElement = [xmlDoc rootElement];
//    
//    //获取根节点下的子节点数量
//    NSArray *childArray = [rootElement children];
//    
//    if ([childArray count] == 0)
//    {
//        return nil;
//    }
//    
//    GDataXMLElement *element = [childArray objectAtIndex:0];
//    if ([element childCount] == 0)
//    {
//        [dictionary setObject:element.stringValue forKey:element.name];
//        return dictionary;
//    }
//    for (int i = 0; i < [element childCount]; i++)
//    {
//        GDataXMLNode *subItem = [element childAtIndex:i];
//        if ([[subItem stringValue] isEqualToString:@"--"])
//        {
//            [dictionary setObject:@"" forKey:subItem.name];
//        }
//        else
//        {
//            [dictionary setObject:subItem.stringValue forKey:subItem.name];
//        }
//    }
//    [xmlDoc release];
//    return dictionary;
//}
@end
