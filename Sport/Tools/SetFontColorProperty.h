//
//  SetFontColorProperty.h
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,CustomFontType) {
    CustomTypeBoldFont,//字体加粗
    CustomTypeDefaultFont,//默认字体
    //-------------------------------不需要传size的参数
    CustomTypeNaviButtonFont,//nav按钮字体
    CustomTypeNaviTitleFont,//nav题目字体
    CustomTypeCancelButtonFont,//界面确定按钮字体
    CustomTypeLeftSizeFont,//左侧显示字体
    CustomTypeRightSizeFont,//右侧输入字体
    CustomTypeReplySizeFont,//回复字体
};

typedef NS_ENUM(NSInteger,CustomFontColor) {
    CustomBackGroundColor,
    CustomStrokeColor,
    CustomTitleDefaultColor,
    CustomTitleBlackColor,
    CustomTitleLightColor,
    CustomButtonGreenColor,
    CustomTextfieldTintColor,
    CustomBarTintColor,
};

@interface SetFontColorProperty : NSObject

+(UIFont *)setCustomFont:(CustomFontType)fontType;

+ (UIFont *)setCustomFont:(CustomFontType)fontType Size:(CGFloat)size;

+ (UIColor *)setCostomColor:(CustomFontColor)FontColor;
////自定义字体颜色
+ (UIColor *)setFontColor:(CGFloat)float1 float2:(CGFloat)float2 float3:(CGFloat)float3;



@end
