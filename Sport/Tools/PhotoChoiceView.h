//
//  PhotoChoiceView.h
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoChoiceViewDelegate <NSObject>

- (void)photoChoice:(UIImage *)image;

@end

@interface PhotoChoiceView : UIView

@property (nonatomic , assign) id<PhotoChoiceViewDelegate>delegate;

@end
