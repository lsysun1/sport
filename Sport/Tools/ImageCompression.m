//
//  ImageCompression.m
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "ImageCompression.h"

@implementation ImageCompression
+ (UIImage *)compressedImage:(UIImage *)image
{
    CGSize imageSize = image.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGRect rect_screen = [[UIScreen mainScreen]bounds];
    CGSize size_screen = rect_screen.size;
    CGFloat scale_screen = [UIScreen mainScreen].scale;
    
    CGFloat resolutionWidth = size_screen.width * scale_screen;
    CGFloat resolutionHeight = size_screen.height * scale_screen;
    
    if (width <= resolutionWidth && height <= resolutionHeight) {
        // no need to compress.
        return image;
    }
    
    if (width == 0 || height == 0) {
        // void zero exception
        return image;
    }
    
    UIImage *newImage = nil;
    CGFloat widthFactor = resolutionWidth / width;
    CGFloat heightFactor = resolutionHeight / height;
    CGFloat scaleFactor = 0.0;
    
    if (widthFactor > heightFactor)
        scaleFactor = heightFactor; // scale to fit height
    else
        scaleFactor = widthFactor; // scale to fit width
    
    CGFloat scaledWidth  = width * scaleFactor;
    CGFloat scaledHeight = height * scaleFactor;
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [image drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
    
}
@end
