//
//  SetFontColorProperty.m
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SetFontColorProperty.h"

static NSString *font1 = @"HelveticaNeue-Medium";//字体加粗
static NSString *font2 = @"HelveticaNeue";//默认字体

@implementation SetFontColorProperty

+(UIFont *)setCustomFont:(CustomFontType)fontType
{
    return [self setCustomFont:fontType Size:0];
}

+ (UIFont *)setCustomFont:(CustomFontType)fontType Size:(CGFloat)size{
    
    UIFont *font;
    switch (fontType) {
        case 0:
            font = [self setFontBold:size];
            break;
        case 1:
            font = [self setFont:size];
            break;
        case 2:
            font = [self setNavButtonFontSize];
            break;
        case 3:
            font = [self setNavTitleFontSize];
            break;
        case 4:
            font = [self setCancelButtonSize];
            break;
        case 5:
            font = [self setLeftSizeFont];
            break;
        case 6:
            font = [self setRightSizeFont];
            break;
        case 7:
            font = [self setReplySizeFont];
        default:
            break;
    }
    return font;
}

//
//typedef NS_ENUM (NSInteger,CustomColor) {
//    BackGroundColor,
//    StrokeColor,
//    TitleDefaultColor,
//    TitleBlackColor,
//    TitleLightColor,
//    ButtonGreenColor,
//};
+ (UIColor *)setCostomColor:(CustomFontColor)FontColor {
    
    UIColor *color;
    switch (FontColor) {
        case 0:
            color = [self setbgColor];
            break;
        case 1:
            color = [self setStrokeColor];
            break;
        case 2:
            color = [self setThedefaultcolor];
            break;
        case 3:
            color = [self setBlackcolor];
            break;
        case 4:
            color = [self setValuecolor];
            break;
        case 5:
            color = [self setGreencolor];
            break;
        case 6:
            color = [self setTextfieldTintcolor];
            break;
        case 7:
            color = [self setBarTintColor];
            break;
        default:
            break;
    }
    return color;
}


//自定义字体加粗大小
+ (UIFont *)setFontBold:(CGFloat)size
{
    return [UIFont fontWithName:font1 size:size];
}

//自定义默认字体大小
+ (UIFont *)setFont:(CGFloat)size
{
    return [UIFont fontWithName:font2 size:size];
}

//导航栏按钮字体大小
+ (UIFont *)setNavButtonFontSize
{
    return [self setFontBold:17];
}

//导航栏标题字体大小
+ (UIFont *)setNavTitleFontSize
{
    return [self setFontBold:19.5];
}

//完成按钮字体大小
+ (UIFont *)setCancelButtonSize
{
    return [self setFontBold:17];
}

//左侧字体大小
+ (UIFont *)setLeftSizeFont
{
    return [self setFontBold:16];
}

//右侧字体大小
+ (UIFont *)setRightSizeFont
{
    return [self setFont:15];
}

//回复字体
+ (UIFont *)setReplySizeFont
{
    return [self setFont:15.7];
}

//背景颜色
+ (UIColor *)setbgColor
{
    return [self setFontColor:238 float2:238 float3:238];
}

//描边
+ (UIColor *)setStrokeColor
{
    return [self setFontColor:225 float2:225 float3:225];
}

//栏目黑色字体
+ (UIColor *)setBlackcolor
{
    
    return [self setFontColor:15 float2:15 float3:15];
    
}

//栏目默认浅色字体
+ (UIColor *)setThedefaultcolor
{
    
    return [self setFontColor:192 float2:192 float3:199];
}

//栏目“值”浅色字体
+ (UIColor *)setValuecolor
{
    
    return [self setFontColor:186 float2:186 float3:186];
}

//按钮绿
+ (UIColor *)setGreencolor
{
    
    return [self setFontColor:30 float2:183 float3:32];
}

//textfield输入光标颜色
+ (UIColor *)setTextfieldTintcolor
{
    return [self setFontColor:192 float2:192 float3:199];
}

//自定义字体颜色
+ (UIColor *)setFontColor:(CGFloat)float1 float2:(CGFloat)float2 float3:(CGFloat)float3
{
    return [UIColor colorWithRed:float1 / 255.0 green:float2 / 255.0 blue:float3 / 255.0 alpha:1];
}

//BarTintColor
+ (UIColor *)setBarTintColor
{
    return [UIColor colorWithRed:10 / 255.0 green:10 / 255.0 blue:8 / 255.0 alpha:0];
}


@end
