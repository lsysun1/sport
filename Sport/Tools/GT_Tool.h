//
//  GT_Tool.h
//  GTElectron
//
//  Created by 小蜜蜂 on 14-3-18.
//  Copyright (c) 2014年 chou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GT_Tool : UIView


/**
 *  十六进制数据转成成颜色
 *
 *  @param stringToConvert 十六进制颜色值
 *
 *  @return UIColor
 */
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert;

//拼装json
+ (NSString *)sendJson:(NSMutableDictionary *)dictionary;
/**
 *  自动适应
 *
 *  @param value                           值
 *  @param fontSize                        字体大小
 *  @param width                           宽度
 *
 *  @return 高度
 */
+ (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width;

/**
 *  检测是否连接网络
 *
 *  @return YES-有   NO-没有
 */
+ (BOOL)isExistenceNetwork;

/**
 * 将UIColor变换为UIImage
 *
 **/
+ (UIImage *)createImageWithColor:(UIColor *)color;

/**
 *  时间格式化
 *
 *  @param timeStr 需要格式化的时间字符串
 *  @param charStr 格式化字符  如：.   -
 *
 *  @return 返回格式化后的时间 如:(2013.11.28)
 */
+ (NSString *)FormatDate:(NSString *)timeStr;

/**
 *  获取唯一识别码
 *
 *  @return 获取唯一识别嘛 uuid
 */
+ (NSString *)uuid;


/**
 *  数据加密
 *
 *  @param _interStr 需要加密的内容
 *
 *  @return 返回加密后的内容
 */
+ (NSString *)encode:(NSString *)_interStr;


/**
 *  邮箱正则判定
 *
 *  @param email 待判断内容
 *
 *  @return YES －    NO－
 */
+ (BOOL)isEmailAddress:(NSString*)email;

/**
 *  手机号正则判定
 *
 *  @param str
 *
 *  @return
 */
+(BOOL)Isphonenumber:(NSString *)str;


/**
 *  时间戳转时间
 */
+ (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline;

+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;


/**
 *  判断星期几
 */
+ (NSString*)weekdayStringFromDate:(NSDate*)inputDate;


@end
