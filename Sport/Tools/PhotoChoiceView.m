//
//  PhotoChoiceView.m
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "PhotoChoiceView.h"
#import "ImageCompression.h"
#import "SetFontColorProperty.h"

@interface PhotoChoiceView()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end


@implementation PhotoChoiceView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initInterface];
    }
    return self;
}

- (void)initInterface
{
    self.backgroundColor = [UIColor clearColor];
    UIView *view = [[UIView alloc] initWithFrame:self.frame];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0;
    view.tag = 699;
    [self addSubview:view];
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds), CGRectGetWidth(view.bounds), 215)];
    spaceView.backgroundColor = [SetFontColorProperty setCostomColor:CustomBackGroundColor];
    spaceView.tag = 700;
    [self addSubview:spaceView];
    NSArray *arr = @[@"拍照",@"从手机相册选择",@"取消"];
    for (int i=0; i<arr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.backgroundColor = [UIColor whiteColor];
        [button setTintColor:[SetFontColorProperty setFontColor:55 float2:55 float3:55]];
        [button setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
        button.layer.cornerRadius = 6;
        button.frame = CGRectMake(0, 0, 278, 44);
        button.center = CGPointMake(CGRectGetMidX(self.bounds), 47 + 52 * i);
        
        if (i==arr.count - 1) {
            button.center = CGPointMake(CGRectGetMidX(self.bounds), 47 + 52 * i + 17);
        }
        [button.titleLabel setFont:[SetFontColorProperty setCustomFont:CustomTypeCancelButtonFont]];
        if (i == arr.count - 1) {
            button.backgroundColor = [UIColor colorWithRed:154 / 255.0 green:154 / 255.0 blue:154 / 255.0 alpha:1];
            [button setTintColor:[UIColor whiteColor]];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else
        {
            button.layer.borderWidth = 0.5;
            button.layer.borderColor = [SetFontColorProperty setCostomColor:CustomStrokeColor].CGColor;
        }
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:arr[i] forState:UIControlStateNormal];
        [spaceView addSubview:button];
    }
    //    spaceView.frame = CGRectMake(0, CGRectGetHeight(self.bounds), CGRectGetWidth(view.bounds), 200);
    [UIView animateWithDuration:0.2 animations:^{
        spaceView.frame = CGRectMake(0, CGRectGetHeight(self.bounds) - 215, CGRectGetWidth(view.bounds), 215);
        view.alpha = 0.3;
    }];
    self.clipsToBounds = YES;
}

- (void)buttonClick:(UIButton *)sender
{
    UIImagePickerController * mipc = [[UIImagePickerController alloc] init];
    if ([sender.titleLabel.text isEqualToString:@"拍照"]) {
        mipc.allowsEditing = NO;//是否可编辑照片
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            NSLog(@"设备不支持访问照相机");
            return;
            
        }
        mipc.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }
    if ([sender.titleLabel.text isEqualToString:@"从手机相册选择"]) {
        mipc.allowsEditing = YES;//是否可编辑照片
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            NSLog(@"设备不支持访问相册");
            
            return;
        }
        mipc.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    if ([sender.titleLabel.text isEqualToString:@"取消"]) {
        UIView *view = (UIView *)[self viewWithTag:700];
        UIView *tempview = (UIView *)[self viewWithTag:699];
        [UIView animateWithDuration:0.2 animations:^{
            view.frame = CGRectMake(0, CGRectGetHeight(self.bounds), CGRectGetWidth(view.bounds), 215);
            tempview.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
        return;
    }
    mipc.delegate = self;//委托
    
    mipc.mediaTypes=[NSArray arrayWithObjects:@"public.movie",@"public.image",nil];
    [[self viewController] presentViewController:mipc animated:YES completion:^{
        
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.view.tag != 700) {
        UIView *view = (UIView *)[self viewWithTag:700];
        UIView *tempview = (UIView *)[self viewWithTag:699];
        [UIView animateWithDuration:0.2 animations:^{
            view.frame = CGRectMake(0, CGRectGetHeight(self.bounds), CGRectGetWidth(view.bounds), 215);
            tempview.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
        return;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage] == nil?info [UIImagePickerControllerOriginalImage]:info[UIImagePickerControllerEditedImage];
    [_delegate photoChoice:[ImageCompression compressedImage:image]];
    UIViewController *controller = [self viewController];
    UIView *view = (UIView *)[self viewWithTag:700];
    view.alpha = 0;
    [view removeFromSuperview];
    [self removeFromSuperview];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[self viewController] dismissViewControllerAnimated:YES completion:NULL];
}

- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next =
         next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController
                                          class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}
@end
