//
//  VerticallyAlignedLabel.h
//  Sport
//
//  Created by 李松玉 on 15/6/5.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum VerticalAlignment {
    VerticalAlignmentTop,
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;



@interface VerticallyAlignedLabel : UILabel
{
    @private
    VerticalAlignment verticalAlignment_;
}

@property (nonatomic, assign) VerticalAlignment verticalAlignment;


@end
