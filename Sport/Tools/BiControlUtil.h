//
//  BiControlUtil.h
//  CodoonBikeApp
//
//  Created by ChenBin on 14-3-28.
//  Copyright (c) 2014年 Nahan Cop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface BiControlUtil : NSObject

+ (UILabel*)getLabel:(CGRect)frame fontSize:(int)fontSize boldFont:(BOOL)boldFont textColor:(UIColor*)textColor align:(NSTextAlignment)align;

+ (UIButton*)getButton:(CGRect)frame fontSize:(int)fontSize boldFont:(BOOL)boldFont textColor:(UIColor*)textColor;

@end
