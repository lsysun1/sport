//
//  SP_TabBarViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_TabBarViewController.h"
#import "SP_TabBar.h"
#import "SP_NewsViewController.h"
#import "SP_RaceViewController.h"
#import "SP_CompanyViewController.h"
#import "SP_GroundViewController.h"
#import "SP_ZoneViewController.h"
#import "SP_LoginViewController.h"


@interface SP_TabBarViewController () <SP_TabBarDelegate>

@end

@implementation SP_TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    SP_NewsViewController *newsVC = [[SP_NewsViewController alloc]init];
    SP_RaceViewController *raceVC = [[SP_RaceViewController alloc]init];
    SP_CompanyViewController *companyVC = [[SP_CompanyViewController alloc]init];
    SP_GroundViewController *groundVC = [[SP_GroundViewController alloc]init];
    SP_ZoneViewController *zoneVC = [[SP_ZoneViewController alloc]init];
    
//    UINavigationController *newsNav = [[UINavigationController alloc] initWithRootViewController:newsVC];
//    UINavigationController *raceNav = [[UINavigationController alloc] initWithRootViewController:raceVC];
//    UINavigationController *companyNav = [[UINavigationController alloc] initWithRootViewController:companyVC];
//    UINavigationController *groundNav = [[UINavigationController alloc] initWithRootViewController:groundVC];
//    UINavigationController *zoneNav = [[UINavigationController alloc] initWithRootViewController:zoneVC];

    self.viewControllers = @[newsVC,raceVC,companyVC,groundVC,zoneVC];

    SP_TabBar *myTabBar = [[SP_TabBar alloc] init];
    myTabBar.backgroundColor = [UIColor whiteColor];
    myTabBar.frame = self.tabBar.frame;
    myTabBar.delegate = self;
    [self.view addSubview:myTabBar];
    
    for (int i = 0; i < self.viewControllers.count; i++) {
        NSString *norImageName = [NSString stringWithFormat:@"TabBar%d", i + 1];
        NSString *disableImageName = [NSString stringWithFormat:@"TabBar%dSel", i + 1];
        [myTabBar addTabBarButtonWithNormalImageName:norImageName andDisableImageName:disableImageName];
    }
    
    [self.tabBar removeFromSuperview];
    

}


#pragma mark - WT_TabBarDelegate Method
- (void)tabBarDidSelectBtnFrom:(NSInteger)from to:(NSInteger)to
{
    // 切换子控制器
    
    if(to != 0){
        
        if([GETUSERID isEqualToString:@"0"]){
            SP_LoginViewController *logVC = [[SP_LoginViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:logVC animated:YES];
            return;
        }
    }
    
    self.selectedIndex = to;
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.selectedViewController beginAppearanceTransition: YES animated: animated];
}

-(void) viewDidAppear:(BOOL)animated
{
    [self.selectedViewController endAppearanceTransition];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [self.selectedViewController beginAppearanceTransition: NO animated: animated];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [self.selectedViewController endAppearanceTransition];
}


@end
