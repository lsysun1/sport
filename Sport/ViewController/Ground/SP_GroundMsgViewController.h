//
//  SP_GroundMsgViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_GroundEntity;
@interface SP_GroundMsgViewController : BaseViewController

@property (nonatomic,strong) SP_GroundEntity *entitiy;
@property (nonatomic,assign) BOOL isFadeBack;


@end
