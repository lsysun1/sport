//
//  SP_GroundDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_GroundDetailsViewController.h"
#import "GroundHeadView.h"
#import "SP_GroundEntity.h"
#import "GroundRaceCell.h"
#import "GroundRaceHeadView.h"
#import "GroundCollectCell.h"
#import "GroundCollectHeadView.h"
#import "SP_GroundRaceEntity.h"
#import "SP_GroundMsgViewController.h"
#import "SP_NewsCommentModel.h"
#import "SP_NewsCommentFrame.h"
#import "SP_NewsCommentCell.h"
#import "MJRefresh.h"
#import "GroundMsgHeadView.h"
#import "SP_UerHomeViewController.h"



@interface SP_GroundDetailsViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    GroundHeadView *_headView;
    NSMutableArray *_raceArray;
    NSArray *_collcetArray;
    NSMutableArray *_msgArray;
}
@end

@implementation SP_GroundDetailsViewController

- (id)init
{
    self = [super init];
    if(self){
        _raceArray = [NSMutableArray array];
        _collcetArray = [NSArray array];
        _msgArray = [NSMutableArray array];
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNavigationBar:@"场地详情"];
    [self addBackButton];
    [self addNavRightView];
    [self setUpTableView];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETCDMATCH(self.entity.ycdid) Tag:TAG_HTTP_GETCDMATCH];
    [self setHTTPRequest:GETCOLLECTEDUSER(self.entity.ycdid) Tag:TAG_HTTP_GETCOLLECTEDUSER];
//    [self setHTTPRequest:GETYCDMSGLIST(self.entity.ycdid,@"10", @"0") Tag:TAG_HTTP_GETYCDMSGLIST];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [_tableView.header beginRefreshing];

}

#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETCDMATCH){
            if (HTTP_SUCCESS(code))
            {
                [_raceArray removeAllObjects];
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    
                    SP_GroundRaceEntity *entity = [SP_GroundRaceEntity objectWithKeyValues:dict];
                    [_raceArray addObject:entity];
                    
                }
            }
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
            [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }else if (request.tag == TAG_HTTP_GETCOLLECTEDUSER){
//            NSLog(@"TAG_HTTP_GETCOLLECTEDUSER  -- resopnse:%@",request.responseString);

            if(HTTP_SUCCESS(code)){
                _collcetArray = dictionary[@"msg"];
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }else if (request.tag == TAG_HTTP_GETYCDMSGLIST){
            NSLog(@"TAG_HTTP_GETYCDMSGLIST  -- resopnse:%@",request.responseString);
            if(HTTP_SUCCESS(code)){
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    
                    SP_NewsCommentFrame *frame = [[SP_NewsCommentFrame alloc]init];
                    SP_NewsCommentModel *entity = [SP_NewsCommentModel objectWithKeyValues:dict];
                    frame.msgModel = entity;
                    [_msgArray addObject:frame];
                    
                }
                
                if(_msgArray.count == 20){
                    [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(refreshComment)];

                }
                
                if(msgArr.count == 0){
                    [self showMessage:@"没有更多的留言了"];
                }
                
                [_tableView.header endRefreshing];
                [_tableView.footer endRefreshing];
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:2];
                [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        
        }
    }
}



- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"留言"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}

#pragma mark - 留言按钮
- (void)btnNavRightClick
{
    SP_GroundMsgViewController *vc = [[SP_GroundMsgViewController alloc]init];
    vc.entitiy = self.entity;
    [[AppDelegate sharedAppDelegate].nav presentViewController:vc animated:YES completion:nil];
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = 0;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadComment)];

    
    _headView = [[GroundHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 280)];
    _headView.entity = self.entity;
    _tableView.tableHeaderView = _headView;
    
    
    
    
    [self.view addSubview:_tableView];
}


- (void) refreshComment
{
    SP_NewsCommentFrame *frame = [_msgArray lastObject];
    SP_NewsCommentModel *model = frame.msgModel;

    [self setHTTPRequest:GETYCDMSGLIST(self.entity.ycdid,@"20",model.ycdmsgid) Tag:TAG_HTTP_GETYCDMSGLIST];
}

- (void) reloadComment
{
    [_msgArray removeAllObjects];
    [self setHTTPRequest:GETYCDMSGLIST(self.entity.ycdid,@"20", @"0") Tag:TAG_HTTP_GETYCDMSGLIST];
}


#pragma mark - _tableView DataSource Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 0;
    }else if(section == 1){
        return 1;
    }else if(section == 2){
        return _msgArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        GroundRaceCell *cell = [GroundRaceCell cellWithTableView:tableView];
        return cell;
    }else if(indexPath.section == 1){
        GroundCollectCell *cell =[GroundCollectCell cellWithTableView:tableView];
        cell.collectArray = _collcetArray;
        return cell;
    }else if (indexPath.section == 2){
        SP_NewsCommentCell *cell = [SP_NewsCommentCell cellWithTableView:tableView];
        SP_NewsCommentFrame *frame = _msgArray[indexPath.row];
//        SP_NewsCommentModel *model = frame.msgModel;
        cell.commentFrame = frame;
        
        SP_NewsCommentModel *cmt = frame.msgModel;
        cell.headButton.tag = [cmt.uid integerValue];
        
        [cell.headButton addTarget:self action:@selector(cmtHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return nil;
}


- (void) cmtHeadBtnDidClick:(UIButton *)btn
{
    SP_UerHomeViewController  *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        return 60;
    }else if (indexPath.section == 1){
        if(_collcetArray.count == 0){
            return 40;
        }else{
            return 100;
        }
    }else if (indexPath.section == 2){
        SP_NewsCommentFrame *frame = _msgArray[indexPath.row];
        return frame.cellHeight;
    }
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        GroundRaceHeadView *view = [[GroundRaceHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
        
        BOOL hiddenNotice;
        if(_raceArray.count == 0){
            hiddenNotice = NO;
        }else{
            hiddenNotice = YES;
        }
        view.noRace = hiddenNotice;
        
        return view;
    }else if (section == 1){
        GroundCollectHeadView *view = [[GroundCollectHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
        view.collectNum.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_collcetArray.count];
        BOOL hiddenNotice;
        if(_collcetArray.count == 0){
            hiddenNotice = NO;
        }else{
            hiddenNotice = YES;
        }
        view.noCollect = hiddenNotice;
        
        return view;
    }else if (section == 2){
        GroundMsgHeadView *view = [[GroundMsgHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
        view.msgNum.text = [NSString stringWithFormat:@"(%ld)",_msgArray.count];
        return view;
    }
    return nil;
}



- (void) setEntity:(SP_GroundEntity *)entity
{
    _entity = entity;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
