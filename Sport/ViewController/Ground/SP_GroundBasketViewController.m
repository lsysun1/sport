//
//  SP_GroundBasketViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_GroundBasketViewController.h"
#import "GroundListCell.h"
#import "SP_GroundEntity.h"
#import "SP_GroundDetailsViewController.h"

@interface SP_GroundBasketViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    int _ballType;
    NSMutableArray *_arr;
    UIButton *_nowBtn;
}
@end

@implementation SP_GroundBasketViewController

- (id)init
{
    self = [super init];
    if(self){
        _arr = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _ballType = 0; //足球是0,篮球1
    
    [self setUpTableView];
    
    NSString *type = @"1";
    NSString *location = [self urlEncodeValue:GETSELECTEDZONENAME];
    NSLog(@"location -- %@",location);
    
    
//    [self setHTTPRequest:GETCDLIST(@"0", @"20", type, location, GETUSERID) Tag:TAG_HTTP_GETCDLIST];
    
    
}


- (void) viewDidAppear:(BOOL)animated
{
    [_tableView.header beginRefreshing];
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETCDLIST -- 足球resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETCDLIST){
            if (HTTP_SUCCESS(code))
            {
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    
                    SP_GroundEntity *entity = [SP_GroundEntity objectWithKeyValues:dict];
                    [_arr addObject:entity];
                    
                }
            }
            
            if(_arr.count == 20){
                [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(refreshList)];
            }
            
            NSLog(@"_arr.count -- %ld",_arr.count);
            [_tableView reloadData];
            [_tableView.header endRefreshing];
            [_tableView.footer endRefreshing];
            
        }else if (request.tag == TAG_HTTP_ADDCOLLECT){
            if(HTTP_SUCCESS(code)){
                
                _nowBtn.selected = YES;
                [self showMessage:@"收藏成功"];
                
            }else{
                [self showMessage:@"收藏失败!"];
            }
            
        }
    }
}


#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 147, ScreenWidth, ScreenHeight - 147 - 48)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 114;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadList)];
    
    [self.view addSubview:_tableView];
}

- (void) refreshList
{
    SP_GroundEntity *entity = [_arr lastObject];
    NSString *type = @"1";
    NSString *location = [self urlEncodeValue:GETSELECTEDZONENAME];
    
//    [self setHTTPRequest:GETCDLIST(entity.ycdid, @"20", type, location, GETUSERID) Tag:TAG_HTTP_GETCDLIST];
    
}

- (void) reloadList
{
    [_arr removeAllObjects];
    
    NSString *type = @"1";
    NSString *location = [self urlEncodeValue:GETSELECTEDZONENAME];
    NSLog(@"location -- %@",location);
    
//    [self setHTTPRequest:GETCDLIST(@"0", @"20", type, location, GETUSERID) Tag:TAG_HTTP_GETCDLIST];
}



#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroundListCell *cell = [GroundListCell cellWithTableView:tableView];
    
    SP_GroundEntity *entity = _arr[indexPath.row];
    cell.entity = entity;
    
    [cell.markBtn addTarget:self action:@selector(cellMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.markBtn.tag = [entity.ycdid integerValue];
    
    
    return cell;
}




- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SP_GroundDetailsViewController *vc = [[SP_GroundDetailsViewController alloc]init];
    SP_GroundEntity *entity = _arr[indexPath.row];
    vc.entity = entity;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}

#pragma mark - 关注按钮
- (void) cellMarkBtnDidClick:(UIButton *)btn
{
    _nowBtn = btn;
    
    
    NSString *linkid = [NSString stringWithFormat:@"%ld",btn.tag];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:linkid forKey:@"linkid"];
    [dict setObject:@"2" forKey:@"type"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
