//
//  SP_GroundMsgViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_GroundMsgViewController.h"
#import "MyTextView.h"
#import "SP_GroundEntity.h"

@interface SP_GroundMsgViewController ()<UITextViewDelegate>
{
    UIView *_textBgView;
    MyTextView *_textView;
}
@end

@implementation SP_GroundMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if(self.isFadeBack == YES){
        [self setNavigationBar:@"反馈与建议"];
    }else{
        [self setNavigationBar:@"留言"];
    }
    
    [self addBackButton];
    [self addNavRightView];
    
    
    _textBgView = [[UIView alloc]initWithFrame:CGRectMake(5, 69, ScreenWidth - 10, 200)];
    _textBgView.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _textBgView.layer.borderWidth = 2;
    [self.view addSubview:_textBgView];
    
    _textView = [[MyTextView alloc]initWithFrame:CGRectMake(5, 5,_textBgView.frame.size.width - 10, 190)];
    _textView.alwaysBounceVertical = YES;
    _textView.delegate = self;
    _textView.font = [UIFont systemFontOfSize:15];
    _textView.placehoder = @"说点什么吧!";
    _textView.returnKeyType = UIReturnKeyDone;
    [_textBgView addSubview:_textView];

}



- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"发送"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}


- (void) btnNavRightClick
{
    if(_textView.text.length == 0){
        [self showMessage:@"留言不能为空!"];
        return;
    }
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    if(self.isFadeBack == YES){

        [dict setObject:_textView.text  forKey:@"content"];

    }else{
        [dict setObject:self.entitiy.ycdid forKey:@"ycdid"];

    [dict setObject:_textView.text  forKey:@"content"];
    }
    [self asyshowHUDView:WAITTING CurrentView:self];
    
    if(self.isFadeBack == YES){
        [self setHttpPostData:PUBLISHFAQ Dictionary:dict Tag:TAG_HTTP_PUBLISHFAQ];
    }else{
        [self setHttpPostData:PUBLISHCDMSG Dictionary:dict Tag:TAG_HTTP_PUBLISHCDMSG];
    }
}

#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETCDMATCH  -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_PUBLISHCDMSG || request.tag == TAG_HTTP_PUBLISHFAQ){
            if (HTTP_SUCCESS(code))
            {
                if(self.isFadeBack== YES){
                    [self showMessage:@"谢谢您的反馈与建议!"];

                }else{
                    [self showMessage:@"留言成功!"];
                }
                
                [self dismissViewControllerAnimated:YES completion:nil];
                
            }else{
                if(self.isFadeBack== YES){
                    [self showMessage:@"提交失败!"];
                    
                }else{
                    [self showMessage:@"留言失败!"];
                }
            }
        }
    }
}






- (void) back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
