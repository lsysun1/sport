//
//  SP_AddGroundViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/16.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddGroundViewController.h"
#import "PhotoChoiceView.h"
#import "SP_MapViewController.h"
#import "SP_LocationEntity.h"
#import "ZipArchive.h"

@interface SP_AddGroundViewController () <UITextViewDelegate,PhotoChoiceViewDelegate,SP_MapViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_groundView;
    UIView *_telView;
    UIView *_typeView;       // 0是足球 1是篮球
    UIView *_addressView;
    UIView *_upLoadImgView;
    
    
    UIView *_photosView;
    
    UIButton *_zuqiuBtn;
    UIButton *_lanqiuBtn;
    UIButton *_selectedBtn;
    
    UITextView *_nameTextView;
    UITextView *_telTextView;
    UITextView *_addressTextView;
    
    UIButton *_photoAddBtn;
    
    NSMutableArray *_photoViewArray;
    NSMutableArray *_imgArray;
    
    SP_LocationEntity *_locationEntity;
    
    long int timeINT;
    NSMutableArray *jpgPathArray;   //转换后的jpg数组
    ZipArchive *jpgZip;



}
@end

@implementation SP_AddGroundViewController


- (id) init
{
    self = [super init];
    if(self){
        _photoViewArray = [NSMutableArray array];
        _imgArray = [NSMutableArray array];
        jpgPathArray = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"添加场地"];
    [self addBackButton];
    [self addNavRightView];
    [self bilidUI];
    
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singTapMethod)];
    [self.view addGestureRecognizer:singtap];

}

- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"确定"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}
- (void) bilidUI
{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight -64)];
    _scrollView.backgroundColor = [UIColor whiteColor];
    
    
    
    //场馆名称View
    _groundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 66, 40)];
    name.font = [UIFont systemFontOfSize:14];
    name.text = @"场馆名称:";
    
    _nameTextView = [[UITextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(name.frame) , 4, ScreenWidth - CGRectGetMaxX(name.frame) - 20, 35)];
    _nameTextView.font = [UIFont systemFontOfSize:14];
    
    UIView *sepLine = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
    sepLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    //联系方式View
    _telView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_groundView.frame), ScreenWidth, 40)];
    
    UILabel *tel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 66, 40)];
    tel.font = [UIFont systemFontOfSize:14];
    tel.text = @"联系方式:";
    
    _telTextView = [[UITextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(tel.frame) , 4, ScreenWidth - CGRectGetMaxX(tel.frame) - 20, 35)];
    _telTextView.font = [UIFont systemFontOfSize:14];
    
    
    UIView *telSepView = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth, 1)];
    telSepView.backgroundColor = UIColorFromRGB(0xdcdcdc);

    
    
    //球类选择View
    _typeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_telView.frame), ScreenWidth, 45)];
    
    UILabel *type = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 66, 40)];
    type.font = [UIFont systemFontOfSize:14];
    type.text = @"场馆类型:";
    
    _zuqiuBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(type.frame) + 20, 1, 56, 38)];
    _zuqiuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _zuqiuBtn.tag = 0;
    _zuqiuBtn.selected = YES;
    _selectedBtn = _zuqiuBtn;
    [_zuqiuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_zuqiuBtn setTitleColor:UIColorFromRGB(0x209c2a) forState:UIControlStateSelected];
    [_zuqiuBtn setTitle:@"足球" forState:UIControlStateNormal];
    [_zuqiuBtn setImage:[UIImage imageNamed:@"sexselect"] forState:UIControlStateNormal];
    [_zuqiuBtn setImage:[UIImage imageNamed:@"sexselected"] forState:UIControlStateSelected];
    [_zuqiuBtn setImageEdgeInsets:UIEdgeInsetsMake(8, 32, 9, 2)];
    [_zuqiuBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -56, 0, 22)];
    [_zuqiuBtn addTarget:self action:@selector(typeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    
    _lanqiuBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 100, 1, 56, 38)];
    _lanqiuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _lanqiuBtn.tag = 1;
    [_lanqiuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_lanqiuBtn setTitleColor:UIColorFromRGB(0x209c2a) forState:UIControlStateSelected];
    [_lanqiuBtn setTitle:@"篮球" forState:UIControlStateNormal];
    [_lanqiuBtn setImage:[UIImage imageNamed:@"sexselect"] forState:UIControlStateNormal];
    [_lanqiuBtn setImage:[UIImage imageNamed:@"sexselected"] forState:UIControlStateSelected];
    [_lanqiuBtn setImageEdgeInsets:UIEdgeInsetsMake(8, 32, 9, 2)];
    [_lanqiuBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -56, 0, 22)];
    [_lanqiuBtn addTarget:self action:@selector(typeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    UIView *typeSepView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, 5)];
    typeSepView.backgroundColor = UIColorFromRGB(0xf4f4f4);

    
    
    
    
    //详细地址
    _addressView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_typeView.frame), ScreenWidth, 45)];
    
    UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 66, 40)];
    address.font = [UIFont systemFontOfSize:14];
    address.text = @"详细地址:";
    

    
    UIImageView *arrowImg = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 20, 11, 9, 18)];
    arrowImg.image = [UIImage imageNamed:@"箭头"];
    
    UILabel *changeAddress = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(arrowImg.frame) - 56, 0, 56, 40)];
    changeAddress.font = [UIFont systemFontOfSize:14];
    changeAddress.textColor = [UIColor redColor];
    changeAddress.text = @"切换地图";
    
    
    UIButton *changeBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(changeAddress.frame), 0, 100, 40)];
    [changeBtn addTarget:self action:@selector(changeBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    _addressTextView = [[UITextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(address.frame) , 4,  CGRectGetMinX(changeAddress.frame) - CGRectGetMaxX(address.frame) - 10, 35)];
    _addressTextView.font = [UIFont systemFontOfSize:14];
    _addressTextView.editable = NO;
    
    
    UIView *addSepView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, 5)];
    addSepView.backgroundColor = UIColorFromRGB(0xf4f4f4);

    
    //上传场馆图片View
    _upLoadImgView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_addressView.frame), ScreenWidth, 40)];
    
    UILabel *upLoadText = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 40)];
    upLoadText.font = [UIFont systemFontOfSize:14];
    upLoadText.text = @"上传场馆图片";
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(5, 39, ScreenWidth - 10, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);

    
    
    
    [self.view addSubview:_scrollView];
    
    [_scrollView addSubview:_groundView];
    [_groundView addSubview:name];
    [_groundView addSubview:_nameTextView];
    [_groundView addSubview:sepLine];
    
    [_scrollView addSubview:_telView];
    [_telView addSubview:tel];
    [_telView addSubview:_telTextView];
    [_telView addSubview:telSepView];
    
    [_scrollView addSubview:_typeView];
    [_typeView addSubview:type];
    [_typeView addSubview:_zuqiuBtn];
    [_typeView addSubview:_lanqiuBtn];
    [_typeView addSubview:typeSepView];
    
    
    [_scrollView addSubview:_addressView];
    [_addressView addSubview:address];
    [_addressView addSubview:_addressTextView];
    [_addressView addSubview:arrowImg];
    [_addressView addSubview:changeAddress];
    [_addressView addSubview:changeBtn];
    [_addressView addSubview:addSepView];
    
    [_scrollView addSubview:_upLoadImgView];
    [_upLoadImgView addSubview:upLoadText];
    [_upLoadImgView addSubview:bottomLine];
    
    
    

    [self createPhotosView];
}

#pragma mark 球类选择按钮点击方法
- (void) typeBtnDidClick:(UIButton *)btn
{
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
}


- (void) createPhotosView
{
    
    _photoAddBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, 90, 90)];
    [_photoAddBtn setBackgroundImage:[UIImage imageNamed:@"addBtn"] forState:UIControlStateNormal];
    [_photoAddBtn addTarget:self action:@selector(addPhotoBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(_imgArray.count == 0){
        [_imgArray addObject:_photoAddBtn];
    }else{
        if([_imgArray[0] isKindOfClass:[UIImage class]]){
            [_imgArray insertObject:_photoAddBtn atIndex:0];
        }
    }
    
    
    _photosView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_upLoadImgView.frame), ScreenWidth, ScreenHeight * 10)];
    _photosView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_photosView];

    CGFloat originX = 15;
    CGFloat originY = 10;
    for(int i = 0; i <_imgArray.count; i ++){
        if(i == 0){
            UIButton *photoView = _imgArray[i];
            photoView.frame = CGRectMake(originX, originY, 90, 90);
            [_photosView addSubview:photoView];
        }else{
            UIImage *photoImg = _imgArray[i];
            
            UIImageView *photoView = [[UIImageView alloc]initWithFrame:CGRectMake(originX, originY, 90, 90)];
            photoView.image = photoImg;
            photoView.tag = i + 100;
            [_photosView addSubview:photoView];
            [_photoViewArray addObject:photoView];
            
            UIButton *delBtn = [[UIButton alloc]initWithFrame:CGRectMake(originX + 83, originY - 5, 15, 15)];
            delBtn.tag = i +100;
            [delBtn setBackgroundImage:[UIImage imageNamed:@"btn_deletepic"] forState:UIControlStateNormal];
            [delBtn addTarget:self action:@selector(delBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            [_photosView addSubview:delBtn];
        }
        
        // 计算下个view的尺寸
        originX += 100;
        
        if((i+1)%3 == 0 && i != 0){
            originX = 16;
            originY += 100;
        }
    }
    
    CGFloat maxH = CGRectGetMaxY(_upLoadImgView.frame) + _imgArray.count / 3 * 110 + 100;
    _scrollView.contentSize = CGSizeMake(ScreenWidth, maxH);
    

}


#pragma mark - push到添加图片ViewController
- (void) addPhotoBtnDidClick
{
    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:self.view.frame];
    under.delegate = self;
    [self.view addSubview:under];
}


#pragma mark 删除图片
- (void) delBtnDidClick:(UIButton *)btn
{
    [_imgArray removeObjectAtIndex:btn.tag - 100];
    [_photosView removeFromSuperview];
    _photosView = nil;
    [self createPhotosView];
    
}





#pragma mark - PhotoChoiceViewDelegate
- (void)photoChoice:(UIImage *)image
{
    [_imgArray addObject:image];
    [self createPhotosView];
}




#pragma mark 切换到选择地图View
- (void) changeBtnDidClick
{
    SP_MapViewController * mapVc = [[SP_MapViewController alloc] init];
    mapVc.delegate = self;
    [self.navigationController pushViewController:mapVc animated:YES];
}

- (void) bringLocaiotnInfoBack:(SP_LocationEntity *)locationEntity
{
    _locationEntity = locationEntity;
    _addressTextView.text = _locationEntity.location;
    NSLog(@"_locationEntity -- %@",_locationEntity);
}


/**
 *  打包图片
 */
- (void) zipImgs
{
    
    
    //获取时间戳
    timeINT= (long)[[NSDate  date] timeIntervalSince1970];
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    
    
    jpgZip = [[ZipArchive alloc] init];
    NSString* zipfile = [NSString stringWithFormat:@"%@/%ld.zip",documentsDirectory,timeINT];
    
    [jpgZip CreateZipFile2:zipfile];
    
    for(int i = 1;i<_imgArray.count;i++){
        
        UIImage *img = _imgArray[i];
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/img%d.jpg",i]];
        
        
        [UIImageJPEGRepresentation(img, 0.5) writeToFile:jpgPath atomically:YES];
        
        [jpgZip addFileToZip:jpgPath newname:[NSString stringWithFormat:@"img%d.jpg",i]];
        
        [fileMgr removeItemAtPath:jpgPath error:nil];
    }
    
    if( ![jpgZip CloseZipFile2] )
    {
        zipfile = @"";
    }
    
}

#pragma mark 发送请求
/**
 *  发送数据请求
 */

//name
//upload   场馆图片（zip包）
//location
//tel
//shi
//xian
//lat
//lng
//type

- (void) sendGroundInfo
{
    
    [self zipImgs];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *picsPath = [NSString stringWithFormat:@"%@/%ld.zip",docDir,timeINT];
    NSString *picsName = [NSString stringWithFormat:@"%ld",timeINT];

    
    NSString *groundName = _nameTextView.text;
    NSString *locationText = _locationEntity.location;
    NSString *tel = _telTextView.text;
    NSString *shi = _locationEntity.city;
    NSString *xian = _locationEntity.zone;
    NSString *lat = _locationEntity.lat;
    NSString *lng = _locationEntity.lat;
    NSString *type = [[NSString alloc]init];
    
    if(_selectedBtn.tag == 0){
        type = @"1";
    }else if (_selectedBtn.tag == 1){
        type = @"0";
    }
    
    
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:groundName forKey:@"name"];
    [dictionary setObject:picsName forKey:@"upload"];
    [dictionary setObject:locationText forKey:@"location"];
    [dictionary setObject:tel forKey:@"tel"];
    [dictionary setObject:shi forKey:@"shi"];
    [dictionary setObject:xian forKey:@"xian"];
    [dictionary setObject:lat forKey:@"lat"];
    [dictionary setObject:lng forKey:@"lng"];
    [dictionary setObject:type forKey:@"type"];

    
    
    [self setHttpPostFile:PUBLISHCD Params:dictionary File:picsPath Data:nil Tag:TAG_HTTP_PUBLISHCD];
    
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_PUBLISHCD){
            if (HTTP_SUCCESS(code))
            {
                [self showMessage:@"添加成功!"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}



- (void) btnNavRightClick
{
    if(_nameTextView.text.length == 0){
        [self showMessage:@"场馆名称不能为空"];
        return;
    }
    
    if(_telTextView.text.length == 0){
        [self showMessage:@"联系方式不能为空"];
        return;
    }
    
    if(_addressTextView.text.length == 0){
        [self showMessage:@"详细地址不能为空"];
        return;
    }
    
    if(_imgArray.count <= 1){
        [self showMessage:@"请选择场馆图片"];
        return;
    }
    
    
    [self sendGroundInfo];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.editing = NO;
    [_nameTextView resignFirstResponder];
    [_telTextView resignFirstResponder];
    [_addressTextView resignFirstResponder];
}


- (void)singTapMethod
{
    self.editing = NO;
    [_nameTextView resignFirstResponder];
    [_telTextView resignFirstResponder];
    [_addressTextView resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
