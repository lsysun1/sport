//
//  SP_GroundSearchViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/18.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@class SP_GroundEntity;
@protocol  SP_GroundSearchViewControllerDelegate <NSObject>

- (void) getGroundEntityBack:(SP_GroundEntity *) entitiy;

@end


@interface SP_GroundSearchViewController : BaseViewController

@property (nonatomic,strong) NSString *isSelected;
@property (nonatomic,assign) id<SP_GroundSearchViewControllerDelegate> delegate;

@end
