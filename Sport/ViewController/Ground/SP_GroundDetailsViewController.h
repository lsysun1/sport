//
//  SP_GroundDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/17.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_GroundEntity;
@interface SP_GroundDetailsViewController : BaseViewController
@property (nonatomic,strong) SP_GroundEntity *entity;
@end
