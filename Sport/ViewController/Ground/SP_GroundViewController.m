//
//  SP_GroundViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_GroundViewController.h"
#import "SP_GroundFootViewController.h"
#import "SP_GroundBasketViewController.h"
#import "SP_AddGroundViewController.h"
#import "SP_MapViewController.h"
#import "SP_LocationEntity.h"
#import "SP_GroundSearchViewController.h"
#import "SP_GroundEntity.h"
#import "GroundListCell.h"
#import "SP_GroundDetailsViewController.h"
#import "SP_AreaEntity.h"
#import "SP_PostActiveViewController.h"


#define kFirstComponent 0
#define kSubComponent 1

@interface SP_GroundViewController ()<SP_MapViewDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    SP_GroundBasketViewController *_basketBallVC;
    SP_GroundFootViewController *_footBallVC;
    
    //顶部的球类选择View
    UIView *_ballView;
    UIButton *_zuqiuBtn;
    UIButton *_lanqiuBtn;
    UIImageView *_ballLine;
    UIButton *_selectedBtn;
    int _ballType; //足球是1，篮球0
    
    //定位View
    UIView *_locationView;
    UILabel *_cityLabel;
    UILabel *_zoneLabel;
    UIButton *_locationBtn;
    
    SP_LocationEntity *_locationEntity;
    
    
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_nowBtn;
    
    UIView *_pickerBgView;
    UIPickerView *_zonePickView;

    NSMutableArray *_allArea;
    NSMutableArray *_cityArr;
    NSMutableArray *_zoneArr;
    NSMutableArray *_tempArr;
    NSMutableArray *_subPickerArray;

    
    
    
}
@end

@implementation SP_GroundViewController


- (id)init
{
    self = [super init];
    if(self){
        _arr = [[NSMutableArray alloc]init];
        _allArea = [NSMutableArray array];
        _cityArr = [[NSMutableArray alloc]init];
        _zoneArr = [[NSMutableArray alloc]init];
        _tempArr = [[NSMutableArray alloc]init];
        _subPickerArray = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults]setObject:GETCITYNAME forKey:GROUNDCITY];
    [[NSUserDefaults standardUserDefaults]setObject:GETZONENAM forKey:GROUNDZONE];

    [self setNavigationBar:@"场地"];
    
    [self addRightButton:nil Target:self Action:@selector(addBtnDidClick) ImageName:@"邀约" Frame:CGRectMake(ScreenWidth - 40, 29, 24, 23)];
    
    [self addRightButton:nil Target:self Action:@selector(searchBtnDidClick) ImageName:@"icon_search" Frame:CGRectMake(ScreenWidth - 80, 29, 24, 23)];
    
    
    NSMutableArray *city = [[NSMutableArray alloc]init];
    NSMutableArray *zone = [[NSMutableArray alloc]init];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    NSDictionary *scDict = dict[@"四川省"];
    
    
    for(int i = 0; i<=20 ; i++){
        
        NSString *index = [NSString stringWithFormat:@"%d",i];
        NSDictionary *cityDict = scDict[index];
        NSArray *cityarr = [cityDict allKeys];
        NSString *cityName = cityarr[0];
        [city addObject:cityName];
        
        NSArray *dicstarr = [cityDict allValues];
        NSArray *dics = dicstarr[0];
        [zone addObject:dics];
    }
    
    _cityArr = city;
    _zoneArr = zone;
    _subPickerArray = _zoneArr[0];

    
    [self setUpBallView];
    [self setUpLocationView];
    [self setUpTableView];
    
    _ballType = 1;
    
    

    
    [self asyshowHUDView:WAITTING CurrentView:self];
    
    
    
    [self getRequsetForGroundListWithYcdid:@"0" num:@"10" type:@"1" xian:GETGROUNDZONE];
}

//ycdid,num,type,xian,uid
- (void)getRequsetForGroundListWithYcdid:(NSString *)ycdid
                                     num:(NSString *)num
                                    type:(NSString *)type
                                    xian:(NSString *)xian
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:ycdid forKey:@"ycdid"];
    [dict setObject:num forKey:@"num"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:xian forKey:@"xian"];
    [dict setObject:GETUSERID forKey:@"uid"];
    
    NSLog(@"dict -- %@",dict);

    [self setHttpPostData:GETCDLIST Dictionary:dict Tag:TAG_HTTP_GETCDLIST];
    

}





#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETCDLIST -- 足球resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETCDLIST){
            if (HTTP_SUCCESS(code))
            {
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    
                    SP_GroundEntity *entity = [SP_GroundEntity objectWithKeyValues:dict];
                    [_arr addObject:entity];
                    
                }
            }
            
            if(_arr.count == 20){
                [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(refreshList)];
            }
            
            NSLog(@"_arr.count -- %ld",(unsigned long)_arr.count);
            [_tableView reloadData];
            [_tableView.header endRefreshing];
            [_tableView.footer endRefreshing];
            
        }else if (request.tag == TAG_HTTP_ADDCOLLECT){
            if(HTTP_SUCCESS(code)){
                
                _nowBtn.selected = YES;
                [self showMessage:@"收藏成功"];
                
            }else{
                [self showMessage:@"收藏失败!"];
            }
            
        }
    }
}

#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 147, ScreenWidth, ScreenHeight - 147 - 48)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 114;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadList)];
    
    [self.view addSubview:_tableView];
}

- (void) refreshList
{
    SP_GroundEntity *entity = [_arr lastObject];

    NSString *type = [NSString stringWithFormat:@"%d",_ballType];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self getRequsetForGroundListWithYcdid:entity.ycdid num:@"10" type:type xian:GETGROUNDZONE];
}

- (void) reloadList
{
    [_arr removeAllObjects];
    
    NSString *type = [NSString stringWithFormat:@"%d",_ballType];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
     [self getRequsetForGroundListWithYcdid:@"0" num:@"10" type:type xian:GETGROUNDZONE];
}



#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroundListCell *cell = [GroundListCell cellWithTableView:tableView];
    
    SP_GroundEntity *entity = _arr[indexPath.row];
    cell.entity = entity;
    
    [cell.markBtn addTarget:self action:@selector(cellMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.markBtn.tag = [entity.ycdid integerValue];
    
    
    cell.postActiveBtn.tag = indexPath.row;
    [cell.postActiveBtn addTarget:self action:@selector(postActiveBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    return cell;
}


- (void)postActiveBtnDidClick:(UIButton *)btn
{
    SP_PostActiveViewController *vc = [[SP_PostActiveViewController alloc]init];
    SP_GroundEntity *entity = _arr[btn.tag];
    vc.entity = entity;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SP_GroundDetailsViewController *vc = [[SP_GroundDetailsViewController alloc]init];
    SP_GroundEntity *entity = _arr[indexPath.row];
    vc.entity = entity;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}

#pragma mark - 关注按钮
- (void) cellMarkBtnDidClick:(UIButton *)btn
{
    _nowBtn = btn;
    
    
    NSString *linkid = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:linkid forKey:@"linkid"];
    [dict setObject:@"2" forKey:@"type"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
    [self openSliderMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //关闭侧滑菜单
    [self closeSliderMenu];
}



#pragma mark - 搜索按钮点击方法
- (void) searchBtnDidClick
{
    SP_GroundSearchViewController *vc = [[SP_GroundSearchViewController alloc]init];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (void) addBtnDidClick
{
    SP_AddGroundViewController *vc = [[SP_AddGroundViewController alloc]init];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


#pragma mark - 初始化球种选择View
- (void) setUpBallView
{
    _ballView = [[UIView alloc]init];
    _ballView.frame = CGRectMake(0, 64, ScreenWidth, 38);
    _ballView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _zuqiuBtn = [[UIButton alloc]init];
    _lanqiuBtn = [[UIButton alloc]init];
    _zuqiuBtn.tag = 0;
    _lanqiuBtn.tag = 1;
    
    _zuqiuBtn.frame = CGRectMake(ScreenWidth/2 - 130, 4, 70, 30);
    _lanqiuBtn.frame = CGRectMake(ScreenWidth/2 + 60, 4, 70, 30);
    
    _zuqiuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_zuqiuBtn setTitle:@"足球" forState:UIControlStateNormal];
    [_zuqiuBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_zuqiuBtn setImage:[UIImage imageNamed:@"icon_zuqiu.png"] forState:UIControlStateNormal];
    [_zuqiuBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 30, 5, 60)];
    [_zuqiuBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    _lanqiuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_lanqiuBtn setTitle:@"篮球" forState:UIControlStateNormal];
    [_lanqiuBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_lanqiuBtn setImage:[UIImage imageNamed:@"icon_lanqiu.png"] forState:UIControlStateNormal];
    [_lanqiuBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 30, 5, 60)];
    [_lanqiuBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];

    
    [self.view addSubview:_ballView];
    [_ballView addSubview:_zuqiuBtn];
    [_ballView addSubview:_lanqiuBtn];
    
    
    
    
    _zuqiuBtn.selected = YES;
    _selectedBtn = _zuqiuBtn;

    
    [_zuqiuBtn addTarget:self action:@selector(ballSelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_lanqiuBtn addTarget:self action:@selector(ballSelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xfd7f45)];
    CGFloat zuqiuMinX = CGRectGetMinX(_zuqiuBtn.frame);
    CGFloat zuiqiuMaxY = CGRectGetMaxY(_zuqiuBtn.frame);
    _ballLine = [[UIImageView alloc]init];
    _ballLine.frame = CGRectMake(zuqiuMinX +1, zuiqiuMaxY , 70, 4);
    _ballLine.image = lineImg;
    [_ballView addSubview:_ballLine];
    
    
}

- (void) ballSelBtnDidClick:(UIButton *)btn
{
    //足球Btn的Tag是0
    //篮球Btn的Tag是1
    CGFloat zuqiuMinX = CGRectGetMinX(_zuqiuBtn.frame);
    CGFloat zuiqiuMaxY = CGRectGetMaxY(_zuqiuBtn.frame);
    CGFloat lanqiuMinX = CGRectGetMinX(_lanqiuBtn.frame);
    
    CGRect lineOneRect = CGRectMake(zuqiuMinX +1, zuiqiuMaxY , 70, 4);
    CGRect lineTwoRect = CGRectMake(lanqiuMinX +1, zuiqiuMaxY , 70, 4);
    
    _selectedBtn.selected = NO;
    if(btn.tag == 0){
        _ballLine.frame = lineOneRect;
        _ballType = 1;
        
        [_arr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        
        
        [self getRequsetForGroundListWithYcdid:@"0" num:@"10" type:@"1" xian:GETGROUNDZONE];

        
        
    }else if (btn.tag == 1){
        _ballLine.frame = lineTwoRect;
        _ballType = 0;
        [_arr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self getRequsetForGroundListWithYcdid:@"0" num:@"10" type:@"0" xian:GETGROUNDZONE];
        
    }
    
    btn.selected = YES;
    _selectedBtn = btn;
}


#pragma mark - 初始定位View
- (void) setUpLocationView
{
    _locationView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_ballView.frame), ScreenWidth, 45)];
    _locationView.backgroundColor = UIColorFromRGB(0xfafafa);
    [self.view addSubview:_locationView];
    
    UIImageView *locationIMG = [[UIImageView alloc]initWithFrame:CGRectMake(10, 14.25, 11, 16.5)];
    locationIMG.image = [UIImage imageNamed:@"icon_address"];
    [_locationView addSubview:locationIMG];
    
    _cityLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(locationIMG.frame) + 10, 0, 60, 45)];
    _cityLabel.textColor = UIColorFromRGB(0xff4e00);
    _cityLabel.font = [UIFont systemFontOfSize:14];
    _cityLabel.text = GETGROUNDCITY;
    [_locationView addSubview:_cityLabel];
    
    _zoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_cityLabel.frame) + 10, 0, 60, 45)];
    _zoneLabel.textColor = UIColorFromRGB(0xff4e00);
    _zoneLabel.font = [UIFont systemFontOfSize:14];
    _zoneLabel.text = GETGROUNDZONE;
    [_locationView addSubview:_zoneLabel];
    
    _locationBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 60, 0, 28, 45)];
    _locationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_locationBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [_locationBtn setTitle:@"切换" forState:UIControlStateNormal];
    [_locationBtn addTarget:self action:@selector(locationBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_locationView addSubview:_locationBtn];
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_locationView addSubview:bottomLine];
}

//定位切换按钮点击方法
- (void) locationBtnDidClick
{
    
    _pickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo)];
    
    [_pickerBgView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:_pickerBgView];
    
    _zonePickView = [[UIPickerView alloc]initWithFrame:CGRectMake(50, (ScreenHeight -64 -44) / 2 - 50, ScreenWidth - 100, 200)];
    _zonePickView.backgroundColor = [UIColor whiteColor];
    _zonePickView.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _zonePickView.layer.borderWidth = 1;
    _zonePickView.dataSource = self;
    _zonePickView.delegate = self;
    [_pickerBgView addSubview:_zonePickView];

    
    _subPickerArray = _zoneArr[0];
    

    NSString *zone = [NSString stringWithFormat:@"%@",_subPickerArray[0]];
    NSString *city = [NSString stringWithFormat:@"%@",_cityArr[0]];
    _cityLabel.text = city;
    _zoneLabel.text = zone;
    [[NSUserDefaults standardUserDefaults] setObject:city forKey:GROUNDCITY];
    [[NSUserDefaults standardUserDefaults] setObject:zone forKey:GROUNDZONE];
}

- (void) Actiondo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GroundZoneChange" object:nil];
    [_pickerBgView removeFromSuperview];
    
    NSString *type = [NSString stringWithFormat:@"%d",_ballType];
    
    [_arr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self getRequsetForGroundListWithYcdid:@"0" num:@"10" type:type xian:GETGROUNDZONE];

    
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(component == kFirstComponent){
        return [_cityArr count];
    }else {
        return [_subPickerArray count];
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0){
        NSString *city =  [NSString stringWithFormat:@"%@",_cityArr[row]];
        return city;
    }else{
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[row]];
        return zone;
    }
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == kFirstComponent) {
        
        NSString *city = [NSString stringWithFormat:@"%@",_cityArr[row]];
        
        [[NSUserDefaults standardUserDefaults] setObject:city forKey:GROUNDCITY];
        
        _subPickerArray = _zoneArr[row];
        [pickerView selectRow:0 inComponent:kSubComponent animated:YES];
        [pickerView reloadComponent:kSubComponent];
        
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[0]];
        _cityLabel.text = city;
        _zoneLabel.text = zone;
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:GROUNDZONE];
        
        
    }else{
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[row]];
        _zoneLabel.text = zone;
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:GROUNDZONE];
    }
}



- (void)bringLocaiotnInfoBack:(SP_LocationEntity *)locationEntity
{
    _locationEntity = locationEntity;

    [[NSUserDefaults standardUserDefaults] setObject:_locationEntity.city forKey:GROUNDCITY];
    [[NSUserDefaults standardUserDefaults] setObject:_locationEntity.zone forKey:GROUNDZONE];
    
    _cityLabel.text = GETGROUNDCITY;
    _zoneLabel.text = GETGROUNDZONE;
    
    
    
}


@end
