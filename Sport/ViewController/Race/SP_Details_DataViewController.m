//
//  SP_Details_DataViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Details_DataViewController.h"
#import "SP_RaceDataRankCell.h"
#import "SP_Race_Data_PersonCell.h"
#import "SP_MoreCell.h"

#import "SP_Race_Rank.h"
#import "SP_Race_MVP.h"
#import "SP_Race_Defen.h"
#import "SP_Race_ZhuGong.h"
#import "SP_Race_Rank_ZuQiu.h"
#import "SP_Race_MVP_Zu.h"
#import "SP_Race_ZhuG_Zu.h"
#import "SP_Race_Defen_Zu.h"

#import "SP_MatchLeagueList.h"
#import "SP_MoreJiFenViewController.h"


@interface SP_Details_DataViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSDictionary *_dataDict;
    
    NSArray *_jifenbangArr;
    NSArray *_mvpDataArr;
    NSArray *_totalscorelistArr;
    NSArray *_totalassistslistArr;
    
    
    NSArray *_keys;
    NSArray *_values;
    
    NSDictionary *_jifenDict;

}
@end

@implementation SP_Details_DataViewController

- (id)init{
    if(self = [super init]){
        _keys = [NSArray array];
        _values = [NSArray array];
        _jifenDict = [NSDictionary dictionary];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor yellowColor];
    [self setUpTableView];
    
    NSString *matchleagueid = self.listModel.matchleagueid;
    
    [self setHTTPRequest:GETMATCHDATA(matchleagueid) Tag:TAG_HTTP_GETMATCHDATA];
    
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETMATCHDATA){
            NSLog(@"TAG_HTTP_GETMATCHDATA -- resopnse:%@",request.responseString);

            if (HTTP_SUCCESS(code))
            {
                _dataDict= dictionary[@"msg"];  //所有数据的字典
                
                
                _jifenDict = _dataDict[@"jifenbang"];  //积分榜字典(所有数据)
                _jifenbangArr = _jifenDict[@"A"];                    //积分榜数组
                
                
                 _keys = [_jifenDict allKeys];
                 _values = [_jifenDict allValues];
                
                if(_values.count != 0){
                    _jifenbangArr = _values[0];
                }
                
                
                _mvpDataArr = _dataDict[@"mvpData"];                        //MVP数组
                _totalscorelistArr = _dataDict[@"totalscorelist"];          //总得分榜数组
                _totalassistslistArr = _dataDict[@"totalassistslist"];   //总助攻榜数组
                [_tableView reloadData];
            }
        }
        
    }
    
}

#pragma mark - SetUpRaceTableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 102, ScreenWidth, ScreenHeight - 102)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 60;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        
            return 3;

        
    }else if (section == 1){
        
        if(_mvpDataArr.count < 3){
            return _mvpDataArr.count;
        }else{
            return 4;
        }
    }else if (section == 2){
        
        if(_totalscorelistArr.count < 3){
            return _totalscorelistArr.count;
        }else{
            return 4;
        }
    }else if (section == 3){
        
        if(_totalassistslistArr.count < 3){
            return _totalassistslistArr.count;
        }else{
            return 4;
        }
        
    }else{
        return 0;
    }
}

- (void)setBallType:(int)ballType
{
    _ballType = ballType;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        if(indexPath.row < 2){
            
            
            NSLog(@"数据控制器 -- ballType -- %d",self.ballType);
            
            SP_RaceDataRankCell *cell = [SP_RaceDataRankCell cellWithTableView:tableView];
            NSDictionary *model =_jifenbangArr[indexPath.row];
            cell.ballType = self.ballType;
            cell.model = model;
            cell.number = indexPath.row + 1;
            return cell;
        }else{
            SP_MoreCell *cell = [SP_MoreCell cellWithTableView:tableView];
            return cell;
        }
    }else{
        if(indexPath.row < 3){
            SP_Race_Data_PersonCell *cell = [SP_Race_Data_PersonCell cellWithTableView:tableView];
            cell.number = indexPath.row + 1;
            
            if(indexPath.section == 1){
                cell.cellType = @"mvp";
                NSDictionary *model =_mvpDataArr[indexPath.row];
                cell.model = model;
            }else if (indexPath.section == 2){
                cell.cellType = @"score";
                NSDictionary *model =_totalscorelistArr[indexPath.row];
                cell.model = model;
            }else if (indexPath.section == 3){
                cell.cellType = @"ass";
                NSDictionary *model =_totalassistslistArr[indexPath.row];
                cell.model = model;
            }
            
            return cell;
        }else{
            SP_MoreCell *cell = [SP_MoreCell cellWithTableView:tableView];
            return cell;
        }
    }
    
    
}


- (CGFloat ) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 3){
        return 30;
    }else{
        return 60;
    }
}


#pragma mark 自定义SectionView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 60;
    }else{
        return 40;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    switch (section) {
        case 0:
        {
            if(self.ballType == 1){
                SP_Race_Rank_ZuQiu *view = [[SP_Race_Rank_ZuQiu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 60)];
                
                if(_keys.count != 0){
                    view.name = [NSString stringWithFormat:@"%@",_keys[0]];
                }
                
                return view;

            }else{
                SP_Race_Rank *view = [[SP_Race_Rank alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 60)];
                if(_keys.count != 0){
                    view.name = [NSString stringWithFormat:@"%@",_keys[0]];
                }
                
                return view;
            }
        }
            break;
        case 1:
        {
            if(self.ballType == 1){
                SP_Race_MVP_Zu *view = [[SP_Race_MVP_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_MVP *view = [[SP_Race_MVP alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }

        }
            break;
        case 2:
        {
            if(self.ballType == 1){
                SP_Race_Defen_Zu *view = [[SP_Race_Defen_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_Defen *view = [[SP_Race_Defen alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }

        }
            break;
        case 3:
        {
            if(self.ballType == 1){
                SP_Race_ZhuG_Zu *view = [[SP_Race_ZhuG_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_ZhuGong *view = [[SP_Race_ZhuGong alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }

        }
        default:
        {
            return nil;
        }
            break;
        }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 7)];
    view.backgroundColor = UIColorFromRGB(0xfafafa);
    
    return view;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0 && indexPath.row == 2){
        
        SP_MoreJiFenViewController *vc = [[SP_MoreJiFenViewController alloc]init];
        vc.arr = _jifenbangArr;
        vc.type = @"1";
        vc.listModel = self.listModel;
        vc.jifenDict = _jifenDict;
        vc.ballType = self.ballType;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
    }else if (indexPath.row == 3){
        if (indexPath.section == 1) {
            
            SP_MoreJiFenViewController *vc = [[SP_MoreJiFenViewController alloc]init];
            vc.arr = _mvpDataArr;
            vc.type = @"2";
            vc.listModel = self.listModel;
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

        }else if (indexPath.section == 2){
            
            SP_MoreJiFenViewController *vc = [[SP_MoreJiFenViewController alloc]init];
            vc.arr = _totalscorelistArr ;
            vc.type = @"3";
            vc.listModel = self.listModel;
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

        
        }else if (indexPath.section == 3){
            
            SP_MoreJiFenViewController *vc = [[SP_MoreJiFenViewController alloc]init];
            vc.arr = _totalassistslistArr ;
            vc.type = @"4";
            vc.listModel = self.listModel;
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
        }
    }
}

@end
