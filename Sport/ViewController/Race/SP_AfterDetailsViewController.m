//
//  SP_AfterDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AfterDetailsViewController.h"
#import "SP_RaceDetailsHeadView.h"
#import "SP_PlayerCell.h"
#import "SP_MatchList.h"
#import "SP_CommentCell.h"

#import "SP_MatchInfo_TopData.h"
#import "SP_MatchInfo_MvpData.h"
#import "SP_MatchInfo_PersonData.h"

#import "SP_CommentFrame.h"
#import "SP_CommentModel.h"

#import "SP_UerHomeViewController.h"
#import "SP_MatchLeagueList.h"


@interface SP_AfterDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,SP_RaceDetailsHeadViewDelegate,SP_CommentCellDelegate>
{
    UITableView *_tableView;
    
    SP_RaceDetailsHeadView *_headView;
    
    SP_MatchInfo_TopData *_topData;
    SP_MatchInfo_MvpData *_mvpData;
    
    NSMutableArray *_aTeamArr;
    NSMutableArray *_bTeamArr;
    
    NSMutableArray *_msgFrameArr;
    
    
    __weak IBOutlet UIView *bottomView;
    __weak IBOutlet UITextField *inputView;
    __weak IBOutlet UIButton *sendBtn;

}
@end

@implementation SP_AfterDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _aTeamArr = [[NSMutableArray alloc]init];
    _bTeamArr = [[NSMutableArray alloc]init];
    _msgFrameArr = [[NSMutableArray alloc]init];

    
    if(self.listModel.name.length == 0){
        [self setNavigationBar:@"比赛详情"];
    }else{
        [self setNavigationBar:self.listModel.name];
    }
    
    [self addBackButton];

    [self setUpTableView];
    [self setUpBottomView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    
    NSString *matchid = self.matchList.matchid;
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:MATCHINFO(matchid) Tag:TAG_HTTP_MATCHINFO];
    [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, @"0", @"10") Tag:TAG_HTTP_MATCHMSGLIST];
    
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_MATCHINFO){
            if (HTTP_SUCCESS(code))         //比赛详情
            {
                NSLog(@"TAG_HTTP_MATCHINFO:%@",request.responseString);
                NSDictionary *msgDict = dictionary[@"msg"];
                _topData = [SP_MatchInfo_TopData objectWithKeyValues:msgDict[@"topData"]];
                _mvpData = [SP_MatchInfo_MvpData objectWithKeyValues:msgDict[@"mvpData"]];
                
                NSDictionary *bottomData = msgDict[@"bottomData"];
                NSArray *ateam = bottomData[@"ateam"];
                NSArray *bteam = bottomData[@"bteam"];

                for(NSDictionary *dict in ateam){
                    SP_MatchInfo_PersonData *person = [SP_MatchInfo_PersonData objectWithKeyValues:dict];
                    [_aTeamArr addObject:person];
                }
                
                for(NSDictionary *dict in bteam){
                    SP_MatchInfo_PersonData *person = [SP_MatchInfo_PersonData objectWithKeyValues:dict];
                    [_bTeamArr addObject:person];
                }

            }
            
            _headView.topData = _topData;
            _headView.mvpData = _mvpData;
//            [_tableView reloadData];
        }else if (request.tag == TAG_HTTP_MATCHMSGLIST){
            if (HTTP_SUCCESS(code)){
                NSDictionary *msgDict = dictionary[@"msg"];
                NSArray *msgArr = msgDict[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_CommentFrame *frame = [[SP_CommentFrame alloc]init];
                    SP_CommentModel *msg = [SP_CommentModel objectWithKeyValues:dict];
                    frame.msgModel = msg;
                    [_msgFrameArr addObject:frame];
                }
            }
            [_tableView.footer endRefreshing];
            [_tableView reloadData];
        }else if (request.tag == TAG_HTTP_PUBLISHMATCHMSG){
            if(HTTP_SUCCESS(code)){
                [self showAlertStr:@"评论成功"];
                inputView.text = @"";
            }
            [_tableView reloadData];

        }else if (request.tag == TAG_HTTP_MATCHSUPPORT){
            if(HTTP_SUCCESS(code)){
                [self showAlertStr:dictionary[@"msg"]];
                NSLog(@"resopnse:%@",request.responseString);

            }else{
                NSLog(@"error msg -- %@",dictionary[@"msg"]);
            }
            NSString *matchid = self.matchList.matchid;
            [self setHTTPRequest:MATCHINFO(matchid) Tag:TAG_HTTP_MATCHINFO];
//            [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, @"0", @"10") Tag:TAG_HTTP_MATCHMSGLIST];

        
        }else if(request.tag == TAG_HTTP_MATCHMSGPRAISE){
            if(HTTP_SUCCESS(code)){
//                NSLog(@"resopnse:%@",request.responseString);
            }else{
                [self showAlertStr:dictionary[@"msg"]];
                NSLog(@"error msg -- %@",dictionary[@"msg"]);

            }
        }
    }
}



- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 43)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 80;
    _tableView.separatorStyle = 0;
    _headView = [[SP_RaceDetailsHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140+97+30)];
    _headView.delegate = self;
    _headView.matchList = self.matchList;
    _headView.listModel = self.listModel;
    
    _tableView.tableHeaderView = _headView;

    [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(locdNewData)];
    
    [self.view addSubview:_tableView];
}

#pragma mark - 上拉加载更多评论
- (void) locdNewData
{
    SP_CommentFrame *frame = [_msgFrameArr lastObject];
    SP_CommentModel *model = frame.msgModel;
    
    NSString *matchid = self.matchList.matchid;
    [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, model.matchmsgid, @"5") Tag:TAG_HTTP_MATCHMSGLIST];
}


#pragma mark - _headView Delegate Method
- (void) headViewSupportBtnDidClick:(UIButton *)btn
{
    NSLog(@"123123");
    NSString *team;
    if(btn.tag == 11){
        team = @"ateam";
    }else if (btn.tag == 22){
        team = @"bteam";
    }
    
    NSString *matchid = self.matchList.matchid;
    
    [self setHTTPRequest:MATCHSUPPORT(matchid,team) Tag:TAG_HTTP_MATCHSUPPORT];
}


#pragma mark _tableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return _aTeamArr.count;
    }else if(section == 1){
        return _bTeamArr.count;
    }else{
        return _msgFrameArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_PlayerCell *cell = [SP_PlayerCell cellWithTableView:tableView];
    [cell.headBtn addTarget:self action:@selector(playerCellHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if(indexPath.section == 0){
        SP_MatchInfo_PersonData *person = _aTeamArr[indexPath.row];
        cell.person = person;
        cell.headBtn.tag = [person.uid integerValue] + 800;
        
        
    }else if(indexPath.section == 1){
        SP_MatchInfo_PersonData *person = _bTeamArr[indexPath.row];
        cell.person = person;
        cell.headBtn.tag = [person.uid integerValue] + 800;
    
    }else{
        SP_CommentCell *cell = [SP_CommentCell cellWithTableView:tableView];
        cell.delegate = self;
        SP_CommentFrame *frame = _msgFrameArr[indexPath.row];
        cell.commentFrame = frame;
        
        cell.headButton.tag = [frame.msgModel.uid integerValue] + 400;
        [cell.headButton addTarget:self action:@selector(headBtnDidClick:) forControlEvents:UIControlEventTouchUpInside]
        ;

        
        return cell;
    }
    
    if(indexPath.row == 0){
        cell.headImgView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    }else{
        cell.headImgView.layer.borderColor = UIColorFromRGB(0xdcdcdc).CGColor;
    }
    
    return cell;
}


- (void) headBtnDidClick:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 400];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}

- (void) playerCellHeadBtnDidClick:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 800];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}




- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 2){
        SP_CommentFrame *frame = _msgFrameArr[indexPath.row];
        return frame.cellHeight;
    }else{
        return 80;
    }
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
}

- (void) praiseBtnIsClick:(NSString *)msgID
{
    [self setHTTPRequest:MATCHMSGPRAISE(msgID, GETUSERID) Tag:TAG_HTTP_MATCHMSGPRAISE];
}



#pragma mark 自定义SectionView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section <= 1){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
        view.backgroundColor = UIColorFromRGB(0xffffff);

        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 200, CGRectGetHeight(view.bounds))];
        label.textColor = UIColorFromRGB(0x696969);
        label.font = [UIFont boldSystemFontOfSize:15];
        
        if(section == 0){
            label.text = _topData.ateamname;
        }
        
        if(section == 1){
            label.text = _topData.bteamname;
        }
        
//        label.text = @"球队名称";
        [view addSubview:label];
        
        UILabel *assLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 65, 12.5, 30, 15)];
        assLabel.font = [UIFont systemFontOfSize:15];
        assLabel.text = @"助攻";
        assLabel.textColor = UIColorFromRGB(0x696969);
        [view addSubview:assLabel];
        
        UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(GREEN_COLOR_VALUE)];
        UIImageView *assImg = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 80, 38, 60, 2)];
        assImg.image = lineImg;
        [view addSubview:assImg];
        
        UILabel *scoreLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(assLabel.frame) - 95, 12.5, 30, 15)];
        scoreLabel.font = [UIFont systemFontOfSize:15];
        scoreLabel.text = @"总分";
        scoreLabel.textColor = UIColorFromRGB(0x696969);
        [view addSubview:scoreLabel];
        
        UIImageView *scoreImg = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(scoreLabel.frame) - 15, 38, 60, 2)];
        scoreImg.image = lineImg;
        [view addSubview:scoreImg];
        return view;
    }else{
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 45)];
        view.backgroundColor = UIColorFromRGB(0xffffff);
        
        UIView *topVoew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 5)];
        topVoew.backgroundColor = UIColorFromRGB(0xfafafa);
        [view addSubview:topVoew];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 40, CGRectGetHeight(view.bounds))];
        label.textColor = UIColorFromRGB(0xe74c38);
        label.font = [UIFont systemFontOfSize:15];
        label.text = @"评论";
        [view addSubview:label];
        
        UILabel *commentNum = [[UILabel alloc]initWithFrame:CGRectMake(58, 0, 40, CGRectGetHeight(view.bounds))];
        commentNum.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_msgFrameArr.count];
        commentNum.font = [UIFont systemFontOfSize:15];
        commentNum.textColor = UIColorFromRGB(0x696969);
        [view addSubview:commentNum];
        return view;
    }
    
    
}

#pragma mark - 设置底部的输入框
- (void) setUpBottomView
{
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [bottomView addSubview:topLine];
    
    bottomView.backgroundColor = UIColorFromRGB(0xfafafa);
    
    UIImage *btnImg = [GT_Tool createImageWithColor:UIColorFromRGB(GREEN_COLOR_VALUE)];
    [sendBtn setBackgroundImage:btnImg forState:UIControlStateNormal];
    [sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sendBtn.layer.cornerRadius = 5;
    sendBtn.layer.masksToBounds = YES;
    [sendBtn addTarget:self action:@selector(sendBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}


- (void) sendBtnDidClick
{
    NSString *matchid = self.matchList.matchid;
    NSString *msg = [self urlEncodeValue:inputView.text];
    NSString *userID = GETUSERID;
    
    [self setHTTPRequest:PUBLISHMATCHMSG(userID,matchid,msg) Tag:TAG_HTTP_PUBLISHMATCHMSG];
}


#pragma mark - 监听键盘
- (void)keyboardWillShow:(NSNotification *)notification
{
    //得到键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    self.view.frame = CGRectMake(0, -keyboardRect.size.height, ScreenWidth, ScreenHeight);
    
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);

}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //使textField取消第一响应者，从而隐藏键盘
    [textField resignFirstResponder];
    return YES;
}


- (void) setMatchList:(SP_MatchList *)matchList
{
    _matchList = matchList;
}

- (void) setListModel:(SP_MatchLeagueList *)listModel
{
    _listModel = listModel;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
