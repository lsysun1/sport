//
//  SP_BeforeDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BeforeDetailsViewController.h"
#import "SP_MatchLeagueList.h"
#import "SP_RaceDetailsBeforeHeadView.h"
#import "SP_MatchList.h"
#import "SP_MatchInfo_PersonData.h"
#import "SP_MatchInfo_TopData.h"
#import "SP_MatchInfo_MvpData.h"
#import "SP_UerHomeViewController.h"

#import "SP_CommentCell.h"
#import "SP_CommentFrame.h"
#import "SP_CommentModel.h"

@interface SP_BeforeDetailsViewController () <UITableViewDataSource,UITableViewDelegate,SP_RaceDetailsBeforeHeadViewDelegate,SP_CommentCellDelegate,UITextFieldDelegate>
{
    UITableView *_tableView;
    SP_RaceDetailsBeforeHeadView *_headView;
    SP_MatchInfo_TopData *_topData;
    SP_MatchInfo_MvpData *_mvpData;
    NSMutableArray *_aTeamArr;
    NSMutableArray *_bTeamArr;
    NSMutableArray *_msgFrameArr;

    
    //底部的评论View
    UIView *_bottomeView;
    UITextField *_msgTextView;
    UIButton *_msgSendBtn;


}
@end

@implementation SP_BeforeDetailsViewController

- (id)init
{
    self = [super init];
    if(self = [super init]){
        _aTeamArr = [NSMutableArray array];
        _bTeamArr = [NSMutableArray array];
        _msgFrameArr = [NSMutableArray array];

    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    if(self.listModel.name.length == 0){
        [self setNavigationBar:@"比赛详情"];
    }else{
        [self setNavigationBar:self.listModel.name];
    }
    
    [self addBackButton];
    [self setUpTableView];
    [self setUpCommentView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    
    NSString *matchid = self.matchList.matchid;
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:MATCHINFO(matchid) Tag:TAG_HTTP_MATCHINFO];
    [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, @"0", @"10") Tag:TAG_HTTP_MATCHMSGLIST];

    
}

#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_MATCHINFO){
            if (HTTP_SUCCESS(code))         //比赛详情
            {
                NSLog(@"TAG_HTTP_MATCHINFO:%@",request.responseString);
                NSDictionary *msgDict = dictionary[@"msg"];
                _topData = [SP_MatchInfo_TopData objectWithKeyValues:msgDict[@"topData"]];
                _mvpData = [SP_MatchInfo_MvpData objectWithKeyValues:msgDict[@"mvpData"]];
                
                NSDictionary *bottomData = msgDict[@"bottomData"];
                NSArray *ateam = bottomData[@"ateam"];
                NSArray *bteam = bottomData[@"bteam"];
                
                for(NSDictionary *dict in ateam){
                    SP_MatchInfo_PersonData *person = [SP_MatchInfo_PersonData objectWithKeyValues:dict];
                    [_aTeamArr addObject:person];
                }
                
                for(NSDictionary *dict in bteam){
                    SP_MatchInfo_PersonData *person = [SP_MatchInfo_PersonData objectWithKeyValues:dict];
                    [_bTeamArr addObject:person];
                }
                
            }
            
            _headView.topData = _topData;
//            _headView.mvpData = _mvpData;
//            [_tableView reloadData];
            
        }else if (request.tag == TAG_HTTP_MATCHMSGLIST){
            if (HTTP_SUCCESS(code)){
                NSDictionary *msgDict = dictionary[@"msg"];
                NSArray *msgArr = msgDict[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_CommentFrame *frame = [[SP_CommentFrame alloc]init];
                    SP_CommentModel *msg = [SP_CommentModel objectWithKeyValues:dict];
                    frame.msgModel = msg;
                    [_msgFrameArr addObject:frame];
                }
            }
            [_tableView.footer endRefreshing];
//            [_tableView reloadData];
        }else if (request.tag == TAG_HTTP_PUBLISHMATCHMSG){
            if(HTTP_SUCCESS(code)){
                [self showAlertStr:@"评论成功"];
                _msgTextView.text = @"";
                [self.view endEditing:YES];
                
                [_msgFrameArr removeAllObjects];
                NSString *matchid = self.matchList.matchid;
                [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, @"0", @"10") Tag:TAG_HTTP_MATCHMSGLIST];

            }
            [_tableView reloadData];
            
        }else if (request.tag == TAG_HTTP_MATCHSUPPORT){
            if(HTTP_SUCCESS(code)){
                [self showAlertStr:dictionary[@"msg"]];
                NSLog(@"resopnse:%@",request.responseString);
                
            }else{
                NSLog(@"error msg -- %@",dictionary[@"msg"]);
            }
            NSString *matchid = self.matchList.matchid;
            [self setHTTPRequest:MATCHINFO(matchid) Tag:TAG_HTTP_MATCHINFO];
//            [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, @"0", @"10") Tag:TAG_HTTP_MATCHMSGLIST];
            

        }else if(request.tag == TAG_HTTP_MATCHMSGPRAISE){
            if(HTTP_SUCCESS(code)){
                //                NSLog(@"resopnse:%@",request.responseString);
            }else{
                [self showAlertStr:dictionary[@"msg"]];
                NSLog(@"error msg -- %@",dictionary[@"msg"]);
                
            }
        }
    }
}

#pragma mark - 设置_tableView
- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 43)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 80;
    _tableView.separatorStyle = 0;
    
    _headView = [[SP_RaceDetailsBeforeHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 140+80)];
    _headView.delegate = self;
    _headView.listModel = self.listModel;
    _tableView.tableHeaderView = _headView;
    
    [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(locdMoreData)];
    
    [self.view addSubview:_tableView];
}


- (void) locdMoreData
{
    SP_CommentFrame *frame = [_msgFrameArr lastObject];
    SP_CommentModel *model = frame.msgModel;
    
    
    NSString *matchid = self.matchList.matchid;
    [self setHTTPRequest:MATCHMSGLIST(GETUSERID, matchid, model.matchmsgid, @"5") Tag:TAG_HTTP_MATCHMSGLIST];

}


#pragma mark  _tableView 代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _msgFrameArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_CommentCell *cell = [SP_CommentCell cellWithTableView:tableView];
    cell.delegate = self;
    SP_CommentFrame *frame = _msgFrameArr[indexPath.row];
    cell.commentFrame = frame;
    
    cell.headButton.tag = [frame.msgModel.uid integerValue] + 400;
    [cell.headButton addTarget:self action:@selector(headBtnDidClick:) forControlEvents:UIControlEventTouchUpInside]
    ;
    return cell;
}

- (void) praiseBtnIsClick:(NSString *)msgID
{
    [self setHTTPRequest:MATCHMSGPRAISE(msgID, GETUSERID) Tag:TAG_HTTP_MATCHMSGPRAISE];
}

- (void) headBtnDidClick:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 400];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_CommentFrame *frame = _msgFrameArr[indexPath.row];
    return frame.cellHeight;
}

#pragma mark - SP_RaceDetailsBeforeHeadView 代理方法(支持球队)
- (void) headViewSupportBtnDidClick:(UIButton *)btn
{
    NSLog(@"123123");
    NSString *team;
    if(btn.tag == 11){
        team = @"ateam";
    }else if (btn.tag == 22){
        team = @"bteam";
    }
    
    NSString *matchid = self.matchList.matchid;
    
    [self setHTTPRequest:MATCHSUPPORT(matchid,team) Tag:TAG_HTTP_MATCHSUPPORT];
}

#pragma mark - 设置底部的评论视图
- (void) setUpCommentView
{
    _bottomeView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 44, ScreenWidth, 44)];
    _bottomeView.backgroundColor = UIColorFromRGB(0xfafafa);
    
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    _msgSendBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, 8, 54, 30)];
    UIImage *btnImg = [GT_Tool createImageWithColor:UIColorFromRGB(GREEN_COLOR_VALUE)];
    [_msgSendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [_msgSendBtn setBackgroundImage:btnImg forState:UIControlStateNormal];
    [_msgSendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _msgSendBtn.layer.cornerRadius = 5;
    _msgSendBtn.layer.masksToBounds = YES;
    [_msgSendBtn addTarget:self action:@selector(sendBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    _msgTextView = [[UITextField alloc]initWithFrame:CGRectMake(8, 7, CGRectGetMinX(_msgSendBtn.frame) - 10, 30)];
    _msgTextView.placeholder = @"我想说两句";
    _msgTextView.font = [UIFont systemFontOfSize:13];
    _msgTextView.borderStyle = UITextBorderStyleRoundedRect;
    _msgTextView.delegate = self;
    
    
    
    
    [self.view addSubview:_bottomeView];
    [_bottomeView addSubview:topLine];
    [_bottomeView addSubview:_msgSendBtn];
    [_bottomeView addSubview:_msgTextView];
    
}

- (void) sendBtnDidClick
{
    NSString *matchid = self.matchList.matchid;
    NSString *msg = [self urlEncodeValue:_msgTextView.text];
    NSString *userID = GETUSERID;
    
    [self setHTTPRequest:PUBLISHMATCHMSG(userID,matchid,msg) Tag:TAG_HTTP_PUBLISHMATCHMSG];
}


#pragma mark - 监听键盘
- (void)keyboardWillShow:(NSNotification *)notification
{
    //得到键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    self.view.frame = CGRectMake(0, -keyboardRect.size.height, ScreenWidth, ScreenHeight);
    
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //使textField取消第一响应者，从而隐藏键盘
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







@end
