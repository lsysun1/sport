//
//  SP_Details_NewsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_MatchLeagueList;

@interface SP_Details_NewsViewController : BaseViewController
@property (nonatomic,strong) SP_MatchLeagueList *listModel;
@end
