//
//  SP_FootBallViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_FootBallViewController.h"
#import "SP_RaceListCell.h"
#import "SP_RaceDetailsViewController.h"
#import "SP_MatchLeagueList.h"
#import "SP_AreaEntity.h"


@interface SP_FootBallViewController () <UITableViewDataSource,UITableViewDelegate>
{
    int _ballType; //足球是1,篮球0
    
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_currentBtn;
    
    NSMutableArray *_allArea;
    NSMutableArray *_cityArr;
    NSMutableArray *_zoneArr;
    NSMutableArray *_tempArr;
    
    UIView *_pickerBgView;
    
    
}
@end

@implementation SP_FootBallViewController

- (id)init
{
    self = [super init];
    if(self){
        _allArea = [NSMutableArray array];
        _arr     = [[NSMutableArray alloc]init];
        _cityArr = [[NSMutableArray alloc]init];
        _zoneArr = [[NSMutableArray alloc]init];
        _tempArr = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _ballType = 1;
    
    [self setUpTableView];
    
    
 
}

- (void) zoneChange
{
    [_arr removeAllObjects];

    NSString *type = @"1";
    
    NSString *location = GETSELECTEDZONENAME;

    
    [self asyshowHUDView:WAITTING CurrentView:self];
    
}



#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_MATCHLEAGUELIST){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchLeagueList *model = [SP_MatchLeagueList objectWithKeyValues:dict];
                    [_arr addObject:model];
                }
            }
            [_tableView reloadData];
            [_tableView.header endRefreshing];
        }else if (request.tag == TAG_HTTP_ADDCOLLECT){

            if(HTTP_SUCCESS(code))
            {
                [self showMessage:@"收藏成功"];
                _currentBtn.selected = YES;
            }else
            {
                [self showMessage:@"收藏失败"];
            }
        
        }else if (request.tag == TAG_HTTP_GETAREALIST){

            if(HTTP_SUCCESS(code)){
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_AreaEntity *area = [SP_AreaEntity objectWithKeyValues:dict];
                    [_allArea addObject:area];
                }
                [self chuliAllAreaArray];
                
                NSLog(@"_cityArr --- %@",_cityArr);
                NSLog(@"_zone --- %@",_zoneArr);

            }
        }
    }
}


- (void) chuliAllAreaArray
{
    for(int i = 0; i<_allArea.count;i++){
        SP_AreaEntity *area = _allArea[i];

        if ([area.pid isEqualToString:@"0"]) {
            [_cityArr addObject:area];
            
            if(_tempArr == nil){
                _tempArr = [NSMutableArray array];
            }
            if(_tempArr.count > 0){
            
                NSMutableArray *zone = [NSMutableArray array];
                
                for(SP_AreaEntity *tempArea in _tempArr){
                    [zone addObject:tempArea];
                }
                [_zoneArr addObject:zone];
                [_tempArr removeAllObjects];
                _tempArr = nil;
            }
        }else{
            
            if(_tempArr == nil){
                _tempArr = [NSMutableArray array];
            }
            
            [_tempArr addObject:area];
            
            if(i == _allArea.count - 1){
                NSMutableArray *zone = [NSMutableArray array];
                
                for(SP_AreaEntity *tempArea in _tempArr){
                    [zone addObject:tempArea];
                }
                [_zoneArr addObject:zone];
                [_tempArr removeAllObjects];
                _tempArr = nil;
            }
        }
    }
}



#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 147, ScreenWidth, ScreenHeight - 147 - 48)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 105;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(locdNewData)];
    
    [self.view addSubview:_tableView];
}


#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceListCell *cell = [SP_RaceListCell cellWithTableView:tableView];
    
    SP_MatchLeagueList *model = _arr[indexPath.row];
    cell.listModel = model;
    
    if([model.collect isEqualToString:@"no"]){
        [cell.markBtn addTarget:self action:@selector(addMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.markBtn addTarget:self action:@selector(cancelMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.markBtn.tag = [model.matchleagueid integerValue];
    
    return cell;
}

/**
 *  添加收藏按钮点击方法
 */
- (void)addMarkBtnDidClick:(UIButton *)btn
{
    _currentBtn = btn;
    
    NSString *collectID = [NSString stringWithFormat:@"%ld",btn.tag];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:collectID forKey:@"linkid"];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:@"1" forKey:@"type"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
}


/**
 *  取消收藏按钮点击方法
 */
- (void)cancelMarkBtnDidClick:(UIButton *)btn
{
    
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceDetailsViewController *vc = [[SP_RaceDetailsViewController alloc]init];
    vc.ballType =_ballType;
    
    SP_MatchLeagueList *listModel = _arr[indexPath.row];
    vc.listModel = listModel;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


/**
 *  下拉刷新数据
 */
- (void) locdNewData
{
    [_arr removeAllObjects];
    
    NSString *type = @"1";
    NSString *location = [self urlEncodeValue:GETSELECTEDZONENAME];

    [self asyshowHUDView:WAITTING CurrentView:self];
}


- (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, NULL, kCFStringEncodingUTF8));
    return result;
}



@end
