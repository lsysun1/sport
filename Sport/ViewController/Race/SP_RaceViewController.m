//
//  SP_RaceViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceViewController.h"
#import "SP_RaceListCell.h"
#import "SP_RaceDetailsViewController.h"

#import "SP_FootBallViewController.h"
#import "SP_BasketBallViewController.h"
#import "SP_RaceSearchViewController.h"

#import "SP_AreaEntity.h"

#import "SP_RaceListCell.h"
#import "SP_RaceDetailsViewController.h"
#import "SP_MatchLeagueList.h"


#define kFirstComponent 0
#define kSubComponent 1

@interface SP_RaceViewController () <UIPickerViewDataSource,UIPickerViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    
    SP_BasketBallViewController *_basketBallVC;
    SP_FootBallViewController *_footBallVC;
    
    //顶部的球类选择View
    IBOutlet UIView *_ballView;
    IBOutlet UIButton *_zuqiuBtn;
    IBOutlet UIButton *_lanqiuBtn;
    UIImageView *_ballLine;
    UIButton *_selectedBtn;
    int _ballType; //足球是1, 篮球0
    
    //定位View
    UIView *_locationView;
    UILabel *_cityLabel;
    UILabel *_zoneLabel;
    UIButton *_locationBtn;
    
    //比赛列表TableView
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_currentBtn;
    
    NSMutableArray *_allArea;
    NSMutableArray *_cityArr;
    NSMutableArray *_zoneArr;
    NSMutableArray *_tempArr;
    NSMutableArray *_subPickerArray;
    
    UIView *_pickerBgView;
    UIPickerView *_zonePickView;
    
    UIButton *_nowBtn;

}
@end

@implementation SP_RaceViewController

- (id)init
{
    self = [super init];
    if(self){
        _arr = [NSMutableArray array];
        _allArea = [NSMutableArray array];
        _cityArr = [[NSMutableArray alloc]init];
        _zoneArr = [[NSMutableArray alloc]init];
        _tempArr = [[NSMutableArray alloc]init];
        _subPickerArray = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBar:@"比赛"];
    [self addRightButton:nil Target:self Action:@selector(searchBtnDidClick) ImageName:@"icon_search" Frame:CGRectMake(ScreenWidth - 40, 29, 24, 23)];
    
    
    
    
    _ballType = 1;
    
    [self setUpBallView];
    [self setUpLocationView];
    [self setUpTableView];
    

    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETAREALIST  Tag:TAG_HTTP_GETAREALIST];

//    if(![GETCURRENTAID isEqualToString:@"0"]){
//        [self requestForMatchListWithLocation:GETCURRENTAID type:@"1"];
//    }else{
//        [self requestForMatchListWithLocation:@"2" type:@"1"];
//    }
}


- (void) requestForMatchListWithLocation:(NSString *)location
                                    type:(NSString *)type
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:location forKey:@"location"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:GETUSERID forKey:@"uid"];
    
    NSLog(@"dict -- %@",dict);
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:MATCHLEAGUELIST Dictionary:dict Tag:TAG_HTTP_MATCHLEAGUELIST];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if (request.tag == TAG_HTTP_GETAREALIST){
            
            if(HTTP_SUCCESS(code)){
                NSLog(@"TAG_HTTP_GETAREALIST -- %@",request.responseString);
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_AreaEntity *area = [SP_AreaEntity objectWithKeyValues:dict];
                    [_allArea addObject:area];
                }
                [self chuliAllAreaArray];
                
//                SP_AreaEntity *firstzone = _subPickerArray[0];
//                SP_AreaEntity *firstcity = _cityArr[0];

//                _cityLabel.text = firstcity.name;
//                _zoneLabel.text = firstzone.name;

                for(NSArray *subArray in _zoneArr){
                    for(SP_AreaEntity *zone in subArray){
                        if([zone.name isEqualToString:GETZONENAM]){
                            [self requestForMatchListWithLocation:zone.aid type:@"1"];
                        }
                    }
                }
                

                
            }
        }else  if(request.tag == TAG_HTTP_MATCHLEAGUELIST){
            if (HTTP_SUCCESS(code))
            {
                NSLog(@"TAG_HTTP_MATCHLEAGUELIST -- %@",request.responseString);

                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchLeagueList *model = [SP_MatchLeagueList objectWithKeyValues:dict];
                    [_arr addObject:model];
                }
            }
            [_tableView reloadData];
            [_tableView.header endRefreshing];
        }else if (request.tag == TAG_HTTP_ADDCOLLECT){
            
            if(HTTP_SUCCESS(code))
            {
                [self showMessage:@"收藏成功"];
                _currentBtn.selected = YES;
                [self locdNewData];
            }else
            {
                [self showMessage:@"收藏失败"];
            }
            
        }else if (request.tag == TAG_HTTP_CANCELCOLLECT){
            if(HTTP_SUCCESS(code)){
                _currentBtn.selected = NO;
                [self showMessage:@"取消收藏成功"];
                [self locdNewData];
            }else{
                [self showMessage:@"取消收藏失败!"];
            }
            
        }
    }
}

- (void) chuliAllAreaArray
{
    for(int i = 0; i<_allArea.count;i++){
        SP_AreaEntity *area = _allArea[i];
        
        if ([area.pid isEqualToString:@"0"]) {
            [_cityArr addObject:area];
            
            if(_tempArr == nil){
                _tempArr = [NSMutableArray array];
            }
            if(_tempArr.count > 0){
                
                NSMutableArray *zone = [NSMutableArray array];
                
                for(SP_AreaEntity *tempArea in _tempArr){
                    [zone addObject:tempArea];
                }
                [_zoneArr addObject:zone];
                [_tempArr removeAllObjects];
                _tempArr = nil;
            }
        }else{
            
            if(_tempArr == nil){
                _tempArr = [NSMutableArray array];
            }
            
            [_tempArr addObject:area];
            
            if(i == _allArea.count - 1){
                NSMutableArray *zone = [NSMutableArray array];
                
                for(SP_AreaEntity *tempArea in _tempArr){
                    [zone addObject:tempArea];
                }
                [_zoneArr addObject:zone];
                [_tempArr removeAllObjects];
                _tempArr = nil;
            }
        }
    }
    
    if(_zoneArr.count != 0){
        _subPickerArray = _zoneArr[0];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
    [self openSliderMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //关闭侧滑菜单
    [self closeSliderMenu];
}



#pragma mark - 搜索按钮点击方法
- (void) searchBtnDidClick
{
    SP_RaceSearchViewController *vc = [[SP_RaceSearchViewController alloc]init];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


#pragma mark - 初始化球种选择View
- (void) setUpBallView
{
    _ballView.frame = CGRectMake(0, 64, ScreenWidth, 38);
    _ballView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    _zuqiuBtn.frame = CGRectMake(ScreenWidth/2 - 130, 4, 70, 30);
    _lanqiuBtn.frame = CGRectMake(ScreenWidth/2 + 60, 4, 70, 30);
    
    _zuqiuBtn.selected = YES;
    _selectedBtn = _zuqiuBtn;

    [_zuqiuBtn addTarget:self action:@selector(ballSelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_lanqiuBtn addTarget:self action:@selector(ballSelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xfd7f45)];
    CGFloat zuqiuMinX = CGRectGetMinX(_zuqiuBtn.frame);
    CGFloat zuiqiuMaxY = CGRectGetMaxY(_zuqiuBtn.frame);
    _ballLine = [[UIImageView alloc]init];
    _ballLine.frame = CGRectMake(zuqiuMinX +1, zuiqiuMaxY , 70, 4);
    _ballLine.image = lineImg;
    [_ballView addSubview:_ballLine];
  

}

- (void) ballSelBtnDidClick:(UIButton *)btn
{
    //足球Btn的Tag是0
    //篮球Btn的Tag是1
    CGFloat zuqiuMinX = CGRectGetMinX(_zuqiuBtn.frame);
    CGFloat zuiqiuMaxY = CGRectGetMaxY(_zuqiuBtn.frame);
    CGFloat lanqiuMinX = CGRectGetMinX(_lanqiuBtn.frame);
    
    CGRect lineOneRect = CGRectMake(zuqiuMinX +1, zuiqiuMaxY , 70, 4);
    CGRect lineTwoRect = CGRectMake(lanqiuMinX +1, zuiqiuMaxY , 70, 4);
    
    _selectedBtn.selected = NO;
    if(btn.tag == 0){
        _ballLine.frame = lineOneRect;
        _ballType = 1;
        
        [_arr removeAllObjects];
        NSString *type = [NSString stringWithFormat:@"%d",_ballType];
        if(![GETCURRENTAID isEqualToString:@"0"]){
            [self requestForMatchListWithLocation:GETCURRENTAID type:type];
        }else{
            [self requestForMatchListWithLocation:@"3" type:type];
        }
        

    }else if (btn.tag == 1){
        _ballLine.frame = lineTwoRect;
        _ballType = 0;
        
        [_arr removeAllObjects];
        NSString *type = [NSString stringWithFormat:@"%d",_ballType];
        if(![GETCURRENTAID isEqualToString:@"0"]){
            [self requestForMatchListWithLocation:GETCURRENTAID type:type];
        }else{
            [self requestForMatchListWithLocation:@"3" type:type];
        }
        
        
        
        
    }

    btn.selected = YES;
    _selectedBtn = btn;
}


#pragma mark - 初始定位View
- (void) setUpLocationView
{
    _locationView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_ballView.frame), ScreenWidth, 45)];
    _locationView.backgroundColor = UIColorFromRGB(0xfafafa);
    [self.view addSubview:_locationView];
    
    UIImageView *locationIMG = [[UIImageView alloc]initWithFrame:CGRectMake(10, 14.25, 11, 16.5)];
    locationIMG.image = [UIImage imageNamed:@"icon_address"];
    [_locationView addSubview:locationIMG];

    _cityLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(locationIMG.frame) + 10, 0, 50, 45)];
    _cityLabel.textColor = UIColorFromRGB(0xff4e00);
    _cityLabel.font = [UIFont systemFontOfSize:14];
    _cityLabel.text = GETCITYNAME;
//    _cityLabel.text = @"成都";
//    
//    if(![GETRACECITY isEqualToString:@"0"]){
//        _cityLabel.text = GETRACECITY;
//
//    }else{
//        _cityLabel.text = GETCITYNAME;
//    }
    
    [_locationView addSubview:_cityLabel];
    
    _zoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_cityLabel.frame) + 10, 0, 60, 45)];
    _zoneLabel.textColor = UIColorFromRGB(0xff4e00);
    _zoneLabel.font = [UIFont systemFontOfSize:14];
    
    _zoneLabel.text = GETZONENAM;
//    _zoneLabel.text = @"高新区";
//    
//    if(![GETRACEZONE isEqualToString:@"0"]){
//        _zoneLabel.text = GETRACEZONE;
//    }else{
//        _zoneLabel.text = GETZONENAM;
//    }
    
    [_locationView addSubview:_zoneLabel];
    
    _locationBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 60, 0, 28, 45)];
    _locationBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_locationBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [_locationBtn setTitle:@"切换" forState:UIControlStateNormal];
    [_locationBtn addTarget:self action:@selector(locationBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_locationView addSubview:_locationBtn];
    
  
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_locationView addSubview:bottomLine];
}

//定位切换按钮点击方法
- (void) locationBtnDidClick
{
    
    _pickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo)];
    
    [_pickerBgView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:_pickerBgView];
    
    _zonePickView = [[UIPickerView alloc]initWithFrame:CGRectMake(50, (ScreenHeight -64 -44) / 2 - 50, ScreenWidth - 100, 200)];
    _zonePickView.backgroundColor = [UIColor whiteColor];
    _zonePickView.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _zonePickView.layer.borderWidth = 1;
    _zonePickView.dataSource = self;
    _zonePickView.delegate = self;
    [_zonePickView reloadAllComponents];
    [_pickerBgView addSubview:_zonePickView];
    
    _subPickerArray = _zoneArr[0];
    SP_AreaEntity *firstzone = _subPickerArray[0];
    SP_AreaEntity *firstcity = _cityArr[0];
    
    _cityLabel.text = firstcity.name;
    _zoneLabel.text = firstzone.name;
    
    
    [[NSUserDefaults standardUserDefaults] setObject:firstcity.name forKey:RACECITY];
    [[NSUserDefaults standardUserDefaults] setObject:firstzone.name forKey:RACEZONE];
    [[NSUserDefaults standardUserDefaults] setObject:firstzone.aid  forKey:CURRENTAID];

    
    
}

- (void) Actiondo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GroundZoneChange" object:nil];
    _subPickerArray = _zoneArr[0];
    [_pickerBgView removeFromSuperview];
    
    
    
    NSLog(@"GETCURRENTAID -- %@",GETCURRENTAID);
    
    NSString *type = [NSString stringWithFormat:@"%d",_ballType];
    [_arr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self requestForMatchListWithLocation:GETCURRENTAID type:type];

}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(component == kFirstComponent){
        return [_cityArr count];
    }else {
        return [_subPickerArray count];
    }

}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == kFirstComponent){
        SP_AreaEntity *city = _cityArr[row];
        return city.name;
    }else{
        SP_AreaEntity *zone = _subPickerArray[row];
        return zone.name;
    }
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == kFirstComponent) {
        
        SP_AreaEntity *city = _cityArr[row];
        _cityLabel.text = city.name;
        
        [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:RACECITY];

        _subPickerArray = _zoneArr[row];
        [pickerView selectRow:0 inComponent:kSubComponent animated:YES];
        
        SP_AreaEntity *zone = _subPickerArray[0];
        _zoneLabel.text = zone.name;
        [[NSUserDefaults standardUserDefaults] setObject:zone.aid forKey:CURRENTAID];
        [[NSUserDefaults standardUserDefaults] setObject:zone.name forKey:RACEZONE];
        
        [pickerView reloadComponent:kSubComponent];


    }else{
        SP_AreaEntity *zone = _subPickerArray[row];
        _zoneLabel.text = zone.name;
        [[NSUserDefaults standardUserDefaults] setObject:zone.aid forKey:CURRENTAID];
        [[NSUserDefaults standardUserDefaults] setObject:zone.name forKey:RACEZONE];
    }
//
    
//    //    如果选取的是第一个选取器
//    if (component == kFirstComponent) {
//        //        得到第一个选取器的当前行
//        SP_AreaEntity *city = _cityArr[row];
//        _cityLabel.text = city.name;
//
//        //        根据从pickerDictionary字典中取出的值，选择对应第二个中的值
//        NSArray *array = [pickerDictionary objectForKey:selectedState];
//        singData=array;
//        [pickerView selectRow:0 inComponent:singPickerView animated:YES];
//        
//        
//        //        重新装载第二个滚轮中的值
//        [pickerView reloadComponent:kSubComponent];
//    }
}


#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 147, ScreenWidth, ScreenHeight - 147 - 48)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 105;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(locdNewData)];
    
    [self.view addSubview:_tableView];
}


#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceListCell *cell = [SP_RaceListCell cellWithTableView:tableView];
    
    SP_MatchLeagueList *model = _arr[indexPath.row];
    cell.listModel = model;
    
    if([model.collect isEqualToString:@"no"]){
        [cell.markBtn addTarget:self action:@selector(addMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.markBtn addTarget:self action:@selector(cancelMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.markBtn.tag = [model.matchleagueid integerValue];
    
    return cell;
}

/**
 *  添加收藏按钮点击方法
 */
- (void)addMarkBtnDidClick:(UIButton *)btn
{
    _currentBtn = btn;
    
    NSString *collectID = [NSString stringWithFormat:@"%ld",btn.tag];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:collectID forKey:@"linkid"];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:@"1" forKey:@"type"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
}


/**
 *  取消收藏按钮点击方法
 */
- (void)cancelMarkBtnDidClick:(UIButton *)btn
{
    _currentBtn = btn;
    NSString *linkid = [NSString stringWithFormat:@"%ld",btn.tag];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:linkid forKey:@"linkid"];
    [dict setObject:@"1" forKey:@"type"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:CANCELCOLLECT Dictionary:dict Tag:TAG_HTTP_CANCELCOLLECT];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceDetailsViewController *vc = [[SP_RaceDetailsViewController alloc]init];
    vc.ballType =_ballType;
    
    SP_MatchLeagueList *listModel = _arr[indexPath.row];
    vc.listModel = listModel;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


/**
 *  下拉刷新数据
 */
- (void) locdNewData
{
    [_arr removeAllObjects];
    
    NSString *type = [NSString stringWithFormat:@"%d",_ballType];
    
    [self asyshowHUDView:WAITTING CurrentView:self];

    if(![GETCURRENTAID isEqualToString:@"0"]){
        [self requestForMatchListWithLocation:GETCURRENTAID type:type];
    }else{
        [self requestForMatchListWithLocation:@"3" type:type];
    }
    
}


- (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, NULL, kCFStringEncodingUTF8));
    return result;
}




@end
