//
//  SP_Details_DataViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_MatchLeagueList;
@interface SP_Details_DataViewController : BaseViewController

@property (nonatomic,assign) int ballType;
@property (nonatomic,strong) SP_MatchLeagueList *listModel;

@end

//
//"msg": {
//    "jifenbang": [
//                  {
//                      "teamid": 11,
//                      "name": "屌丝队",
//                      "img": "http://121.40.220.245/Public/upload/teamheader/143243686979895.jpg",
//                      "shen": 1,
//                      "ping": 1,
//                      "fu": 0,
//                      "jsq": 3,
//                      "jifen": 4
//                  },
//                  {
//                      "teamid": 10,
//                      "name": "无敌队",
//                      "img": "http://121.40.220.245/Public/upload/teamheader/143210643937576.jpg",
//                      "shen": 0,
//                      "ping": 1,
//                      "fu": 1,
//                      "jsq": -3,
//                      "jifen": 1
//                  }
//                  ],
//    "mvpData": [
//                {
//                    "uid": 11,
//                    "nickname": "囧你个囧",
//                    "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//                    "sex": "1",
//                    "postion": "后卫",
//                    "teamname": "无敌队",
//                    "role": "1",
//                    "mvpcount": 1
//                }
//                ],
//    "totalscorelist": [
//                       {
//                           "uid": "10",
//                           "teamid": "11",
//                           "totalscore": "5",
//                           "teamname": "屌丝队",
//                           "nickname": "小仙子",
//                           "headimg": "http://121.40.220.245/Public/upload/header/143541568286634.jpg",
//                           "sex": "1",
//                           "postion": "守门员",
//                           "role": "2"
//                       },
//                       {
//                           "uid": "11",
//                           "teamid": "10",
//                           "totalscore": "3",
//                           "teamname": "无敌队",
//                           "nickname": "囧你个囧",
//                           "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//                           "sex": "1",
//                           "postion": "后卫",
//                           "role": "1"
//                       },
//                       {
//                           "uid": "12",
//                           "teamid": "10",
//                           "totalscore": "0",
//                           "teamname": "无敌队",
//                           "nickname": "夏敏洁",
//                           "headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//                           "sex": "0",
//                           "postion": "前锋",
//                           "role": "2"
//                       }
//                       ],
//    "totalassistslist": [
//                         {
//                             "uid": "12",
//                             "teamid": "10",
//                             "totalassists": "5",
//                             "teamname": "无敌队",
//                             "nickname": "夏敏洁",
//                             "headimg": "http://121.40.220.245/Public/upload/header/143720775237282.jpg",
//                             "sex": "0",
//                             "postion": "前锋",
//                             "role": "2"
//                         },
//                         {
//                             "uid": "10",
//                             "teamid": "11",
//                             "totalassists": "1",
//                             "teamname": "屌丝队",
//                             "nickname": "小仙子",
//                             "headimg": "http://121.40.220.245/Public/upload/header/143541568286634.jpg",
//                             "sex": "1",
//                             "postion": "守门员",
//                             "role": "2"
//                         },
//                         {
//                             "uid": "11",
//                             "teamid": "10",
//                             "totalassists": "0",
//                             "teamname": "无敌队",
//                             "nickname": "囧你个囧",
//                             "headimg": "http://121.40.220.245/Public/upload/header/143178200116851.jpg",
//                             "sex": "1",
//                             "postion": "后卫",
//                             "role": "1"
//                         }
//                         ]
//}
//}