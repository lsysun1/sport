//
//  SP_RaceDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceDetailsViewController.h"
#import "SP_RaceDetailsCell.h"
#import "SP_NewsListCell.h"
#import "SP_Race_Rank.h"
#import "SP_RaceDataRankCell.h"
#import "SP_Race_Data_PersonCell.h"
#import "SP_Race_MVP.h"
#import "SP_Race_Defen.h"
#import "SP_Race_ZhuGong.h"

#import "SP_Details_DataViewController.h"
#import "SP_Details_NewsViewController.h"
#import "SP_Details_RaceViewController.h"

#import "SP_MatchLeagueList.h"
#import "SP_RaceDetailsViewController.h"



@interface SP_RaceDetailsViewController ()
{
    SP_Details_NewsViewController *_newsVC;
    SP_Details_RaceViewController *_raceVC;
    SP_Details_DataViewController *_dataVC;
    
    
    IBOutlet UIView *_itemView;
    IBOutlet UIButton *_raceBtn;
    IBOutlet UIButton *_newsBtn;
    IBOutlet UIButton *_dataBtn;
    UIImageView *_itemLine;
    UIButton *_selectedBtn;
    


}
@end

@implementation SP_RaceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if(self.ballType == 0 ){
//        [self setNavigationBar:@"篮球比赛详情"];
//    }else{
//        [self setNavigationBar:@"足球比赛详情"];
//    }
    
    
    
    NSLog(@"比赛详情控制器 ballType -- %d",self.ballType);
    
    [self setNavigationBar:self.listModel.name];
    
    [self addBackButton];

    [self setUpItemView];
    
    SP_Details_RaceViewController *raceVC=[[SP_Details_RaceViewController alloc] init];
    raceVC.listModel = self.listModel;
    _raceVC = raceVC;
    [self.view insertSubview:_raceVC.view atIndex:0];
    
    
}


- (void)setBallType:(int)ballType
{
    _ballType = ballType;
}



#pragma mark - 顶部选择菜单
- (void) setUpItemView
{
    _itemView.frame = CGRectMake(0, 64, ScreenWidth, 38);
    _itemView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _newsBtn.frame = CGRectMake(ScreenWidth/2 -35 , 4, 70, 30);
    CGFloat newsMinX = CGRectGetMinX(_newsBtn.frame);
    CGFloat newsMaxX = CGRectGetMaxX(_newsBtn.frame);
    
    _raceBtn.frame = CGRectMake(newsMinX - 110, 4, 70, 30);
    _dataBtn.frame = CGRectMake(newsMaxX + 40, 4, 70, 30);
    
    [_newsBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_raceBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_dataBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    _raceBtn.selected = YES;
    _selectedBtn = _raceBtn;
    
    UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xfd7f45)];
    CGFloat raceMinX = CGRectGetMinX(_raceBtn.frame);
    CGFloat raceMaxY = CGRectGetMaxY(_raceBtn.frame);
    _itemLine = [[UIImageView alloc]init];
    _itemLine.frame = CGRectMake(raceMinX +1, raceMaxY , 70, 4);
    _itemLine.image = lineImg;
    [_itemView addSubview:_itemLine];
    
    
}

#pragma mark 顶部选择菜单按钮点击方法
- (void) menuBtnDidClick:(UIButton *)btn
{
    //比赛Btn的Tag是0
    //新闻Btn的Tag是1
    //数据Btn的Tag是2
    
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
    
    
    
    
    
    CGFloat raceMinX = CGRectGetMinX(_raceBtn.frame);
    CGFloat raceMaxY = CGRectGetMaxY(_raceBtn.frame);
    CGFloat newsMinX = CGRectGetMinX(_newsBtn.frame);
    CGFloat dataMinX = CGRectGetMinX(_dataBtn.frame);
    
    CGRect lineOneRect = CGRectMake(raceMinX +1, raceMaxY , 70, 4);
    CGRect lineTwoRect = CGRectMake(newsMinX +1, raceMaxY , 70, 4);
    CGRect lineThreeRect = CGRectMake(dataMinX +1, raceMaxY , 70, 4);

    switch (btn.tag) {
        case 0:
        {
            _itemLine.frame = lineOneRect;
            
            if (_raceVC.view.superview==nil)//判断是否为根视图
            {
                if (_raceVC==nil)//判断视图控制器是否初始化
                {
                    SP_Details_RaceViewController *raceVC=[[SP_Details_RaceViewController alloc] init];
                    raceVC.listModel = self.listModel;
                    _raceVC = raceVC;
                }
                [_dataVC.view removeFromSuperview];
                [_newsVC.view removeFromSuperview];
                [self.view insertSubview:_raceVC.view atIndex:0];
            }
            
        }
            break;
        case 1:
        {
            _itemLine.frame = lineTwoRect;
            
            if (_newsVC.view.superview==nil)//判断是否为根视图
            {
                if (_newsVC==nil)//判断视图控制器是否初始化
                {
                    SP_Details_NewsViewController *newsVC=[[SP_Details_NewsViewController alloc] init];
                    newsVC.listModel = self.listModel;
                    _newsVC = newsVC;
                }
                [_dataVC.view removeFromSuperview];
                [_raceVC.view removeFromSuperview];
                [self.view insertSubview:_newsVC.view atIndex:0];
            }
        }
            break;
        case 2:
        {
            _itemLine.frame = lineThreeRect;
         
            if (_dataVC.view.superview==nil)//判断是否为根视图
            {
                if (_dataVC==nil)//判断视图控制器是否初始化
                {
                    SP_Details_DataViewController *dataVC=[[SP_Details_DataViewController alloc] init];
                    dataVC.listModel = self.listModel;
                    _dataVC = dataVC;
                    _dataVC.ballType = self.ballType;
                }
                [_raceVC.view removeFromSuperview];
                [_newsVC.view removeFromSuperview];
                [self.view insertSubview:_dataVC.view atIndex:0];
            }
        }
            break;
        default:
            break;
    }
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
