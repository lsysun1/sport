//
//  SP_MvpViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_MatchInfo_TopData,SP_MatchLeagueList;
@interface SP_MvpViewController : BaseViewController
@property (nonatomic,strong) SP_MatchInfo_TopData *topData;
@property (nonatomic,strong) SP_MatchLeagueList *listModel;

@end
