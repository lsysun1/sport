//
//  SP_BasketBallViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BasketBallViewController.h"
#import "SP_RaceListCell.h"
#import "SP_RaceDetailsViewController.h"
#import "SP_MatchLeagueList.h"

@interface SP_BasketBallViewController () <UITableViewDelegate,UITableViewDataSource>
{
    int _ballType; //足球是1,篮球0
    UITableView *_tableView;
    NSMutableArray *_listArr;
}

@end

@implementation SP_BasketBallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _listArr = [[NSMutableArray alloc]init];
    
    _ballType = 0;
    [self setUpTableView];
    
    
    NSString *type = @"0";
    NSString *location = [self urlEncodeValue:@"成都"];
    [self asyshowHUDView:WAITTING CurrentView:self];
    
    
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"MATCHLEAGUELIST --- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_MATCHLEAGUELIST){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchLeagueList *model = [SP_MatchLeagueList objectWithKeyValues:dict];
                    [_listArr addObject:model];
                }
            }
            [_tableView reloadData];
            [_tableView.header endRefreshing];
        }
    }
}

#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 147, ScreenWidth, ScreenHeight - 147 - 48)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 105;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(locdNewData)];

    
    [self.view addSubview:_tableView];
}


#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceListCell *cell = [SP_RaceListCell cellWithTableView:tableView];
    
    SP_MatchLeagueList *listModel = _listArr[indexPath.row];
    cell.listModel = listModel;
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceDetailsViewController *vc = [[SP_RaceDetailsViewController alloc]init];
    vc.ballType =_ballType;
    
    SP_MatchLeagueList *listModel = _listArr[indexPath.row];
    vc.listModel = listModel;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}

#pragma mark 下拉重新加载数据
- (void)locdNewData
{
    [_listArr removeAllObjects];
    
    NSString *type = @"0";
    NSString *location = [self urlEncodeValue:@"成都"];
    [self asyshowHUDView:WAITTING CurrentView:self];
}



@end
