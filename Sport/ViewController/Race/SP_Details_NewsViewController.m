//
//  SP_Details_NewsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/15.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Details_NewsViewController.h"
#import "SP_NewsListCell.h"
#import "SP_MatchLeagueList.h"
#import "SP_MatchList.h"
#import "SP_NewsList.h"
#import "SP_NewsDetailsViewController.h"

@interface SP_Details_NewsViewController () <UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSMutableArray *_newsListArr;
}
@end

@implementation SP_Details_NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _newsListArr = [[NSMutableArray alloc]init];
    
    [self setUpTableView];
    
    NSString *matchleagueid = self.listModel.matchleagueid;
    [self setHTTPRequest:GETRACENEWSLIST(@"0", @"10", matchleagueid) Tag:TAG_HTTP_GETRACENEWSLIST];


}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETRACENEWSLIST){
            NSLog(@"TAG_HTTP_GETRACENEWSLIST -- resopnse:%@",request.responseString);

            if (HTTP_SUCCESS(code))
            {
                NSArray *listArr = dictionary[@"msg"];
                for(NSDictionary *dict in listArr){
                    SP_NewsList *model = [SP_NewsList objectWithKeyValues:dict];
                    [_newsListArr addObject:model];
                }
                
                NSLog(@"_newsListArr.count -- %ld",_newsListArr.count);
                [_tableView reloadData];
                
            }
        }
        
    }
    
}

#pragma mark - SetUpRaceTableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 102, ScreenWidth, ScreenHeight - 102)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 84;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _newsListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsListCell *cell = [SP_NewsListCell cellWithTableView:tableView];
    SP_NewsList *model = _newsListArr[indexPath.row];
    cell.model = model;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsDetailsViewController *vc = [[SP_NewsDetailsViewController alloc]init];
    SP_NewsList *model = _newsListArr[indexPath.row];
    vc.newsList = model;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}

@end
