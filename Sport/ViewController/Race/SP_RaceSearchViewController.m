//
//  SP_RaceSearchViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RaceSearchViewController.h"
#import "SP_RaceListCell.h"
#import "SP_MatchLeagueList.h"
#import "SP_RaceDetailsViewController.h"

@interface SP_RaceSearchViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITextField * _textfInput;
    UITableView *_tableView;
    NSMutableArray *_arr;

}
@end

@implementation SP_RaceSearchViewController

- (id)init
{
    self = [super init];
    if(self){
        _arr = [NSMutableArray array];
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"比赛搜索"];
    [self addBackButton];
    [self _initSearchView];
    [self setUpTableView];
    // Do any additional setup after loading the view from its nib.
}


- (void)_initSearchView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, [self viewTop]-1, ScreenWidth+2, 55)];
    
    [self.view addSubview:view];
    
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView * inputView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-20*2, 34)];
    [view addSubview:inputView];
    inputView.backgroundColor = [UIColor whiteColor];
    inputView.layer.borderWidth = 1;
    
    inputView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    inputView.layer.cornerRadius = 2;
    
    _textfInput = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, inputView.frame.size.width-60, 24)];
    [inputView addSubview:_textfInput];
    _textfInput.delegate = self;
    _textfInput.placeholder = @"请输入比赛名称";
    _textfInput.returnKeyType = UIReturnKeySearch;
    
    UIView * viewVerline = [[UIView alloc] initWithFrame:CGRectMake(inputView.frame.size.width - 50, 5, 1, 24)];
    [inputView addSubview:viewVerline];
    viewVerline.backgroundColor = UIColorFromRGB(0xe5e5e5);
    
    UIButton * btnSearch = GET_BUTTON(CGRectMake(inputView.frame.size.width-50, 0, 40, 34), 14, NO, nil);
    [inputView addSubview:btnSearch];
    [btnSearch setImage:[UIImage imageNamed:@"person_search"] forState:UIControlStateNormal];
    [btnSearch setImageEdgeInsets:UIEdgeInsetsMake(7, 10, 7, 10)];
    [btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
}



#pragma -mark Btn Action
- (void)btnSearchClick{
    
    NSString *name = [self urlEncodeValue:_textfInput.text];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:MATCHLEAGUESEARCH(name) Tag:TAG_HTTP_MATCHLEAGUESEARCH];
}

#pragma -mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    //直接请求接口
    [self btnSearchClick];
    
    return YES;
}

#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, [self viewTop]-1 + 55, ScreenWidth, ScreenHeight -[self viewTop] + 1 - 55)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 105;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_tableView];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_MATCHLEAGUESEARCH){
//            NSLog(@"res -- %@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchLeagueList *model = [SP_MatchLeagueList objectWithKeyValues:dict];
                    [_arr addObject:model];
                }
            }
            
            [_tableView reloadData];
            
        }
    }
}
#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceListCell *cell = [SP_RaceListCell cellWithTableView:tableView];
    
    SP_MatchLeagueList *model =_arr[indexPath.row];
    cell.listModel = model;
    
    return cell;
}




- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceDetailsViewController *vc = [[SP_RaceDetailsViewController alloc]init];
    
    SP_MatchLeagueList *listModel = _arr[indexPath.row];
    vc.listModel = listModel;
    vc.ballType = [listModel.type intValue];

    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
