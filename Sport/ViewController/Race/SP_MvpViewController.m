//
//  SP_MvpViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MvpViewController.h"
#import "SP_MvpCell.h"
#import "SP_MatchInfo_TopData.h"
#import "SP_MvpModel.h"
#import "SP_MatchLeagueList.h"
#import "SP_UerHomeViewController.h"

@interface SP_MvpViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_aTeamArr;
    NSMutableArray *_bTeamArr;
    NSMutableDictionary *_aTeamInfo;
    NSMutableDictionary *_bTeamInfo;

}
@end

@implementation SP_MvpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _aTeamArr = [[NSMutableArray alloc]init];
    _bTeamArr = [[NSMutableArray alloc]init];
    
    
    
    [self setNavigationBar:@"MVP"];
    [self addBackButton];
    [self setUpTableView];
    
    NSString *mid = self.topData.matchid;
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETMATCHMVPLIST(GETUSERID,mid) Tag:TAG_HTTP_GETMATCHMVPLIST];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"GETMATCHMVPLIST --- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETMATCHMVPLIST){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msgDict = dictionary[@"msg"];
                
                _aTeamInfo = msgDict[@"aTeamData"];
                _bTeamInfo = msgDict[@"bTeamData"];
                
                NSArray *aTeamArray = msgDict[@"aTeamPlayer"];
                NSArray *bTeamArray = msgDict[@"bTeamPlayer"];
                
                for(NSDictionary *dict in aTeamArray){
                    SP_MvpModel *model = [SP_MvpModel objectWithKeyValues:dict];
                    [_aTeamArr addObject:model];
                }
                
                for(NSDictionary *dict in bTeamArray){
                    SP_MvpModel *model = [SP_MvpModel objectWithKeyValues:dict];
                    [_bTeamArr addObject:model];
                }
                
            }
            
            [_tableView reloadData];
        }else if (request.tag == TAG_HTTP_SUPPORTMVP){
            NSLog(@"TAG_HTTP_SUPPORTMVP --- resopnse:%@",request.responseString);

            if(HTTP_SUCCESS(code)){
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
                
                [_aTeamArr removeAllObjects];
                [_bTeamArr removeAllObjects];
                
                NSString *mid = self.topData.matchid;
                [self asyshowHUDView:WAITTING CurrentView:self];
                [self setHTTPRequest:GETMATCHMVPLIST(GETUSERID,mid) Tag:TAG_HTTP_GETMATCHMVPLIST];
            }else{
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
            }
            
            
        }
    }
}



- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 75;
    _tableView.separatorStyle = 0;
    [self.view addSubview:_tableView];
}


#pragma mark - _talbleView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _aTeamArr.count >= _bTeamArr.count ? _aTeamArr.count : _bTeamArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_MvpCell *cell = [SP_MvpCell cellWithTableView:tableView];
    
    SP_MvpModel *aPlayer = _aTeamArr[indexPath.row];
    SP_MvpModel *bPlayer = _bTeamArr[indexPath.row];

    cell.aTeamMvp = aPlayer;
    cell.bTeamMvp = bPlayer;
    
    
    
    cell.aPlayerBtn.tag = 100 + indexPath.row;
    cell.bPlayerBtn.tag = 200 + indexPath.row;
    
    cell.goAPlayer.tag =  600 + indexPath.row;
    cell.goBPlayer.tag =  900 + indexPath.row;

    
    [cell.goAPlayer addTarget:self action:@selector(goAInfoVCBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.goBPlayer addTarget:self action:@selector(goBInfoVCBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    
    
    [cell.aPlayerBtn addTarget:self action:@selector(supportAteamPlayer:) forControlEvents:UIControlEventTouchUpInside];
    [cell.bPlayerBtn addTarget:self action:@selector(supportBteamPlayer:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (void) goAInfoVCBtnDidClick:(UIButton *)btn
{
    SP_MvpModel *aPlayer = _aTeamArr[btn.tag - 600];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = aPlayer.uid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}

- (void) goBInfoVCBtnDidClick:(UIButton *)btn
{
    SP_MvpModel *bPlayer = _bTeamArr[btn.tag - 900];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = bPlayer.uid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}



- (void) supportAteamPlayer:(UIButton *)btn
{
    
    SP_MvpModel *aPlayer = _aTeamArr[btn.tag - 100];
    
    [self setHTTPRequest:SUPPORTMVP(GETUSERID, aPlayer.uid, self.topData.ateamid, self.topData.matchid) Tag:TAG_HTTP_SUPPORTMVP];
    
}




- (void) supportBteamPlayer:(UIButton *)btn
{
    SP_MvpModel *bPlayer = _bTeamArr[btn.tag - 200];
    
    [self setHTTPRequest:SUPPORTMVP(GETUSERID, bPlayer.uid, self.topData.bteamid, self.topData.matchid) Tag:TAG_HTTP_SUPPORTMVP];

}





- (void) setTopData:(SP_MatchInfo_TopData *)topData
{
    _topData = topData;
}









#pragma mark 自定义SectionView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 90;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 90)];
    view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 200, 40)];
    title.font = [UIFont systemFontOfSize:15];
    title.textColor = [UIColor grayColor];
    title.text = [NSString stringWithFormat:@"%@的MVP",self.listModel.name];
    [view addSubview:title];
    
    UILabel *notice = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 150, 25, 140, 14)];
    notice.font = [UIFont systemFontOfSize:13];
    notice.textColor = UIColorFromRGB(0xe74c38);
    notice.text = @"注:每个人只能投一票";
    [view addSubview:notice];
    
    
    UIView *teamView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, ScreenWidth, 50)];
    teamView.backgroundColor = [UIColor whiteColor];
    [view addSubview:teamView];
    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth / 2 - 0.5, 5, 1, 45)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [teamView addSubview:midLine];
    
    UIImageView *aTeamImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 35, 35)];
    [teamView addSubview:aTeamImg];
    NSURL *aIMG = [NSURL URLWithString:_aTeamInfo[@"img"]];
    [aTeamImg sd_setImageWithURL:aIMG];
    
    
    UILabel *aTeamLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(aTeamImg.frame) + 5, 20, 100, 14)];
    aTeamLabel.text = _aTeamInfo[@"name"];
    aTeamLabel.font = [UIFont systemFontOfSize:14];
    aTeamLabel.textColor = UIColorFromRGB(0x696969);
    [teamView addSubview:aTeamLabel];
    
    
    UIImageView *bTeamImg = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(midLine.frame) + 10, 10, 35, 35)];
    [teamView addSubview:bTeamImg];
    NSURL *bIMG = [NSURL URLWithString:_bTeamInfo[@"img"]];
    [bTeamImg sd_setImageWithURL:bIMG];

    
    UILabel *bTeamLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(bTeamImg.frame) + 5, 20, 100, 14)];
    bTeamLabel.text = _bTeamInfo[@"name"];
    bTeamLabel.font = [UIFont systemFontOfSize:14];
    bTeamLabel.textColor = UIColorFromRGB(0x696969);
    [teamView addSubview:bTeamLabel];
    
    
    
    
    
    
    
    
    
    
    
    
    return view;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
