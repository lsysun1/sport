//
//  SP_MoreJiFenViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MoreJiFenViewController.h"
#import "SP_MatchLeagueList.h"
#import "SP_RaceDataRankCell.h"
#import "SP_Race_Data_PersonCell.h"
#import "SP_MoreCell.h"

#import "SP_Race_Rank.h"
#import "SP_Race_MVP.h"
#import "SP_Race_Defen.h"
#import "SP_Race_ZhuGong.h"
#import "SP_Race_Rank_ZuQiu.h"
#import "SP_Race_MVP_Zu.h"
#import "SP_Race_ZhuG_Zu.h"
#import "SP_Race_Defen_Zu.h"

@interface SP_MoreJiFenViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_talbeview;
    
    NSArray *_keys;
    NSArray *_values;


}
@end

@implementation SP_MoreJiFenViewController



- (id)init{
    if(self = [super init]){
        _keys = [NSArray array];
        _values = [NSArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBar:self.listModel.name];
    [self addBackButton];
    
    
    _talbeview = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _talbeview.delegate = self;
    _talbeview.dataSource = self;
    _talbeview.separatorStyle = 0;
    _talbeview.rowHeight = 60;
    [self.view addSubview:_talbeview];
    
    if([self.type isEqualToString:@"1"]){
        _keys = [self.jifenDict allKeys];
        _values = [self.jifenDict allValues];

    
    }
    
    
}

- (void)setBallType:(int)ballType
{
    _ballType = ballType;
}


#pragma mark - UITableView DataSource Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([self.type isEqualToString:@"1"]){
        return _keys.count;
    }else{
        return 1;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.type isEqualToString:@"1"]){
        NSArray *temp  =_values[section];
        return temp.count;
    }else{
        return _arr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.type isEqualToString:@"1"]){
        NSArray *temp  =_values[indexPath.section];
        SP_RaceDataRankCell *cell = [SP_RaceDataRankCell cellWithTableView:tableView];
        cell.ballType = _ballType;
        NSDictionary *model =temp[indexPath.row];
        cell.model = model;
        cell.number = indexPath.row + 1;
        return cell;
    }else {
        SP_Race_Data_PersonCell *cell = [SP_Race_Data_PersonCell cellWithTableView:tableView];
        cell.number = indexPath.row + 1;
        
        if([self.type isEqualToString:@"2"]){
            cell.cellType = @"mvp";
            NSDictionary *model =_arr[indexPath.row];
            cell.model = model;
        }else if ([self.type isEqualToString:@"3"]){
            cell.cellType = @"score";
            NSDictionary *model =_arr[indexPath.row];
            cell.model = model;
        }else if ([self.type isEqualToString:@"4"]){
            cell.cellType = @"ass";
            NSDictionary *model =_arr[indexPath.row];
            cell.model = model;
        }
        
        return cell;
    }
    return nil;
}

#pragma mark - UITableView Delegate Method
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([self.type isEqualToString:@"1"]){
        return 60;
    }else{
        return 40;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if([self.type isEqualToString:@"1"]){

            if(self.ballType == 1){
                SP_Race_Rank_ZuQiu *view = [[SP_Race_Rank_ZuQiu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 60)];
                
                if(_keys.count != 0){
                    NSString *name = _keys[section];
                    view.name = [NSString stringWithFormat:@"%@",name];
                }
                
                return view;
                
            }else{
                SP_Race_Rank *view = [[SP_Race_Rank alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 60)];
                if(_keys.count != 0){
                    view.name = [NSString stringWithFormat:@"%@",_keys[0]];
                }
                
                return view;
            }
    }else if([self.type isEqualToString:@"2"]){

            if(self.ballType == 1){
                SP_Race_MVP_Zu *view = [[SP_Race_MVP_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_MVP *view = [[SP_Race_MVP alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }
            
    }else if([self.type isEqualToString:@"3"]){

            if(self.ballType == 1){
                SP_Race_Defen_Zu *view = [[SP_Race_Defen_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_Defen *view = [[SP_Race_Defen alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }
            
    }else if([self.type isEqualToString:@"4"]){

            if(self.ballType == 1){
                SP_Race_ZhuG_Zu *view = [[SP_Race_ZhuG_Zu alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
                
            }else{
                SP_Race_ZhuGong *view = [[SP_Race_ZhuGong alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
                return view;
            }
    }
    return nil;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
