//
//  SP_RaceDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@class SP_MatchLeagueList;
@interface SP_RaceDetailsViewController : BaseViewController

@property (nonatomic,strong) SP_MatchLeagueList *listModel;
@property (nonatomic,assign) int ballType; //足球是1,篮球0


@end
