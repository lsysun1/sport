//
//  SP_BeforeDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@class SP_MatchList,SP_MatchLeagueList;
@interface SP_BeforeDetailsViewController : BaseViewController

@property (nonatomic,strong) SP_MatchList *matchList;
@property (nonatomic,strong) SP_MatchLeagueList *listModel;

@end
