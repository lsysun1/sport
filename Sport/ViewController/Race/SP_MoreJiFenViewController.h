//
//  SP_MoreJiFenViewController.h
//  Sport
//
//  Created by 李松玉 on 15/8/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_MatchLeagueList;
@interface SP_MoreJiFenViewController : BaseViewController
@property (nonatomic,strong) SP_MatchLeagueList *listModel;
@property (nonatomic,copy) NSArray *arr;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,assign) int ballType;
@property (nonatomic,copy) NSDictionary *jifenDict;


@end
