//
//  SP_CompanyViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_CompanyViewController : BaseViewController


@property (nonatomic,assign)int index; //1足球,2篮球,3跑步,4其他

@property (nonatomic,strong)NSString * dateStr; //1足球,2篮球,3跑步,4其他

@property (nonatomic,strong)NSString * sexStr;

@end
