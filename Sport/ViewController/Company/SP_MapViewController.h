//
//  testgpsViewController.h
//  TrunkIndustryManageSystem
//
//  Created by WT_lyy on 15/5/21.
//  Copyright (c) 2015年 李璐西. All rights reserved.
//

#import "BaseViewController.h"
#import <MAMapKit/MAMapKit.h>

@class SP_LocationEntity;
@protocol SP_MapViewDelegate <NSObject>

- (void) bringLocaiotnInfoBack:(SP_LocationEntity *)locationEntity;

@end


@interface SP_MapViewController :BaseViewController

//@property (nonatomic,strong) NSString * e_id;
//@property (nonatomic,strong) NSArray * cusTomerArr;
//@property (nonatomic,assign) NSInteger isExecuted; // 0weikaishi  1executing 2over
//
//- (void)refreshMap;

@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, assign) id<SP_MapViewDelegate> delegate;


@end
