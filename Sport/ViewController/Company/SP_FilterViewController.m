//
//  SP_FilterViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/22.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_FilterViewController.h"
#import "SP_CompanyViewController.h"

#import "SP_TabBarViewController.h"

#define BTN_TAG_SEX_NO          @"101"
#define BTN_TAG_SEX_MAN         @"102"
#define BTN_TAG_SEX_WOMAN       @"103"

#define BTN_TAG_TIME_MON        @"201"
#define BTN_TAG_TIME_TUES       @"202"
#define BTN_TAG_TIME_WED        @"203"
#define BTN_TAG_TIME_THUR       @"204"
#define BTN_TAG_TIME_FRI        @"205"
#define BTN_TAG_TIME_SAT        @"206"
#define BTN_TAG_TIME_SUN        @"207"

#define BTN_TAG_TYPE_NO         @"301"
#define BTN_TAG_TYPE_FOOT       @"302"
#define BTN_TAG_TYPE_BASEKET    @"303"
#define BTN_TAG_TYPE_RUN        @"304"
#define BTN_TAG_TYPE_SNOOKER    @"305"
#define BTN_TAG_TYPE_ROOM       @"306"



@interface SP_FilterViewController ()
{
    UIScrollView *_scrollView;
    
    UIView *_sexView;
    UIView *_dayView;
    UIView *_typeView;
    
    UIButton *_sexSlectedBtn;
    UIButton *_daySlectedBtn;
    UIButton *_typeSlectedBtn;
    
    UIView *_locationView;
    
    UIView *_desView;
    
    UIButton *_submitBtn;
    
    NSString *_monday;
    NSString *_tuesday;
    NSString *_wednesday;
    NSString *_thursday;
    NSString *_friday;
    NSString *_saturday;
    NSString *_sunday;
    
    
    
}


@end

@implementation SP_FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"筛选活动"];
    [self addBackButton];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    [self.view addSubview:_scrollView];
    _scrollView.backgroundColor = UIColorFromRGB(0xfafafa);
    _scrollView.contentSize = CGSizeMake(ScreenWidth, 653 + 64);
    [self setUpSexView];
    [self setUpDayBtnView];
    [self setUpTypeView];
    [self setUpSubmitBtn];
    [self setUpTimeStr];
    
}







#pragma mark - 顶部的性别选择按钮
- (void) setUpSexView
{
    _sexView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth,116)];
    _sexView.backgroundColor = UIColorFromRGB(0xffffff);
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"性别";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"不限",@"男",@"女",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_SEX_NO,BTN_TAG_SEX_MAN,BTN_TAG_SEX_WOMAN,nil];
    
    UIImage *sexBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xe84c3d)];
    
    
    CGFloat btnX = 16;
    CGFloat btnY = 51;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:sexBtnBgImg superView:_sexView Tag:btnTag action:@selector(sexBtnDidClick:)];
        
        
        btnX += 61;
    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth, 5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    [_scrollView addSubview:_sexView];
    [_sexView addSubview:title];
    [_sexView addSubview:topLine];
    [_sexView addSubview:bottomView];
    
    
}

#pragma mark 性别选择按钮点击方法
- (void) sexBtnDidClick:(UIButton *)btn
{
    _sexSlectedBtn.selected = NO;
    _sexSlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _sexSlectedBtn = btn;
    

}



#pragma - mark 日期选择按钮
- (void) setUpDayBtnView
{
    _dayView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_sexView.frame), ScreenWidth, 116)];
    _dayView.backgroundColor = UIColorFromRGB(0xffffff);
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"运动时间";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"周一",@"周二",@"周三",@"周四",@"周五",@"星期六",@"星期天",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_TIME_MON,BTN_TAG_TIME_TUES,BTN_TAG_TIME_WED,BTN_TAG_TIME_THUR,BTN_TAG_TIME_FRI,BTN_TAG_TIME_SAT,BTN_TAG_TIME_SUN,nil];
    
    UIImage *dayBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xe77e23)];
    
    UIScrollView *btnScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(16, 51, ScreenWidth, 45)];
    btnScrollView.showsHorizontalScrollIndicator = NO;
    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:dayBtnBgImg superView:btnScrollView Tag:btnTag action:@selector(dayBtnDidClick:)];
        
        btnX += 61;
        btnScrollView.contentSize = CGSizeMake(btnX, 45);
        
    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth,5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    [_scrollView addSubview:_dayView];
    [_dayView addSubview:title];
    [_dayView addSubview:topLine];
    [_dayView addSubview:bottomView];
    [_dayView addSubview:btnScrollView];
    
}


- (void) dayBtnDidClick:(UIButton *)btn
{
    _daySlectedBtn.selected = NO;
    _daySlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _daySlectedBtn = btn;
    
}


#pragma mark - 设置运动种类选择
- (void) setUpTypeView
{
    _typeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_dayView.frame), ScreenWidth, 116)];
    _typeView.backgroundColor = UIColorFromRGB(0xffffff);
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"运动项目";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"不限",@"足球",@"篮球",@"跑步",@"台球",@"健身房",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_TYPE_NO,BTN_TAG_TYPE_FOOT,BTN_TAG_TYPE_BASEKET,BTN_TAG_TYPE_RUN,BTN_TAG_TYPE_SNOOKER,BTN_TAG_TYPE_ROOM,nil];
    
    UIImage *typeBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xef1c40f)];
    
    UIScrollView *btnScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(16, 51, ScreenWidth, 45)];
    btnScrollView.showsHorizontalScrollIndicator = NO;
    
    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:typeBtnBgImg superView:btnScrollView Tag:btnTag action:@selector(typeBtnDidClick:)];
        
        btnX += 61;
        btnScrollView.contentSize = CGSizeMake(btnX, 45);
        
    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth, 5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    [_scrollView addSubview:_typeView];
    [_typeView addSubview:title];
    [_typeView addSubview:topLine];
    [_typeView addSubview:bottomView];
    [_typeView addSubview:btnScrollView];
    
}

- (void) typeBtnDidClick:(UIButton *)btn
{
    _typeSlectedBtn.selected = NO;
    _typeSlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _typeSlectedBtn = btn;
    NSLog(@"_typeSlectedBtn.tag -- %ld",_typeSlectedBtn.tag);
    
}




#pragma mark - 所有按钮的方法
- (void) createBtns:(NSString *)title Frame:(CGRect)frame BgImg:(UIImage *)img superView:(UIView *)supview Tag:(NSInteger )tag action:(SEL)action
{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    btn.tag = tag;
    
    UIImage *btnBg = [GT_Tool createImageWithColor:UIColorFromRGB(0xffffff)];
    
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0x6a6a6a) forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    [btn setBackgroundImage:btnBg forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateSelected];
    btn.layer.cornerRadius = 22.5;
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = UIColorFromRGB(0x6a6a6a).CGColor;
    btn.layer.masksToBounds = YES;
    
    
    if(tag % 10 ==1){
        btn.selected = YES;
        btn.layer.borderWidth = 0;
        
        if(tag / 100 == 1){
            _sexSlectedBtn = btn;
            NSLog(@"_sexSlectedBtn -- %ld",_sexSlectedBtn.tag);
        }else if (tag / 100 == 2)
        {
            _daySlectedBtn = btn;
            NSLog(@"_daySlectedBtn -- %ld",_daySlectedBtn.tag);
            
        }else if (tag / 100 == 3){
            _typeSlectedBtn = btn;
            NSLog(@"_typeSlectedBtn -- %ld",_typeSlectedBtn.tag);
            
        }
    }
    
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    
    [supview addSubview:btn];
    
}






#pragma mark - 创建确认按钮
- (void) setUpSubmitBtn
{
    _submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_typeView.frame) + 40, 200, 40)];
    _submitBtn.layer.cornerRadius = 10;
    _submitBtn.layer.masksToBounds = YES;
    _submitBtn.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [_submitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:_submitBtn];
}

- (void) submitBtnDidClick
{

    
    NSString *type = [NSString stringWithFormat:@"%ld",(_typeSlectedBtn.tag % 300) - 1];
    
    NSString *sex;
    if(_sexSlectedBtn.tag == [BTN_TAG_SEX_NO integerValue])
    {
        sex = @"10";
    }else if(_sexSlectedBtn.tag == [BTN_TAG_SEX_MAN integerValue]){
        sex = @"1";
    }else{
        sex = @"0";
    }
    
    NSString *date;
    switch (_daySlectedBtn.tag % 200) {
        case 1:
            date = _monday;
            break;
        case 2:
            date = _tuesday;
            break;
        case 3:
            date = _wednesday;
            break;
        case 4:
            date = _thursday;
            break;
        case 5:
            date = _friday;
            break;
        case 6:
            date = _saturday;
            break;
        case 7:
            date = _sunday;
            break;
        default:
            break;
    }
    

    NSDate * nowDate = [UIUtils dateFromFomate:date formate:@"yyyy-MM-dd"];
    
    if ([nowDate earlierDate:[NSDate date]] == nowDate) {
        
        NSTimeInterval  oneDay = 24*60*60*1;  //1天的长度
        
        nowDate = [nowDate initWithTimeIntervalSinceNow:+oneDay*7 ];
    }
    
    date = [UIUtils stringFromFomate:nowDate formate:@"yyyy-MM-dd"];
    
    SP_CompanyViewController * comVc = [self CompanyViewControllerFromNav];
    
    if (comVc) {
        
        comVc.sexStr = sex;
        
        comVc.dateStr = date;
        
        comVc.index = [type intValue];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - 获取日期
- (void) setUpTimeStr
{
    NSDate *newDate = [NSDate date];
    if (newDate == nil) {
        newDate = [NSDate date];
    }
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:2];//设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitWeekOfMonth startDate:&beginDate interval:&interval forDate:newDate];
    //分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval-1];
    }else {
        return;
    }
    
    
    
    NSDate *one  = beginDate;
    NSDate *two = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([beginDate timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *three = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([two timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *four = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([three timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *five= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([four timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *six= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([five timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *seven= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([six timeIntervalSinceReferenceDate] + 24*3600)];
    
    
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"YYYY-MM-dd"];
    _monday = [myDateFormatter stringFromDate:one];
    _tuesday = [myDateFormatter stringFromDate:two];
    _wednesday = [myDateFormatter stringFromDate:three];
    _thursday = [myDateFormatter stringFromDate:four];
    _friday = [myDateFormatter stringFromDate:five];
    _saturday = [myDateFormatter stringFromDate:six];
    _sunday = [myDateFormatter stringFromDate:seven];
    
    
    
    
    
}




- (SP_CompanyViewController *)CompanyViewControllerFromNav{

    SP_CompanyViewController * comVc = nil;
    
    for (UIViewController * vc in self.navigationController.viewControllers) {
        
        if ([vc isKindOfClass:[SP_TabBarViewController class]]) {
            
            SP_TabBarViewController * tabVc = (SP_TabBarViewController *)vc;
            
            for (UIViewController * vvcc in tabVc.viewControllers) {
                
                if ([vvcc isKindOfClass:[SP_CompanyViewController class]]) {
                    
                    comVc = (SP_CompanyViewController *)vvcc;
                    
                    break;
                }
            }
        }
    }
    
    return comVc;
}


@end
