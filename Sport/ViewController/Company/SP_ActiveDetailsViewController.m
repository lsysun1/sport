//
//  SP_ActiveDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ActiveDetailsViewController.h"
#import "SP_CompanyHeadView.h"
#import "FaceView.h"
#import "MyTextView.h"
#import "SP_YhbList.h"
#import "SP_YhbCmtFrame.h"
#import "SP_YhbCommentEntity.h"
#import "SP_YhbCmtCell.h"
#import "SP_YhbReCmtEntity.h"
#import "SP_YhbReCmtFrame.h"
#import "SP_YhbReCmtCell.h"
#import "SP_YhbHfBtn.h"
#import "SP_YhbReCmtTextView.h"
#import "SP_CommentTextSpecial.h"
#import "SP_YhbInfoEntity.h"
#import "SP_UerHomeViewController.h"

@interface SP_ActiveDetailsViewController () <UITableViewDataSource,UITableViewDelegate,FaceViewDelegate,UITextViewDelegate,SP_YhbReCmtTextViewDelegate>
{
    UITableView *_tableView;
    UIView *_sendMsgView;
    
    MyTextView *_msgTextView;
    UIButton *_msgSendBtn;
    FaceView *_faceView;
    SP_CompanyHeadView *_headView;
    
    NSMutableArray *_cmtArr;
    
    NSString    *_currentName; //当前回复人的Name
    NSIndexPath *_currentIndex;
    NSString    *_currentHfuid;
    NSString    *_currentYhbplid;
    
    
    int         _currentType;       // 0  直接评论      1  评论回复
}

@end

@implementation SP_ActiveDetailsViewController


- (id)init
{
    self = [super init];
    if(self){
        _cmtArr         = [NSMutableArray array];
        _currentName    = [[NSString alloc]init];
        _currentHfuid   = [[NSString alloc]init];
        _currentYhbplid = [[NSString alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"活动详情"];
    [self addBackButton];
    
    [self createTableView];
    [self setUpSendMsgView];
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil] ;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil] ;

    
    _currentType = 0;
    
    [self setHTTPRequest:GETYHBINFO(GETUSERID,self.yhbList.yhbid) Tag:TAG_HTTP_GETYHBINFO];
    [self setHTTPRequest:GETYHBCOMMENT(GETUSERID, self.yhbList.yhbid, @"0", @"10") Tag:TAG_HTTP_GETYHBCOMMENT];
}



#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_ADDCOLLECT){
            //  添加收藏
            
            if (HTTP_SUCCESS(code))
            {
                _headView.markBtn.selected = YES;
                [self showMessage:@"收藏成功"];
            }else{
                [self showMessage:@"收藏失败"];
            }
            
        }else if (request.tag == TAG_HTTP_GETYHBCOMMENT){
            if (HTTP_SUCCESS(code)){
                NSLog(@"活动详情评论 --- %@",request.responseString);
                NSArray *msg = dictionary[@"msg"];
                
                for(NSDictionary *dict in msg){
                    SP_YhbCommentEntity *cmtEntity = [SP_YhbCommentEntity objectWithKeyValues:dict];
                    SP_YhbCmtFrame *cmtFrame = [[SP_YhbCmtFrame alloc]init];
                    cmtFrame.cmtEntity = cmtEntity;
                    
                    for(NSDictionary *reDict in cmtEntity.hfdata){
                        SP_YhbReCmtEntity *reCmtEntity = [SP_YhbReCmtEntity objectWithKeyValues:reDict];
                        SP_YhbReCmtFrame *reCmtFrame = [[SP_YhbReCmtFrame alloc]init];
                        reCmtFrame.reCmtEntity = reCmtEntity;
                        
                        [cmtFrame.reCmtArr addObject:reCmtFrame];
                    }
                    
                    [_cmtArr addObject:cmtFrame];

                }
                [_tableView reloadData];
            }
        }else if (request.tag == TAG_HTTP_YHBCOMMENT){
            //  评论活动
            if(HTTP_SUCCESS(code)){
                [self showMessage:@"评论成功"];
                [_cmtArr removeAllObjects];
                [self setHTTPRequest:GETYHBCOMMENT(GETUSERID, self.yhbList.yhbid, @"0", @"10") Tag:TAG_HTTP_GETYHBCOMMENT];
                _msgTextView.text = @"";
                [_msgTextView resignFirstResponder];
                _currentType = 0;
                _msgTextView.placehoder = @"说几句吧";
            }else{
                [self showMessage:@"评论失败"];
            }
        }else if (request.tag == TAG_HTTP_REPLYCOMMENT){
            // 评论回复
            if(HTTP_SUCCESS(code)){
                [self showMessage:@"回复成功"];
                [_cmtArr removeAllObjects];
                [self setHTTPRequest:GETYHBCOMMENT(GETUSERID, self.yhbList.yhbid, @"0", @"10") Tag:TAG_HTTP_GETYHBCOMMENT];
                _msgTextView.text = @"";
                [_msgTextView resignFirstResponder];
                _currentType = 0;
                _msgTextView.placehoder = @"说几句吧";
            }else{
                [self showMessage:@"回复失败"];
            }
        }else if (request.tag == TAG_HTTP_GETYHBINFO){
            //  回复详情
            if(HTTP_SUCCESS(code)){
                SP_YhbInfoEntity *infoEntity = [SP_YhbInfoEntity objectWithKeyValues:dictionary[@"msg"]];
                _headView.infoEntity = infoEntity;
                _tableView.tableHeaderView = _headView;
            }
        }
    }
}


//- (void)tapClick{
//    
//    [_msgTextView resignFirstResponder];
//}

#pragma mark - 设置底部的聊天框
- (void) setUpSendMsgView
{
    _sendMsgView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 40, ScreenWidth, 40)];
    _sendMsgView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    UIButton *faceBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 8, 20, 20)];
    [faceBtn setBackgroundImage:[UIImage imageNamed:@"ico_face"] forState:UIControlStateNormal];
    [faceBtn setBackgroundImage:[UIImage imageNamed:@"ico_face_pressed"] forState:UIControlStateHighlighted];
    [faceBtn addTarget:self action:@selector(faceBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_sendMsgView addSubview:faceBtn];
    

    UIView *textBgView = [[UIView alloc]initWithFrame:CGRectMake(35, 5, ScreenWidth - 100 , 30)];
    textBgView.backgroundColor = UIColorFromRGB(0xffffff);
    textBgView.layer.borderColor = UIColorFromRGB(0xdcdcdc).CGColor;
    textBgView.layer.borderWidth = 1;
    textBgView.layer.cornerRadius = 5;
    
    
    _msgTextView = [[MyTextView alloc]initWithFrame:CGRectMake(5, 0, ScreenWidth - 110, 30)];
    _msgTextView.placehoder = @"说几句吧";
    _msgTextView.font = [UIFont systemFontOfSize:15];
    _msgTextView.delegate = self;
    
    _msgSendBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(textBgView.frame) + 10, 0, 40, 40)];
    [_msgSendBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [_msgSendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [_msgSendBtn addTarget:self action:@selector(sendMsgBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    _msgSendBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    
    
    [self.view addSubview:_sendMsgView];
    [_sendMsgView addSubview:topLine];
    [_sendMsgView addSubview:textBgView];
    [textBgView addSubview:_msgTextView];
    [_sendMsgView addSubview:_msgSendBtn];

}


#pragma mark - 发送留言
- (void) sendMsgBtnDidClick
{
    if(_msgTextView.text.length == 0){
        return;
    }

    if(_currentType == 0){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.yhbList.yhbid forKey:@"yhbid"];
        [dict setObject:_msgTextView.text forKey:@"content"];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:YHBCOMMENT Dictionary:dict Tag:TAG_HTTP_YHBCOMMENT];
    }else{
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:_currentYhbplid forKey:@"yhbplid"];
        [dict setObject:_currentHfuid forKey:@"hfuid"];
        [dict setObject:_msgTextView.text forKey:@"content"];
        
        NSLog(@"评论回复dict --- %@",dict);
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:REPLYCOMMENT Dictionary:dict Tag:TAG_HTTP_REPLYCOMMENT];
    }
}


- (void)faceBtnDidClick:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected == YES) {
        //        [_textField resignFirstResponder];
        _faceView = [[FaceView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 216)];
        _faceView.delegate = self;
        _msgTextView.inputView = _faceView;
        [_msgTextView reloadInputViews];
        [_msgTextView becomeFirstResponder];
    }else
    {
        _msgTextView.inputView = nil;
        [_msgTextView reloadInputViews];
        [_msgTextView becomeFirstResponder];
    }
}


#pragma mark - 设置_tableView
- (void) createTableView
{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 40)];
    _tableView.separatorStyle = 0;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _headView = [[SP_CompanyHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 316+34+20)];
    _headView.yhbList = self.yhbList;
    [_headView.markBtn addTarget:self action:@selector(markBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_headView.headBtn addTarget:self action:@selector(topHeadBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.view addSubview:_tableView];

}

- (void)topHeadBtnDidClick
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = self.yhbList.uid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _cmtArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SP_YhbCmtFrame *cmtFrame = _cmtArr[section];
    
    NSLog(@"cmtFrame.reCmtArr.count --- %ld",cmtFrame.reCmtArr.count);
    
    return 1 + cmtFrame.reCmtArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_YhbCmtCell *cell = [SP_YhbCmtCell cellWithTableView:tableView];
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        cell.cmtFrame = frame;
        
        frame.index = indexPath;
        
        SP_YhbCommentEntity *cmtEntity = frame.cmtEntity;
        
        cell.headButton.tag = [cmtEntity.uid integerValue];
        [cell.headButton addTarget:self action:@selector(cmtHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.hfButton addTarget:self action:@selector(hfBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }else{
        SP_YhbReCmtCell *cell =[SP_YhbReCmtCell cellWithTableView:tableView];
        cell.commentTextView.sdelegate = self;
        

        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        frame.index = indexPath;
        SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
        cell.frameModel = reFrame;
        
        return cell;
    }
}


- (void)cmtHeadBtnDidClick:(UIButton *)btn
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


/**
 *  回复按钮点击时间
 */
- (void) hfBtnDidClick:(SP_YhbHfBtn *)btn
{
    
    _currentType = 1;
    _currentName = btn.name;
    _currentIndex = btn.index;
    _currentHfuid = btn.hfuid;
    _currentYhbplid = btn.yhbplid;
    
    _msgTextView.placehoder = [NSString stringWithFormat:@"回复%@:",btn.name];
    [_msgTextView becomeFirstResponder];
    
    
    

}

/**
 *  点击回复评论的某一个人
*/
- (void) getSpecialBack:(SP_CommentTextSpecial *)special
{
    NSLog(@"%@  %@  %@  %@  %@",special.nickName,special.hfName,special.yhbplid,special.hfuid ,special.uid);
    
    _currentType = 1;
    
    if(special.nickName.length > 0){
        _currentName = special.nickName;
    }else{
        _currentName = special.hfName;
    }
    
    if(special.uid.length > 0){
        _currentHfuid = special.uid;
    }else{
        _currentHfuid = special.hfuid;
    }

    _currentYhbplid = special.yhbplid;
    
    _msgTextView.placehoder = [NSString stringWithFormat:@"回复%@:",_currentName];
    [_msgTextView becomeFirstResponder];

}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        return frame.cellHeight;
    }else{
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
        return reFrame.cellHeight;
    }
}



#pragma mark 收藏按钮点击方法
- (void) markBtnDidClick
{
    
//    *  type  0活动  1比赛  2场馆
//    *  linkid  对应的活动，比赛 场馆id
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:@"0" forKey:@"type"];
    [dict setObject:self.yhbList.yhbid forKey:@"linkid"];

    [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
}




- (void) setYhbList:(SP_YhbList *)yhbList
{
    _yhbList = yhbList;
}



-(void)faceViewDelegate:(NSInteger)tag{
    // 获得光标所在的位置
    _msgTextView.placehoder=@"";
    NSInteger location = _msgTextView.selectedRange.location;
    // 将UITextView中的内容进行调整（主要是在光标所在的位置进行字符串截取，再拼接你需要插入的文字即可）
    NSString *content = _msgTextView.text;
    if (tag == 118) {
        if (location == 0) {
            return;
        }
        NSString *result = [NSString stringWithFormat:@"%@%@",[content substringToIndex:location - 1],[content substringFromIndex:location]];
        _msgTextView.text = result;
        NSRange range;
        range.location = location - 1;
        range.length = 0;
        _msgTextView.selectedRange = range;
        //        [self textViewDidChange:myTextView_content];
        return;
    }
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"emoji" ofType:nil];
    NSString *str= [NSString stringWithContentsOfFile:plistPath encoding:NSUTF8StringEncoding error:nil];
    NSArray *array = [str componentsSeparatedByString:@"\n"];
    
    NSString *string = array[tag];
    NSRange rang1 = [string rangeOfString:@"["];
    NSRange rang2 = [string rangeOfString:@"]"];
    
    NSString *result = [NSString stringWithFormat:@"%@%@%@",[content substringToIndex:location],[string substringWithRange:NSMakeRange(rang1.location, rang2.location - rang1.location + 1)],[content substringFromIndex:location]];
    // 将调整后的字符串添加到UITextView上面
    _msgTextView.text = result;
    NSRange range;
    range.location = location + [[string substringWithRange:NSMakeRange(rang1.location, rang2.location - rang1.location + 1)] length];
    range.length = 0;
    _msgTextView.selectedRange = range;
    //    [self textViewDidChange:myTextView_content];
}



/**
 *  当键盘出现的时候，移动View的位置
 */
- (void)KeyBoardWillShow:(NSNotification *)note
{
    //  1.取得键盘最后的frame
    CGRect keyBoardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //  2.计算控制器view需要平移的距离
    CGFloat transformY = keyBoardFrame.origin.y - (self.view.frame.size.height);
    
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.transform = CGAffineTransformMakeTranslation(0, transformY);
        
    }];
}

- (void)KeyBoardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_msgTextView resignFirstResponder];
}



#pragma mark -textview delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([@"\n" isEqualToString:text] == YES)
    {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView{

}


- (void)textViewDidEndEditing:(UITextView *)textView{
    

    
}



@end
