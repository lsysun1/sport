//
//  SP_CompanyViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CompanyViewController.h"
#import "SP_CompanyCell.h"
#import "SP_ActiveDetailsViewController.h"
#import "SP_FilterViewController.h"
#import "SP_PostActiveViewController.h"
#import "SP_MapViewController.h"
#import "SP_LocationEntity.h"
#import "SP_YhbList.h"
#import "SP_GroundSearchViewController.h"
#import "SP_GroundEntity.h"
#import "GroundListCell.h"
#import "SP_GroundDetailsViewController.h"
#import "SP_AreaEntity.h"

#define kFirstComponent 0
#define kSubComponent 1

@interface SP_CompanyViewController () <UITableViewDelegate,UITableViewDataSource,SP_MapViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIView *_itemMenuView;
    UIImageView *_locationIMG;
    UILabel *_locationName;
    UIView *_locationLine;
    UIButton *_mapBtn;
    UIView *_mapLine;
    UIButton *_selectedItemBtn;
    
    UITableView *_talbleView;
    
    NSMutableArray *_yhbListArr;
    NSString *_currentType;
    NSString *_currentSex;
    
    NSString *_currentCity;
    NSString *_currentZone;
    
    UILabel * _labEmpty;
    
    UIButton *_responeBtn;
    
    UIButton *_locationBtn;
    
    SP_LocationEntity *_locationEntity;
    
    
    UIView *_pickerBgView;
    UIPickerView *_zonePickView;
    
    NSMutableArray *_allArea;
    NSMutableArray *_cityArr;
    NSMutableArray *_zoneArr;
    NSMutableArray *_tempArr;
    NSMutableArray *_subPickerArray;

}
@end

@implementation SP_CompanyViewController

- (id)init{

    self = [super init];
    
    if (self) {
        
        _index = 1;
        _yhbListArr = [[NSMutableArray alloc]init];
        _currentCity = [[NSString alloc]init];
        _currentZone = [[NSString alloc]init];
        _currentCity = GETSELECTEDCITYNAME;
        _currentZone = GETSELECTEDZONENAME;
        _allArea = [NSMutableArray array];
        _cityArr = [[NSMutableArray alloc]init];
        _zoneArr = [[NSMutableArray alloc]init];
        _tempArr = [[NSMutableArray alloc]init];
        _subPickerArray = [NSMutableArray array];

        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setObject:GETZONENAM forKey:SELECTEDZONENAME];

    
    if(GETUSERID){
        [self asyshowHUDView:@"请先登录!" CurrentView:self];
    }
    
    
    [self setNavigationBar:@"伙伴"];
    
//    [self setHTTPRequest:GETAREALIST  Tag:TAG_HTTP_GETAREALIST];

    [self addRightButton:nil Target:self Action:@selector(addBtnDidClick) ImageName:@"邀约" Frame:CGRectMake(ScreenWidth - 40, 29, 24, 23)];
    
    [self addRightButton:nil Target:self Action:@selector(filterBtnDidClick) ImageName:@"筛选" Frame:CGRectMake(ScreenWidth - 80, 29, 24, 23)];

    
    [self setUpItemMenu];
    [self createTableView];
    
    
    _labEmpty = GET_LABEL(CGRectMake(0, 0, ScreenWidth, 24), 17, NO, [UIColor blackColor], NSTextAlignmentCenter);
    [self.view addSubview:_labEmpty];
    _labEmpty.text = @"没有数据(^_^)";
    _labEmpty.center = self.view.center;
    _labEmpty.hidden = YES;
    
    


    
    
    NSMutableArray *city = [[NSMutableArray alloc]init];
    NSMutableArray *zone = [[NSMutableArray alloc]init];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc]initWithContentsOfFile:plistPath];
    NSDictionary *scDict = dict[@"四川省"];
    

    for(int i = 0; i<=20 ; i++){
    
        NSString *index = [NSString stringWithFormat:@"%d",i];
        NSDictionary *cityDict = scDict[index];
        NSArray *cityarr = [cityDict allKeys];
        NSString *cityName = cityarr[0];
        [city addObject:cityName];
        
        NSArray *dicstarr = [cityDict allValues];
        NSArray *dics = dicstarr[0];
        [zone addObject:dics];
    }
    
    _cityArr = city;
    _zoneArr = zone;
    _subPickerArray = _zoneArr[0];


}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
    [self openSliderMenu];
    
    
    UIButton * btn = (UIButton *)[_itemMenuView viewWithTag:_index];
    
    _selectedItemBtn.selected = NO;
    
    btn.selected = YES;
    
    _selectedItemBtn = btn;
    if([GETSELECTEDZONENAME isEqualToString:@""]){
        _locationName.text = GETZONENAM;

    }else{
        _locationName.text = GETSELECTEDZONENAME;
    }

    NSString * typeString = [NSString stringWithFormat:@"%d",_index];
    
    NSString * seX = _sexStr?:@"";
    NSString * dateStr = _dateStr?:@"";
    
    [_yhbListArr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self getYHBListWithID:@"" Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:seX date:dateStr type:typeString];

}



#pragma mark - Nav按钮点击时间
- (void) addBtnDidClick
{
    SP_PostActiveViewController *vc = [[SP_PostActiveViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) filterBtnDidClick
{
    SP_FilterViewController *vc = [[SP_FilterViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - 创建顶部的球类选择View
- (void) setUpItemMenu
{
    _itemMenuView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 45)];
    _itemMenuView.backgroundColor = UIColorFromRGB(0xfafafa);
    [self.view addSubview:_itemMenuView];
    
    _locationIMG = [[UIImageView alloc]initWithFrame:CGRectMake(10, 14.25, 11, 16.5)];
    _locationIMG.image = [UIImage imageNamed:@"icon_address"];
    [_itemMenuView addSubview:_locationIMG];
    
    _locationName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_locationIMG.frame) + 10, 0, 60, 45)];
    _locationName.textColor = UIColorFromRGB(0xff4e00);
    _locationName.font = [UIFont systemFontOfSize:14];
    _locationName.text = SELECTEDZONENAME;
    [_itemMenuView addSubview:_locationName];
    
    _locationBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_locationIMG.frame) + 10, 0, 60, 45)];
    [_locationBtn addTarget:self action:@selector(locationBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_itemMenuView addSubview:_locationBtn];

    
    
    
    
    
    
    _locationLine = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_locationName.frame), 10, 1, 25)];
    _locationLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_itemMenuView addSubview:_locationLine];
    
    
    _mapBtn = [self createBtnWithFrame:CGRectMake(ScreenWidth - 49, 0, 39, 45) Title:@"地图" Img:@"切换地图模式图标1.png" Img_S:@"切换地图模式图标2.png" Tag:111];
    [_mapBtn addTarget:self action:@selector(btnMapClick) forControlEvents:UIControlEventTouchUpInside];
    [_itemMenuView addSubview:_mapBtn];
    
    _mapLine = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_mapBtn.frame) - 8, 10, 1, 25)];
    _mapLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_itemMenuView addSubview:_mapLine];
    
    NSMutableDictionary *footDict = [[NSMutableDictionary alloc]init];
    [footDict setObject:@"足球1" forKey:@"img"];
    [footDict setObject:@"足球2" forKey:@"img_s"];
    [footDict setObject:@"足球" forKey:@"title"];
    
    NSMutableDictionary *ballDict = [[NSMutableDictionary alloc]init];
    [ballDict setObject:@"篮球1" forKey:@"img"];
    [ballDict setObject:@"篮球2" forKey:@"img_s"];
    [ballDict setObject:@"篮球" forKey:@"title"];
    
    NSMutableDictionary *runDict = [[NSMutableDictionary alloc]init];
    [runDict setObject:@"跑步1" forKey:@"img"];
    [runDict setObject:@"跑步2" forKey:@"img_s"];
    [runDict setObject:@"跑步" forKey:@"title"];
    
    NSMutableDictionary *otherDict = [[NSMutableDictionary alloc]init];
    [otherDict setObject:@"其他1" forKey:@"img"];
    [otherDict setObject:@"其他2" forKey:@"img_s"];
    [otherDict setObject:@"其他" forKey:@"title"];
    
    NSMutableArray *itemsArr = [[NSMutableArray alloc]init];
    [itemsArr addObject:footDict];
    [itemsArr addObject:ballDict];
    [itemsArr addObject:runDict];
    [itemsArr addObject:otherDict];

    
    
    
    CGFloat minX = CGRectGetMaxX(_locationLine.frame);
    CGFloat maxX = CGRectGetMinX(_mapLine.frame);
    CGFloat width = (maxX - minX) /4;
    
    for(int i=0;i<4;i++){

        
        NSDictionary *dict = itemsArr[i];
        NSString *img = dict[@"img"];
        NSString *img_s = dict[@"img_s"];
        NSString *title = dict[@"title"];
        
//        NSLog(@"img -- %@",img);
        
        UIButton *btn = [self createBtnWithFrame:CGRectMake(minX + 6, 0, 39, 45) Title:title Img:img Img_S:img_s Tag:i +1];
        [btn addTarget:self action:@selector(itemBtnsDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_itemMenuView addSubview:btn];
        
        if(i == 0){
            _selectedItemBtn = btn;
            _selectedItemBtn.selected = YES;
            
        }
        
        
        minX += width;
        

    }
    
}


//定位切换按钮点击方法
- (void) locationBtnDidClick
{
    
    _pickerBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo)];
    
    [_pickerBgView addGestureRecognizer:tapGesture];
    
    [self.view addSubview:_pickerBgView];
    
    _zonePickView = [[UIPickerView alloc]initWithFrame:CGRectMake(50, (ScreenHeight -64 -44) / 2 - 50, ScreenWidth - 100, 200)];
    _zonePickView.backgroundColor = [UIColor whiteColor];
    _zonePickView.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _zonePickView.layer.borderWidth = 1;
    _zonePickView.dataSource = self;
    _zonePickView.delegate = self;
    [_pickerBgView addSubview:_zonePickView];
    
    
    _subPickerArray = _zoneArr[0];
    
    NSString *zone = [NSString stringWithFormat:@"%@",_subPickerArray[0]];
    _locationName.text = zone;
    [[NSUserDefaults standardUserDefaults] setObject:zone forKey:SELECTEDCITYNAME];
    
}

- (void) Actiondo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GroundZoneChange" object:nil];
    [_pickerBgView removeFromSuperview];
    
    
    if(![GETGROUNDZONE isEqualToString:@"0"]){
        UIButton * btn = (UIButton *)[_itemMenuView viewWithTag:_index];
        
        _selectedItemBtn.selected = NO;
        
        btn.selected = YES;
        
        _selectedItemBtn = btn;
        
        NSString * typeString = [NSString stringWithFormat:@"%d",_index];
        
        NSString * seX = _sexStr?:@"";
        NSString * dateStr = _dateStr?:@"";

        [_yhbListArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self getYHBListWithID:@"" Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:seX date:dateStr type:typeString];
        
    }else{
        UIButton * btn = (UIButton *)[_itemMenuView viewWithTag:_index];
        
        _selectedItemBtn.selected = NO;
        
        btn.selected = YES;
        
        _selectedItemBtn = btn;
        
        NSString * typeString = [NSString stringWithFormat:@"%d",_index];
        
        NSString * seX = _sexStr?:@"";
        NSString * dateStr = _dateStr?:@"";

        [_yhbListArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self getYHBListWithID:@"" Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:seX date:dateStr type:typeString];
    }
    
}



#pragma mark 球类按钮点击方法
- (void) itemBtnsDidClick:(UIButton *)btn
{
    self.index = (int)btn.tag;
    
    _selectedItemBtn.selected = NO;
    btn.selected = YES;
    _selectedItemBtn = btn;
    
    NSString *type = [NSString stringWithFormat:@"%ld",_selectedItemBtn.tag];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    
    
    [_yhbListArr removeAllObjects];
    [self getYHBListWithID:@"" Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:@"" date:@"" type:type];

    
}


- (void)btnMapClick{
    
    SP_MapViewController * mapVc = [[SP_MapViewController alloc] init];
    mapVc.delegate = self;
    [self.navigationController pushViewController:mapVc animated:YES];
}

#pragma mark  创建球类按钮
- (UIButton *) createBtnWithFrame:(CGRect)frame Title:(NSString *)title Img:(NSString *)img Img_S:(NSString *)img_s Tag:(NSInteger)tag
{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    btn.tag = tag;
    btn.titleLabel.font = [UIFont systemFontOfSize:10];
    [btn setTitleColor:UIColorFromRGB(0xff4d00) forState:UIControlStateSelected];
    [btn setTitleColor:UIColorFromRGB(0xff4d00) forState:UIControlStateHighlighted];
    [btn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:img_s] forState:UIControlStateHighlighted];
    [btn setImage:[UIImage imageNamed:img_s] forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(28, -60, 0, 0)];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(3, 3, 15, 6)];
    return btn;
}



#pragma mark - 网络请求
- (void) getYHBListWithID:(NSString *)yhbid
                      Num:(NSString *)num
                      Uid:(NSString *)uid
                 Location:(NSString *)location
                      sex:(NSString *)sex
                     date:(NSString *)date
                     type:(NSString *)type
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];
    [dictionary setObject:yhbid forKey:@"yhbid"];
    [dictionary setObject:num forKey:@"num"];
    [dictionary setObject:location forKey:@"location"];
    [dictionary setObject:sex forKey:@"sex"];
    [dictionary setObject:date forKey:@"date"];
    [dictionary setObject:type forKey:@"type"];


    NSLog(@"dict === %@",dictionary);
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:GETYHBLIST Dictionary:dictionary Tag:TAG_HTTP_GETYHBLIST];

}

- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAP_HTTP_GETYHBLIST -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETYHBLIST){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_YhbList *model = [SP_YhbList objectWithKeyValues:dict];
                    [_yhbListArr addObject:model];
                }

                if(_yhbListArr.count == 10){
                    [_talbleView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                }
                
                [_talbleView.header endRefreshing];
                [_talbleView.footer endRefreshing];
                [_talbleView reloadData];
            
            }
        }else if (request.tag == TAG_HTTP_RESPONSEYHB){
            if(HTTP_SUCCESS(code)){
                [self showMessage:@"响应成功"];
                _responeBtn.selected = YES;
            
            }else{
                [self showMessage:@"响应失败"];
            }
        
        }
    }
    
}




#pragma mark - 创建TableView
- (void) createTableView
{
    _talbleView = [[UITableView alloc]initWithFrame:CGRectMake(0, 109, ScreenWidth, ScreenHeight - 109 - 49)];
    _talbleView.delegate = self;
    _talbleView.dataSource = self;
    _talbleView.rowHeight = 180;
    _talbleView.separatorStyle = 0;
    
    [_talbleView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadNewData)];
    
    [self.view addSubview:_talbleView];
}


#pragma mark 下拉重新加载数据
- (void) reloadNewData
{
    [_yhbListArr removeAllObjects];
    
    UIButton * btn = (UIButton *)[_itemMenuView viewWithTag:_index];
    
    _selectedItemBtn.selected = NO;
    
    btn.selected = YES;
    
    _selectedItemBtn = btn;
    
    NSString * typeString = [NSString stringWithFormat:@"%d",_index];
    
    NSString * seX = _sexStr?:@"";
    NSString * dateStr = _dateStr?:@"";
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    
    [self getYHBListWithID:@"" Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:seX date:dateStr type:typeString];

    
    
}


#pragma mark 加载更多的数据
- (void) loadMoreData
{
    SP_YhbList *yhbList = [_yhbListArr lastObject];
    NSString *yhbid = yhbList.yhbid;
    
    UIButton * btn = (UIButton *)[_itemMenuView viewWithTag:_index];
    
    _selectedItemBtn.selected = NO;
    
    btn.selected = YES;
    
    _selectedItemBtn = btn;
    
    NSString * typeString = [NSString stringWithFormat:@"%d",_index];
    
    NSString * seX = _sexStr?:@"";
    NSString * dateStr = _dateStr?:@"";
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self getYHBListWithID:yhbid Num:@"10" Uid:GETUSERID Location:GETSELECTEDZONENAME sex:seX date:dateStr type:typeString];

}



#pragma mark _talbleView 代理方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_yhbListArr.count == 0) {
        _labEmpty.hidden = NO;
    }else{
    
        _labEmpty.hidden = YES;
    }
    
    return _yhbListArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_CompanyCell *cell = [SP_CompanyCell cellWithTableView:tableView];
    cell.zoneName = GETSELECTEDZONENAME;
    SP_YhbList *yhbList = _yhbListArr[indexPath.row];
    cell.yhbList = yhbList;
    
    cell.inBtn.tag = 100 + indexPath.row;
    
    [cell.inBtn addTarget:self action:@selector(respondsBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    return cell;
}

/**
 *  响应约伙伴按钮
 *
 */
- (void) respondsBtnDidClick:(UIButton *)btn
{
    _responeBtn = btn;
    if(btn.selected == NO){
        
        SP_YhbList *yhbList = _yhbListArr[btn.tag - 100];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:RESPONSEYHB(GETUSERID, yhbList.yhbid) Tag:TAG_HTTP_RESPONSEYHB];
    }
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_ActiveDetailsViewController *detailsVC = [[SP_ActiveDetailsViewController alloc]init];
    
    SP_YhbList *yhbList = _yhbListArr[indexPath.row];
    detailsVC.yhbList = yhbList;
    
    [self.navigationController pushViewController:detailsVC animated:YES];
}




- (void)bringLocaiotnInfoBack:(SP_LocationEntity *)locationEntity
{
    _locationEntity = locationEntity;
    
    [[NSUserDefaults standardUserDefaults] setObject:_locationEntity.city forKey:SELECTEDCITYNAME];
    [[NSUserDefaults standardUserDefaults] setObject:_locationEntity.zone forKey:SELECTEDZONENAME];
    
    
    _currentZone = _locationEntity.zone;
    
    _locationName.text = GETSELECTEDZONENAME;
    
    

    
    
    
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(component == kFirstComponent){
        return [_cityArr count];
    }else {
        return [_subPickerArray count];
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0){
        NSString *city =  [NSString stringWithFormat:@"%@",_cityArr[row]];
        return city;
    }else{
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[row]];
        return zone;
    }
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == kFirstComponent) {
        
        NSString *city = [NSString stringWithFormat:@"%@",_cityArr[row]];
        
        [[NSUserDefaults standardUserDefaults] setObject:city forKey:SELECTEDCITYNAME];
        
        _subPickerArray = _zoneArr[row];
        [pickerView selectRow:0 inComponent:kSubComponent animated:YES];
        [pickerView reloadComponent:kSubComponent];
        
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[0]];
        _locationName.text = zone;
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:SELECTEDZONENAME];
        
        
    }else{
        NSString *zone =  [NSString stringWithFormat:@"%@",_subPickerArray[row]];
        _locationName.text = zone;
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:SELECTEDZONENAME];
    }
}



- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //关闭侧滑菜单
    [self closeSliderMenu];
}


@end
