//
//  testgpsViewController.m
//  TrunkIndustryManageSystem
//
//  Created by WT_lyy on 15/5/21.
//  Copyright (c) 2015年 李璐西. All rights reserved.
//

#import "SP_MapViewController.h"
#import "CommonUtility.h"
#import "GeocodeAnnotation.h"
#import "SP_LocationEntity.h"

@interface SP_MapViewController ()<MAMapViewDelegate,AMapSearchDelegate,UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>{

    NSMutableArray * _trackArr;
    
}

#define GeoPlaceHolder @"名称"

@property (nonatomic, strong) MAMapView * mapView;
@property (nonatomic, retain) MAPolyline * polyline;
@property (nonatomic, readwrite, strong) AMapGeocode *geocode;
@property (nonatomic ,strong) AMapReGeocode *reGeocode;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *displayController;
@property (nonatomic, strong) NSMutableArray *tips;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIButton *confrimeBtn;
@property (nonatomic, strong) UITextField *addressTextView;
@property (nonatomic, strong) SP_LocationEntity *locationEntity;
@property (nonatomic, strong) SP_LocationEntity *orignEntity;

@end

@implementation SP_MapViewController

@synthesize tips = _tips;
@synthesize searchBar = _searchBar;
@synthesize displayController = _displayController;

@synthesize search  = _search;


- (void)clearSearch
{
    self.search.delegate = nil;
}

#pragma mark - Handle Action

- (void)returnAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self clearSearch];
}

#pragma mark - AMapSearchDelegate

- (void)searchRequest:(id)request didFailWithError:(NSError *)error
{
    NSLog(@"%s: searchRequest = %@, errInfo= %@", __func__, [request class], error);
}

- (void)initSearch
{
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:MAPKEY_GD Delegate:self];
    self.search.delegate = self;
}


/* 地理编码 搜索. */
- (void)searchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode
{
    if (key.length == 0)
    {
        return;
    }
    
    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
    geo.address = key;
    
    if (adcode.length > 0)
    {
        geo.city = @[adcode];
    }
    
    [self.search AMapGeocodeSearch:geo];
}

/* 输入提示 搜索.*/
- (void)searchTipsWithKey:(NSString *)key
{
    if (key.length == 0)
    {
        return;
    }
    
    AMapInputTipsSearchRequest *tips = [[AMapInputTipsSearchRequest alloc] init];
    tips.keywords = key;
    [self.search AMapInputTipsSearch:tips];
}

/* 清除annotation. */
- (void)clear
{
    [self.mapView removeAnnotations:self.mapView.annotations];
}

- (void)clearAndSearchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode
{
    /* 清除annotation. */
    [self clear];
    
    [self searchGeocodeWithKey:key adcode:adcode];
}

- (void)gotoDetailForGeocode:(AMapGeocode *)geocode
{
//    if (geocode != nil)
//    {
//        GeoDetailViewController *geoDetailViewController = [[GeoDetailViewController alloc] init];
//        geoDetailViewController.geocode = geocode;
//        
//        [self.navigationController pushViewController:geoDetailViewController animated:YES];
//    }
}

#pragma mark - MAMapViewDelegate


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation
updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
    
    
    
    //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
    AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
    regeoRequest.searchType = AMapSearchType_ReGeocode;
    
    regeoRequest.location = [AMapGeoPoint locationWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
    regeoRequest.radius = 10000;
    
    regeoRequest.requireExtension = YES;
    
    
    self.locationEntity.lat = [NSString stringWithFormat:@"%lf",userLocation.coordinate.latitude];
    self.locationEntity.lng = [NSString stringWithFormat:@"%lf",userLocation.coordinate.longitude];
    
    
    //发起逆地理编码
    [_search AMapReGoecodeSearch: regeoRequest];
}

- (void)mapView:(MAMapView *)mapView annotationView:(MAAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if ([view.annotation isKindOfClass:[GeocodeAnnotation class]])
    {
        [self gotoDetailForGeocode:[(GeocodeAnnotation*)view.annotation geocode]];
    }
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if ([annotation isKindOfClass:[GeocodeAnnotation class]])
    {
        static NSString *geoCellIdentifier = @"geoCellIdentifier";
        
        MAPinAnnotationView *poiAnnotationView = (MAPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:geoCellIdentifier];
        if (poiAnnotationView == nil)
        {
            poiAnnotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:geoCellIdentifier];
        }
        
        poiAnnotationView.canShowCallout = YES;
        poiAnnotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        return poiAnnotationView;
    }
    
    return nil;
}

#pragma mark - AMapSearchDelegate

//实现逆地理编码的回调函数
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if(response.regeocode != nil)
    {
        //通过AMapReGeocodeSearchResponse对象处理搜索结果
        //        NSString *result = [NSString stringWithFormat:@"ReGeocode: %@", response.regeocode];
        AMapReGeocode *result = response.regeocode;
        
        self.reGeocode = response.regeocode;
        
        _addressTextView.text = result.formattedAddress;
        
        
        self.locationEntity.location = result.formattedAddress;
    
        if(result.addressComponent.city.length == 0){
            
            self.locationEntity.city = result.addressComponent.province;
            
        }else{
            self.locationEntity.city = result.addressComponent.city;
        }
        
        self.locationEntity.zone = result.addressComponent.district;
        
        
        
        NSString *city = result.addressComponent.city;
        NSString *zone = result.addressComponent.district;
        
        
        [[NSUserDefaults standardUserDefaults] setObject:city forKey:CITYNAME];
        [[NSUserDefaults standardUserDefaults] setObject:zone forKey:CITYNAME];
        
    }
}


/* 地理编码回调.*/
- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response
{
    if (response.geocodes.count == 0)
    {
        return;
    }
    

    
    NSMutableArray *annotations = [NSMutableArray array];
    
    [response.geocodes enumerateObjectsUsingBlock:^(AMapGeocode *obj, NSUInteger idx, BOOL *stop) {
        GeocodeAnnotation *geocodeAnnotation = [[GeocodeAnnotation alloc] initWithGeocode:obj];
        self.geocode = obj;
        [annotations addObject:geocodeAnnotation];
    }];
    
    NSLog(@"self.geocode -- %@",self.geocode.formattedAddress);
    
    self.addressTextView.text = self.geocode.formattedAddress;
    
    self.locationEntity.location = self.geocode.formattedAddress;
    self.locationEntity.lat = [NSString stringWithFormat:@"%lf", self.geocode.location.latitude];
    self.locationEntity.lng = [NSString stringWithFormat:@"%lf",self.geocode.location.longitude];
    
    if(self.geocode.city.length == 0){
        self.locationEntity.city = self.geocode.province;
    }else{
        self.locationEntity.city = self.geocode.city;
    }
    
    self.locationEntity.zone = self.geocode.district;
    
    
    
    if (annotations.count == 1)
    {
        [self.mapView setCenterCoordinate:[annotations[0] coordinate] animated:YES];
    }
    else
    {
        [self.mapView setVisibleMapRect:[CommonUtility minMapRectForAnnotations:annotations]
                               animated:YES];
    }
    
    [self.mapView addAnnotations:annotations];
}

/* 输入提示回调. */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response
{
    [self.tips setArray:response.tips];
    
    [self.displayController.searchResultsTableView reloadData];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *key = searchBar.text;
    
    [self clearAndSearchGeocodeWithKey:key adcode:nil];
    
    [self.displayController setActive:NO animated:NO];
    
    self.searchBar.placeholder = key;
}

#pragma mark - UISearchDisplayDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self searchTipsWithKey:searchString];
    
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tips.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *tipCellIdentifier = @"tipCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tipCellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:tipCellIdentifier];
    }
    
    AMapTip *tip = self.tips[indexPath.row];
    
    cell.textLabel.text = tip.name;
    cell.detailTextLabel.text = tip.adcode;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AMapTip *tip = self.tips[indexPath.row];
    
    NSString *key = [NSString stringWithFormat:@"%@%@", tip.district, tip.name];
    [self clearAndSearchGeocodeWithKey:key adcode:tip.adcode];
    
    [self.displayController setActive:NO animated:NO];
    
    self.searchBar.placeholder = tip.name;
}

#pragma mark - Initialization

- (void)initSearchBar
{
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), 44)];
    self.searchBar.barStyle     = UIBarStyleBlack;
    self.searchBar.translucent  = YES;
    self.searchBar.delegate     = self;
    self.searchBar.placeholder  = GeoPlaceHolder;
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    
    [self.view addSubview:self.searchBar];
}

- (void)initSearchDisplay
{
    self.displayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.displayController.delegate                = self;
    self.displayController.searchResultsDataSource = self;
    self.displayController.searchResultsDelegate   = self;
}


/* 构建mapView. */
- (void)setupMapView
{
    [MAMapServices sharedServices].apiKey = MAPKEY_GD;
    self.mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 64 + 44, ScreenWidth, ScreenHeight - 164 - 50)];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.distanceFilter = 200;
    self.mapView.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.mapView.pausesLocationUpdatesAutomatically = NO;
    [self.view addSubview:self.mapView];
    
}

#pragma mark - 设置底部的BottomView
- (void) setUpBottomView
{
    self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.mapView.frame), ScreenWidth, 150)];
    self.bottomView.backgroundColor = UIColorFromRGB(0xf4f4f4);
    
    self.addressTextView = [[UITextField alloc]initWithFrame:CGRectMake(20, 10, ScreenWidth - 40, 40)];
    self.addressTextView.placeholder = @"请填写地址";
    self.addressTextView.font = [UIFont systemFontOfSize:14];
    self.addressTextView.backgroundColor = UIColorFromRGB(0xffffff);
    self.addressTextView.layer.cornerRadius = 8;
    self.addressTextView.layer.masksToBounds = YES;
    
    
    self.confrimeBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(self.addressTextView.frame) + 10, 200, 40)];
    self.confrimeBtn.layer.cornerRadius = 10;
    self.confrimeBtn.layer.masksToBounds = YES;
    self.confrimeBtn.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [self.confrimeBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.confrimeBtn addTarget:self action:@selector(submitBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:self.bottomView];
    [self.bottomView addSubview:self.addressTextView];
    [self.bottomView addSubview:self.confrimeBtn];
}


- (void) submitBtnDidClick
{
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(bringLocaiotnInfoBack:)]){
        [self.delegate bringLocaiotnInfoBack:self.locationEntity];
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}


#pragma mark - Life Cycle

- (id)init
{
    if (self = [super init])
    {
        self.tips = [NSMutableArray array];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar:@"选择地址"];
    [self addBackButton];
    self.view.backgroundColor = UIColorFromRGB(0xf4f4f4);
    
    [self setupMapView];
    
    [self initSearchBar];
    
    [self initSearchDisplay];
    [self initSearch];
    
    [self setUpBottomView];
    
    
    self.locationEntity = [[SP_LocationEntity alloc]init];
    
}




@end
