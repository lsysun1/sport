//
//  SP_ActiveDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_YhbList;
@interface SP_ActiveDetailsViewController : BaseViewController
@property (nonatomic,strong) SP_YhbList *yhbList;
@end
