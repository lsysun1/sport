//
//  SP_PostActiveViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_PostActiveViewController.h"
#import "SP_MapViewController.h"
#import "CLLocation+YCLocation.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>
#import "MyTextView.h"
#import "SP_GroundEntity.h"
#import "SP_LocationEntity.h"

#define BTN_TAG_SEX_NO          @"101"
#define BTN_TAG_SEX_MAN         @"102"
#define BTN_TAG_SEX_WOMAN       @"103"

#define BTN_TAG_TIME_MON        @"201"
#define BTN_TAG_TIME_TUES       @"202"
#define BTN_TAG_TIME_WED        @"203"
#define BTN_TAG_TIME_THUR       @"204"
#define BTN_TAG_TIME_FRI        @"205"
#define BTN_TAG_TIME_SAT        @"206"
#define BTN_TAG_TIME_SUN        @"207"

#define BTN_TAG_TYPE_NO         @"301"
#define BTN_TAG_TYPE_FOOT       @"302"
#define BTN_TAG_TYPE_BASEKET    @"303"
#define BTN_TAG_TYPE_RUN        @"304"
#define BTN_TAG_TYPE_SNOOKER    @"305"
#define BTN_TAG_TYPE_ROOM       @"306"

CGRect Frame;

@interface SP_PostActiveViewController ()<UITextViewDelegate,CLLocationManagerDelegate,AMapSearchDelegate,SP_MapViewDelegate>
{
    UIScrollView *_scrollView;
    
    UIView *_sexView;
    UIView *_dayView;
    UIView *_typeView;
    
    UIButton *_sexSlectedBtn;
    UIButton *_daySlectedBtn;
    UIButton *_typeSlectedBtn;
    
    UIView *_locationView;
    
    UIView *_desView;
    
    UIButton *_submitBtn;
    
    NSString *_monday;
    NSString *_tuesday;
    NSString *_wednesday;
    NSString *_thursday;
    NSString *_friday;
    NSString *_saturday;
    NSString *_sunday;
    
    MyTextView *_desTextView;
    CLLocationManager* locationMgr;
    
    UILabel * _labLoc;
    
    CGFloat _currentLat;
    CGFloat _currentLng;
    NSString *_currentZone;
    
    CGFloat _mapcurrentLat;
    CGFloat _mapcurrentLng;
    NSString *_mapcurrentZone;
    
    
    SP_LocationEntity *_locationEntity;


}
@property (nonatomic, strong) AMapSearchAPI *search;
@end

@implementation SP_PostActiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"发布活动"];
    [self addBackButton];
    [self openLocation];

    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    [self.view addSubview:_scrollView];
    _scrollView.delegate = self;
    _scrollView.backgroundColor = UIColorFromRGB(0xfafafa);
    _scrollView.contentSize = CGSizeMake(ScreenWidth, 653 + 64);
    [self setUpSexView];
    [self setUpDayBtnView];
    [self setUpTypeView];
    [self setUpLocationView];
    [self setUpDesView];
    [self setUpSubmitBtn];
    [self setUpTimeStr];
    
    _locationEntity = [[SP_LocationEntity alloc]init];
    _currentZone = [[NSString alloc]init];

}


- (void)openLocation{
    
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:MAPKEY_GD Delegate:self];
    self.search.delegate = self;

    
    locationMgr = [[CLLocationManager alloc] init];
    locationMgr.delegate = self;
    locationMgr.desiredAccuracy = kCLLocationAccuracyBest;
    locationMgr.distanceFilter = 200; //位置更新最小距离
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        [locationMgr requestAlwaysAuthorization];
        [locationMgr requestWhenInUseAuthorization];
    }
    
    if ([CLLocationManager locationServicesEnabled]) {
        // 启动位置更新
        // 开启位置更新需要与服务器进行轮询所以会比较耗电，在不需要时用stopUpdatingLocation方法关闭;
        [locationMgr startUpdatingLocation];
    }
    else {
        [self showMessage:@"请开启定位功能！"];
    }
}

#pragma mark - 网络请求方法
- (void) postActiveWithLocation:(NSString *)location
                           Type:(NSString *)type
                            Sex:(NSString *)sex
                           Date:(NSString *)date
                           Desc:(NSString *)desc
                            Lat:(CGFloat  )lat
                            lng:(CGFloat  )lng
{
    NSString *latStr = [NSString stringWithFormat:@"%f",lat];
    NSString *lngStr = [NSString stringWithFormat:@"%f",lng];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];
    [dictionary setObject:location forKey:@"location"];
    [dictionary setObject:type forKey:@"type"];
    [dictionary setObject:sex forKey:@"sex"];
    [dictionary setObject:date forKey:@"date"];
    [dictionary setObject:desc forKey:@"desc"];
    [dictionary setObject:latStr forKey:@"lat"];
    [dictionary setObject:lngStr forKey:@"lng"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:PUBLISHYHBMSG Dictionary:dictionary Tag:TAG_HTTP_PUBLISHYHBMSG];

}

#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"PUBLISHYHBMSG -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_PUBLISHYHBMSG){
            if (HTTP_SUCCESS(code))
            {
                [self asyshowHUDView:@"发布成功" CurrentView:self];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
    
}


- (void)setEntity:(SP_GroundEntity *)entity
{
    _entity = entity;
    
    
    _labLoc.text = entity.location;
    
    
}


#pragma mark - 顶部的性别选择按钮
- (void) setUpSexView
{
    _sexView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth,116)];
    _sexView.backgroundColor = UIColorFromRGB(0xffffff);
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"性别";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"不限",@"男",@"女",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_SEX_NO,BTN_TAG_SEX_MAN,BTN_TAG_SEX_WOMAN,nil];
    
    UIImage *sexBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xe84c3d)];

    
    CGFloat btnX = 16;
    CGFloat btnY = 51;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:sexBtnBgImg superView:_sexView Tag:btnTag action:@selector(sexBtnDidClick:)];

        
        btnX += 61;
    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth, 5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);

    
    [_scrollView addSubview:_sexView];
    [_sexView addSubview:title];
    [_sexView addSubview:topLine];
    [_sexView addSubview:bottomView];
    
    
}

#pragma mark 性别选择按钮点击方法
- (void) sexBtnDidClick:(UIButton *)btn
{
    _sexSlectedBtn.selected = NO;
    _sexSlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _sexSlectedBtn = btn;

}



#pragma - mark 日期选择按钮
- (void) setUpDayBtnView
{
    _dayView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_sexView.frame), ScreenWidth, 116)];
    _dayView.backgroundColor = UIColorFromRGB(0xffffff);

    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"运动时间";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"周一",@"周二",@"周三",@"周四",@"周五",@"星期六",@"星期天",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_TIME_MON,BTN_TAG_TIME_TUES,BTN_TAG_TIME_WED,BTN_TAG_TIME_THUR,BTN_TAG_TIME_FRI,BTN_TAG_TIME_SAT,BTN_TAG_TIME_SUN,nil];

    UIImage *dayBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xe77e23)];
    
    UIScrollView *btnScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(16, 51, ScreenWidth, 45)];
    btnScrollView.showsHorizontalScrollIndicator = NO;
    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:dayBtnBgImg superView:btnScrollView Tag:btnTag action:@selector(dayBtnDidClick:)];
        
        btnX += 61;
        btnScrollView.contentSize = CGSizeMake(btnX + 3, 45);

    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth,5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    [_scrollView addSubview:_dayView];
    [_dayView addSubview:title];
    [_dayView addSubview:topLine];
    [_dayView addSubview:bottomView];
    [_dayView addSubview:btnScrollView];
    
}


- (void) dayBtnDidClick:(UIButton *)btn
{
    _daySlectedBtn.selected = NO;
    _daySlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _daySlectedBtn = btn;

}


#pragma mark -textview delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([@"\n" isEqualToString:text] == YES)
    {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    Frame= _scrollView.frame;
    
    
    _scrollView.frame = CGRectMake(0, _scrollView.frame.origin.y - 200, _scrollView.frame.size.width, _scrollView.frame.size.height);
    
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    
    [textView resignFirstResponder];
    
    _scrollView.frame = Frame;
    
}

#pragma mark - 设置运动种类选择
- (void) setUpTypeView
{
    _typeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_dayView.frame), ScreenWidth, 116)];
    _typeView.backgroundColor = UIColorFromRGB(0xffffff);

    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"运动项目";
    title.font = [UIFont systemFontOfSize:14];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(16, 31, ScreenHeight, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    NSArray *btnName = [NSArray arrayWithObjects:@"不限",@"足球",@"篮球",@"跑步",@"台球",@"健身房",nil];
    NSArray *btnTag = [NSArray arrayWithObjects:BTN_TAG_TYPE_NO,BTN_TAG_TYPE_FOOT,BTN_TAG_TYPE_BASEKET,BTN_TAG_TYPE_RUN,BTN_TAG_TYPE_SNOOKER,BTN_TAG_TYPE_ROOM,nil];
    
    UIImage *typeBtnBgImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xef1c40f)];
    
    UIScrollView *btnScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(16, 51, ScreenWidth, 45)];
    btnScrollView.showsHorizontalScrollIndicator = NO;

    
    CGFloat btnX = 0;
    CGFloat btnY = 0;
    CGFloat btnW = 45;
    CGFloat btnH = 45;
    for (int i = 0; i<btnName.count; i++) {
        CGRect btnFrame = CGRectMake(btnX, btnY, btnW, btnH);
        NSString *tagStr = btnTag[i];
        NSString *name = btnName[i];
        NSInteger btnTag = [tagStr integerValue];
        [self createBtns:name Frame:btnFrame BgImg:typeBtnBgImg superView:btnScrollView Tag:btnTag action:@selector(typeBtnDidClick:)];
        
        btnX += 61;
        btnScrollView.contentSize = CGSizeMake(btnX + 3, 45);

    }
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 107, ScreenWidth, 5)];
    bottomView.backgroundColor = UIColorFromRGB(0xdcdcdc);

    
    [_scrollView addSubview:_typeView];
    [_typeView addSubview:title];
    [_typeView addSubview:topLine];
    [_typeView addSubview:bottomView];
    [_typeView addSubview:btnScrollView];

}

- (void) typeBtnDidClick:(UIButton *)btn
{
    _typeSlectedBtn.selected = NO;
    _typeSlectedBtn.layer.borderWidth = 1;
    
    btn.selected = YES;
    btn.layer.borderWidth = 0;
    
    _typeSlectedBtn = btn;

}




#pragma mark - 所有按钮的方法
- (void) createBtns:(NSString *)title Frame:(CGRect)frame BgImg:(UIImage *)img superView:(UIView *)supview Tag:(NSInteger )tag action:(SEL)action
{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    btn.tag = tag;
    
    UIImage *btnBg = [GT_Tool createImageWithColor:UIColorFromRGB(0xffffff)];
    
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0x6a6a6a) forState:UIControlStateNormal];
    [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    [btn setBackgroundImage:btnBg forState:UIControlStateNormal];
    [btn setBackgroundImage:img forState:UIControlStateSelected];
    btn.layer.cornerRadius = 22.5;
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = UIColorFromRGB(0x6a6a6a).CGColor;
    btn.layer.masksToBounds = YES;
    

    if(tag % 10 ==1){
        btn.selected = YES;
        btn.layer.borderWidth = 0;
        
        if(tag / 100 == 1){
            _sexSlectedBtn = btn;
        }else if (tag / 100 == 2)
        {
            _daySlectedBtn = btn;

        }else if (tag / 100 == 3){
            _typeSlectedBtn = btn;

        }
    }
    
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    
    [supview addSubview:btn];
    
}


#pragma mark - 创建地址栏
- (void) setUpLocationView
{
    _locationView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_typeView.frame), ScreenWidth, 85)];
    _locationView.backgroundColor = UIColorFromRGB(0xffffff);

    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(20,10, 11, 15.5)];
    imgView.image = [UIImage imageNamed:@"位置"];
    
    _labLoc = GET_LABEL(CGRectMake(40, 8, ScreenWidth-80, 20), 13, NO, [UIColor darkGrayColor], NSTextAlignmentLeft);
    
    if(self.entity != nil){
        _labLoc.text = self.entity.location;
    }
    
    UILabel *topLine = [[UILabel alloc]initWithFrame:CGRectMake(10, 36, ScreenWidth - 20, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    UILabel *addres = [[UILabel alloc]initWithFrame:CGRectMake(20, 37,ScreenWidth - 100, 40)];
    addres.text = @"具体地址";
    addres.font = [UIFont systemFontOfSize:15];
    addres.textColor = UIColorFromRGB(0x696969);
    
    UIButton * btnadd = [[UIButton alloc] initWithFrame:addres.frame];
    [btnadd addTarget:self action:@selector(btnMapClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *arrowImg = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 50, 50, 9, 18)];
    arrowImg.image = [UIImage imageNamed:@"箭头"];
    
    UIView *bottoView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(addres.frame), ScreenWidth, 5)];
    bottoView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    
    [_scrollView addSubview:_locationView];
    [_locationView addSubview:imgView];
    [_locationView addSubview:_labLoc];
    [_locationView addSubview:topLine];
    [_locationView addSubview:addres];
    [_locationView addSubview:arrowImg];
    [_locationView addSubview:bottoView];
    [_locationView addSubview:btnadd];
    [_locationView bringSubviewToFront:btnadd];

}


- (void)btnMapClick{
    
    SP_MapViewController * mapVc = [[SP_MapViewController alloc] init];
    mapVc.delegate = self;
    [self.navigationController pushViewController:mapVc animated:YES];
}


#pragma mark - 创建活动详情栏
- (void) setUpDesView
{
    _desView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_locationView.frame), ScreenWidth, 180)];
    _desView.backgroundColor = UIColorFromRGB(0xffffff);

    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, 60, 21)];
    title.text = @"活动详情";
    title.font = [UIFont systemFontOfSize:14];
    
    UILabel *topLine = [[UILabel alloc]initWithFrame:CGRectMake(10, 36, ScreenWidth - 20, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    _desTextView = [[MyTextView alloc]initWithFrame:CGRectMake(10, 40, ScreenWidth - 20, 160)];
    _desTextView.alwaysBounceVertical = YES;
    _desTextView.delegate = self;
    _desTextView.font = [UIFont systemFontOfSize:15];
    _desTextView.placehoder = @"请输入活动的具体详情";
    _desTextView.returnKeyType = UIReturnKeyDone;
    _desTextView.layer.cornerRadius  = 3;
    _desTextView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    _desTextView.layer.borderWidth = 0.5;
    
    
    [_scrollView addSubview:_desView];
    [_desView addSubview:title];
    [_desView addSubview:topLine];
    [_desView addSubview:_desTextView];

}

#pragma mark - 创建确认按钮
- (void) setUpSubmitBtn
{
    _submitBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_desView.frame) + 40, 200, 40)];
    _submitBtn.layer.cornerRadius = 10;
    _submitBtn.layer.masksToBounds = YES;
    _submitBtn.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [_submitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:_submitBtn];
}

- (void) submitBtnDidClick
{
    if(_desTextView.text.length == 0){
        [self showAlertStr:@"活动描述不能为空"];
        return;
    }
    
   
    NSString *type = [NSString stringWithFormat:@"%ld",(_typeSlectedBtn.tag % 300) - 1];
    
    NSString *sex;
    if(_sexSlectedBtn.tag == [BTN_TAG_SEX_NO integerValue])
    {
        sex = @"10";
    }else if(_sexSlectedBtn.tag == [BTN_TAG_SEX_MAN integerValue]){
        sex = @"1";
    }else{
        sex = @"0";
    }
    
    NSString *date;
    switch (_daySlectedBtn.tag % 200) {
        case 1:
            date = _monday;
            break;
        case 2:
            date = _tuesday;
            break;
        case 3:
            date = _wednesday;
            break;
        case 4:
            date = _thursday;
            break;
        case 5:
            date = _friday;
            break;
        case 6:
            date = _saturday;
            break;
        case 7:
            date = _sunday;
            break;
        default:
            break;
    }
    
    
   

    NSString *location = [[NSString alloc]init];
    CGFloat lat;
    CGFloat lng;

    
    if(_mapcurrentZone.length == 0){
        location = _currentZone;
        lat = _currentLat;
        lng = _currentLng;
    }else{
        location = _mapcurrentZone;
        lat = _mapcurrentLat;
        lng = _mapcurrentLng;
    }
    
    [self postActiveWithLocation:location Type:type Sex:sex Date:date Desc:_desTextView.text Lat:lat lng:lng];
    
}


#pragma mark - 获取日期
- (void) setUpTimeStr
{
    NSDate *newDate = [NSDate date];
    if (newDate == nil) {
        newDate = [NSDate date];
    }
    double interval = 0;
    NSDate *beginDate = nil;
    NSDate *endDate = nil;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setFirstWeekday:2];//设定周一为周首日
    BOOL ok = [calendar rangeOfUnit:NSCalendarUnitWeekOfMonth startDate:&beginDate interval:&interval forDate:newDate];
    //分别修改为 NSDayCalendarUnit NSWeekCalendarUnit NSYearCalendarUnit
    if (ok) {
        endDate = [beginDate dateByAddingTimeInterval:interval-1];
    }else {
        return;
    }

    
    
    NSDate *one  = beginDate;
    NSDate *two = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([beginDate timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *three = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([two timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *four = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([three timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *five= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([four timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *six= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([five timeIntervalSinceReferenceDate] + 24*3600)];
    NSDate *seven= [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:([six timeIntervalSinceReferenceDate] + 24*3600)];

    
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"YYYY-MM-dd"];
    _monday = [myDateFormatter stringFromDate:one];
    _tuesday = [myDateFormatter stringFromDate:two];
    _wednesday = [myDateFormatter stringFromDate:three];
    _thursday = [myDateFormatter stringFromDate:four];
    _friday = [myDateFormatter stringFromDate:five];
    _saturday = [myDateFormatter stringFromDate:six];
    _sunday = [myDateFormatter stringFromDate:seven];



    

}


#pragma mark - AMapSearchDelegate


-(void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation
updatingLocation:(BOOL)updatingLocation
{
    if(updatingLocation)
    {
        //取出当前位置的坐标
        NSLog(@"latitude : %f,longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
    }
    
    
    
    //构造AMapReGeocodeSearchRequest对象，location为必选项，radius为可选项
    AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
    regeoRequest.searchType = AMapSearchType_ReGeocode;
    
    regeoRequest.location = [AMapGeoPoint locationWithLatitude:userLocation.coordinate.latitude longitude:userLocation.coordinate.longitude];
    regeoRequest.radius = 10000;
    
    regeoRequest.requireExtension = YES;
    
    
//    self.locationEntity.lat = [NSString stringWithFormat:@"%lf",userLocation.coordinate.latitude];
//    self.locationEntity.lng = [NSString stringWithFormat:@"%lf",userLocation.coordinate.longitude];
    
    
    //发起逆地理编码
    [_search AMapReGoecodeSearch: regeoRequest];
}


/* 逆地理编码回调. */

-(void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response{
    
    NSString * addrestr =  response.regeocode.formattedAddress;

    _labLoc.text = addrestr;
    
    _currentZone = response.regeocode.addressComponent.district;
}

#pragma mark - CLLocationManagerDelegate
//地理位置发生改变时触发
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:MAPKEY_GD Delegate:self];
    newLocation = [newLocation locationMarsFromEarth];
    
    [self searchReGeocodeWithCoordinate:newLocation.coordinate];
    
    [locationMgr stopUpdatingLocation];
    
    locationMgr.delegate = nil;
    
}


// 定位失误时触发
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"error:%@",error);
}


- (void)searchReGeocodeWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    
    _currentLat = coordinate.latitude;
    _currentLng = coordinate.longitude;

    regeo.location = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    regeo.requireExtension = YES;
    
    
    [self.search AMapReGoecodeSearch:regeo];
    
}


- (void)bringLocaiotnInfoBack:(SP_LocationEntity *)locationEntity
{
    _locationEntity = locationEntity;
    
    _mapcurrentLat = [locationEntity.lat floatValue];
    _mapcurrentLng = [locationEntity.lng floatValue];
    _mapcurrentZone = locationEntity.zone;
    
    _labLoc.text = locationEntity.location;
    
//    _locationName.text = GETSELECTEDZONENAME;

}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_desTextView resignFirstResponder];

}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    [_desTextView resignFirstResponder];
}

@end
