//
//  SP_EditPersonInfoViewController.h
//  Sport
//
//  Created by WT_lyy on 15/4/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_UserInfo;
@interface SP_EditPersonInfoViewController : BaseViewController
@property (nonatomic,strong) SP_UserInfo *userInfoMsg;
@end
