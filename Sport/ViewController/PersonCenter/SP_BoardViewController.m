//
//  SP_BoardViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BoardViewController.h"
#import "SP_BoardListEntity.h"
#import "SP_BoardCellFrame.h"
#import "SP_BoardListCell.h"
#import "SP_UserMsgViewController.h"
#import "SP_BoardDetailsViewController.h"

@interface SP_BoardViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_listArr;
}
@end

@implementation SP_BoardViewController

- (id)init
{
    if(self = [super init]){
        
        _listArr = [NSMutableArray array];
    
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"战术板"];
    [self addBackButton];
    [self setUpTableView];
    [self addNavRightView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [_listArr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETZSBMSGLIST(self.teamID) Tag:TAG_HTTP_GETZSBMSGLIST];
}


- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"留言"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) btnNavRightClick
{
    SP_UserMsgViewController *vc = [[SP_UserMsgViewController alloc]init];
    vc.type = @"board";
    vc.teamID = self.teamID;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
//        NSLog(@"战术板 --- resopnse:%@",request.responseString);

        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(HTTP_SUCCESS(code)){

            NSArray *msg = dictionary[@"msg"];
            
            for(NSDictionary *dict in msg){
                SP_BoardListEntity *entity = [SP_BoardListEntity objectWithKeyValues:dict];
                SP_BoardCellFrame *frmae = [[SP_BoardCellFrame alloc]init];
                frmae.boardEntity = entity;
                [_listArr addObject:frmae];
            }
            [_tableView reloadData];
        }
    }
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = 0;
    [self.view addSubview:_tableView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_BoardListCell *cell = [SP_BoardListCell cellWithTableView:tableView];
    SP_BoardCellFrame *frame = _listArr[indexPath.row];
    cell.cellFrame = frame;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_BoardCellFrame *frame = _listArr[indexPath.row];
    return frame.cellHeight;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_BoardCellFrame *frame = _listArr[indexPath.row];
    SP_BoardListEntity *entity = frame.boardEntity;
    SP_BoardDetailsViewController *vc = [[SP_BoardDetailsViewController alloc]init];
    vc.listEntity = entity;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
