//
//  PersonHomeViewController.m
//  Sport
//
//  Created by WT_lyy on 15/4/25.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_PersonHomeViewController.h"

#import "SP_EditPersonInfoViewController.h"

#import "SP_AddFriendViewController.h"
#import "SP_UserInfo.h"

#import "SP_PersonHome_ProfileViewController.h"
#import "SP_PersonHome_TeamViewController.h"





@interface SP_PersonHomeViewController() <SP_PersonHome_ProfileDelegate,SP_PersonHome_TeamDelegate>
{
    UIScrollView *_scrollview;
    

    SP_UserInfo *_userInfo;

    SP_PersonHome_ProfileViewController *_profileVC;
    SP_PersonHome_TeamViewController *_teamVC;
}

@end

@implementation SP_PersonHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBar:@"个人中心"];
    [self addBackButton];
    [self addNavRightView];
    
    _scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight)];
    _scrollview.contentSize = CGSizeMake(ScreenWidth, 510 + 215 + 30);
    _scrollview.bounces = NO;
    [self.view addSubview:_scrollview];
    
//    _profileVC = [[SP_PersonHome_ProfileViewController alloc] init];
//    _profileVC.delegate = self;
//    [_scrollview insertSubview:_profileVC.view atIndex:0];



    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
//    [self asyshowHUDView:WAITTING CurrentView:self];
//    [self setHTTPRequest:GETUSERDATA(GETUSERID, GETUSERID) Tag:TAG_HTTP_GETUSERDATA];


}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setHTTPRequest:GETUSERDATA(GETUSERID, GETUSERID) Tag:TAG_HTTP_GETUSERDATA];

}



#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"GETUSERDATA -- 个人资料resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETUSERDATA){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msgDict = dictionary[@"msg"];
                SP_UserInfo *info = [SP_UserInfo objectWithKeyValues:msgDict];
                _userInfo = info;
                
//                _profileVC.userInfoMsg = _userInfo;
                
                if(_profileVC.view.superview != nil){
                    [_profileVC.view removeFromSuperview];
                    _profileVC = nil;
                }
                
                _profileVC = [[SP_PersonHome_ProfileViewController alloc] init];
                _profileVC.delegate = self;

                [_scrollview insertSubview:_profileVC.view atIndex:0];
                
                
            }
        }
    }

}




#pragma mark - 设置导航栏右侧按钮
- (void)addNavRightView{
    UIButton * btnedit = GET_BUTTON(CGRectMake(ScreenWidth-90, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
    
    [self.topView addSubview:btnedit];
    
    [btnedit setImage:[UIImage imageNamed:@"person_edit"] forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
    btnedit.tag = 1;
    
    UIButton * btneAdd = GET_BUTTON(CGRectMake(ScreenWidth-45, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
    
    [self.topView addSubview:btneAdd];
    
    [btneAdd setImage:[UIImage imageNamed:@"person_atten"] forState:UIControlStateNormal];
    
    [btneAdd setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btneAdd addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
    btneAdd.tag = 2;
    
}



#pragma mark - Btn Action
- (void)btnNavRightClick:(UIButton *)button{
    
    if (1 == button.tag) {
        //edit
        SP_EditPersonInfoViewController *editVC = [[SP_EditPersonInfoViewController alloc]init];
        editVC.userInfoMsg = _userInfo;
        [self.navigationController pushViewController:editVC animated:YES];
        
    }else if (2 == button.tag){
        //add
        SP_AddFriendViewController *addVC = [[SP_AddFriendViewController alloc]init];
        addVC.userInfoMsg = _userInfo;
        [self.navigationController pushViewController:addVC animated:YES];
        
        
        
    }
}

- (void) bottomBtnDidClick
{
    if (_teamVC.view.superview==nil)//判断是否为根视图
    {
        if (_teamVC==nil)//判断视图控制器是否初始化
        {
            _teamVC = [[SP_PersonHome_TeamViewController alloc] init];
            _teamVC.delegate = self;
            _teamVC.userInfoMsg = _userInfo;
        }
        [_profileVC.view removeFromSuperview];
        [_scrollview insertSubview:_teamVC.view atIndex:0];
    }
}


- (void) backToProfileView
{
    if (_profileVC.view.superview==nil)//判断是否为根视图
    {
        if (_profileVC==nil)//判断视图控制器是否初始化
        {
            _profileVC = [[SP_PersonHome_ProfileViewController alloc] init];
            _profileVC.delegate = self;
            _profileVC.userInfoMsg = _userInfo;
        }
        [_teamVC.view removeFromSuperview];
        [_scrollview insertSubview:_profileVC.view atIndex:0];
        _scrollview.contentSize = CGSizeMake(ScreenWidth, 510 + 215 + 30);
    }
}


- (void)getScrollHeightToHomeView:(CGFloat)height
{
    _scrollview.contentSize = CGSizeMake(ScreenWidth, height);
}

@end