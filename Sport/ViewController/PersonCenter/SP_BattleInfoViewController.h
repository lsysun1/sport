//
//  SP_BattleInfoViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_BattleInfoViewController : BaseViewController

@property (nonatomic,copy) NSString *teamid;

@end

