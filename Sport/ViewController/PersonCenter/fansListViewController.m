
//
//  fansListViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "fansListViewController.h"
#import "pinyin.h"
#import "ChineseString.h"
#import "SP_FriendModel.h"
#import "SP_AddTeamMateCell.h"
#import "SP_UerHomeViewController.h"
#import "SP_AddFriendViewController.h"

@interface fansListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_friArr;
    NSMutableArray *_sortedArrForArrays;
    NSMutableArray *_sectionHeadsKeys;

}
@end

@implementation fansListViewController

- (id)init{
    
    self = [super init];
    if (self) {
        _friArr = [[NSMutableArray alloc] init];
        _sortedArrForArrays = [[NSMutableArray alloc] init];
        _sectionHeadsKeys = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if([self.type isEqualToString:@"1"]){
        [self setNavigationBar:@"关注的好友"];
        [self addBackButton];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETFRIENDLIST(GETUSERID) Tag:TAG_HTTP_GETFRIENDLIST];
        
        UIButton * btneAdd = GET_BUTTON(CGRectMake(ScreenWidth-45, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
        
        [self.topView addSubview:btneAdd];
        
        [btneAdd setImage:[UIImage imageNamed:@"person_atten"] forState:UIControlStateNormal];
        
        [btneAdd setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
        
        [btneAdd addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [self setNavigationBar:@"粉丝列表"];
        [self asyshowHUDView:WAITTING CurrentView:self];

        [self setHTTPRequest:GETFANSLIST(GETUSERID) Tag:TAG_HTTP_GETFANSLIST];

        [self addBackButton];

    }
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight -64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 59;
    _tableView.separatorStyle = 0;
    
    [self.view addSubview:_tableView];
    
    

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [_friArr removeAllObjects];
    [_sortedArrForArrays removeAllObjects];
    [_sectionHeadsKeys removeAllObjects];
    
    if([self.type isEqualToString:@"1"]){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETFRIENDLIST(GETUSERID) Tag:TAG_HTTP_GETFRIENDLIST];
    }else{
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETFANSLIST(GETUSERID) Tag:TAG_HTTP_GETFANSLIST];
    }
}


- (void)btnNavRightClick{
    //add
    SP_AddFriendViewController *addVC = [[SP_AddFriendViewController alloc]init];
    addVC.userInfoMsg = self.userInfoMsg;
    [self.navigationController pushViewController:addVC animated:YES];

}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETFRIENDLIST -- resopnse:%@\n",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETFRIENDLIST){
            if (HTTP_SUCCESS(code))
            {
                [_friArr removeAllObjects];
                [_sortedArrForArrays removeAllObjects];
                [_sectionHeadsKeys removeAllObjects];
                
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                    SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                    
                    [_friArr addObject:model];
                }
                _sortedArrForArrays = [self getChineseStringArr:_friArr];

                [_tableView reloadData];
                
            }
        }else if (request.tag == TAG_HTTP_DISATTENTION){
                        if (HTTP_SUCCESS(code))
            {
                
                
                NSString *msgStr = dictionary[@"msg"];
                [self showAlertStr:msgStr];
                
                [_friArr removeAllObjects];
                [_sortedArrForArrays removeAllObjects];
                [_sectionHeadsKeys removeAllObjects];

                
                [self setHTTPRequest:GETFRIENDLIST(GETUSERID) Tag:TAG_HTTP_GETFRIENDLIST];
            }else{
                NSString *msgStr = dictionary[@"msg"];
                [self showAlertStr:msgStr];
                
            }

        
        }else if (request.tag == TAG_HTTP_GETFANSLIST){
            if (HTTP_SUCCESS(code))
            {
                [_friArr removeAllObjects];
                [_sortedArrForArrays removeAllObjects];
                [_sectionHeadsKeys removeAllObjects];
                
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                    SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                    
                    [_friArr addObject:model];
                }
                _sortedArrForArrays = [self getChineseStringArr:_friArr];
                
                
                
                [_tableView reloadData];
                
            }
        
        }
    }
    
}


- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort
{
    
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    for(int i = 0; i < [arrToSort count]; i++) {
        
        SP_FriendModel *model = _friArr[i];
        
        
        if(model.nickname==nil){
            model.nickname=@"";
        }
        
        if(![model.nickname isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < model.nickname.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([model.nickname characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            model.pinYin = pinYinResult;
        } else {
            model.pinYin = @"";
        }
        [chineseStringsArray addObject:model];
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        //        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        SP_FriendModel *chineseStr = chineseStringsArray[index];
        
        
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        if(strchar.length != 0){
        NSString *sr= [strchar substringToIndex:1];
        NSLog(@"%@",sr);        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
            }
        }
        }
    }
    
    
    
    
    
    return arrayForArrays;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[_sortedArrForArrays objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_sortedArrForArrays count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_sectionHeadsKeys objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_AddTeamMateCell *cell = [SP_AddTeamMateCell cellWithTableView:tableView];
    
    if([self.type isEqualToString:@"1"])
    {
        cell.type = @"1";

    }
    
    
    
    
    if ([_sortedArrForArrays count] > indexPath.section) {
        NSArray *arr = [_sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            SP_FriendModel *model = (SP_FriendModel *) [arr objectAtIndex:indexPath.row];
            cell.friendModel = model;
            
            cell.cancel.tag = [model.touid integerValue];
            [cell.cancel addTarget:self action:@selector(cancelBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            
        } else {
            NSLog(@"arr out of range");
        }
    } else {
        NSLog(@"sortedArrForArrays out of range");
    }
    
    return cell;
}

- (void)cancelBtnDidClick:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [self setHTTPRequest:DISATTENTION(GETUSERID,touid) Tag:TAG_HTTP_DISATTENTION];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_sortedArrForArrays count] > indexPath.section) {
        NSArray *arr = [_sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            SP_FriendModel *model = (SP_FriendModel *) [arr objectAtIndex:indexPath.row];
            SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
            if([self.type isEqualToString:@"1"]){
                vc.touid = model.touid;
            }else{
                vc.touid = model.uid;
            }
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        } else {
            NSLog(@"arr out of range");
        }
    } else {
        NSLog(@"sortedArrForArrays out of range");
    }


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
