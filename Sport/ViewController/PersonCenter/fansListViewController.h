//
//  fansListViewController.h
//  Sport
//
//  Created by 李松玉 on 15/8/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"


@class SP_UserInfo;
@interface fansListViewController : BaseViewController

@property (strong,nonatomic) SP_UserInfo *userInfoMsg;
@property (nonatomic,copy)NSString *type;//1是关注  2是粉丝


@end
