//
//  SP_TeamInfoViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/5.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_TeamInfoViewController.h"
#import "SP_TeamModel.h"
#import "SP_TeamMateModel.h"
#import "AddPhotoCollectionViewCell.h"
#import "PhotosCollectionReusableView.h"
#import "SP_CommentFrame.h"
#import "SP_CommentModel.h"
#import "SP_CommentCell.h"
#import "SP_UerHomeViewController.h"
#import "TeamPhotoCollectionViewCell.h"
#import "SP_TeamImgCateList.h"
#import "PhotoBroswerVC.h"
#import "SP_AddTeamPhotoViewController.h"
#import "SP_TeamMemberCollectionCell.h"
#import "SP_UerHomeViewController.h"
#import "SP_SendBattleViewController.h"
#import "SP_BoardViewController.h"
#import "SP_BattleInfoViewController.h"
#import "SP_EditTeamInfoViewController.h"


#define BTN_TAG_MSG     101
#define BTN_TAG_BATTLE  102


@interface SP_TeamInfoViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,SP_CommentCellDelegate,UITextFieldDelegate>
{
    UIScrollView *_scrollView;
    
    
    //顶部的球队资料和球员
    UIView *_teamInfoView;
    UIImageView *_teamImgView;
    UILabel *_teamNameLabel;
    UILabel *_teamDesLabel;
    UIImageView *_leaderOneImg;
    UILabel *_leaderOneTag;
    UILabel *_leaderOneName;
    UILabel *_leaderOneTel;
    UICollectionView *_memberCollectionView;
    UIButton *_leaderBtn;
    
    
    
    SP_TeamModel *_teamModel;
    NSMutableArray *_teamUserDataArr;
    SP_TeamMateModel *_leaderOneModel;
    NSMutableArray *_teamMemberArr;
    
    //中间的两个按钮
    UIView *_midBtnView;
    UIButton *_msgBtn;
    UIButton *_battleBtn;
    UILabel *_msgNumLabel;
    UILabel *_battleNumLabel;
    
    
    //相册的View
    UICollectionView *_photosCollectionView;
    NSMutableArray *_imgCateListArr;
    UIView *_noImgsView;
    SP_TeamImgCateList *_currentImgCate;

    
    //评论TableView
    UITableView *_commentTableView;
    NSMutableArray *_commentFrameArr;
    
    
    //底部的评论View
    UIView *_bottomeView;
    UITextField *_msgTextView;
    UIButton *_msgSendBtn;
    
    
    
    //btnType    中间按钮的  --- 挑战0  战术板1
    NSString *_btnType;
    
    
    //有挑战的权限,是一个队伍的队长或者副队长          1  有权限   0没有权限
    NSString *_haveRight;
}

@end




@implementation SP_TeamInfoViewController

- (id)init
{
    self = [super init];
    if(self){
        
        _teamUserDataArr = [[NSMutableArray alloc]init];    //所有队员的信息
        
        _teamMemberArr = [[NSMutableArray alloc]init];      //除了队长,其他所有队员的信息
        
        _commentFrameArr = [[NSMutableArray alloc]init];
        
        _imgCateListArr  = [[NSMutableArray alloc]init];
        
        _btnType =[[NSString alloc]init];
        _haveRight = [[NSString alloc]init];
        
    }
    return self;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBar:self.teamName];
    [self addBackButton];
    
    _btnType = @"0";
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 44)];
    _scrollView.backgroundColor = UIColorFromRGB(0xfafafa);
    [_scrollView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(locdMoreData)];
    [self.view addSubview:_scrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    
    
//    [self setHTTPRequest:GETTEAMINFO(self.teamID) Tag:TAG_HTTP_GETTEAMINFO];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setHTTPRequest:GETJOINTEAM(GETUSERID) Tag:TAG_HTTP_GETJOINTEAM];

}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        
        if(request.tag == TAG_HTTP_GETJOINTEAM){
            if (HTTP_SUCCESS(code))
            {
                NSLog(@"TAG_HTTP_GETJOINTEAM - resopnse:%@",request.responseString);

                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_TeamModel *team = [SP_TeamModel objectWithKeyValues:dict];
                    
                    if(team.teamid.length != 0){
                    
                        if([team.teamid isEqualToString:self.teamID]){
                            
                            _btnType = @"1";
                        }
                        
                        NSArray *memberArr = team.playerData;
                        for(NSDictionary *renDict in memberArr){
                            
                            
    //                        NSLog(@"teamName -- %@",team.name);
    //                        NSLog(@"role -- %@",renDict[@"role"]);

                            if([renDict[@"uid"] isEqualToString:GETUSERID]){
                                NSLog(@"teamName -- %@",team.name);
                                NSLog(@"role -- %@",renDict[@"role"]);
                                
                                if([renDict[@"role"] isEqualToString:@"0"]){
                                    _haveRight = @"0";
                                }else{
                                    _haveRight = @"1";
                                    
                                    
                                }
                                
                            }
                        
                        }
                    }
                    
                    
                }
                [self setHTTPRequest:GETTEAMINFO(self.teamID) Tag:TAG_HTTP_GETTEAMINFO];

            }
        }else if(request.tag == TAG_HTTP_GETTEAMINFO){   //球队信息
            
                if (HTTP_SUCCESS(code))
            {
                [_teamUserDataArr removeAllObjects];
                [_teamMemberArr removeAllObjects];
                NSLog(@"TAG_HTTP_GETTEAMINFO - resopnse:%@",request.responseString);

                //取得球队信息
                NSDictionary *msgDict = dictionary[@"msg"];
                NSDictionary *teamData = msgDict[@"teamData"];
                SP_TeamModel *teamModel = [SP_TeamModel objectWithKeyValues:teamData];
                _teamModel = teamModel;
                
                //取得所有球员信息
                NSArray *teamUserData = msgDict[@"teamUserData"];
                for(NSDictionary *dict in teamUserData){
                    
                    SP_TeamMateModel *model = [SP_TeamMateModel objectWithKeyValues:dict];
                    if([model.isthrough isEqualToString:@"1"]){
                        [_teamUserDataArr addObject:model];
                    }

                }
                
                for(int i = 0;i<_teamUserDataArr.count;i++){
                    
                    SP_TeamMateModel *model = _teamUserDataArr[i];
                    
                    if([model.role isEqualToString:@"2"]){
                        _leaderOneModel = model;
                        
                        if([GETUSERID isEqualToString:model.uid]){
                            UIButton * btnedit = GET_BUTTON(CGRectMake(ScreenWidth-45, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
                            
                            [self.topView addSubview:btnedit];
                            
                            [btnedit setImage:[UIImage imageNamed:@"person_edit"] forState:UIControlStateNormal];
                            
                            [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                            
                            [btnedit addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
                            btnedit.tag = 1;

                        }
                        
                        
                    }else if ([model.role isEqualToString:@"1"]){
                        UIButton * btnedit = GET_BUTTON(CGRectMake(ScreenWidth-45, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
                        
                        [self.topView addSubview:btnedit];
                        
                        [btnedit setImage:[UIImage imageNamed:@"person_edit"] forState:UIControlStateNormal];
                        
                        [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                        
                        [btnedit addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
                        btnedit.tag = 1;

                    
                    }else{
                        [_teamMemberArr addObject:model];
                    }

                    
                    
                    
                }
                
                [self setHTTPRequest:GETTEAMIMGCATELIST(self.teamID) Tag:TAG_HTTP_GETTEAMIMGCATELIST];
                
                
            }
        }else if(request.tag == TAG_HTTP_GETTEAMMSGLIST){   //评论列表
            if(HTTP_SUCCESS(code)){
                //                NSLog(@"TAG_HTTP_PUBLISHMATCHMSG - resopnse:%@",request.responseString);
                [_commentFrameArr removeAllObjects];
                NSDictionary *msgDict = dictionary[@"msg"];
                NSArray *msgArr = msgDict[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_CommentFrame *frame = [[SP_CommentFrame alloc]init];
                    SP_CommentModel *msg = [SP_CommentModel objectWithKeyValues:dict];
                    frame.msgModel = msg;
                    [_commentFrameArr addObject:frame];
                }
                
                
                [self resizeCommentTableViewHeight];
                [_scrollView.footer endRefreshing];
                [_commentTableView reloadData];
                
                
                
            }
        }else if (request.tag == TAG_HTTP_PUBLISHTEAMMSG){   //发布评论
            if(HTTP_SUCCESS(code)){
                //                NSLog(@"TAG_HTTP_PUBLISHTEAMMSG - resopnse:%@",request.responseString);
                [self showMessage:@"评论成功"];
                _msgTextView.text = @"";
            }else{
                [self showMessage:@"评论失败"];
            }
        }else if (request.tag == TAG_HTTP_TEAMMSGPRAISE){   //评论点赞
            if(HTTP_SUCCESS(code)){
                //                NSLog(@"TAG_HTTP_TEAMMSGPRAISE - resopnse:%@",request.responseString);
            }
            
        }else if (request.tag == TAG_HTTP_GETTEAMIMGCATELIST){   //相册列表
            if(HTTP_SUCCESS(code)){
                NSArray *msgArr = dictionary[@"msg"];
                [_imgCateListArr removeAllObjects];
                for(NSDictionary *dict in msgArr){
                    
                    SP_TeamImgCateList *list = [SP_TeamImgCateList objectWithKeyValues:dict];
                    [_imgCateListArr addObject:list];
                    
                }
                
                
                //                创建页面的视图
                [self bulidUI];
                [self setHTTPRequest:GETTEAMMSGLIST(self.teamID, @"0", @"5", GETUSERID) Tag:TAG_HTTP_GETTEAMMSGLIST];
            }
        }else if (request.tag == TAG_HTTP_GETTEAMCATEIMG){     //获取相册里面的图片地址
            if(HTTP_SUCCESS(code)){
//                NSLog(@"TAG_HTTP_GETTEAMCATEIMG - resopnse:%@",request.responseString);
                NSArray *msgArr = dictionary[@"msg"];
                NSMutableArray *imgArr = [[NSMutableArray alloc]init];
                for(NSDictionary *dict in msgArr){
                    NSString *url = dict[@"img"];
                    [imgArr addObject:url];
                }
                
                if(imgArr.count !=0 ){  //显示图片
                    [self networkImageShow:0 imgArr:imgArr];
                }else{
                    //尚未添加图片,进入添加图片控制器
                    [self createNoPhotoView];
                }
                
            }
        
        }
        
        
        
        
        
        
    }
}

- (void) btnNavRightClick:(UIButton *)btn
{
    SP_EditTeamInfoViewController *vc = [[SP_EditTeamInfoViewController alloc]init];
    vc.teamID = self.teamID;
    vc.teamName = self.teamName;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



- (void) bulidUI
{
    [self setUpCommentView];
    [self setUpTeamInfoView];
    [self setUpMidBtnView];
    [self setUpPhotosCollectionView];
    [self setUpCommentTableView];
}

- (void) resizeCommentTableViewHeight
{
    //重新计算tableView和scrollView的高度
    CGFloat tableViewHeight;
    for(SP_CommentFrame *frame in _commentFrameArr){
        tableViewHeight += frame.cellHeight;
    }
    
    _commentTableView.frame = CGRectMake(0, CGRectGetMaxY(_photosCollectionView.frame) + 10, ScreenWidth, tableViewHeight + 40);
    _scrollView.contentSize = CGSizeMake(ScreenWidth, CGRectGetMaxY(_commentTableView.frame));
    
}




#pragma mark - 设置顶部的TeamInfoView
- (void) setUpTeamInfoView
{
    _teamInfoView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 135)];
    _teamInfoView.backgroundColor = [UIColor whiteColor];
    
    _teamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 45, 45)];
    _teamImgView.layer.cornerRadius = 8;
    _teamImgView.layer.masksToBounds = YES;
    NSURL *imgURL = [NSURL URLWithString:_teamModel.img];
    [_teamImgView sd_setImageWithURL:imgURL];
    
    
    
    _teamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_teamImgView.frame) + 10, 15, 200, 14)];
    _teamNameLabel.font = [UIFont systemFontOfSize:14];
    _teamNameLabel.text = _teamModel.name;
    
    
    _teamDesLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_teamImgView.frame) + 10, CGRectGetMaxY(_teamNameLabel.frame) + 5, ScreenWidth - CGRectGetMaxX(_teamImgView.frame) - 20, 13)];
    _teamDesLabel.font = [UIFont systemFontOfSize:13];
    _teamDesLabel.text = _teamModel.desc;
    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_teamImgView.frame) + 10, ScreenWidth - 20, 1)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    _leaderOneImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 10, 45, 45)];
    _leaderOneImg.layer.cornerRadius = 8;
    _leaderOneImg.layer.masksToBounds = YES;
    _leaderOneImg.layer.borderWidth = 1;
    _leaderOneImg.layer.borderColor = [UIColor redColor].CGColor;
    NSURL *oneUrl = [NSURL URLWithString:_leaderOneModel.headimg];
    [_leaderOneImg sd_setImageWithURL:oneUrl];
    
    
    _leaderBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 10, 45, 45)];
    [_leaderBtn addTarget:self action:@selector(leaderBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UILabel *duizhang = [[UILabel alloc]initWithFrame:CGRectMake(45, CGRectGetMaxY(midLine.frame) + 4, 26, 13)];
    duizhang.font = [UIFont systemFontOfSize:13];
    duizhang.text = @"队长";
    duizhang.backgroundColor = UIColorFromRGB(0xe67f22);
    duizhang.layer.cornerRadius = 5;
    duizhang.layer.masksToBounds = YES;
    duizhang.textColor = [UIColor whiteColor];
    
    
    _leaderOneName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderOneImg.frame) + 10, CGRectGetMaxY(midLine.frame) + 25, 100, 13)];
    _leaderOneName.font = [UIFont systemFontOfSize:13];
    _leaderOneName.text = _leaderOneModel.nickname;
    _leaderOneName.textColor = UIColorFromRGB(0x696969);
    
    
    _leaderOneTel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderOneImg.frame) + 10, CGRectGetMaxY(_leaderOneName.frame) + 5, 100, 12)];
    _leaderOneTel.font = [UIFont systemFontOfSize:13];
    _leaderOneTel.text = _leaderOneModel.tel;
    _leaderOneTel.textColor = UIColorFromRGB(0x696969);
    
    
    // 1.流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 2.每个cell的尺寸
    layout.itemSize = CGSizeMake(60, 90);
    // 3.设置cell之间的水平间距
    layout.minimumInteritemSpacing = 10;
    // 4.设置cell之间的垂直间距
    layout.minimumLineSpacing = 10;
    // 5.设置四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(layout.minimumLineSpacing, 10, 0, 10);
    
    
    
    
    _memberCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_leaderOneImg.frame), ScreenWidth, 100 + _teamMemberArr.count / 4 * 90 ) collectionViewLayout:layout];
    _memberCollectionView.tag = 666;
    _memberCollectionView.delegate = self;
    _memberCollectionView.dataSource = self;
    _memberCollectionView.scrollEnabled = NO;
    _memberCollectionView.backgroundColor = [UIColor whiteColor];

    [_memberCollectionView registerNib:[UINib nibWithNibName:@"SP_TeamMemberCollectionCell" bundle:nil]  forCellWithReuseIdentifier:@"SP_TeamMemberCollectionCell"];

    
    _teamInfoView.frame = CGRectMake(0, 0, ScreenWidth, 135 + _memberCollectionView.frame.size.height);
    
    [_scrollView addSubview:_teamInfoView];
    [_teamInfoView addSubview:_teamImgView];
    [_teamInfoView addSubview:_teamNameLabel];
    [_teamInfoView addSubview:_teamDesLabel];
    [_teamInfoView addSubview:midLine];
    [_teamInfoView addSubview:_leaderOneImg];
    [_teamInfoView addSubview:_leaderBtn];
    [_teamInfoView addSubview:_leaderOneName];
    [_teamInfoView addSubview:_leaderOneTel];
    [_teamInfoView addSubview:duizhang];
    [_teamInfoView addSubview:_memberCollectionView];
    
    
}


- (void) leaderBtnDidClick
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = _leaderOneModel.uid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


#pragma mark - 设置中间的按钮View
- (void) setUpMidBtnView
{
    _midBtnView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_teamInfoView.frame) + 10, ScreenWidth, 40)];
    _midBtnView.backgroundColor = [UIColor whiteColor];
    
    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth / 2, 4, 1, 32)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);

    
    _msgBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth/2, 40)];
    _msgBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_msgBtn setTitle:@"战术板" forState:UIControlStateNormal];
    [_msgBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_msgBtn addTarget:self action:@selector(msgBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _battleBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 40)];
    _battleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_battleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    if([_btnType isEqualToString:@"0"]){
        _msgBtn.hidden = YES;
        _battleBtn.frame = CGRectMake(0, 0, ScreenWidth, 40);
        [_battleBtn setTitle:@"向他发起挑战" forState:UIControlStateNormal];
        _battleBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [_battleBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_battleBtn addTarget:self action:@selector(sendBattleBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [_battleBtn setTitle:@"对战邀请信息" forState:UIControlStateNormal];
        [_battleBtn addTarget:self action:@selector(battleInfoBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [_midBtnView addSubview:midLine];

    }
    
    
    
    
    
    
    
    [_scrollView addSubview:_midBtnView];
    [_midBtnView addSubview:_msgBtn];
    [_midBtnView addSubview:_battleBtn];
//    [_midBtnView addSubview:_msgNumLabel];
//    [_midBtnView addSubview:_battleNumLabel];
    
}


#pragma mark - 战术板点击事件
- (void) msgBtnDidClick:(UIButton *)btn
{
    SP_BoardViewController *vc = [[SP_BoardViewController alloc]init];
    vc.teamID = self.teamID;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



#pragma mark - 发起挑战点击事件
- (void) sendBattleBtnDidClick:(UIButton *)btn
{
    if([_haveRight isEqualToString:@"0"]){
        [self showMessage:@"您还没有权限"];
    }else{
        SP_SendBattleViewController *vc = [[SP_SendBattleViewController alloc]init];
        vc.bteamid = self.teamID;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}

#pragma mark - 对战邀请详情点击事件
- (void) battleInfoBtnDidClick:(UIButton *)btn
{
    SP_BattleInfoViewController *vc = [[SP_BattleInfoViewController alloc]init];
    vc.teamid = self.teamID;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



#pragma mark 设置相册的CollectionView
- (void) setUpPhotosCollectionView
{
    // 1.流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 2.每个cell的尺寸
    layout.itemSize = CGSizeMake(110, 110);
    // 3.设置cell之间的水平间距
    layout.minimumInteritemSpacing = 10;
    // 4.设置cell之间的垂直间距
    layout.minimumLineSpacing = 10;
    // 5.设置四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(layout.minimumLineSpacing, 10, 0, 10);
    
    _photosCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_midBtnView.frame) + 10, ScreenWidth, 160 + _imgCateListArr.count / 2 * 100 + 80) collectionViewLayout:layout];
    _photosCollectionView.tag = 999;
    _photosCollectionView.delegate = self;
    _photosCollectionView.dataSource = self;
    _photosCollectionView.scrollEnabled = NO;
    _photosCollectionView.backgroundColor = [UIColor whiteColor];
    
    
    //    UINib * nib = [UINib nibWithNibName: @"AddPhotoCollectionViewCell" bundle: nil];
    [_photosCollectionView registerNib:[UINib nibWithNibName:@"AddPhotoCollectionViewCell" bundle:nil]  forCellWithReuseIdentifier:@"AddPhotoCollectionViewCell"];
    
    [_photosCollectionView registerNib:[UINib nibWithNibName:@"TeamPhotoCollectionViewCell" bundle:nil]  forCellWithReuseIdentifier:@"TeamPhotoCollectionViewCell"];
    
    
    [_photosCollectionView registerNib:[UINib nibWithNibName:@"PhotosCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"PhotosCollectionReusableView"];
    
    
    
    [_scrollView addSubview:_photosCollectionView];
    
}


#pragma - mark CollectView datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView.tag == 999){
        return _imgCateListArr.count + 1;
    }else if(collectionView.tag == 666){
        return _teamMemberArr.count;
    }
    return 0;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(collectionView.tag == 999){
        if(indexPath.row == _imgCateListArr.count){
            static NSString * myidentify  = @"AddPhotoCollectionViewCell";
            
            AddPhotoCollectionViewCell *cell = nil;
            
            cell = (AddPhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
            
            return cell;
            
        }else{
            static NSString * myidentify  = @"TeamPhotoCollectionViewCell";
            
            TeamPhotoCollectionViewCell *cell = nil;
            cell = (TeamPhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
            
            SP_TeamImgCateList *imgCateList = _imgCateListArr[indexPath.row];
            
            cell.imgCateList = imgCateList;
            
            NSLog(@"imgCateList == %@",imgCateList.name);
            
            
            return cell;
            
        }
    }else if (collectionView.tag == 666){
        
        static NSString * myidentify  = @"SP_TeamMemberCollectionCell";
        
        SP_TeamMemberCollectionCell *cell = nil;
        
        cell = (SP_TeamMemberCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
        
        SP_TeamMateModel *model = _teamMemberArr[indexPath.row];
        cell.mateInfo = model;
        
        return cell;
    
    }
    
    
    
    return nil;
}

- (void) collectionView:(nonnull UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if(collectionView.tag == 999){
        if(indexPath.row == _imgCateListArr.count ){
            SP_AddTeamPhotoViewController *addVC = [[SP_AddTeamPhotoViewController alloc]init];
            addVC.teamID = self.teamID;
            [[AppDelegate sharedAppDelegate].nav pushViewController:addVC animated:YES];
            
        }else{
            SP_TeamImgCateList *imgCateList = _imgCateListArr[indexPath.row];
            _currentImgCate = imgCateList;
            [self setHTTPRequest:GETTEAMCATEIMG(imgCateList.teamimgcateid) Tag:TAG_HTTP_GETTEAMCATEIMG];
        }
    }else if (collectionView.tag == 666){
        SP_UerHomeViewController *userVC = [[SP_UerHomeViewController alloc]init];
        SP_TeamMateModel *model = _teamMemberArr[indexPath.row];
        userVC.touid = model.uid;
        [[AppDelegate sharedAppDelegate].nav pushViewController:userVC animated:YES];

    }
}


/*
 *  展示网络图片
 */
-(void)networkImageShow:(NSUInteger)index imgArr:(NSArray *)networkImages
{
    
    [PhotoBroswerVC show:self type:PhotoBroswerVCTypePush cateListModel:_currentImgCate index:index photoModelBlock:^NSArray *{
        
        NSMutableArray *modelsM = [NSMutableArray arrayWithCapacity:networkImages.count];
        for (NSUInteger i = 0; i< networkImages.count; i++) {
            
            PhotoModel *pbModel=[[PhotoModel alloc] init];
            pbModel.mid = i + 1;
            pbModel.title = _currentImgCate.name;
            pbModel.desc = _currentImgCate.date;
            pbModel.image_HD_U = networkImages[i];
            pbModel.cateModel = _currentImgCate;

            
            [modelsM addObject:pbModel];
        }
        
        return modelsM;
    }];
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if(collectionView.tag == 999){
        CGSize size={ScreenWidth,40};
        return size;
    }
    return CGSizeMake(0, 0);
}



- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView.tag == 999){
        NSString *reuseIdentifier = @"PhotosCollectionReusableView";
        PhotosCollectionReusableView *view =  (PhotosCollectionReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
    
        return view;
    }
    return nil;
}



#pragma mark 设置评论TableView
- (void) setUpCommentTableView
{
    _commentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_photosCollectionView.frame) + 10, ScreenWidth, 200)];
    _commentTableView.delegate = self;
    _commentTableView.dataSource = self;
    _commentTableView.scrollEnabled = NO;
    
    
    [_scrollView addSubview:_commentTableView];
    
    
}

#pragma mark - 上拉加载更多
- (void)locdMoreData
{
    SP_CommentFrame *frame = [_commentFrameArr lastObject];
    
    NSString *teammsgid = frame.msgModel.teammsgid;
    
    [self setHTTPRequest:GETTEAMMSGLIST(self.teamID, teammsgid, @"5", GETUSERID) Tag:TAG_HTTP_GETTEAMMSGLIST];
    
}


#pragma mark _tableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _commentFrameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SP_CommentCell *cell = [SP_CommentCell cellWithTableView:tableView];
    cell.delegate = self;
    SP_CommentFrame *frame = _commentFrameArr[indexPath.row];
    cell.commentFrame = frame;
    
    //    cell.tag = [frame.msgModel.teammsgid integerValue] + 200;
    
    [cell.praiseBtn addTarget:self action:@selector(praiseBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.praiseBtn.tag = [frame.msgModel.teammsgid integerValue] + 200;
    
    
    cell.headButton.tag = [frame.msgModel.uid integerValue] + 400;
    [cell.headButton addTarget:self action:@selector(headBtnDidClick:) forControlEvents:UIControlEventTouchUpInside]
    ;
    
    
    return cell;
}

- (void) praiseBtnDidClick:(UIButton *)btn
{
    NSString *teammsgID = [NSString stringWithFormat:@"%ld",btn.tag - 200];
    [self setHTTPRequest:TEAMMSGPRAISE(GETUSERID, teammsgID) Tag:TAG_HTTP_TEAMMSGPRAISE];
    
}

- (void) headBtnDidClick:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 400];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_CommentFrame *frame = _commentFrameArr[indexPath.row];
    return frame.cellHeight;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 45)];
    view.backgroundColor = UIColorFromRGB(0xffffff);
    
    UIView *topVoew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 5)];
    topVoew.backgroundColor = UIColorFromRGB(0xfafafa);
    [view addSubview:topVoew];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 40, CGRectGetHeight(view.bounds))];
    label.textColor = UIColorFromRGB(0xe74c38);
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"评论";
    [view addSubview:label];
    
    UILabel *commentNum = [[UILabel alloc]initWithFrame:CGRectMake(58, 0, 40, CGRectGetHeight(view.bounds))];
    commentNum.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_commentFrameArr.count];
    commentNum.font = [UIFont systemFontOfSize:15];
    commentNum.textColor = UIColorFromRGB(0x696969);
    [view addSubview:commentNum];
    return view;
    
}



#pragma mark - 设置底部的评论视图
- (void) setUpCommentView
{
    _bottomeView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 44, ScreenWidth, 44)];
    _bottomeView.backgroundColor = UIColorFromRGB(0xfafafa);
    
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    _msgSendBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, 8, 54, 30)];
    UIImage *btnImg = [GT_Tool createImageWithColor:UIColorFromRGB(GREEN_COLOR_VALUE)];
    [_msgSendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [_msgSendBtn setBackgroundImage:btnImg forState:UIControlStateNormal];
    [_msgSendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _msgSendBtn.layer.cornerRadius = 5;
    _msgSendBtn.layer.masksToBounds = YES;
    [_msgSendBtn addTarget:self action:@selector(sendBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    _msgTextView = [[UITextField alloc]initWithFrame:CGRectMake(8, 7, CGRectGetMinX(_msgSendBtn.frame) - 10, 30)];
    _msgTextView.placeholder = @"我想说两句";
    _msgTextView.font = [UIFont systemFontOfSize:13];
    _msgTextView.borderStyle = UITextBorderStyleRoundedRect;
    _msgTextView.delegate = self;
    
    
    
    
    [self.view addSubview:_bottomeView];
    [_bottomeView addSubview:topLine];
    [_bottomeView addSubview:_msgSendBtn];
    [_bottomeView addSubview:_msgTextView];
    
    
}


- (void) sendBtnDidClick
{
    if(_msgTextView.text.length == 0){
        [self showMessage:@"评论不能为空"];
        return;
    }
    
    [self setHTTPRequest:PUBLISHMATCHMSG(GETUSERID, self.teamID, _msgTextView.text) Tag:TAG_HTTP_PUBLISHTEAMMSG];
}




#pragma mark - 监听键盘
- (void)keyboardWillShow:(NSNotification *)notification
{
    //得到键盘高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    self.view.frame = CGRectMake(0, -keyboardRect.size.height, ScreenWidth, ScreenHeight);
    
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //使textField取消第一响应者，从而隐藏键盘
    [textField resignFirstResponder];
    return YES;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.editing = NO;
}




#pragma mark - 创建没有图片时的View
- (void) createNoPhotoView
{
    _noImgsView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _noImgsView.backgroundColor = [UIColor blackColor];
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(noImgsViewSingleTapMethod)];
    [_noImgsView addGestureRecognizer:singtap];

    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 30, 39, 44)];
    NSString *normalStr = @"back_btn.png";
    [backBtn setBackgroundImage:[UIImage imageNamed:normalStr] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(noPicBackBtnDidClick) forControlEvents:UIControlEventTouchUpInside];

    UILabel *noticeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, ScreenHeight/2 - 30, ScreenWidth, 60)];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.text = @"您没有上传图片哦,赶紧去添加吧~~";
    noticeLabel.textColor = [UIColor whiteColor];
    
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 150, ScreenWidth, 50)];
    bottomView.backgroundColor = [UIColor darkGrayColor];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 50)];
    name.textAlignment = NSTextAlignmentLeft;
    name.text = _currentImgCate.name;
    name.textColor = [UIColor whiteColor];
    
    UILabel *time = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 200, 0, 180, 50)];
    time.textAlignment = NSTextAlignmentRight;
    time.text = _currentImgCate.date;
    time.textColor = [UIColor whiteColor];
    
    UIButton *pushBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(bottomView.frame) + 40, 200, 40)];
    [pushBtn setTitle:@"添加图片" forState:UIControlStateNormal];
    [pushBtn setBackgroundImage:[UIImage imageNamed:@"添加图片1"] forState:UIControlStateNormal];
    [pushBtn addTarget:self action:@selector(noimgPushToAddVc) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:_noImgsView];
    [_noImgsView addSubview:backBtn];
    [_noImgsView addSubview:noticeLabel];
    [_noImgsView addSubview:bottomView];
    [bottomView addSubview:name];
    [bottomView addSubview:time];
    [_noImgsView addSubview:pushBtn];



}


- (void) noImgsViewSingleTapMethod
{
    [_noImgsView removeFromSuperview];
}

- (void) noPicBackBtnDidClick
{
    [_noImgsView removeFromSuperview];
}

- (void) noimgPushToAddVc
{
    SP_AddTeamPhotoViewController *addVC = [[SP_AddTeamPhotoViewController alloc]init];
    addVC.imgCate = _currentImgCate;
    [_noImgsView removeFromSuperview];
    [[AppDelegate sharedAppDelegate].nav pushViewController:addVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
