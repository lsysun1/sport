//
//  SP_AddFriendViewController.m
//  Sport
//
//  Created by WT_lyy on 15/4/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddFriendViewController.h"
#import "AddfriendReusableView.h"
#import "AddfriendCollectionCell.h"
#import "SP_FriendModel.h"
#import "SP_UserInfo.h"
#import "AddCollectionFooterView.h"
#import "SP_UerHomeViewController.h"

#define USERCOLLECTIONVIEW 10
#define COLLECTIONVIEW     20


@interface SP_AddFriendViewController()<UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate>{

    UIScrollView *_scrollView;
    
    UICollectionView * _collectView;

    

    UIButton *_schoolNextBtn;
    UIButton *_idolNextBtn;

    
    NSMutableArray * _firIntroArr;
    
    NSMutableArray * _secIntroARR;
    
    UITextField * _textfInput;
    
    UIButton *_currentFocusBtn;
    
    UIView *_searchUserView;
    UIImageView *_userHeadImg;
    UILabel *_userName;
    UILabel *_userAge;
    UIButton *_userFocusBtn;
    UILabel *_userIdol;
    UILabel *_userCoSchool;
    UILabel *_userSign;
    UILabel *_userTag;
    
    UICollectionView * _userCollectionView;
    NSDictionary *_userDict;
    NSMutableArray *_userAttentionArr;
    
    
    
    
}

@end

@implementation SP_AddFriendViewController

- (id)init{

    self = [super init];
    
    if (self) {
        
        _firIntroArr = [[NSMutableArray alloc] init];
        
        _secIntroARR = [[NSMutableArray alloc] init];
        
        _userAttentionArr = [[NSMutableArray alloc]init];
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationBar:@"添加好友"];
    [self addBackButton];
    
    //初始化搜索栏
    [self _initSearchView];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, [self viewTop]+54, ScreenWidth, ScreenHeight - [self viewTop]+54)];
    _scrollView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setItemSize:CGSizeMake(ScreenWidth/2-10, 50)];//设置cell的尺寸
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//设置其布局方向
    flowLayout.sectionInset = UIEdgeInsetsMake(8, 0, 0, 5);//设置其边界
    _collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, [self viewTop]+54, ScreenWidth, ScreenHeight-[self viewTop]-54) collectionViewLayout:flowLayout];
    UINib* nib = [UINib nibWithNibName: @"AddfriendCollectionCell" bundle: nil];
    
    [_collectView registerNib:nib forCellWithReuseIdentifier:@"AddfriendCollectionCell"];
    
    [self.view addSubview:_collectView];
    [_collectView registerNib:[UINib nibWithNibName:@"AddfriendReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"AddfriendReusableView"];
    [_collectView registerNib:[UINib nibWithNibName:@"AddCollectionFooterView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"AddCollectionFooterView"];

    _collectView.tag = COLLECTIONVIEW;
    _collectView.dataSource = self;
    _collectView.delegate = self;
    
    _collectView.backgroundColor = [UIColor whiteColor];
    
    
    
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETSAMEIDOLFRIEND(GETUSERID) Tag:TAG_HTTP_GETSAMEIDOLFRIEND];
    [self setHTTPRequest:GETRECFRIEND(GETUSERID) Tag:TAG_HTTP_GETRECFRIEND];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate].drawerController closeDrawerAnimated:NO completion:nil];
    
}



#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETSAMEIDOLFRIEND){
            if (HTTP_SUCCESS(code))
            {
                NSLog(@"TAG_HTTP_GETSAMEIDOLFRIEND - resopnse:%@",request.responseString);

                NSDictionary *msgDict = dictionary[@"msg"];
                if (msgDict[@"friendData"] == [NSNull null]){
                    return;
                }
                
                NSArray *friendDataArr = msgDict[@"friendData"];
                if(friendDataArr.count != 0){

                    [_firIntroArr removeAllObjects];
                    for(NSDictionary *dict in friendDataArr){
                        SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                        [_firIntroArr addObject:model];
                    
                    
                    }
                    
    //                NSLog(@"_firIntroArr.count -- %ld",_firIntroArr.count);
                    NSIndexSet *sec = [[NSIndexSet alloc]initWithIndex:1];
                    [_collectView reloadSections:sec];
                }
                
            }
        }else if (request.tag == TAG_HTTP_GETRECFRIEND){
            NSLog(@"TAG_HTTP_GETRECFRIEND - resopnse:%@",request.responseString);

            NSDictionary *msgDict = dictionary[@"msg"];
            NSArray *friendDataArr = msgDict[@"friendData"];
            if (HTTP_SUCCESS(code))
            {
            [_secIntroARR removeAllObjects];
            for(NSDictionary *dict in friendDataArr){
                SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                [_secIntroARR addObject:model];
                
                }

            
        NSIndexSet *sec = [[NSIndexSet alloc]initWithIndex:0];
        [_collectView reloadSections:sec];
            }
        
        }else if (request.tag == TAG_HTTP_ATTENTION){
            
        
//            NSLog(@"添加好友 - resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                NSString *msgStr = dictionary[@"msg"];
                [self showAlertStr:msgStr];
                _currentFocusBtn.selected = YES;
            }else{
                [self showAlertStr:@"关注失败"];

            }
        
        
        }else if (request.tag == TAG_HTTP_SEARCHUSER){
            NSLog(@"搜索用户 - resopnse:%@",request.responseString);
            [_userAttentionArr removeAllObjects];
            if(HTTP_SUCCESS(code)){
                _userDict = dictionary[@"msg"];
                NSArray *attentionArr = _userDict[@"attentionData"];
                for(NSDictionary *dict in attentionArr){
                    SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                    [_userAttentionArr addObject:model];
                }
                [self setUpSearchUserView];
                [_userCollectionView reloadData];
            }
        }else if (request.tag == TAG_HTTP_DISATTENTION){
            
            
            //            NSLog(@"添加好友 - resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                NSString *msgStr = dictionary[@"msg"];
                [self showAlertStr:msgStr];
                _currentFocusBtn.selected = NO;
            }else{
                NSString *msgStr = dictionary[@"msg"];
                [self showAlertStr:msgStr];
                
            }
        }
    }
}





- (void)_initSearchView{

    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, [self viewTop]-1, ScreenWidth+2, 55)];
    
    [self.view addSubview:view];
    
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView * inputView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-20*2, 34)];
    [view addSubview:inputView];
    inputView.backgroundColor = [UIColor whiteColor];
    inputView.layer.borderWidth = 1;
    
    inputView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    inputView.layer.cornerRadius = 2;
    
    _textfInput = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, inputView.frame.size.width-60, 24)];
    [inputView addSubview:_textfInput];
    _textfInput.delegate = self;
    _textfInput.keyboardType = UIKeyboardTypePhonePad;
    _textfInput.placeholder = @"输入手机号查找";
    _textfInput.returnKeyType = UIReturnKeySearch;
    
    UIView * viewVerline = [[UIView alloc] initWithFrame:CGRectMake(inputView.frame.size.width - 50, 5, 1, 24)];
    [inputView addSubview:viewVerline];
    viewVerline.backgroundColor = UIColorFromRGB(0xe5e5e5);
    
    UIButton * btnSearch = GET_BUTTON(CGRectMake(inputView.frame.size.width-50, 0, 40, 34), 14, NO, nil);
    [inputView addSubview:btnSearch];
    [btnSearch setImage:[UIImage imageNamed:@"person_search"] forState:UIControlStateNormal];
    [btnSearch setImageEdgeInsets:UIEdgeInsetsMake(7, 10, 7, 10)];
    [btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
}
#pragma -mark Btn Action
- (void)btnSearchClick{
    [_textfInput resignFirstResponder];

    if (![GT_Tool Isphonenumber:_textfInput.text]) {
        
        [self showAlertStr:@"请输入正确的手机号"];
        
        return;
    }
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:SEARCHUSER(GETUSERID, _textfInput.text) Tag:TAG_HTTP_SEARCHUSER];
    
    [_collectView removeFromSuperview];
    [self.view addSubview:_scrollView];
    

}

#pragma -mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [_textfInput resignFirstResponder];
    
    
    //直接请求接口
    [self btnSearchClick];
    
    return YES;
}

//UIView *_searchUserView;
//UIImageView *_userHeadImg;
//UILabel *_userName;
//UILabel *_userAge;
//UIButton *_userFocusBtn;
//UILabel *_userIdol;
//UILabel *_userCoSchool;
//UILabel *_userSign;
//UILabel *_userTag;
#pragma mark 设置搜索用户顶部的界面
- (void) setUpSearchUserView
{
    _searchUserView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 190)];
    _searchUserView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_searchUserView];
    
    _userHeadImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 45, 45)];
    _userHeadImg.layer.cornerRadius = 8;
    _userHeadImg.layer.masksToBounds = YES;
    _userHeadImg.backgroundColor = [UIColor blackColor];
    NSURL *url = [NSURL URLWithString:_userDict[@"headimg"]];
    [_userHeadImg sd_setImageWithURL:url];
    if([_userDict[@"sex"] isEqualToString:@"0"]){
        _userHeadImg.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    }else{
        _userHeadImg.layer.borderColor = UIColorFromRGB(0x599ffff).CGColor;
    }
    
    
    _userName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_userHeadImg.frame) + 10, 18, 200, 14)];
    _userName.text = [NSString stringWithFormat:@"%@",_userDict[@"nickname"]];
    _userName.font = [UIFont systemFontOfSize:15];
    
    _userAge = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_userHeadImg.frame) + 10, CGRectGetMaxY(_userName.frame) + 10, 200, 13)];
    _userAge.text = @"保密";
    _userAge.font = [UIFont systemFontOfSize:14];
    _userAge.textColor = UIColorFromRGB(0x696969);
    
    _userFocusBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, 25, 50, 22)];
    [_userFocusBtn setImage:[UIImage imageNamed:@"person_attend.png"] forState:UIControlStateNormal];
    [_userFocusBtn setImage:[UIImage imageNamed:@"person_add_attened"] forState:UIControlStateSelected];
    _userFocusBtn.tag = 100 + [_userDict[@"uid"] intValue];
    [_userFocusBtn addTarget:self action:@selector(foucsFrinedBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    if([_userDict[@"isattention"] isEqualToString:@"yes"]){
        _userFocusBtn.selected = YES;
    }else{
        _userFocusBtn.selected = NO;
    }
    

    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_userHeadImg.frame) + 15, ScreenWidth -20, 1)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    _userIdol = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 10, 200, 15)];
    _userIdol.text = [NSString stringWithFormat:@"偶像:%@",_userDict[@"idol"]];
    _userIdol.font = [UIFont systemFontOfSize:15];
    
    _userCoSchool = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_userIdol.frame) + 10, 200, 15)];
    _userCoSchool.font = [UIFont systemFontOfSize:15];
    if([_userDict[@"school"] isEqualToString:@""]){
        _userCoSchool.text = [NSString stringWithFormat:@"公司:%@",_userDict[@"company"]];
    }else if([_userDict[@"company"] isEqualToString:@""]){
        _userCoSchool.text = [NSString stringWithFormat:@"学校:%@",_userDict[@"school"]];
    }
    

    _userSign = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_userCoSchool.frame) + 10, 200, 15)];
    _userSign.text = [NSString stringWithFormat:@"个性签名:%@",_userDict[@"signature"]];
    _userSign.font = [UIFont systemFontOfSize:15];
    
    _userTag = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_userSign.frame) + 10, ScreenWidth - 15, 15)];
    _userTag.text = @"标签:暂无";
    _userTag.font = [UIFont systemFontOfSize:15];
    
    
    
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setItemSize:CGSizeMake(ScreenWidth/2-10, 50)];//设置cell的尺寸
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//设置其布局方向
    flowLayout.sectionInset = UIEdgeInsetsMake(8, 0, 0, 5);//设置其边界
    _userCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userTag.frame) + 20, ScreenWidth, ScreenHeight-[self viewTop]-54) collectionViewLayout:flowLayout];
    UINib * nib = [UINib nibWithNibName: @"AddfriendCollectionCell" bundle: nil];
    
    [_userCollectionView registerNib:nib forCellWithReuseIdentifier:@"AddfriendCollectionCell"];
    [_userCollectionView registerNib:[UINib nibWithNibName:@"AddfriendReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"AddfriendReusableView"];
    [_userCollectionView registerNib:[UINib nibWithNibName:@"AddCollectionFooterView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"AddCollectionFooterView"];
    
    _userCollectionView.tag = USERCOLLECTIONVIEW;
    _userCollectionView.dataSource = self;
    _userCollectionView.delegate = self;
    
    _userCollectionView.backgroundColor = [UIColor whiteColor];

    
    _scrollView.contentSize = CGSizeMake(ScreenWidth, ScreenHeight + 100);
    
    [_searchUserView addSubview:_userHeadImg];
    [_searchUserView addSubview:_userName];
    [_searchUserView addSubview:_userAge];
    [_searchUserView addSubview:_userFocusBtn];
    [_searchUserView addSubview:midLine];
    [_searchUserView addSubview:_userIdol];
    [_searchUserView addSubview:_userCoSchool];
    [_searchUserView addSubview:_userSign];
    [_searchUserView addSubview:_userTag];
    [_scrollView addSubview:_userCollectionView];

}


#pragma -mark CollectView datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView.tag == COLLECTIONVIEW){
        if (1 == section) {

            return _firIntroArr.count < 6 ? _firIntroArr.count:6;

        }else if (0 == section){

            return _secIntroARR.count < 6 ? _secIntroARR.count:6;
        }
    }else{
    
        return _userAttentionArr.count < 6 ? _userAttentionArr.count:6;
    }
    
    return 0;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView.tag == COLLECTIONVIEW){
        return 2;
    }else{
        return 1;
    }
    
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{

    static NSString * myidentify  = @"AddfriendCollectionCell";
    
    AddfriendCollectionCell *cell = nil;
    
    cell = (AddfriendCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
    [cell.focusBtn addTarget:self action:@selector(foucsFrinedBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.headbtn addTarget:self action:@selector(headbtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    if(collectionView.tag == COLLECTIONVIEW){
        if(indexPath.section == 0){
            if(_secIntroARR.count != 0){
//                NSLog(@"_secIntroARR.count == %ld",_secIntroARR.count);
                SP_FriendModel *model = _secIntroARR[indexPath.row];
                cell.friendModel = model;
                cell.focusBtn.tag = 100 + [cell.friendModel.uid intValue];
                cell.headbtn.tag = 200 + [cell.friendModel.uid intValue];

            }
        }
        else if(indexPath.section == 1)
        {
            if(_firIntroArr.count != 0){
//                NSLog(@"_firIntroArr == %ld",_secIntroARR.count);
                SP_FriendModel *model = _firIntroArr[indexPath.row];
                cell.friendModel = model;
                cell.focusBtn.tag = 100 + [cell.friendModel.uid intValue];
                cell.headbtn.tag = 200 + [cell.friendModel.uid intValue];

            }

        }
    }else if(collectionView.tag == USERCOLLECTIONVIEW){
        if(_userAttentionArr.count != 0){
//            NSLog(@"_firIntroArr == %ld",_userAttentionArr.count);
            SP_FriendModel *model = _userAttentionArr[indexPath.row];
            cell.friendModel = model;
            cell.focusBtn.tag = 100 + [cell.friendModel.uid intValue];
            cell.headbtn.tag = 200 + [cell.friendModel.uid intValue];

        }
        
    }
    
    
    
    //  cell.backgroundColor = [UIColor grayColor];
    return cell;
}


- (void) headbtnDidClick:(UIButton *)btn
{
   
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 200];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}




- (void) foucsFrinedBtnDidClick:(UIButton *)btn
{
    _currentFocusBtn = btn;
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 100];
    if(btn.selected == NO){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:ATTENTION(GETUSERID,touid) Tag:TAG_HTTP_ATTENTION];
    }else if (btn.selected == YES){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:DISATTENTION(GETUSERID,touid) Tag:TAG_HTTP_DISATTENTION];

    }
}


////定义每个UICollectionViewCell 的大小
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(ScreenWidth/2-10, 50);
//}
////定义每个Section 的 margin
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(15, 15, 5, 15);//分别为上、左、下、右
//}
//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size={ScreenWidth,35};
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    if(collectionView.tag == COLLECTIONVIEW){
        if(section == 0){
            if(_secIntroARR.count == 6){
                CGSize size={ScreenWidth,60};
                return size;
            }else{
                CGSize size={0,0};
                return size;
            }
        }else if(section == 1){
            if(_firIntroArr.count == 6){
                CGSize size={ScreenWidth,60};
                return size;
            }else{
                CGSize size={0,0};
                return size;

            }
        }
    }else if (collectionView.tag == USERCOLLECTIONVIEW){
        if(_userAttentionArr.count == 6){
            CGSize size={ScreenWidth,60};
            return size;
        }else{
            CGSize size={0,0};
            return size;

        }

        
    }
    
    CGSize size={ScreenWidth,60};
    return size;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    NSString *reuseIdentifier;
    if ([kind isEqualToString: UICollectionElementKindSectionFooter ]){
        reuseIdentifier = @"AddCollectionFooterView";
    }else{
        reuseIdentifier = @"AddfriendReusableView";
    }
    
    AddfriendReusableView *view =  (AddfriendReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
    
    
    if(collectionView.tag == COLLECTIONVIEW){
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
            
            UILabel *label = (UILabel *)[view viewWithTag:1];
            if (0 == indexPath.section) {
                label.text = [NSString stringWithFormat:@"同校好友推荐"];
            }else{
               label.text = [NSString stringWithFormat:@"都爱%@",self.userInfoMsg.idol];
            }
            return view;

        }
        else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
            AddCollectionFooterView *footView = (AddCollectionFooterView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
            
            if(indexPath.section == 0){
                [footView.changeGroupBtn addTarget:self action:@selector(schoolFriendChange:) forControlEvents:UIControlEventTouchUpInside];
            }else if (indexPath.section == 1){
                [footView.changeGroupBtn addTarget:self action:@selector(idolFrinedChange:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            return footView;
            
        }
    }else if (collectionView.tag == USERCOLLECTIONVIEW){
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
            
            UILabel *label = (UILabel *)[view viewWithTag:1];
                label.text = [NSString stringWithFormat:@"Ta关注的好友"];

            return view;
        }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
            AddCollectionFooterView *footView = (AddCollectionFooterView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
            [footView.changeGroupBtn addTarget:self action:@selector(userFriendChange:) forControlEvents:UIControlEventTouchUpInside];

        
        }
    }
    
    
    return view;
}


- (void) schoolFriendChange:(UIButton *)btn
{
//    NSLog(@"同学");
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETRECFRIEND(GETUSERID) Tag:TAG_HTTP_GETRECFRIEND];


}

- (void) idolFrinedChange:(UIButton *)btn
{
//    NSLog(@"偶像");
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETSAMEIDOLFRIEND(GETUSERID) Tag:TAG_HTTP_GETSAMEIDOLFRIEND];
}

- (void)userFriendChange:(UIButton *)btn
{
    NSLog(@"用户关注的好友");
}



#pragma -mark CollectView delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    

    
}





@end
