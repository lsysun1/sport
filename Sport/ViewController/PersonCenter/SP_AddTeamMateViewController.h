//
//  SP_AddTeamMateViewController.h
//  Sport
//
//  Created by 李松玉 on 15/6/2.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_FriendModel;
@protocol SP_AddTeamMateDelegate <NSObject>

- (void) backLeaderTwoInfo:(SP_FriendModel *)leaderTwo;
- (void) backTeamMateInfo:(SP_FriendModel *) teamMate;

@end

@interface SP_AddTeamMateViewController : BaseViewController

@property (nonatomic,assign) id<SP_AddTeamMateDelegate> delegate;

/**
 *  0 - 添加副队长
 *  1 - 添加队员
 */
@property (nonatomic,copy) NSString *addType;



@property (nonatomic,copy) NSString *type;

// 0 添加队友
// 1 已关注的好友


@end
