//
//  AddfriendCollectionCell.m
//  Sport
//
//  Created by WT_lyy on 15/4/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "AddfriendCollectionCell.h"
#import "SP_FriendModel.h"
#import "UIImageView+WebCache.h"
#import "SP_TeamModel.h"

@interface AddfriendCollectionCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *ageLabel;

@end

@implementation AddfriendCollectionCell

- (void)awakeFromNib {
    // Initialization code
    [self.focusBtn setImage:[UIImage imageNamed:@"person_add_attened.png"] forState:UIControlStateSelected];
    self.headImgView.image = [UIImage imageNamed:@"default_head_img.png"];
    
    _headImgView.layer.borderWidth = 1;
    _headImgView.layer.cornerRadius = 8;
    _headImgView.layer.masksToBounds = YES;

}


- (void) setFriendModel:(SP_FriendModel *)friendModel
{
    _friendModel = friendModel;
    


    
    if ([friendModel.sex isEqualToString:@"0"]) {
        self.headImgView.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        self.headImgView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    
    if(![friendModel.headimg isEqualToString:@""]){
        NSURL *imgURL =[NSURL URLWithString:friendModel.headimg];
        [self.headImgView sd_setImageWithURL:imgURL];
    }
    
    
    self.nameLabel.text = friendModel.nickname;
    
    
    
    
    if([friendModel.isattention isEqualToString:@"yes"]){
        self.focusBtn.selected = YES;
    }else{
        self.focusBtn.selected = NO;
    }
  
}

-(void)setTeammodel:(SP_TeamModel *)teammodel
{
    _teammodel = teammodel;
    _headImgView.layer.borderWidth = 0;

    if(![teammodel.img isEqualToString:@""]){
        NSURL *imgURL =[NSURL URLWithString:teammodel.img];
        [self.headImgView sd_setImageWithURL:imgURL];
    }
    
    
    self.nameLabel.text = teammodel.name;
    
    
    
    
    if([teammodel.isattention isEqualToString:@"yes"]){
        self.focusBtn.selected = YES;
    }else{
        self.focusBtn.selected = NO;
    }


}






@end
