//
//  AddfriendCollectionCell.h
//  Sport
//
//  Created by WT_lyy on 15/4/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SP_FriendModel,SP_TeamModel;
@interface AddfriendCollectionCell : UICollectionViewCell
@property (nonatomic,strong) SP_FriendModel *friendModel;
@property (nonatomic,strong) SP_TeamModel *teammodel;


@property (weak, nonatomic) IBOutlet UIButton *focusBtn;

@property (weak, nonatomic) IBOutlet UIButton *headbtn;

@end
