//
//  AddCollectionFooterView.h
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCollectionFooterView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIButton *changeGroupBtn;



@end
