//
//  TeamPhotoCollectionViewCell.m
//  
//
//  Created by 李松玉 on 15/6/10.
//
//

#import "TeamPhotoCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "SP_TeamImgCateList.h"

@interface TeamPhotoCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *teamImg;
@property (weak, nonatomic) IBOutlet UILabel *imgCateName;

@property (weak, nonatomic) IBOutlet UILabel *imgCateTime;
@end



@implementation TeamPhotoCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
        
    self.teamImg.layer.cornerRadius = 8;
    self.teamImg.layer.masksToBounds = YES;

    
    
    
}


- (void) setImgCateList:(SP_TeamImgCateList *)imgCateList
{
    _imgCateList = imgCateList;
    
    NSURL *imgURL = [NSURL URLWithString:imgCateList.cover];
    [self.teamImg sd_setImageWithURL:imgURL];
    
    self.imgCateName.text = imgCateList.name;
    self.imgCateTime.text = imgCateList.date;
}



@end
