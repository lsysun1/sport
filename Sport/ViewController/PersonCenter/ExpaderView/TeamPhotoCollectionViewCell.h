//
//  TeamPhotoCollectionViewCell.h
//  
//
//  Created by 李松玉 on 15/6/10.
//
//

#import <UIKit/UIKit.h>
@class SP_TeamImgCateList;
@interface TeamPhotoCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) SP_TeamImgCateList *imgCateList;
@end
