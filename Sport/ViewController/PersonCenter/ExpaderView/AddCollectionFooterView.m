//
//  AddCollectionFooterView.m
//  Sport
//
//  Created by 李松玉 on 15/6/3.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "AddCollectionFooterView.h"

@interface AddCollectionFooterView ()

@end

@implementation AddCollectionFooterView

- (void)awakeFromNib {
    // Initialization code
    [self.changeGroupBtn setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
    self.changeGroupBtn.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    self.changeGroupBtn.layer.borderWidth = 1;
    
    
}

@end
