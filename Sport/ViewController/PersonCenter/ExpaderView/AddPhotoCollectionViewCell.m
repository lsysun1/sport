//
//  AddPhotoCollectionViewCell.m
//  Sport
//
//  Created by 李松玉 on 15/6/6.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "AddPhotoCollectionViewCell.h"

@interface AddPhotoCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UILabel *addTitle;

@end


@implementation AddPhotoCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    self.bgView.backgroundColor = UIColorFromRGB(0xe0e0e0);
    self.addTitle.textColor = UIColorFromRGB(0x696969);
    
}

@end
