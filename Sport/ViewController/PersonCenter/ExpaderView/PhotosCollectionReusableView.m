//
//  PhotosCollectionReusableView.m
//  Sport
//
//  Created by 李松玉 on 15/6/6.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "PhotosCollectionReusableView.h"

@interface PhotosCollectionReusableView()

@property (weak, nonatomic) IBOutlet UIView *bottomLine;


@end


@implementation PhotosCollectionReusableView

- (void)awakeFromNib {
    // Initialization code
    self.bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
}

@end
