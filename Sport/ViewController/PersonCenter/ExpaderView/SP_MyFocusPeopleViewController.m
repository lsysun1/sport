//
//  SP_AddTeamMateViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/2.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MyFocusPeopleViewController.h"
#import "SP_AddTeamMateCell.h"
#import "pinyin.h"
#import "ChineseString.h"
#import "SP_FriendModel.h"


@interface SP_MyFocusPeopleViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITextField *_textfInput;
    UITableView *_tableView;
    NSMutableArray *_friArr;
    NSMutableArray *_sortedArrForArrays;
    NSMutableArray *_sectionHeadsKeys;
    
}
@end

@implementation SP_MyFocusPeopleViewController

- (id)init{
    
    self = [super init];
    if (self) {
        _friArr = [[NSMutableArray alloc] init];
        _sortedArrForArrays = [[NSMutableArray alloc] init];
        _sectionHeadsKeys = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavigationBar:@"添加队友"];
    [self addBackButton];
    [self _initSearchView];
    [self setUpFocusFriendTableview];
    
    //    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETFRIENDLIST(GETUSERID) Tag:TAG_HTTP_GETFRIENDLIST];
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETFRIENDLIST -- resopnse:%@\n",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETFRIENDLIST){
            if (HTTP_SUCCESS(code))
            {
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                    SP_FriendModel *model = [SP_FriendModel objectWithKeyValues:dict];
                    
                    [_friArr addObject:model];
                }
                _sortedArrForArrays = [self getChineseStringArr:_friArr];
                
                
                
                [_tableView reloadData];
                
            }
        }
    }
    
}


#pragma mark - 初始化搜索栏
- (void)_initSearchView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, [self viewTop]-1, ScreenWidth+2, 55)];
    
    [self.view addSubview:view];
    
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView * inputView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-20*2, 34)];
    [view addSubview:inputView];
    inputView.backgroundColor = [UIColor whiteColor];
    inputView.layer.borderWidth = 1;
    
    inputView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    inputView.layer.cornerRadius = 2;
    
    _textfInput = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, inputView.frame.size.width-60, 24)];
    [inputView addSubview:_textfInput];
    _textfInput.delegate = self;
    _textfInput.placeholder = @"输入手机号查找";
    _textfInput.keyboardType = UIKeyboardTypePhonePad;
    _textfInput.returnKeyType = UIReturnKeySearch;
    
    UIView * viewVerline = [[UIView alloc] initWithFrame:CGRectMake(inputView.frame.size.width - 50, 5, 1, 24)];
    [inputView addSubview:viewVerline];
    viewVerline.backgroundColor = UIColorFromRGB(0xe5e5e5);
    
    UIButton * btnSearch = GET_BUTTON(CGRectMake(inputView.frame.size.width-50, 0, 40, 34), 14, NO, nil);
    [inputView addSubview:btnSearch];
    [btnSearch setImage:[UIImage imageNamed:@"person_search"] forState:UIControlStateNormal];
    [btnSearch setImageEdgeInsets:UIEdgeInsetsMake(7, 10, 7, 10)];
    [btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
}

#pragma -mark Btn Action
- (void)btnSearchClick{
    
    if (![GT_Tool Isphonenumber:_textfInput.text]) {
        
        [self showAlertStr:@"请输入正确的手机号"];
        
        return;
    }
}

#pragma -mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    //直接请求接口
    [self btnSearchClick];
    
    return YES;
}


- (void) setUpFocusFriendTableview
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, [self viewTop]-1 + 55, ScreenWidth, ScreenHeight -  [self viewTop]-1 - 55)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 59;
    
    [self.view addSubview:_tableView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[_sortedArrForArrays objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_sortedArrForArrays count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_sectionHeadsKeys objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_AddTeamMateCell *cell = [SP_AddTeamMateCell cellWithTableView:tableView];
    
    if ([_sortedArrForArrays count] > indexPath.section) {
        NSArray *arr = [_sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            SP_FriendModel *model = (SP_FriendModel *) [arr objectAtIndex:indexPath.row];
            cell.friendModel = model;
        } else {
            NSLog(@"arr out of range");
        }
    } else {
        NSLog(@"sortedArrForArrays out of range");
    }
    
    return cell;
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_sortedArrForArrays count] > indexPath.section) {
        NSArray *arr = [_sortedArrForArrays objectAtIndex:indexPath.section];
        if ([arr count] > indexPath.row) {
            SP_FriendModel *model = (SP_FriendModel *) [arr objectAtIndex:indexPath.row];
            NSLog(@"点击cell -- %@",model.nickname);
            if([self.addType isEqualToString:@"0"]){
                if(self.delegate && [self.delegate respondsToSelector:@selector(backLeaderTwoInfo:)]){
                    [self.delegate backLeaderTwoInfo:model];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }else if ([self.addType isEqualToString:@"1"]){
                if(self.delegate && [self.delegate respondsToSelector:@selector(backTeamMateInfo:)]){
                    [self.delegate backTeamMateInfo:model];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        } else {
            NSLog(@"arr out of range");
        }
    } else {
        NSLog(@"sortedArrForArrays out of range");
    }
    
}




- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort
{
    
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    for(int i = 0; i < [arrToSort count]; i++) {
        
        SP_FriendModel *model = _friArr[i];
        
        
        if(model.nickname==nil){
            model.nickname=@"";
        }
        
        if(![model.nickname isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < model.nickname.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([model.nickname characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            model.pinYin = pinYinResult;
        } else {
            model.pinYin = @"";
        }
        [chineseStringsArray addObject:model];
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        //        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        SP_FriendModel *chineseStr = chineseStringsArray[index];
        
        
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        NSString *sr= [strchar substringToIndex:1];
        NSLog(@"%@",sr);        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
            }
        }
    }
    
    
    
    
    
    return arrayForArrays;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
