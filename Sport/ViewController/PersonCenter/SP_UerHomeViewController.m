//
//  SP_UerHomeViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/4.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_UerHomeViewController.h"
#import "SP_UserInfo.h"
#import "SP_ProfileOne_Cell.h"
#import "SP_SignCellTableViewCell.h"
#import "SP_ProfileTwoCell.h"
#import "SP_ProfileThreeCell.h"
#import "SP_UserMsgViewController.h"
#import "SP_UserTeamOrHistoryViewController.h"
#import "SP_AddTagViewController.h"
#import "SP_TagModel.h"
#import "SP_OtherRaceViewController.h"
#import "SP_OtherActivityInvitedViewController.h"



@interface SP_UerHomeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIScrollView *_scrollView;
    
    //顶部的图片View
    UIImageView *_topImgView;
    UIImageView *_headBoardView;
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UIButton *_focusBtn;
    UILabel *_fansLabel;
    UIView *_topGreenLine;
    UIButton *_topSwitchBtn;
    
    SP_UserInfo *_userInfoMsg;
    
    UITableView *_infoTableView;
    
    UIButton *_sendMsgBtn;
    
    NSMutableArray *_signArr;

}
@end

@implementation SP_UerHomeViewController


- (id)init
{
    self = [super init];
    if(self){
        _signArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"个人中心"];
    [self addBackButton];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 35)];
    _scrollView.bounces = NO;
    [self.view addSubview:_scrollView];
    
    _sendMsgBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, ScreenHeight - 35, ScreenWidth, 35)];
    _sendMsgBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    _sendMsgBtn.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [_sendMsgBtn setTitle:@"留言" forState:UIControlStateNormal];
    [_sendMsgBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sendMsgBtn addTarget:self action:@selector(msgBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendMsgBtn];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETUSERDATA(self.touid, GETUSERID) Tag:TAG_HTTP_GETUSERDATA];
    
    [self setHTTPRequest:GETUSERTAG(self.touid) Tag:TAG_HTTP_GETUSERTAG];

    
}


- (void) msgBtnDidClick
{
    SP_UserMsgViewController *vc = [[SP_UserMsgViewController alloc]init];
    vc.touid = self.touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
//        NSLog(@"GETUSERDATA -- 个人资料resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETUSERDATA){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msgDict = dictionary[@"msg"];
                SP_UserInfo *info = [SP_UserInfo objectWithKeyValues:msgDict];
                _userInfoMsg = info;
                
                [self setUpTopView];
                [self setUpInfoTableView];
                [self setUpMidLine];
                _scrollView.contentSize = CGSizeMake(ScreenWidth, 570);



            }
        }else if (request.tag == TAG_HTTP_ATTENTION){
            if(HTTP_SUCCESS(code)){
                _focusBtn.selected = YES;
                [self showMessage:@"关注成功"];
            }else{
                _focusBtn.selected = NO;
                [self showMessage:@"关注失败"];
            }
        
        
        
        }else if(request.tag == TAG_HTTP_GETUSERTAG){
            if(HTTP_SUCCESS(code)){
               
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                    SP_TagModel *model = [SP_TagModel objectWithKeyValues:dict];
                    [_signArr addObject:model];
                }
                
                [_infoTableView reloadData];
            }
        }else if(request.tag == TAG_HTTP_DISATTENTION){
            if(HTTP_SUCCESS(code)){
                _focusBtn.selected = NO;
                [self showMessage:@"取消关注成功"];
            }else{
                _focusBtn.selected = YES;
                [self showMessage:@"取消关注失败"];
            }
        
        }
    }
}






#pragma mark - 设置顶部View
- (void) setUpTopView
{
    _topImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
    _topImgView.image = [UIImage imageNamed:@"上图.jpg"];
    
    _headBoardView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 40,20 , 80, 80)];
    _headBoardView.layer.borderWidth = 1;
    _headBoardView.layer.cornerRadius = 40;
    if([_userInfoMsg.sex isEqualToString:@"0"]){
        _headBoardView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    }else{
        _headBoardView.layer.borderColor = UIColorFromRGB(0x599ffff).CGColor;
    }
    
    
    _headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 37.5, 22.5, 75, 75)];
    _headImgView.image = [UIImage imageNamed:@"default_head_img"];
    _headImgView.layer.cornerRadius = 37.5;
    _headImgView.layer.masksToBounds = YES;
    NSURL *imgURL = [NSURL URLWithString:_userInfoMsg.headimg];
    [_headImgView sd_setImageWithURL:imgURL];
    
    
    
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_headImgView.frame) + 20, 200, 15)];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    if(![_userInfoMsg.postion isEqualToString:@""]){
        _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",_userInfoMsg.nickname,_userInfoMsg.postion];
    }else{
        _nameLabel.text = [NSString stringWithFormat:@"%@",_userInfoMsg.nickname];
    }
    _nameLabel.font = [UIFont systemFontOfSize:15];
    
    
    
    
    
    _focusBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 -70, CGRectGetMaxY(_nameLabel.frame) + 13, 50, 22)];
    [_focusBtn setBackgroundImage:[UIImage imageNamed:@"person_attend"] forState:UIControlStateNormal];
    [_focusBtn setBackgroundImage:[UIImage imageNamed:@"person_add_attened"] forState:UIControlStateSelected];
    if([_userInfoMsg.isattention isEqualToString:@"yes"]){
        _focusBtn.selected = YES;
        _focusBtn.tag = 666;
    }else{
        _focusBtn.selected = NO;
        _focusBtn.tag = 999;
    }
    [_focusBtn addTarget:self action:@selector(focusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    _fansLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth / 2 + 28, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _fansLabel.textColor = [UIColor whiteColor];
    _fansLabel.textAlignment = NSTextAlignmentLeft;
    _fansLabel.text = [NSString stringWithFormat:@"粉丝:%@",_userInfoMsg.fansCount];
    _fansLabel.font = [UIFont systemFontOfSize:14];
    
    
    
    
    [_scrollView  addSubview:_topImgView];
    [_scrollView  addSubview:_headBoardView];
    [_scrollView  addSubview:_headImgView];
    [_scrollView  addSubview:_nameLabel];
    [_scrollView  addSubview:_focusBtn];
    [_scrollView  addSubview:_fansLabel];
    
}

#pragma mark - 设置中间绿条以及按钮
- (void) setUpMidLine
{
    _topGreenLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topImgView.frame), ScreenWidth, 15)];
    _topGreenLine.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _topSwitchBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 - 17.5, CGRectGetMaxY(_topGreenLine.frame) - 32, 35, 43)];
    [_topSwitchBtn setBackgroundImage:[UIImage imageNamed:@"person_teanm"] forState:UIControlStateNormal];
    [_topSwitchBtn addTarget:self action:@selector(topSwitchBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_scrollView addSubview:_topGreenLine];
    [_scrollView addSubview:_topSwitchBtn];
    
}

- (void) topSwitchBtnDidClick
{
    SP_UserTeamOrHistoryViewController *vc = [[SP_UserTeamOrHistoryViewController alloc]init];
    vc.touid = self.touid;
    vc.userInfoMsg = _userInfoMsg;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:NO];
}


- (void)focusBtnDidClick:(UIButton *)btn
{
    if(btn.selected == NO){
        [self setHTTPRequest:ATTENTION(GETUSERID, self.touid) Tag:TAG_HTTP_ATTENTION];
    }else{
        [self setHTTPRequest:DISATTENTION(GETUSERID, self.touid) Tag:TAG_HTTP_DISATTENTION];
    }
}



#pragma mark - 设置infoTableView
- (void) setUpInfoTableView
{
    _infoTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 215, ScreenWidth, 130 + 80 + 95 + 50)];
    _infoTableView.delegate = self;
    _infoTableView.dataSource = self;
    _infoTableView.separatorStyle = 0;
    _infoTableView.scrollEnabled = NO;
    
    [_scrollView addSubview:_infoTableView];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_ProfileOne_Cell *cell = [SP_ProfileOne_Cell cellWithTableView:tableView];
        cell.type = @"2";
        cell.userInfoMsg = _userInfoMsg;
        return cell;
    }else if (indexPath.row == 1){
        SP_SignCellTableViewCell *cell = [SP_SignCellTableViewCell cellWithTableView:tableView];
        cell.signArr = _signArr;
        return cell;
    
    }else if(indexPath.row == 2){
        SP_ProfileTwoCell *cell = [SP_ProfileTwoCell cellWithTableView:tableView];
        
        return cell;
    }else if (indexPath.row == 3){
        SP_ProfileThreeCell *cell = [SP_ProfileThreeCell cellWithTableView:tableView];
        cell.titleLabel.text = @"活动消息";
        cell.imgView.image = [UIImage imageNamed:@"icon_invite.png"];
        cell.arrowImg.image = [UIImage imageNamed:@"arro_invite.png"];
        return cell;
        
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1){
        SP_AddTagViewController *tagVC = [[SP_AddTagViewController alloc]init];
        tagVC.touid = _userInfoMsg.uid;
        tagVC.sex = _userInfoMsg.sex;
        [[AppDelegate sharedAppDelegate].nav pushViewController:tagVC animated:YES];
    }else if (indexPath.row == 2){
        SP_OtherRaceViewController *vc = [[SP_OtherRaceViewController alloc]init];
        vc.touid = _userInfoMsg.uid;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
        SP_OtherActivityInvitedViewController *vc = [[SP_OtherActivityInvitedViewController alloc]init];
        vc.touid = _userInfoMsg.uid;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 130;
    }else {
        return 50;
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
