//
//  SP_AddTagViewController.h
//  Sport
//
//  Created by 李松玉 on 15/8/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_AddTagViewController : BaseViewController

@property (nonatomic,copy) NSString *touid;
@property (nonatomic,copy) NSString *sex;


@end
