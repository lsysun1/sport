//
//  SP_SendBattleViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_SendBattleViewController.h"
#import "SP_TextView.h"
#import "SP_TeamModel.h"
#import "SP_GroundSearchViewController.h"
#import "SP_GroundEntity.h"



#define TAG_PICKERVIEW_TEAM     101

@interface SP_SendBattleViewController ()<UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,SP_GroundSearchViewControllerDelegate>
{
    UIScrollView *_scrollView;
    
    SP_TextView *_msgTextView;
    
    UIView *_teamView;
    UILabel *_teamName;
    
    UIPickerView *_teamSelectPickerView;
    
    NSMutableArray *_teamArr;
    SP_TeamModel *_selectedTeam;
    
    
    UIView *_groundSelecteView;
    UILabel *_groundName;
    SP_GroundEntity *_entity;
    
    UIView *_dateSelecteView;
    UILabel *_timeLabel;
    UIDatePicker *_datePickerView;
    NSString *_selectedTime;
    
    
}
@end

@implementation SP_SendBattleViewController

- (id)init
{
    self = [super init];
    if(self){
    
        _teamArr = [NSMutableArray array];
        _selectedTime = [[NSString alloc]init];
        
    }
    return self;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"邀战"];
    [self addBackButton];
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    [self.view addSubview:_scrollView];
    
    [self addNavRightView];
    [self setUpMsgTextView];
    [self setUpSelectedTeamView];
    [self setUpGroundSelecteView];
    [self setUpdateSelecteView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Actiondo)];
    
    [_scrollView addGestureRecognizer:tapGesture];
    
    
    
    
    [self setHTTPRequest:GETJOINTEAM(GETUSERID) Tag:TAG_HTTP_GETJOINTEAM];

    
    // Do any additional setup after loading the view.
}

- (void) Actiondo
{
    if(_teamSelectPickerView.superview != nil){
        [_teamSelectPickerView removeFromSuperview];
        _teamSelectPickerView = nil;
    }
    
    if(_datePickerView.superview != nil){
        [_datePickerView removeFromSuperview];
        _datePickerView = nil;
    }
}


- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"确定"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}



- (void) btnNavRightClick
{
    if(_msgTextView.text.length == 0){
        [self showMessage:@"请说点什么?"];
        return;
    }
    
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:self.bteamid forKey:@"bteamid"];
    [dict setObject:_selectedTeam.teamid forKey:@"ateamid"];
    [dict setObject:_selectedTime forKey:@"matchdate"];
    [dict setObject:_entity.ycdid forKey:@"ycdid"];
    [dict setObject:_msgTextView.text forKey:@"othernote"];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:TEAMCHALLENGE Dictionary:dict Tag:TAG_HTTP_TEAMCHALLENGE];
    
    
}


//ateamid 自己球队id
//bteamid 挑战球队id
//matchdate 比赛日期 10位时间戳
//ycdid  场馆id
//othernote  还要说的话 255字以内


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        
        if(request.tag == TAG_HTTP_GETJOINTEAM){
            if (HTTP_SUCCESS(code))
            {
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_TeamModel *team = [SP_TeamModel objectWithKeyValues:dict];
 
                    NSArray *memberArr = team.playerData;
                    for(NSDictionary *renDict in memberArr){
                        
                        if([renDict[@"uid"] isEqualToString:GETUSERID]){
                            if([renDict[@"role"] isEqualToString:@"0"]){
                            }else{
                                [_teamArr addObject:team];
                            }
                        }
                    }
                }
            }
        }else if (request.tag == TAG_HTTP_TEAMCHALLENGE){
            
//                    NSLog(@"挑战 --- resopnse:%@",request.responseString);
            
            if(HTTP_SUCCESS(code)){
                [self showMessage:@"挑战请求发送成功!"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showMessage:@"挑战请求发送失败!"];
            }
        }
    }
}




- (void) setUpMsgTextView
{
    _msgTextView = [[SP_TextView alloc]initWithFrame:CGRectMake(10, 10, ScreenWidth - 20, 150)];
    _msgTextView.placehoder = @"  这一刻的想法";
    _msgTextView.delegate = self;
    _msgTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _msgTextView.layer.borderWidth = 1;
    [_scrollView  addSubview:_msgTextView];
}

- (void) setUpSelectedTeamView
{
    _teamView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_msgTextView.frame) + 10, ScreenWidth - 40, 40)];
    [_scrollView addSubview:_teamView];
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 30)];
    titleLabel.text = @"参赛球队选择";
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = UIColorFromRGB(0x696969);
    [_teamView addSubview:titleLabel];
    
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 60, 8, 10, 18)];
    arrow.image = [UIImage imageNamed:@"arro_invite.png"];
    [_teamView addSubview:arrow];
    
    _teamName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(arrow.frame) - 200, 0, 180, 30)];
    _teamName.textAlignment = NSTextAlignmentRight;
    _teamName.text = @"球队名称";
    _teamName.font = [UIFont systemFontOfSize:14];
    _teamName.textColor = UIColorFromRGB(0x696969);
    [_teamView addSubview:_teamName];
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth - 40, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_teamView addSubview:bottomLine];
    
    UIButton *teamNameBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 150, 0, 150, 40)];
    [teamNameBtn addTarget:self action:@selector(teamSelecteBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_teamView addSubview:teamNameBtn];

}


#pragma mark - 球队选择
- (void) teamSelecteBtnDidClick
{
    
    if(_datePickerView.superview != nil){
        [_datePickerView removeFromSuperview];
        _datePickerView = nil;
    }
    
    
    if(_teamSelectPickerView == nil){
        _teamSelectPickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 250, ScreenWidth, 250)];
        _teamSelectPickerView.delegate = self;
        _teamSelectPickerView.tag = TAG_PICKERVIEW_TEAM;
        _teamSelectPickerView.dataSource = self;
        [_scrollView addSubview:_teamSelectPickerView];
    }
}


- (void) setUpGroundSelecteView
{
    _groundSelecteView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_teamView.frame) + 10, ScreenWidth - 40, 40)];
    [_scrollView addSubview:_groundSelecteView];
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 30)];
    titleLabel.text = @"场馆选择";
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = UIColorFromRGB(0x696969);
    [_groundSelecteView addSubview:titleLabel];
    
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 60, 8, 10, 18)];
    arrow.image = [UIImage imageNamed:@"arro_invite.png"];
    [_groundSelecteView addSubview:arrow];
    
    _groundName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(arrow.frame) - 200, 0, 180, 30)];
    _groundName.textAlignment = NSTextAlignmentRight;
    _groundName.text = @"选择场馆";
    _groundName.font = [UIFont systemFontOfSize:14];
    _groundName.textColor = UIColorFromRGB(0x696969);
    [_groundSelecteView addSubview:_groundName];
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth - 40, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_groundSelecteView addSubview:bottomLine];
    
    UIButton *teamNameBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 150, 0, 150, 40)];
    [teamNameBtn addTarget:self action:@selector(groundSelecteBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_groundSelecteView addSubview:teamNameBtn];

}

#pragma mark 场馆选择点击事件
- (void)groundSelecteBtnDidClick
{
    if(_teamSelectPickerView.superview != nil){
        [_teamSelectPickerView removeFromSuperview];
        _teamSelectPickerView = nil;
    }
    
    if(_datePickerView.superview != nil){
        [_datePickerView removeFromSuperview];
        _datePickerView = nil;
    }
    
    
    SP_GroundSearchViewController *vc = [[SP_GroundSearchViewController alloc]init];
    vc.isSelected = @"yes";
    vc.delegate = self;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (void)getGroundEntityBack:(SP_GroundEntity *)entitiy
{
    _entity = entitiy;
    _groundName.text = entitiy.name;
}




- (void) setUpdateSelecteView
{
    _dateSelecteView = [[UIView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_groundSelecteView.frame) + 10, ScreenWidth - 40, 40)];
    [_scrollView addSubview:_dateSelecteView];
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 30)];
    titleLabel.text = @"场馆选择";
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = UIColorFromRGB(0x696969);
    [_dateSelecteView addSubview:titleLabel];
    
    
    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth - 60, 8, 10, 18)];
    arrow.image = [UIImage imageNamed:@"arro_invite.png"];
    [_dateSelecteView addSubview:arrow];
    
    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(arrow.frame) - 200, 0, 180, 30)];
    _timeLabel.textAlignment = NSTextAlignmentRight;
    _timeLabel.text = @"选择时间";
    _timeLabel.font = [UIFont systemFontOfSize:14];
    _timeLabel.textColor = UIColorFromRGB(0x696969);
    [_dateSelecteView addSubview:_timeLabel];
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth - 40, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [_dateSelecteView addSubview:bottomLine];
    
    UIButton *teamNameBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 150, 0, 150, 40)];
    [teamNameBtn addTarget:self action:@selector(dateSelecteBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_dateSelecteView addSubview:teamNameBtn];
    
}


#pragma mark - 时间选择
- (void) dateSelecteBtnDidClick
{
    if(_teamSelectPickerView.superview != nil){
        [_teamSelectPickerView removeFromSuperview];
        _teamSelectPickerView = nil;
    }
    
    if(_datePickerView.superview == nil){
        NSDate *currentTime  = [NSDate date];
         _datePickerView = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, ScreenHeight - 250, ScreenWidth, 250)];
         [_datePickerView  setTimeZone:[NSTimeZone defaultTimeZone]];
         [_datePickerView  setTimeZone:[NSTimeZone timeZoneWithName:@"GMT+8"]];
         [_datePickerView  setDate:currentTime animated:YES];
         [_datePickerView  setDatePickerMode:UIDatePickerModeDateAndTime];
         [_datePickerView  addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
         [_scrollView addSubview:_datePickerView];

    }
    

}

-(void)datePickerValueChanged:(id)sender
{
    NSDate *selected = [_datePickerView date];
    NSLog(@"date: %@", selected);
    _timeLabel.text = [self stringFromDate:selected];
}

- (NSString *)stringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];

    NSString *timeSp = [NSString stringWithFormat:@"%ld",(long)[date timeIntervalSince1970]];

    _selectedTime = timeSp;
    
    return destDateString;
    
}




#pragma mark -  pickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView.tag == TAG_PICKERVIEW_TEAM){
        return _teamArr.count;
    }
    return 0;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView.tag == TAG_PICKERVIEW_TEAM){
        SP_TeamModel *team = _teamArr[row];
        return team.name;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == TAG_PICKERVIEW_TEAM){
        SP_TeamModel *team = _teamArr[row];
        _teamName.text = team.name;
        _selectedTeam = team;
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
