//
//  SP_TeamInfoViewController.h
//  Sport
//
//  Created by 李松玉 on 15/6/5.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_TeamInfoViewController : BaseViewController
@property (nonatomic,copy) NSString *teamName;
@property (nonatomic,copy) NSString *teamID;


@end
