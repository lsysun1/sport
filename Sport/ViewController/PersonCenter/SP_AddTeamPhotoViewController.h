//
//  SP_AddTeamPhotoViewController.h
//  Sport
//
//  Created by 李松玉 on 15/6/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_TeamImgCateList;
@interface SP_AddTeamPhotoViewController : BaseViewController
@property (nonatomic,copy) NSString *teamID;
@property (nonatomic,strong) SP_TeamImgCateList *imgCate;
@end
