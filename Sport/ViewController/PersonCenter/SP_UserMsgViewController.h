//
//  SP_UserMsgViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_UserMsgViewController : BaseViewController

@property (nonatomic,copy) NSString *touid;
@property (nonatomic,copy) NSString *type;      //留言板消息类型 board
@property (nonatomic,copy) NSString *teamID;

@end


