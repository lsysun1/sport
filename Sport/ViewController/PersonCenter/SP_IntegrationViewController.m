//
//  SP_IntegrationViewController.m
//  Sport
//
//  Created by WT_lyy on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_IntegrationViewController.h"
#import "SP_JifenCell.h"

@interface SP_IntegrationViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    UITableView * _tableView;
    
    NSArray * _titleArr;
    
    NSArray * _detailArr;
    
    UILabel * _labTotalScore;
    
    
    NSMutableArray *_jfArr;
}


@end

@implementation SP_IntegrationViewController


- (id)init
{
    self = [super init];
    if(self){
        _jfArr = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleArr = [NSArray arrayWithObjects:@"登录",@"评论、点赞、分享",@"阅读本地比赛新闻",@"欣赏广告",@"发送邀约信息",nil];
    
    _detailArr = [NSArray arrayWithObjects:@"连续登录每天获得8积分",@"2分一次/次（对同一新闻只能完成一次操作）",@"每日首次阅读5积分",@"每日首次10积分",@"每日首次10积分",nil];
    
    
    [self setNavigationBar:@"我的积分"];
    [self addBackButton];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETJFLIST(GETUSERID) Tag:TAG_HTTP_GETJFLIST];
    

    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight-64)];
    
    [self.view addSubview:_tableView];
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    _tableView.rowHeight = 44;
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 60)];
    
    view.backgroundColor = UIColorFromRGB(0xf5f5f5);
    
    UILabel * lab = GET_LABEL(CGRectMake(15, 22, 80, 16), 14, NO, [UIColor blackColor], NSTextAlignmentLeft);
    
    [view addSubview:lab];
    
    lab.text = @"已有积分:";
    
    _labTotalScore = GET_LABEL(CGRectMake(80, 22, 220, 16), 14, NO, [UIColor orangeColor], NSTextAlignmentLeft);
    
    [view addSubview:_labTotalScore];
    
    _labTotalScore.text = @"0分";
    
    _tableView.tableHeaderView = view;
    
}



#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        //                NSLog(@"TAG_HTTP_GETMYYHBLIST :%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETJFLIST){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msg = dictionary[@"msg"];
                
                NSString *total = msg[@"total"];
                _labTotalScore.text = [NSString stringWithFormat:@"%@分",total];
                
                
                NSArray *jfArr = msg[@"jfhis"];
                
                for(NSDictionary *dict in jfArr){
                    [_jfArr addObject:dict];
                }
                
                [_tableView reloadData];
            }
        }
    }
    
}




#pragma -mark UitabeViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma -mark UitabeViewDatasource

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _jfArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SP_JifenCell *cell = [SP_JifenCell cellWithTableView:tableView];
    
    NSDictionary *dict = _jfArr[indexPath.row];
    cell.jifenDict = dict;
    
    
    return cell;
}



@end
