//
//  SP_AddFriendViewController.h
//  Sport
//
//  Created by WT_lyy on 15/4/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"
@class SP_UserInfo;
@interface SP_AddFriendViewController : BaseViewController
@property (strong,nonatomic) SP_UserInfo *userInfoMsg;
@end
