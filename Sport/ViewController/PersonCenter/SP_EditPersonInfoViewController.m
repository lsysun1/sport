//
//  SP_EditPersonInfoViewController.m
//  Sport
//
//  Created by WT_lyy on 15/4/26.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_EditPersonInfoViewController.h"
#import "PhotoChoiceView.h"
#import "SP_UserInfo.h"

@interface SP_EditPersonInfoViewController ()<PhotoChoiceViewDelegate,UITextFieldDelegate,UITextViewDelegate>
{

    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet UIButton *_btnCoSch;
    __weak IBOutlet UIImageView *headImg;
    __weak IBOutlet UIButton *headBtn;

    __weak IBOutlet UITextField *userName;
    
    __weak IBOutlet UITextField *idolName;
    
    __weak IBOutlet UIButton *manBtn;
    __weak IBOutlet UIButton *womanBtn;
    UIButton *_selectedSexBtn;
    
    __weak IBOutlet UITextView *signTextView;
    BOOL signTextEdit;
    
    __weak IBOutlet UILabel *ramdomLabel;
    __weak IBOutlet UIButton *ramdomBtn;
    
    __weak IBOutlet UITextField *_coSchTextView;
    __weak IBOutlet UITextField *positionTextView;
    
    UIView *_coSchView;
    BOOL _coSchViewIsOnView;
    UIButton *_schoolBtn;
    UIButton *_companyBtn;
    int _schoolOrCompany;
    
    UIView *_inputIdolView;
    UITextField *_idolInput;
    
    __weak IBOutlet UIButton *idolBtn;
    
    
    
    __weak IBOutlet UIView *_bottomView;

}

@end

@implementation SP_EditPersonInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"编辑个人信息"];
    [self addBackButton];
    
    [self addNavRightView];
    
    
    [idolBtn addTarget:self action:@selector(idolbtndidclick) forControlEvents:UIControlEventTouchUpInside];
    
    
    userName.text = self.userInfoMsg.nickname;
    
    NSURL *imgURL = [NSURL URLWithString:self.userInfoMsg.headimg];
    [headImg sd_setImageWithURL:imgURL];
    
    if([self.userInfoMsg.sex isEqualToString:@"0"])
    {
        manBtn.selected = NO;
        womanBtn.selected = YES;
    }else{
        manBtn.selected = YES;
        womanBtn.selected = NO;
        
    }
    
    idolName.text = self.userInfoMsg.idol;
    signTextView.text = self.userInfoMsg.signature;
    signTextView.editable = NO;
    positionTextView.text = self.userInfoMsg.postion;
    
    if([self.userInfoMsg.company isEqualToString:@""]){
        [_btnCoSch setTitle:@"学校" forState:UIControlStateNormal];
        _coSchTextView.text = self.userInfoMsg.school;
    }else if([self.userInfoMsg.school isEqualToString:@""]){
        [_btnCoSch setTitle:@"公司" forState:UIControlStateNormal];
        _coSchTextView.text = self.userInfoMsg.company;
    }
    
    

    
    
    
    [ramdomBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    ramdomBtn.frame = CGRectMake(ScreenWidth - 45, 260, 35, 65);
    ramdomLabel.frame = CGRectMake(ScreenWidth - 35, 264, 35, 62);
//    ramdomLabel.layer.borderColor = [UIColor orangeColor].CGColor;
//    ramdomLabel.layer.borderWidth = 1;
    
    [manBtn setImage:[UIImage imageNamed:@"sexselect"] forState:UIControlStateNormal];
    manBtn.selected = YES;
    _selectedSexBtn = manBtn;
    [manBtn setImage:[UIImage imageNamed:@"sexselected"] forState:UIControlStateSelected];
    
    [womanBtn setImage:[UIImage imageNamed:@"sexselect"] forState:UIControlStateNormal];
    [womanBtn setImage:[UIImage imageNamed:@"sexselected"] forState:UIControlStateSelected];
    
    
    _scrollView.contentSize = CGSizeMake(ScreenWidth, 603);

    headImg.layer.cornerRadius = 44.5;
    headImg.layer.masksToBounds = YES;
    
    
    _btnCoSch.layer.cornerRadius = 2;
    _btnCoSch.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    _btnCoSch.layer.borderWidth = 1;
    

    
    _coSchViewIsOnView = NO;
    _coSchView = [[UIView alloc]initWithFrame:CGRectMake(25, CGRectGetMaxY(_btnCoSch.frame) + 7, 130, 40)];
    UIImageView *coschBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 130, 40)];
    coschBgView.image = [UIImage imageNamed:@"学校，公司选择框"];
    [_coSchView addSubview:coschBgView];
    
    _schoolBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 17.5, 40, 15)];
    _schoolBtn.tag = 1;
    [_schoolBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_schoolBtn setTitle:@"学校" forState:UIControlStateNormal];
    [_schoolBtn addTarget:self action:@selector(schoolBtnOrCompanyBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    _schoolBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_coSchView addSubview:_schoolBtn];
    
    _companyBtn = [[UIButton alloc]initWithFrame:CGRectMake(70, 17.5, 40, 15)];
    _companyBtn.tag = 0;
    [_companyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_companyBtn setTitle:@"公司" forState:UIControlStateNormal];
    [_companyBtn addTarget:self action:@selector(schoolBtnOrCompanyBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    _companyBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [_coSchView addSubview:_companyBtn];
    
    
    
    
    _bottomView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil] ;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil] ;

    
    
}


- (void)idolbtndidclick{
    _inputIdolView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 250, 80)];
    _inputIdolView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    _inputIdolView.layer.cornerRadius = 8;
    _inputIdolView.layer.masksToBounds = YES;
    _inputIdolView.center = self.view.center;
    
    _idolInput = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, 230, 30)];
    _idolInput.backgroundColor = UIColorFromRGB(0xffffff);
    _idolInput.layer.cornerRadius = 4;
    _idolInput.layer.masksToBounds = YES;
    
    UIButton *comfirm = [[UIButton alloc]initWithFrame:CGRectMake(40, 45, 60, 30)];
    comfirm.titleLabel.font = [UIFont systemFontOfSize:14];
    comfirm.backgroundColor = UIColorFromRGB(0xffffff);
    comfirm.layer.cornerRadius = 3;
    comfirm.layer.masksToBounds = YES;
    [comfirm setTitle:@"确定" forState:UIControlStateNormal];
    [comfirm setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [comfirm addTarget:self action:@selector(comfirmDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancel = [[UIButton alloc]initWithFrame:CGRectMake(165, 45, 60, 30)];
    cancel.titleLabel.font = [UIFont systemFontOfSize:14];
    cancel.backgroundColor = UIColorFromRGB(0xffffff);
    cancel.layer.cornerRadius = 3;
    cancel.layer.masksToBounds = YES;
    [cancel setTitle:@"取消" forState:UIControlStateNormal];
    [cancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancelDidClick) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    [self.view addSubview:_inputIdolView];
    [_inputIdolView addSubview:_idolInput];
    [_inputIdolView addSubview:comfirm];
    [_inputIdolView addSubview:cancel];

}


- (void)comfirmDidClick{
    NSString *str = idolName.text;
    
    if(_idolInput.text.length != 0){
        str = [str stringByAppendingString:[NSString stringWithFormat:@"、%@",_idolInput.text]];
        idolName.text = str;
        [_inputIdolView removeFromSuperview];
        return;
    }
    [_inputIdolView removeFromSuperview];

}



- (void)cancelDidClick{
    [_inputIdolView removeFromSuperview];

}



- (void)addNavRightView{
    UIButton * btnedit = GET_BUTTON(CGRectMake(ScreenWidth-60, ios7_height, 44, 44), 17, NO, [UIColor whiteColor]);
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"完成"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
    
    btnedit.tag = 1;
    
}





//uid
//nickname  昵称
//sex       1男  0女
//headimg   base64传
//idol      偶像
//signature 个性签名
//school		毕业院校
//company	工作单位
//position   场上位置
//borndate   出生日期
#pragma mark - 更改个人资料数据请求
- (void) upDataUserInfoWithName:(NSString *)nickname
                            Sex:(NSString *)sex
                        IconImg:(UIImage  *)iconimg
                           Idol:(NSString *)idol
                     Signature :(NSString *)signature
                         School:(NSString *)school
                        Company:(NSString *)company
                       Position:(NSString *)position
                            uid:(NSString *)uid
                       borndate:(NSString *)borndate
{
    UIImage *img = [GT_Tool scaleToSize:iconimg size:CGSizeMake(90, 90)];
    NSData *iconImg = UIImageJPEGRepresentation(img,0.5);
    NSString *iconStr = [GTMBase64 stringByEncodingData:iconImg];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];
    [dictionary setObject:nickname forKey:@"nickname"];
    [dictionary setObject:sex forKey:@"sex"];
    [dictionary setObject:iconStr forKey:@"headimg"];
    [dictionary setObject:idol forKey:@"idol"];
    [dictionary setObject:signature forKey:@"signature"];
    [dictionary setObject:school forKey:@"school"];
    [dictionary setObject:company forKey:@"company"];
    [dictionary setObject:position forKey:@"postion"];
    [dictionary setObject:borndate forKey:@"borndate"];


    NSLog(@"dict -- %@",dictionary);
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:SETUSERDATA Dictionary:dictionary Tag:TAG_HTTP_SETUSERDATA];

}






#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
//        NSLog(@"GETUSERDATA -- 修改个人资料resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_SETUSERDATA){
            if (HTTP_SUCCESS(code))
            {
                NSString *msg = dictionary[@"msg"];
                [self showMessage:msg];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else if (request.tag == TAG_HTTP_GETSIGNATURE){
            if (HTTP_SUCCESS(code))
            {
                NSString *signStr = dictionary[@"msg"];
                signTextView.text = signStr;
            }
            
        }
    }
    
}

#pragma -mark Btn Action
- (IBAction)addTeamIcon:(UIButton *)sender {
    
}

- (void)btnNavRightClick:(UIButton *)button{

    [self asyshowHUDView:WAITTING CurrentView:self];
    
    NSString *name = userName.text;
    NSString *sex = [NSString stringWithFormat:@"%ld",_selectedSexBtn.tag];
    UIImage *iconImg = headImg.image;
    NSString *idol = idolName.text;
    NSString *sign = signTextView.text;
    NSString *school;
    NSString *company;
    NSString *positon = positionTextView.text;
    
    if(_schoolOrCompany == 0){
        company = _coSchTextView.text;
        school = @"";
    }else{
        company = @"";
        school = _coSchTextView.text;
    }
    
    
    
    [self upDataUserInfoWithName:name Sex:sex IconImg:iconImg Idol:idol Signature:sign School:school Company:company Position:positon uid:GETUSERID borndate:@""];
    
    
}


- (IBAction)sexBtnDidClick:(UIButton *)sender {
    _selectedSexBtn.selected = NO;
    sender.selected = YES;
    _selectedSexBtn = sender;
}


#pragma mark - 随机签名按钮点击方法
- (IBAction)randomBtnDidClick:(UIButton *)sender {
    [self setHTTPRequest:GETSIGNATURE Tag:TAG_HTTP_GETSIGNATURE];

}



#pragma mark - 头像按钮点击方法
- (IBAction)headBtnDidClick:(UIButton *)sender {
    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:self.view.frame];
    under.delegate = self;
    [self.view addSubview:under];
}


- (void)photoChoice:(UIImage *)image
{
    headImg.image = image;
}

- (IBAction)schoolOrCompanyBtnDidClick:(UIButton *)sender
{
    if(_coSchViewIsOnView == NO){
        [_scrollView addSubview:_coSchView];
        _coSchViewIsOnView = YES;
    }else{
        [_coSchView removeFromSuperview];
        _coSchViewIsOnView = NO;
    }
    
    
}


- (void) schoolBtnOrCompanyBtnDidClick:(UIButton *)btn
{
    //1 学校 0 公司
    if(btn.tag == 1){
        [_btnCoSch setTitle:@"学校" forState:UIControlStateNormal];
        _schoolOrCompany = 1;
        [_coSchView removeFromSuperview];
        _coSchViewIsOnView = NO;
    }else{
        [_btnCoSch setTitle:@"公司" forState:UIControlStateNormal];
        _schoolOrCompany = 0;
        [_coSchView removeFromSuperview];
        _coSchViewIsOnView = NO;
    }

}


/**
 *  当键盘出现的时候，移动View的位置
 */
- (void)KeyBoardWillShow:(NSNotification *)note
{
    
    
    if(userName.editing == YES){
        return;
    }
    
    if(idolName.editing == YES){
        [UIView animateWithDuration:0.25 animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, -100);
            
        }];
        return;
    }
    
    if(_idolInput.editing == YES){
        [UIView animateWithDuration:0.25 animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, -100);
        }];
        return;
    }
    
    if(signTextEdit == YES){
        [UIView animateWithDuration:0.25 animations:^{
            self.view.transform = CGAffineTransformMakeTranslation(0, -200);
            
        }];
        return;
    }
    
    CGRect keyBoardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //  2.计算控制器view需要平移的距离
    CGFloat transformY = keyBoardFrame.origin.y - (self.view.frame.size.height);
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, transformY);
        
    }];
    
}

- (void)KeyBoardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}



- (void)textViewDidBeginEditing:(UITextView *)textView
{
    signTextEdit = YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    signTextEdit = NO;
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //让当前view结束所有控件的编辑状态
    [self.view endEditing:YES];
    
    if(_inputIdolView.superview != nil){
        [_inputIdolView removeFromSuperview];
    }
    
}


@end
