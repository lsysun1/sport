//
//  SP_BattleInfoViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BattleInfoViewController.h"
#import "SP_SendBattleCell.h"
#import "SP_SendBattleEntity.h"
#import "SP_UerHomeViewController.h"
#import "SP_ReceiveCell.h"
#import "SP_BattleBtn.h"

#define TAG_TAB_SEND         666
#define TAG_TAB_RECEIVE      999


@interface SP_BattleInfoViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_sendTableView;
    UITableView *_receiveTableView;

    UIView * _lineView;
    
    NSMutableArray *_sendArr;
    NSMutableArray *_receiveArr;

    int _type;  //  0 留言板    1 球队消息

}
@end

@implementation SP_BattleInfoViewController


- (id)init
{
    self = [super init];
    if(self){
        _sendArr = [NSMutableArray array];
        _receiveArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"邀战信息"];
    [self addBackButton];
    [self _initGreenView];
    [self setUpSendTableView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETCHALLENGELIST(self.teamid) Tag:TAG_HTTP_GETCHALLENGELIST];
    [self setHTTPRequest:GETSENDCHALLENGELIST(self.teamid) Tag:TAG_HTTP_GETSENDCHALLENGELIST];
}

- (void) setUpSendTableView
{
    _sendTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 44, ScreenWidth, ScreenHeight - 64 -44 )];
    _sendTableView.separatorStyle = 0;
    _sendTableView.delegate = self;
    _sendTableView.dataSource = self;
    _sendTableView.tag = TAG_TAB_SEND;
    _sendTableView.rowHeight = 203;
    [self.view addSubview:_sendTableView];
}

- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if(request.responseString != nil){
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        
        if(request.tag == TAG_HTTP_GETCHALLENGELIST){
            if(HTTP_SUCCESS(code)){
                        NSLog(@"TAG_HTTP_GETCHALLENGELIST --- resopnse:%@",request.responseString);

                NSArray *msg = dictionary[@"msg"];
                for(NSDictionary *dict in msg){
                    SP_SendBattleEntity *sendEntity = [SP_SendBattleEntity objectWithKeyValues:dict];
                    [_receiveArr addObject:sendEntity];
                }
                [_receiveTableView reloadData];
            }
        }else if (request.tag == TAG_HTTP_GETSENDCHALLENGELIST){
            if(HTTP_SUCCESS(code)){

            NSArray *msg = dictionary[@"msg"];
            for(NSDictionary *dict in msg){
                SP_SendBattleEntity *sendEntity = [SP_SendBattleEntity objectWithKeyValues:dict];
                [_sendArr addObject:sendEntity];
            }
            [_sendTableView reloadData];

            }
        }else if (request.tag == TAG_HTTP_TEAMCHALLENGEHANDLE){
            if(HTTP_SUCCESS(code)){
                [_receiveArr removeAllObjects];
                [self setHTTPRequest:GETCHALLENGELIST(self.teamid) Tag:TAG_HTTP_GETCHALLENGELIST];
            }
        }
    }
}

- (void) setUpReceiveTableView
{
    if(_receiveTableView == nil){
        _receiveTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 44, ScreenWidth, ScreenHeight - 64- 44)];
        _receiveTableView.separatorStyle = 0;
        _receiveTableView.delegate = self;
        _receiveTableView.dataSource = self;
        _receiveTableView.tag = TAG_TAB_RECEIVE;
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == TAG_TAB_SEND){
        return _sendArr.count;
    }else{
        return _receiveArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_TAB_SEND){
        SP_SendBattleCell *cell = [SP_SendBattleCell cellWithTableView:tableView];
        SP_SendBattleEntity *sendEntity = _sendArr[indexPath.row];
        cell.entity = sendEntity;
        
        if(sendEntity.playerdata.count == 1){
            NSDictionary *aDict = sendEntity.playerdata[0];
            cell.aBtn.tag = [aDict[@"uid"] integerValue];
            [cell.aBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        }else if (sendEntity.playerdata.count == 2){
            
            NSDictionary *aDict = sendEntity.playerdata[0];
            cell.aBtn.tag = [aDict[@"uid"] integerValue];
            [cell.aBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

            NSDictionary *bDict = sendEntity.playerdata[1];
            cell.bBtn.tag = [bDict[@"uid"] integerValue];
            [cell.bBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

        }
        return cell;
    
    }else{
        SP_ReceiveCell *cell = [SP_ReceiveCell cellWithTableView:tableView];
        SP_SendBattleEntity *sendEntity = _receiveArr[indexPath.row];
        cell.entity = sendEntity;
        
        if(sendEntity.playerdata.count == 1){
            NSDictionary *aDict = sendEntity.playerdata[0];
            cell.aBtn.tag = [aDict[@"uid"] integerValue];
            [cell.aBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        }else if (sendEntity.playerdata.count == 2){
            
            NSDictionary *aDict = sendEntity.playerdata[0];
            cell.aBtn.tag = [aDict[@"uid"] integerValue];
            [cell.aBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            
            NSDictionary *bDict = sendEntity.playerdata[1];
            cell.bBtn.tag = [bDict[@"uid"] integerValue];
            [cell.bBtn addTarget:self action:@selector(playerHeadBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        if([sendEntity.isthrough isEqualToString:@"0"]){
            
            cell.yesBtn.teammatchid = sendEntity.teammatchid;
            cell.yesBtn.type = @"1";
            cell.noBtn.teammatchid = sendEntity.teammatchid;
            cell.noBtn.type = @"2";
            
            [cell.yesBtn addTarget:self action:@selector(yesOrNoBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.noBtn addTarget:self action:@selector(yesOrNoBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_TAB_SEND){
        return 203;
    }else if (tableView.tag == TAG_TAB_RECEIVE){
        SP_SendBattleEntity *sendEntity = _receiveArr[indexPath.row];
        if([sendEntity.isthrough isEqualToString:@"0"]){
            return 280;
        }else{
            return 243;
        }
    }
    return 0;
}

- (void) yesOrNoBtnDidClick:(SP_BattleBtn *)btn
{
    NSString *teammatchid = btn.teammatchid;
    NSString *type = btn.type;
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:TEAMCHALLENGEHANDLE(teammatchid, type) Tag:TAG_HTTP_TEAMCHALLENGEHANDLE];
    
}



- (void) playerHeadBtnDidClick:(UIButton *)btn
{
    if(![GETUSERID isEqualToString:[NSString stringWithFormat:@"%ld",btn.tag]]){
        SP_UerHomeViewController *homeVC = [[SP_UerHomeViewController alloc]init];
        homeVC.touid = [NSString stringWithFormat:@"%ld",btn.tag];
        [[AppDelegate sharedAppDelegate].nav pushViewController:homeVC animated:YES];
    }
}




- (void)_initGreenView{
    
    UIView * greenView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, 44)];
    greenView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [self.view addSubview:greenView];
    
    UIButton * Btnlaunch = GET_BUTTON(CGRectMake(0, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnlaunch setTitle:@"发出的邀战" forState:UIControlStateNormal];
    [greenView addSubview:Btnlaunch];
    Btnlaunch.tag = 101;
    
    UIButton * Btnjoin = GET_BUTTON(CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnjoin setTitle:@"收到的邀战" forState:UIControlStateNormal];
    [greenView addSubview:Btnjoin];
    Btnjoin.tag = 102;
    
    [Btnlaunch addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    [Btnjoin addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/8, 40, ScreenWidth/4, 4)];
    
    [greenView addSubview:_lineView];
    _lineView.backgroundColor = UIColorFromRGB(0xe27e33);
}


- (void) btnActivitySelClick:(UIButton *)btn
{
    CGPoint linpoint = _lineView.center;
    
    linpoint.x = btn.center.x;
    
    _lineView.center = linpoint;
    
    if(btn.tag == 101){
        _type = 0;
        
        [_receiveTableView removeFromSuperview];
        [self.view addSubview:_sendTableView];
        
    }else{
        _type = 1;
        [self setUpReceiveTableView];

        [_sendTableView removeFromSuperview];
        [self.view addSubview:_receiveTableView];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
