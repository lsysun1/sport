//
//  SP_AddTeamPhotoViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddTeamPhotoViewController.h"
#import "PhotoChoiceView.h"
#import "ZipArchive.h"
#import "SP_TeamImgCateList.h"
#import "PhotoChoiceView.h"

@interface SP_AddTeamPhotoViewController ()<PhotoChoiceViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *_nameView;
    UITextField *_nameTextField;
    UIView *_timeView;
    UILabel *_timeLabel;
    UIButton *_timeBtn;
    UIView *_photosView;
    
    UIButton *_photoAddBtn;
    NSMutableArray *_photoViewArray;
    NSMutableArray *jpgPathArray;   //转换后的jpg数组
    ZipArchive *jpgZip;
    NSMutableArray *_imgArray;
    
    UIDatePicker *_datePicker;
    
    long int timeINT;
    
    NSString *_teamimgcateid;
    
    
}

@end

@implementation SP_AddTeamPhotoViewController

- (id)init
{
    self = [super init];
    if(self){
        _photoViewArray = [[NSMutableArray alloc]init];
        _imgArray = [[NSMutableArray alloc]init];
        jpgPathArray = [[NSMutableArray alloc]init];

    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"添加相册"];
    [self addBackButton];
    
    
    _teamimgcateid = self.imgCate.teamimgcateid;

    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64,ScreenWidth, ScreenHeight - 64)];
    _scrollView.backgroundColor = UIColorFromRGB(0xfafafa);
    [self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singTapMethod)];
    [_scrollView addGestureRecognizer:singtap];
    
    
    [self addNavRightView];
    [self setUpNameView];
    [self setUpTimeView];
    [self createDefaultTitleAndPhoto];
    
}


- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"确定"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_ADDTEAMIMGCATE){   //创建球队相册
            NSLog(@"TAG_HTTP_ADDTEAMIMGCATE - resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                
                NSDictionary *msgDict = dictionary[@"msg"];
                NSString *teamimgcateid = msgDict[@"teamimgcateid"];
                _teamimgcateid = teamimgcateid;
                [self addTeamImg];

            }
        }else if (request.tag == TAG_HTTP_UPLOADTEAMIMG){ //添加图片
            NSLog(@"TAG_HTTP_UPLOADTEAMIMG - resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                [self showMessage:@"图片添加成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        
        }
            
    
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self removeHUDView:self];
    NSLog(@"error -- %@",request.error);
}

/**
 *  确认按钮点击方法(执行网络请求)
 */
- (void) btnNavRightClick
{
    if(_nameTextField.text.length == 0){
        [self showMessage:@"活动名称不能为空"];
        return;
    }
    
    NSLog(@"tead -- %@",self.teamID);
    
    
    if(_teamimgcateid == nil){  //创建新相册
        
        NSString *teamdID = self.teamID;
        NSString *nameStr = _nameTextField.text;
        NSString *dateStr = [self urlEncodeValue:_timeLabel.text];
        
        // 先创建相册
        [self setHTTPRequest:ADDTEAMIMGCATE(teamdID, nameStr, dateStr) Tag:TAG_HTTP_ADDTEAMIMGCATE];
        
    }else{  //给已有相册添加图片
        [self addTeamImg];
    }
}



/**
 *  添加球队相册
 */
- (void) addTeamImg
{
    if(_imgArray.count <= 1){
        [self showMessage:@"请选择上传图片"];
        return;
    }
    
    [self zipImgs];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *picsPath = [NSString stringWithFormat:@"%@/%ld.zip",docDir,timeINT];
    NSString *picsName = [NSString stringWithFormat:@"%ld",timeINT];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:_teamimgcateid forKey:@"teamimgcateid"];
    [dictionary setObject:picsName forKey:@"upload"];
    
    [self setHttpPostFile:UPLOADTEAMIMG Params:dictionary File:picsPath Data:nil Tag:TAG_HTTP_UPLOADTEAMIMG];
    
}
/**
 *  打包图片
 */
- (void) zipImgs
{
    
    
    //获取时间戳
    timeINT= (long)[[NSDate  date] timeIntervalSince1970];
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    
    
    jpgZip = [[ZipArchive alloc] init];
    NSString* zipfile = [NSString stringWithFormat:@"%@/%ld.zip",documentsDirectory,timeINT];
    
    [jpgZip CreateZipFile2:zipfile];
    
    for(int i = 1;i<_imgArray.count;i++){
        
        UIImage *img = _imgArray[i];
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/img%d.jpg",i]];
        
        
        [UIImageJPEGRepresentation(img, 0.5) writeToFile:jpgPath atomically:YES];
        
        [jpgZip addFileToZip:jpgPath newname:[NSString stringWithFormat:@"img%d.jpg",i]];
        
        [fileMgr removeItemAtPath:jpgPath error:nil];
    }
    
    if( ![jpgZip CloseZipFile2] )
    {
        zipfile = @"";
    }
    
}






#pragma mark - 上传相册图片
- (void) upImgsWithName:(NSString *)nickname
                    Sex:(NSString *)sex

{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];

    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:SETUSERDATA Dictionary:dictionary Tag:TAG_HTTP_SETUSERDATA];
}





- (void) setUpNameView
{
    _nameView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    _nameView.backgroundColor = [UIColor whiteColor];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 70, 40)];
    name.font = [UIFont systemFontOfSize:14];
    name.textColor = UIColorFromRGB(0x696969);
    name.text = @"活动名称:";
    
    _nameTextField = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(name.frame), 0, ScreenWidth, 40)];
    _nameTextField.font = [UIFont systemFontOfSize:14];
    _nameTextField.textColor = UIColorFromRGB(0x696969);
    
    if(self.imgCate != nil ){
        _nameTextField.text = self.imgCate.name;
    }
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(10, 39, ScreenWidth - 20, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    
    
    [_scrollView addSubview:_nameView];
    [_nameView addSubview:name];
    [_nameView addSubview:_nameTextField];
    [_nameView addSubview:bottomLine];
}


- (void) setUpTimeView
{

    _timeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_nameView.frame), ScreenWidth, 40)];
    _timeView.backgroundColor = [UIColor whiteColor];

    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 70, 40)];
    name.font = [UIFont systemFontOfSize:14];
    name.textColor = UIColorFromRGB(0x696969);
    name.text = @"活动时间:";
    
    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(name.frame), 0, ScreenWidth, 40)];
    _timeLabel.font = [UIFont systemFontOfSize:14];
    _timeLabel.textColor = UIColorFromRGB(0x696969);
    NSDate *select = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    _timeLabel.text = dateAndTime;
    
    if(self.imgCate != nil ){
        _timeLabel.text = self.imgCate.date;
    }

    
    _timeBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(name.frame), 0, ScreenWidth, 40)];
    [_timeBtn addTarget:self action:@selector(timeBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(10, 39, ScreenWidth - 20, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    
    
    [_scrollView addSubview:_timeView];
    [_timeView addSubview:name];
    [_timeView addSubview:_timeLabel];
    [_timeView addSubview:_timeBtn];
    [_timeView addSubview:bottomLine];





}



#pragma mark - 选择时间
- (void) timeBtnDidClick
{
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, ScreenHeight - 200, ScreenWidth, 200)];
    [_datePicker setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [_datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_Hans_CN"]];
    
    if(_timeLabel.text != nil){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
        NSDate *date = [dateFormatter dateFromString:_timeLabel.text];
        [_datePicker setDate:date animated:YES];
        
    }else{
        [_datePicker setDate:[NSDate date] animated:YES];
        
    }
    
    [_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_datePicker];

}

- (void) datePickerValueChanged:(UIDatePicker *)picker
{
    NSDate *select = [picker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *dateAndTime =  [dateFormatter stringFromDate:select];
    _timeLabel.text = dateAndTime;
}



/**
 *  初始化添加标题和添加图片按钮
 */
- (void) createDefaultTitleAndPhoto
{
    
    UILabel *des = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_timeView.frame), ScreenWidth, 40)];
    des.backgroundColor = UIColorFromRGB(0xffffff);
    des.font = [UIFont systemFontOfSize:14];
    des.textColor = UIColorFromRGB(0x696969);
    des.text = @"    上传活动图片";
    [_scrollView addSubview:des];
    
    _photosView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_timeView.frame), ScreenWidth, 100)];
    _photosView.backgroundColor = [UIColor whiteColor];
    
    
    
    _photoAddBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, 90, 90)];
    [_photoAddBtn setBackgroundImage:[UIImage imageNamed:@"addBtn"] forState:UIControlStateNormal];
    [_photoAddBtn addTarget:self action:@selector(addPhotoBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if(_imgArray.count == 0){
        [_imgArray addObject:_photoAddBtn];
    }else{
        if([_imgArray[0] isKindOfClass:[UIImage class]]){
            [_imgArray insertObject:_photoAddBtn atIndex:0];
        }
    }
    
    
    
    [self bulidPhotosView];
    
}

- (void) bulidPhotosView
{
    _photosView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_timeView.frame) + 40, ScreenWidth, ScreenHeight * 10)];
    _photosView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_photosView];
    
    CGFloat originX = 15;
    CGFloat originY = 10;
    for(int i = 0; i <_imgArray.count; i ++){
        if(i == 0){
            UIButton *photoView = _imgArray[i];
            photoView.frame = CGRectMake(originX, originY, 90, 90);
            [_photosView addSubview:photoView];
        }else{
            UIImage *photoImg = _imgArray[i];
            
            UIImageView *photoView = [[UIImageView alloc]initWithFrame:CGRectMake(originX, originY, 90, 90)];
            photoView.image = photoImg;
            photoView.tag = i + 100;
            [_photosView addSubview:photoView];
            [_photoViewArray addObject:photoView];
            
            UIButton *delBtn = [[UIButton alloc]initWithFrame:CGRectMake(originX + 83, originY - 5, 15, 15)];
            delBtn.tag = i +100;
            [delBtn setBackgroundImage:[UIImage imageNamed:@"btn_deletepic"] forState:UIControlStateNormal];
            [delBtn addTarget:self action:@selector(delBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            [_photosView addSubview:delBtn];
        }
        
        // 计算下个view的尺寸
        originX += 100;
        
        if((i+1)%3 == 0 && i != 0){
            originX = 16;
            originY += 100;
        }
    }

    CGFloat maxH = CGRectGetMaxY(_timeView.frame) + 40 + _imgArray.count / 3 * 110 + 100;
    _scrollView.contentSize = CGSizeMake(ScreenWidth, maxH);
    

}

- (void) delBtnDidClick:(UIButton *)btn
{
    [_imgArray removeObjectAtIndex:btn.tag - 100];
    [_photosView removeFromSuperview];
    _photosView = nil;
    [self bulidPhotosView];

}



- (void) addPhotoBtnDidClick
{
    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:self.view.frame];
    under.delegate = self;
    [self.view addSubview:under];
}

#pragma mark - PhotoChoiceViewDelegate
- (void)photoChoice:(UIImage *)image
{
    [_imgArray addObject:image];
    [self bulidPhotosView];
}





- (void) singTapMethod
{
    if(_datePicker.superview != nil){
        [_datePicker removeFromSuperview];
    }
    
    [self.view endEditing:YES];
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
