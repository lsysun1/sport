//
//  SP_SendBattleViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_SendBattleViewController : BaseViewController

@property (nonatomic,copy) NSString *bteamid;       //所有挑战的球队ID


@end
