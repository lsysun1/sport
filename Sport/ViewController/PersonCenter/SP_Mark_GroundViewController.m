//
//  SP_Mark_GroundViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Mark_GroundViewController.h"
#import "GroundListCell.h"
#import "SP_GroundEntity.h"
#import "SP_GroundDetailsViewController.h"


@interface SP_Mark_GroundViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_nowBtn;
}
@end

@implementation SP_Mark_GroundViewController

- (id)init
{
    self = [super init];
    if(self){
        _arr = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setUpTableView];
    
    [self getMyCollectListWithType:@"2" Num:@"10" CollectId:@"0"];
}



//uid
//type  0活动  1比赛  2场馆
//num
//collectid  第一次传0




- (void) getMyCollectListWithType:(NSString *)type
                              Num:(NSString *)num
                        CollectId:(NSString *)collectid
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:num forKey:@"num"];
    [dict setObject:collectid forKey:@"collectid"];
    
    [self setHttpPostData:GETCOLLECTLIST Dictionary:dict Tag:TAG_HTTP_GETCOLLECTLIST];
    
}

- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+38, ScreenWidth, ScreenHeight - 64-38)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = 0;
    _tableView.rowHeight  = 114;
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadList)];
    [self.view addSubview:_tableView];
}


- (void) reloadList
{
    [_arr removeAllObjects];
    

    
    [self getMyCollectListWithType:@"2" Num:@"10" CollectId:@"0"];
}

- (void) refreshList
{
    SP_GroundEntity *entity = [_arr lastObject];
    
    [self getMyCollectListWithType:@"2" Num:@"10" CollectId:entity.ycdid];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETCDLIST -- 足球resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETCOLLECTLIST){
            if (HTTP_SUCCESS(code))
            {
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    
                    SP_GroundEntity *entity = [SP_GroundEntity objectWithKeyValues:dict];
                    [_arr addObject:entity];
                    
                }
            }
            
            if(_arr.count == 10){
                [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(refreshList)];
            }
            
            
            NSLog(@"_arr.count -- %ld",_arr.count);
            [_tableView reloadData];
            [_tableView.header endRefreshing];
            [_tableView.footer endRefreshing];
            
        }else if (request.tag == TAG_HTTP_ADDCOLLECT){
            if(HTTP_SUCCESS(code)){
                
                _nowBtn.selected = YES;
                [self showMessage:@"收藏成功"];
                
            }else{
                [self showMessage:@"收藏失败!"];
            }
            
        }else if (request.tag == TAG_HTTP_CANCELCOLLECT){
            if(HTTP_SUCCESS(code)){
                _nowBtn.selected = NO;
                [self showMessage:@"取消收藏成功"];
                
            }else{
                [self showMessage:@"取消收藏失败!"];
            }
        
        }
    }
}




#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroundListCell *cell = [GroundListCell cellWithTableView:tableView];
    
    SP_GroundEntity *entity = _arr[indexPath.row];
    cell.entity = entity;
    
    [cell.markBtn addTarget:self action:@selector(cellMarkBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.markBtn.tag = [entity.ycdid integerValue];
    
    
    return cell;
}




- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SP_GroundDetailsViewController *vc = [[SP_GroundDetailsViewController alloc]init];
    SP_GroundEntity *entity = _arr[indexPath.row];
    vc.entity = entity;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}

#pragma mark - 关注按钮
- (void) cellMarkBtnDidClick:(UIButton *)btn
{
    _nowBtn = btn;
    
    
    if(btn.selected == NO){
        NSString *linkid = [NSString stringWithFormat:@"%ld",btn.tag];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:linkid forKey:@"linkid"];
        [dict setObject:@"2" forKey:@"type"];
        
        [self setHttpPostData:ADDCOLLECT Dictionary:dict Tag:TAG_HTTP_ADDCOLLECT];
    }else if (btn.selected == YES){
       
        NSString *linkid = [NSString stringWithFormat:@"%ld",btn.tag];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:linkid forKey:@"linkid"];
        [dict setObject:@"2" forKey:@"type"];
        
        [self setHttpPostData:CANCELCOLLECT Dictionary:dict Tag:TAG_HTTP_CANCELCOLLECT];
    
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
