//
//  SP_MyMessageViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MyMessageViewController.h"
#import "SP_MYMessageCell.h"
#import "SP_YhbCmtCell.h"
#import "SP_YhbCmtFrame.h"
#import "SP_YhbCommentEntity.h"
#import "SP_YhbReCmtCell.h"
#import "SP_YhbReCmtEntity.h"
#import "SP_YhbReCmtFrame.h"
#import "SP_YhbReCmtTextView.h"
#import "SP_YhbHfBtn.h"
#import "SP_CommentTextSpecial.h"
#import "SP_SendMsgViewController.h"
#import "SP_JoinTeamListCell.h"
#import "SP_JoinTeamListEntity.h"


#define TAG_TAB_PERSONAL  666
#define TAG_TAB_TEAM      999

@interface SP_MyMessageViewController () <UITableViewDelegate,UITableViewDataSource,SP_YhbReCmtTextViewDelegate>
{
    
    UITableView *_teamTableView;
    UITableView *_personalTableView;

    UIView * _lineView;
    
    NSMutableArray *_cmtArr;
    NSMutableArray *_joinListArr;

    int _type;  //  0 留言板    1 球队消息
    
}

@end

@implementation SP_MyMessageViewController

- (id) init
{
    self = [super init];
    if(self){
        _cmtArr = [NSMutableArray array];
        _joinListArr = [NSMutableArray array];
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xfafafa);
    [self setNavigationBar:@"我的消息"];
    [self addBackButton];
    [self _initGreenView];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:GETUSERID forKey:@"touid"];
    [dict setObject:@"0" forKey:@"usermsgid"];
    [dict setObject:@"10" forKey:@"num"];
    [self setHttpPostData:USERGETCOMMENT Dictionary:dict Tag:TAG_HTTP_USERGETCOMMENT];
    
    [self setHTTPRequest:GETJOINTEAMLIST(GETUSERID) Tag:TAG_HTTP_GETJOINTEAMLIST];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iAmBackHaHa) name:@"iAmBack" object:nil];

    
    [self setUpPersonalTableView];
    [self setUpTeamTableView];
    
}

- (void) iAmBackHaHa
{
    [_cmtArr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:GETUSERID forKey:@"touid"];
    [dict setObject:@"0" forKey:@"usermsgid"];
    [dict setObject:@"10" forKey:@"num"];
    [self setHttpPostData:USERGETCOMMENT Dictionary:dict Tag:TAG_HTTP_USERGETCOMMENT];

}


- (void) setUpPersonalTableView
{
    _personalTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 44, ScreenWidth, ScreenHeight - 64 -44 )];
    _personalTableView.separatorStyle = 0;
    _personalTableView.delegate = self;
    _personalTableView.dataSource = self;
    _personalTableView.tag = TAG_TAB_PERSONAL;
    [self.view addSubview:_personalTableView];
}


- (void) setUpTeamTableView
{
    _teamTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64 + 44, ScreenWidth, ScreenHeight - 64- 44)];
    _teamTableView.separatorStyle = 0;
    _teamTableView.delegate = self;
    _teamTableView.dataSource = self;
    _teamTableView.tag = TAG_TAB_TEAM;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == TAG_TAB_PERSONAL){
        return _cmtArr.count;
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == TAG_TAB_PERSONAL){
        SP_YhbCmtFrame *cmtFrame = _cmtArr[section];
        
        NSLog(@"cmtFrame.reCmtArr.count --- %ld",cmtFrame.reCmtArr.count);
        
        return 1 + cmtFrame.reCmtArr.count;
    }else{
        return _joinListArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_TAB_PERSONAL){
        if(indexPath.row == 0){
            SP_YhbCmtCell *cell = [SP_YhbCmtCell cellWithTableView:tableView];
            SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
            cell.cmtFrame = frame;
            
            
            frame.index = indexPath;
            
            SP_YhbCommentEntity *entity = frame.cmtEntity;
            
            
            cell.hfButton.yhbplid = entity.usermsgid;
            cell.hfButton.hfuid = entity.touid;
            
            [cell.hfButton addTarget:self action:@selector(hfBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            return cell;
        }else{
            SP_YhbReCmtCell *cell =[SP_YhbReCmtCell cellWithTableView:tableView];
            cell.commentTextView.sdelegate = self;
            
            
            SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
            frame.index = indexPath;
            SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
            cell.frameModel = reFrame;
            
            return cell;
        }
    }else{
        SP_JoinTeamListCell *cell = [SP_JoinTeamListCell cellWithTableView:tableView];
        
        SP_JoinTeamListEntity *entity = _joinListArr[indexPath.row];
        cell.entity = entity;
        
        cell.agreeBtn.tag = [entity.teamid integerValue];
        cell.notAgreeBtn.tag = [entity.teamid integerValue];

        
        
        [cell.agreeBtn addTarget:self action:@selector(agreeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.notAgreeBtn addTarget:self action:@selector(notAgreeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (void)agreeBtnDidClick:(UIButton *)btn
{

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:[NSString stringWithFormat:@"%ld",btn.tag] forKey:@"teamid"];
    [dict setObject:@"1" forKey:@"type"];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:JOINTEAMHANDLE Dictionary:dict Tag:TAG_HTTP_JOINTEAMHANDLE];
}

- (void)notAgreeBtnDidClick:(UIButton *)btn
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:[NSString stringWithFormat:@"%ld",btn.tag] forKey:@"teamid"];
    [dict setObject:@"2" forKey:@"type"];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:JOINTEAMHANDLE Dictionary:dict Tag:TAG_HTTP_JOINTEAMHANDLE];

    
}


- (void) hfBtnDidClick:(SP_YhbHfBtn *)btn
{
 
    SP_SendMsgViewController *vc = [[SP_SendMsgViewController alloc]init];
    vc.type = @"3";
    vc.usermsgid = btn.yhbplid;
    vc.hfuid = btn.hfuid;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}




- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if(request.responseString != nil){
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_USERGETCOMMENT){
            if(HTTP_SUCCESS(code)){
                NSArray *msg = dictionary[@"msg"];
                
                for(NSDictionary *dict in msg){
                    SP_YhbCommentEntity *cmtEntity = [SP_YhbCommentEntity objectWithKeyValues:dict];
                    SP_YhbCmtFrame *cmtFrame = [[SP_YhbCmtFrame alloc]init];
                    cmtFrame.cmtEntity = cmtEntity;
                    
                    for(NSDictionary *reDict in cmtEntity.hfdata){
                        SP_YhbReCmtEntity *reCmtEntity = [SP_YhbReCmtEntity objectWithKeyValues:reDict];
                        SP_YhbReCmtFrame *reCmtFrame = [[SP_YhbReCmtFrame alloc]init];
                        reCmtFrame.reCmtEntity = reCmtEntity;
                        
                        [cmtFrame.reCmtArr addObject:reCmtFrame];
                    }
                    
                    [_cmtArr addObject:cmtFrame];
                    
                }
                [_personalTableView reloadData];

            
            }
        }else if (request.tag == TAG_HTTP_GETJOINTEAMLIST){
                        NSLog(@"TAG_HTTP_GETJOINTEAMLIST -- %@",request.responseString);

            if(HTTP_SUCCESS(code)){
                NSArray *msg = dictionary[@"msg"];
                
                for(NSDictionary *dict in msg){
                    SP_JoinTeamListEntity *entity = [SP_JoinTeamListEntity objectWithKeyValues:dict];
                    if(![entity.isthrough isEqualToString:@"3"]){
                        [_joinListArr addObject:entity];
                    }
                }
                
                [_teamTableView reloadData];
            }
        }else if(request.tag == TAG_HTTP_JOINTEAMHANDLE){
            if(HTTP_SUCCESS(code)){
                [_joinListArr removeAllObjects];
                [self setHTTPRequest:GETJOINTEAMLIST(GETUSERID) Tag:TAG_HTTP_GETJOINTEAMLIST];
            }
        
        }
    }
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_TAB_PERSONAL){
        if(indexPath.row == 0){
            SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
            return frame.cellHeight;
        }else{
            SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
            SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
            return reFrame.cellHeight;
        }
    }else{
        return 70;
    }
}

/**
 *  点击回复评论的某一个人
 */
- (void) getSpecialBack:(SP_CommentTextSpecial *)special
{
    NSLog(@"%@  %@  %@  %@  %@",special.nickName,special.hfName,special.usermsgid,special.hfuid ,special.uid);
    
    NSString *uid = [[NSString alloc]init];
    if(special.hfuid.length == 0){
        uid = special.uid;
    }else{
        uid = special.hfuid;
    }
    
    if([GETUSERID isEqualToString:uid]){
        return;
    }
    
    
    
    
    SP_SendMsgViewController *vc = [[SP_SendMsgViewController alloc]init];
    vc.type = @"3";
    vc.usermsgid = special.usermsgid;
    vc.hfuid = uid;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}



- (void)_initGreenView{
    
    UIView * greenView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, 44)];
    greenView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [self.view addSubview:greenView];
    
    UIButton * Btnlaunch = GET_BUTTON(CGRectMake(0, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnlaunch setTitle:@"发起的活动" forState:UIControlStateNormal];
    [greenView addSubview:Btnlaunch];
    Btnlaunch.tag = 101;
    
    UIButton * Btnjoin = GET_BUTTON(CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnjoin setTitle:@"参与的活动" forState:UIControlStateNormal];
    [greenView addSubview:Btnjoin];
    Btnjoin.tag = 102;
    
    [Btnlaunch addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    [Btnjoin addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/8, 40, ScreenWidth/4, 4)];
    
    [greenView addSubview:_lineView];
    _lineView.backgroundColor = UIColorFromRGB(0xe27e33);
}


- (void) btnActivitySelClick:(UIButton *)btn
{
    CGPoint linpoint = _lineView.center;
    
    linpoint.x = btn.center.x;
    
    _lineView.center = linpoint;
    
    if(btn.tag == 101){
        _type = 0;
        
        [_teamTableView removeFromSuperview];
        [self.view addSubview:_personalTableView];
        
    }else{
        _type = 1;

        [_personalTableView removeFromSuperview];
        [self.view addSubview:_teamTableView];
        
    }
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
