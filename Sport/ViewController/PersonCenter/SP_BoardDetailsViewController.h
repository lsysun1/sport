//
//  SP_BoardDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"


@class SP_BoardListEntity;
@interface SP_BoardDetailsViewController : BaseViewController
@property (nonatomic,strong) SP_BoardListEntity *listEntity;
@end
