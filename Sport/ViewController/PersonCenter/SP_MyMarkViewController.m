//
//  SP_MyMarkViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MyMarkViewController.h"

#import "SP_Mark_ActiveViewController.h"
#import "SP_Mark_GroundViewController.h"
#import "SP_Mark_RaceViewController.h"


@interface SP_MyMarkViewController ()
{
    
    SP_Mark_ActiveViewController *_activeVC;
    SP_Mark_GroundViewController *_groundVC;
    SP_Mark_RaceViewController *_raceVC;

    
    
    IBOutlet UIView *_itemView;
    IBOutlet UIButton *_activeBtn;
    IBOutlet UIButton *_raceBtn;
    IBOutlet UIButton *_groundBtn;
    UIImageView *_itemLine;
    UIButton *_selectedBtn;
    
    
}
@end

@implementation SP_MyMarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"我的收藏"];
    [self addBackButton];
    
    [self setUpItemView];
    
    SP_Mark_ActiveViewController *activeVC=[[SP_Mark_ActiveViewController alloc] init];
    _activeVC = activeVC;
    [self.view insertSubview:_activeVC.view atIndex:0];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate].drawerController closeDrawerAnimated:NO completion:nil];
    
}


#pragma mark - 顶部选择菜单
- (void) setUpItemView
{
    _itemView.frame = CGRectMake(0, 64, ScreenWidth, 38);
    _itemView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _raceBtn.frame = CGRectMake(ScreenWidth/2 -35 , 4, 70, 30);
    CGFloat raceMinX = CGRectGetMinX(_raceBtn.frame);
    CGFloat raceMaxX = CGRectGetMaxX(_raceBtn.frame);
    
    _activeBtn.frame = CGRectMake(raceMinX - 110, 4, 70, 30);
    _groundBtn.frame = CGRectMake(raceMaxX + 40, 4, 70, 30);
    
    [_activeBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_raceBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_groundBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _activeBtn.selected = YES;
    _selectedBtn = _raceBtn;
    
    UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xfd7f45)];
    CGFloat activeMinX = CGRectGetMinX(_activeBtn.frame);
    CGFloat activeMaxY = CGRectGetMaxY(_activeBtn.frame);
    _itemLine = [[UIImageView alloc]init];
    _itemLine.frame = CGRectMake(activeMinX +1, activeMaxY , 70, 4);
    _itemLine.image = lineImg;
    [_itemView addSubview:_itemLine];
    
    
}

#pragma mark 顶部选择菜单按钮点击方法
- (void) menuBtnDidClick:(UIButton *)btn
{
    //活动Btn的Tag是0
    //比赛Btn的Tag是1
    //场地Btn的Tag是2
    
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
    
    
    
    
    
    CGFloat activeMinX = CGRectGetMinX(_activeBtn.frame);
    CGFloat activeMaxY = CGRectGetMaxY(_activeBtn.frame);
    CGFloat raceMinX = CGRectGetMinX(_raceBtn.frame);
    CGFloat groundMinX = CGRectGetMinX(_groundBtn.frame);
    
    CGRect lineOneRect = CGRectMake(activeMinX +1, activeMaxY , 70, 4);
    CGRect lineTwoRect = CGRectMake(raceMinX +1, activeMaxY , 70, 4);
    CGRect lineThreeRect = CGRectMake(groundMinX +1, activeMaxY , 70, 4);
    
    switch (btn.tag) {
        case 0:
        {
            _itemLine.frame = lineOneRect;
            
            if (_activeVC.view.superview==nil)//判断是否为根视图
            {
                if (_activeVC==nil)//判断视图控制器是否初始化
                {
                    SP_Mark_ActiveViewController *activeVC=[[SP_Mark_ActiveViewController alloc] init];
                    _activeVC = activeVC;
                }
                
                [_raceVC.view removeFromSuperview];
                [_groundVC.view removeFromSuperview];
                [self.view insertSubview:_activeVC.view atIndex:0];
            }
            
        }
            break;
        case 1:
        {
            _itemLine.frame = lineTwoRect;
            
            if (_raceVC.view.superview==nil)//判断是否为根视图
            {
                if (_raceVC==nil)//判断视图控制器是否初始化
                {
                    SP_Mark_RaceViewController *raceVC=[[SP_Mark_RaceViewController alloc] init];
                    _raceVC = raceVC;
                }
                [_activeVC.view removeFromSuperview];
                [_groundVC.view removeFromSuperview];
                [self.view insertSubview:_raceVC.view atIndex:0];
            }
        }
            break;
        case 2:
        {
            _itemLine.frame = lineThreeRect;
            
            if (_groundVC.view.superview==nil)//判断是否为根视图
            {
                if (_groundVC==nil)//判断视图控制器是否初始化
                {
                    SP_Mark_GroundViewController *groundVC=[[SP_Mark_GroundViewController alloc] init];
                    _groundVC = groundVC;
                }
                [_raceVC.view removeFromSuperview];
                [_activeVC.view removeFromSuperview];
                [self.view insertSubview:_groundVC.view atIndex:0];
            }
        }
            break;
        default:
            break;
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
