//
//  SP_BoardDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_BoardDetailsViewController.h"
#import "SP_BoardHeadView.h"
#import "SP_BoardListEntity.h"
#import "SP_YhbCmtCell.h"
#import "SP_YhbCmtFrame.h"
#import "SP_YhbCommentEntity.h"
#import "SP_YhbReCmtCell.h"
#import "SP_YhbReCmtEntity.h"
#import "SP_YhbReCmtFrame.h"
#import "SP_YhbReCmtTextView.h"
#import "SP_YhbHfBtn.h"
#import "SP_CommentTextSpecial.h"
#import "SP_SendMsgViewController.h"


@interface SP_BoardDetailsViewController () <UITableViewDataSource,UITableViewDelegate,SP_YhbReCmtTextViewDelegate>
{
    SP_BoardHeadView *_headView;
    UITableView *_tableView;
    NSMutableArray *_cmtArr;
}

@end

@implementation SP_BoardDetailsViewController

- (id)init
{
    self = [super init];
    if(self){
        _cmtArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"战术板详情"];
    [self addBackButton];
    [self setUpTableView];
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [_cmtArr removeAllObjects];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETZSBPLLIST(self.listEntity.zsbid, self.listEntity.teamid) Tag:TAG_HTTP_GETZSBPLLIST];
}



- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if(request.responseString != nil){
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
//        NSLog(@"战术板评论 --- resopnse:%@",request.responseString);

        if(request.tag == TAG_HTTP_GETZSBPLLIST){
            if(HTTP_SUCCESS(code)){
                NSArray *msg = dictionary[@"msg"];
                
                for(NSDictionary *dict in msg){
                    SP_YhbCommentEntity *cmtEntity = [SP_YhbCommentEntity objectWithKeyValues:dict];
                    SP_YhbCmtFrame *cmtFrame = [[SP_YhbCmtFrame alloc]init];
                    cmtFrame.cmtEntity = cmtEntity;
                    
                    for(NSDictionary *reDict in cmtEntity.hfdata){
                        SP_YhbReCmtEntity *reCmtEntity = [SP_YhbReCmtEntity objectWithKeyValues:reDict];
                        SP_YhbReCmtFrame *reCmtFrame = [[SP_YhbReCmtFrame alloc]init];
                        reCmtFrame.reCmtEntity = reCmtEntity;
                        
                        [cmtFrame.reCmtArr addObject:reCmtFrame];
                    }
                    
                    [_cmtArr addObject:cmtFrame];
                    
                }
                [_tableView reloadData];
                
                
            }
        }
    }
}
- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = 0;
    
    _headView = [[SP_BoardHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 122)];
    _headView.entity = self.listEntity;
    _tableView.tableHeaderView = _headView;

    [self.view addSubview:_tableView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _cmtArr.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SP_YhbCmtFrame *cmtFrame = _cmtArr[section];
    
    
    return 1 + cmtFrame.reCmtArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_YhbCmtCell *cell = [SP_YhbCmtCell cellWithTableView:tableView];
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        cell.cmtFrame = frame;
        
        
        frame.index = indexPath;
        
        SP_YhbCommentEntity *entity = frame.cmtEntity;
        
        
        cell.hfButton.yhbplid = entity.usermsgid;
        cell.hfButton.zsbplid = entity.zsbplid;
        cell.hfButton.hfuid = entity.uid;
        
        [cell.hfButton addTarget:self action:@selector(hfBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }else{
        SP_YhbReCmtCell *cell =[SP_YhbReCmtCell cellWithTableView:tableView];
        cell.commentTextView.sdelegate = self;
        
        
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        frame.index = indexPath;
        SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
        cell.frameModel = reFrame;
        
        return cell;
    }
}




- (void) hfBtnDidClick:(SP_YhbHfBtn *)btn
{
    
    if(![GETUSERID isEqualToString:btn.hfuid]){
    
        SP_SendMsgViewController *vc = [[SP_SendMsgViewController alloc]init];
        vc.type = @"4";
        vc.zsbplid = btn.zsbplid;
        vc.hfuid = btn.hfuid;
        vc.teamID = self.listEntity.teamid;
        
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}

/**
 *  点击回复评论的某一个人
 */
- (void) getSpecialBack:(SP_CommentTextSpecial *)special
{
    NSLog(@"%@  %@  %@  %@  %@",special.nickName,special.hfName,special.zsbplid,special.hfuid ,special.uid);
    
    NSString *uid = [[NSString alloc]init];
    if(special.hfuid.length == 0){
        uid = special.uid;
    }else{
        uid = special.hfuid;
    }
    
    if([GETUSERID isEqualToString:uid]){
        return;
    }
    
    
    
    
    SP_SendMsgViewController *vc = [[SP_SendMsgViewController alloc]init];
    vc.type = @"4";
    vc.zsbplid = special.zsbplid;
    vc.teamID = self.listEntity.teamid;
    vc.hfuid = uid;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        return frame.cellHeight;
    }else{
        SP_YhbCmtFrame *frame = _cmtArr[indexPath.section];
        SP_YhbReCmtFrame *reFrame  = frame.reCmtArr[indexPath.row - 1];
        return reFrame.cellHeight;
    }
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
