//
//  SP_MyActivityInvitedViewController.m
//  Sport
//
//  Created by WT_lyy on 15/5/24.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MyActivityInvitedViewController.h"
#import "SP_YhbList.h"
#import "SP_CompanyCell.h"
#import "SP_ActiveDetailsViewController.h"

@interface SP_MyActivityInvitedViewController ()<UITableViewDataSource,UITableViewDelegate>{

    UITableView * _tableView;
    
    UIView * _lineView;
    
    NSMutableArray *_yhbListArr;
    
    int _type;  //  0自己发布的  1参与的
}

@end

@implementation SP_MyActivityInvitedViewController

- (id)init
{
    self = [super init];
    if(self){
        _yhbListArr = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self setNavigationBar:@"我的活动邀请"];
    [self addBackButton];

    [self _initGreenView];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 108, ScreenWidth, ScreenHeight-108)];
    
    [self.view addSubview:_tableView];
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableView.delegate = self;
    
    _tableView.dataSource = self;
    
    _tableView.rowHeight = 180;
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETMYYHBLIST(GETUSERID, @"0", @"10", @"0") Tag:TAG_HTTP_GETMYYHBLIST];
    
}

#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
                NSLog(@"TAG_HTTP_GETMYYHBLIST :%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETMYYHBLIST){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msg = dictionary[@"msg"];
                for(NSDictionary *dict in msg){
                
                    SP_YhbList *model = [SP_YhbList objectWithKeyValues:dict];
                    [_yhbListArr addObject:model];
                }
                
                [_tableView reloadData];
            }
        }
    }
    
}


- (void)_initGreenView{

    UIView * greenView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, ScreenWidth, 44)];
    greenView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [self.view addSubview:greenView];
    
    UIButton * Btnlaunch = GET_BUTTON(CGRectMake(0, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnlaunch setTitle:@"发起的活动" forState:UIControlStateNormal];
    [greenView addSubview:Btnlaunch];
    Btnlaunch.tag = 101;
    
    UIButton * Btnjoin = GET_BUTTON(CGRectMake(ScreenWidth/2, 0, ScreenWidth/2, 44), 14, NO, [UIColor whiteColor]);
    [Btnjoin setTitle:@"参与的活动" forState:UIControlStateNormal];
    [greenView addSubview:Btnjoin];
    Btnjoin.tag = 102;
    
    [Btnlaunch addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    [Btnjoin addTarget:self action:@selector(btnActivitySelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lineView = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth/8, 40, ScreenWidth/4, 4)];
    
    [greenView addSubview:_lineView];
    _lineView.backgroundColor = UIColorFromRGB(0xe27e33);
}




- (void)btnActivitySelClick:(UIButton *)button{

    CGPoint linpoint = _lineView.center;
    
    linpoint.x = button.center.x;
    
    _lineView.center = linpoint;
    
    if(button.tag == 101){
        
        [_yhbListArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETMYYHBLIST(GETUSERID, @"0", @"10", @"0") Tag:TAG_HTTP_GETMYYHBLIST];

        _type = 0;
    }else{
        [_yhbListArr removeAllObjects];
        _type = 1;
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETMYYHBLIST(GETUSERID, @"1", @"10", @"0") Tag:TAG_HTTP_GETMYYHBLIST];

    }
    
}


#pragma -mark UitabeViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SP_ActiveDetailsViewController *detailsVC = [[SP_ActiveDetailsViewController alloc]init];
    
    SP_YhbList *yhbList = _yhbListArr[indexPath.row];
    detailsVC.yhbList = yhbList;
    
    [self.navigationController pushViewController:detailsVC animated:YES];

}

#pragma -mark UitabeViewDatasource

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _yhbListArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SP_CompanyCell *cell = [SP_CompanyCell cellWithTableView:tableView];
    
    
    SP_YhbList *yhbList = _yhbListArr[indexPath.row];
    
    cell.yhbList = yhbList;

    return cell;
}



@end
