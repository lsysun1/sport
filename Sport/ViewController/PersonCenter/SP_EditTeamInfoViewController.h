//
//  SP_EditTeamInfoViewController.h
//  Sport
//
//  Created by 李松玉 on 15/8/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_EditTeamInfoViewController : BaseViewController

@property (nonatomic,copy) NSString *teamName;
@property (nonatomic,copy) NSString *teamID;


@end
