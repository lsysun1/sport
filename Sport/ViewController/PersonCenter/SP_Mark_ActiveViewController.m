//
//  SP_Mark_ActiveViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_Mark_ActiveViewController.h"
#import "SP_CompanyCell.h"
#import "SP_YhbList.h"
#import "SP_ActiveDetailsViewController.h"

@interface SP_Mark_ActiveViewController () <UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_responeBtn;
}
@end

@implementation SP_Mark_ActiveViewController

- (id)init
{
    self = [super init];
    if(self){
        _arr = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    
    
    [self setUpTableView];

    [self getMyCollectListWithType:@"0" Num:@"10" CollectId:@"0"];
}



//uid
//type  0活动  1比赛  2场馆
//num
//collectid  第一次传0




- (void) getMyCollectListWithType:(NSString *)type
                              Num:(NSString *)num
                        CollectId:(NSString *)collectid
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:type forKey:@"type"];
    [dict setObject:num forKey:@"num"];
    [dict setObject:collectid forKey:@"collectid"];
    
    [self setHttpPostData:GETCOLLECTLIST Dictionary:dict Tag:TAG_HTTP_GETCOLLECTLIST];
    
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETCOLLECTLIST){   //创建球队相册
//            NSLog(@"TAG_HTTP_ADDTEAMIMGCATE - resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_YhbList *list = [SP_YhbList objectWithKeyValues:dict];
                    [_arr addObject:list];
                }
                
                if(_arr.count == 10){
                    [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                }
                
                [_tableView reloadData];
                [_tableView.header endRefreshing];
                [_tableView.footer endRefreshing];
                
            }
        }else if (request.tag == TAG_HTTP_RESPONSEYHB){
            if(HTTP_SUCCESS(code)){
                [self showMessage:@"响应成功"];
                _responeBtn.selected = YES;
                
            }else{
                [self showMessage:@"响应失败"];
            }
            
        }
    
    }
}



- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64+38, ScreenWidth, ScreenHeight - 64-38)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = 0;
    _tableView.rowHeight  = 180;
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadNewData)];

    [self.view addSubview:_tableView];
}

- (void) reloadNewData
{
    [_arr removeAllObjects];
    [self getMyCollectListWithType:@"0" Num:@"10" CollectId:@"0"];
}

- (void) loadMoreData
{

    SP_YhbList *entity = [_arr lastObject];
    
    [self getMyCollectListWithType:@"2" Num:@"10" CollectId:entity.yhbid];

}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_CompanyCell *cell = [SP_CompanyCell cellWithTableView:tableView];
    SP_YhbList *model = _arr[indexPath.row];
    cell.yhbList = model;
    cell.inBtn.tag = 100 + indexPath.row;
    
    [cell.inBtn addTarget:self action:@selector(respondsBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_ActiveDetailsViewController *detailsVC = [[SP_ActiveDetailsViewController alloc]init];
    
    SP_YhbList *yhbList = _arr[indexPath.row];
    detailsVC.yhbList = yhbList;
    
    [[AppDelegate sharedAppDelegate].nav pushViewController:detailsVC animated:YES];

}


- (void) respondsBtnDidClick:(UIButton *)btn
{
    _responeBtn = btn;
    if(btn.selected == NO){
        
        SP_YhbList *yhbList = _arr[btn.tag - 100];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:RESPONSEYHB(GETUSERID, yhbList.yhbid) Tag:TAG_HTTP_RESPONSEYHB];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
