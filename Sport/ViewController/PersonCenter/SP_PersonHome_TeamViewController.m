//
//  SP_PersonHome_TeamViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_PersonHome_TeamViewController.h"
#import "SP_UserInfo.h"
#import "SP_TeamCell.h"
#import "SP_CreatTeamViewController.h"
#import "SP_TeamModel.h"
#import "MJExtension.h"
#import "SP_TeamMateModel.h"
#import "SP_UerHomeViewController.h"
#import "SP_TeamInfoViewController.h"
#import "SP_MatchList.h"
#import "SP_RaceDetailsCell.h"
#import "SP_BeforeDetailsViewController.h"
#import "SP_AfterDetailsViewController.h"
#import "fansListViewController.h"

#define TAG_BTN_TEAM        21
#define TAG_BTN_HISTORY     22

#define TAG_TAB_TEAM        12
#define TAG_TAB_HISTORY     13


@interface SP_PersonHome_TeamViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    //顶部的图片View
    UIImageView *_topImgView;
    UIImageView *_headBoardView;
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_foucesLabel;
    UILabel *_fansLabel;
    
    UIView *_teamGreenLine;             //球队和战绩切换条
    UIButton *_teamBtn;                 //球队按钮
    UIButton *_historyBtn;              //战绩按钮
    UIView *_teamBottomLine;            //球队和战绩的切换条
    UITableView *_teamTableView;        //球队TableView
    UITableView *_historyTableView;     //球队战绩TableView
    UIView *_bottomGreenLine;           //底部的切换回个人资料视图的绿色条
    UIButton *_bottomSwitchBtn;         //底部的切换回个人资料视图按钮
    UIButton *_createTeamBtn;           //创建球队按钮
    

    NSMutableArray *_teamsArr;
    NSMutableArray *_historyArr;
    NSMutableArray *_dayArr;
    
    
    UIButton *_foucsBtn;
    UIButton *_fansBtn;


}
@end

@implementation SP_PersonHome_TeamViewController

- (id)init
{
    self = [super init];
    if(self){
        
        _teamsArr = [[NSMutableArray alloc]init];
        _historyArr = [[NSMutableArray alloc]init];
        _dayArr = [[NSMutableArray alloc]init];
    
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self setUpTopView];
    [self setUpTeamMidLine];
    [self setUpTeamTableView];
    [self setUpHistoryTableView];
    [self setUpBottomView];
    [self setUpCreateTeamBtn];
    self.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight * 2);
    [self getTeamTableViewHeight];

//    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETJOINTEAM(GETUSERID) Tag:TAG_HTTP_GETJOINTEAM];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getTeamTableViewHeight];
    
    [self setHTTPRequest:GETJOINTEAM(GETUSERID) Tag:TAG_HTTP_GETJOINTEAM];
    [self setHTTPRequest:GETMYMATCH(GETUSERID, @"1") Tag:TAG_HTTP_GETMYMATCH];

}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETJOINTEAM -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETJOINTEAM){
            if (HTTP_SUCCESS(code))
            {
                [_teamsArr removeAllObjects];
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_TeamModel *team = [SP_TeamModel objectWithKeyValues:dict];
                    if(team.name.length != 0){
                        [_teamsArr addObject:team];
                    }
                }
                [_teamTableView reloadData];
                [self getTeamTableViewHeight];
            }
        }else if (request.tag == TAG_HTTP_GETMYMATCH){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchList *matchList = [SP_MatchList objectWithKeyValues:dict];
                    [_historyArr addObject:matchList];
                    
                }
            }
            NSLog(@"_arr -- %ld",_historyArr.count);
            
            [self chuliData];
            [_historyTableView reloadData];
            [self getTeamTableViewHeight];

            
        
        }
    }
}

#pragma marl - 处理数组,相同的日期放在同一个数组
- (void) chuliData
{
    for (int i = 0; i < _historyArr.count; i ++) {
        
        SP_MatchList *match = _historyArr[i];
        NSString *string = [self dateWithTimeIntervalSince1970:match.date];
        
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        for (int j = 0; j < _historyArr.count; j ++) {
            
            SP_MatchList *match = _historyArr[j];
            NSString *jstring = [self dateWithTimeIntervalSince1970:match.date];
            
            if([string isEqualToString:jstring]){
                [tempArray addObject:match];
            }
        }
        
        [_dayArr addObject:tempArray];
        
        
        for (int i = 0; i < [tempArray count] - 1; i++) {
            [_historyArr removeObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    
}





#pragma mark - 设置顶部View
- (void) setUpTopView
{
    _topImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
    _topImgView.image = [UIImage imageNamed:@"上图.jpg"];
    
    _headBoardView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 40,20 , 80, 80)];
    _headBoardView.layer.borderWidth = 1;
    _headBoardView.layer.cornerRadius = 40;
    if([self.userInfoMsg.sex isEqualToString:@"0"]){
        _headBoardView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    }else{
        _headBoardView.layer.borderColor = UIColorFromRGB(0x599ffff).CGColor;
    }
    
    
    _headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 37.5, 22.5, 75, 75)];
    _headImgView.image = [UIImage imageNamed:@"default_head_img"];
    _headImgView.layer.cornerRadius = 37.5;
    _headImgView.layer.masksToBounds = YES;
    NSURL *imgURL = [NSURL URLWithString:self.userInfoMsg.headimg];
    [_headImgView sd_setImageWithURL:imgURL];
    
    
    
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_headImgView.frame) + 20, 200, 15)];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    if(![self.userInfoMsg.postion isEqualToString:@""]){
        _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",self.userInfoMsg.nickname,self.userInfoMsg.postion];
    }else{
        _nameLabel.text = [NSString stringWithFormat:@"%@",self.userInfoMsg.nickname];
    }
    _nameLabel.font = [UIFont systemFontOfSize:15];
    
    _foucesLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth / 2 -70, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _foucesLabel.textColor = [UIColor whiteColor];
    _foucesLabel.textAlignment = NSTextAlignmentLeft;
    _foucesLabel.text = [NSString stringWithFormat:@"关注:%@",self.userInfoMsg.attentionCount];
    _foucesLabel.font = [UIFont systemFontOfSize:14];
    
    _fansLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth / 2 + 28, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _fansLabel.textColor = [UIColor whiteColor];
    _fansLabel.textAlignment = NSTextAlignmentLeft;
    _fansLabel.text = [NSString stringWithFormat:@"粉丝:%@",self.userInfoMsg.fansCount];
    _fansLabel.font = [UIFont systemFontOfSize:14];
    
    
    _foucsBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 -70, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _foucsBtn.tag = 1111;
    [_foucsBtn addTarget:self action:@selector(fansOrFocusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _fansBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 + 28, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _fansBtn.tag = 2222;
    [_fansBtn addTarget:self action:@selector(fansOrFocusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view  addSubview:_foucsBtn];
    [self.view  addSubview:_fansBtn];

    
    [self.view addSubview:_topImgView];
    [self.view  addSubview:_headBoardView];
    [self.view  addSubview:_headImgView];
    [self.view  addSubview:_nameLabel];
    [self.view  addSubview:_foucesLabel];
    [self.view  addSubview:_fansLabel];
    
}

-(void)fansOrFocusBtnDidClick:(UIButton *)btn
{
    if(btn.tag == 1111){
        NSLog(@"关注");
        fansListViewController *vc = [[fansListViewController alloc]init];
        vc.type = @"1";
        vc.userInfoMsg = self.userInfoMsg;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
    }else if (btn.tag == 2222){
        NSLog(@"粉丝");
        fansListViewController *vc = [[fansListViewController alloc]init];
        vc.type = @"2";
        vc.userInfoMsg = self.userInfoMsg;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}


#pragma mark - 设置球队和战绩中间的绿条
- (void) setUpTeamMidLine
{
    _teamGreenLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topImgView.frame), ScreenWidth, 30)];
    _teamGreenLine.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _teamBtn = [[UIButton alloc]initWithFrame:CGRectMake(70, 0, 60, 30)];
    _teamBtn.tag = TAG_BTN_TEAM;
    _teamBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_teamBtn setTitle:@"球队" forState:UIControlStateNormal];
    [_teamBtn addTarget:self action:@selector(teamOrHistorySwitch:) forControlEvents:UIControlEventTouchUpInside];
    [_teamBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _historyBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 130, 0, 60, 30)];
    _historyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    _historyBtn.tag =  TAG_BTN_HISTORY;
    [_historyBtn setTitle:@"战绩" forState:UIControlStateNormal];
    [_historyBtn addTarget:self action:@selector(teamOrHistorySwitch:) forControlEvents:UIControlEventTouchUpInside];
    [_historyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _teamBottomLine = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_teamBtn.frame) - 10, 26, 80, 4)];
    _teamBottomLine.backgroundColor = UIColorFromRGB(0xffffff);
    
    
    [self.view addSubview:_teamGreenLine];
    [_teamGreenLine addSubview:_teamBtn];           //给绿色切换条添加战队按钮
    [_teamGreenLine addSubview:_historyBtn];        //给绿色切换条添加战队按钮
    [_teamGreenLine addSubview:_teamBottomLine];    //给绿色切换条添加按钮下面的白条

    
}

- (void) teamOrHistorySwitch:(UIButton *)btn
{
    if(btn.tag == TAG_BTN_TEAM){
        //球队
        _teamBottomLine.frame = CGRectMake(CGRectGetMinX(_teamBtn.frame) - 10, 26, 80, 4);
        [_historyTableView removeFromSuperview];
        [self.view addSubview:_teamTableView];
        [self getTeamTableViewHeight];
    }else if (btn.tag == TAG_BTN_HISTORY){
        //战绩
        _teamBottomLine.frame = CGRectMake(CGRectGetMinX(_historyBtn.frame) - 10, 26, 80, 4);
        [_teamTableView removeFromSuperview];
        [self.view addSubview:_historyTableView];
        [self getTeamTableViewHeight];
    }
}


#pragma mark - 设置球队TableView
- (void) setUpTeamTableView
{
    _teamTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_teamGreenLine.frame) + 15, ScreenWidth, 450)];
    _teamTableView.tag = TAG_TAB_TEAM;
    _teamTableView.delegate = self;
    _teamTableView.dataSource = self;
    _teamTableView.separatorStyle = 0;
    _teamTableView.scrollEnabled = NO;
    _teamTableView.rowHeight = 150;
    [self.view addSubview:_teamTableView];
}

#pragma mark - 设置战绩TableView
- (void) setUpHistoryTableView
{
    _historyTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_teamGreenLine.frame) + 15, ScreenWidth, 450)];
    _historyTableView.tag = TAG_TAB_HISTORY;
    _historyTableView.delegate = self;
    _historyTableView.dataSource = self;
    _historyTableView.separatorStyle = 0;
    _historyTableView.scrollEnabled = NO;
    _historyTableView.rowHeight = 80;
}





#pragma mark - 设置战队和战绩底部的切换按钮
- (void) setUpBottomView
{
    _bottomGreenLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_teamGreenLine.frame) + 15 + 550, ScreenWidth, 15)];
    _bottomGreenLine.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _bottomSwitchBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 - 17.25, CGRectGetMaxY(_teamGreenLine.frame) + 15 + 530, 34.5, 30)];
    [_bottomSwitchBtn setBackgroundImage:[UIImage imageNamed:@"球队上拉按钮.png"] forState:UIControlStateNormal];
    [_bottomSwitchBtn addTarget:self action:@selector(bottomSwitchBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_bottomGreenLine];
    [self.view addSubview:_bottomSwitchBtn];
    
}

/**
 *  切换回个人资料视图
 */
- (void) bottomSwitchBtnDidClick:(UIButton *)btn
{
    NSLog(@"123123");
    if(self.delegate && [self.delegate respondsToSelector:@selector(backToProfileView)]){
        [self.delegate backToProfileView];
    }
}



#pragma mark - 设置创建球队按钮
- (void) setUpCreateTeamBtn
{
    _createTeamBtn = [[UIButton alloc]init];
    _createTeamBtn.frame = CGRectMake(ScreenWidth/2 -125, CGRectGetMaxY(_teamGreenLine.frame) + 15 + 480, 250, 40);
    _createTeamBtn.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [_createTeamBtn setTitle:@"创建球队" forState:UIControlStateNormal];
    [_createTeamBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_createTeamBtn addTarget:self action:@selector(createTeamBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_createTeamBtn];
    
}

- (void)createTeamBtnDidClick
{
    
    NSLog(@"123");
    SP_CreatTeamViewController *vc = [[SP_CreatTeamViewController alloc]init];
    vc.userInfoMsg = self.userInfoMsg;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == TAG_TAB_TEAM){
        return 1;
    }else{
        return _dayArr.count;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == TAG_TAB_TEAM){
        return _teamsArr.count;
    }else{
        NSMutableArray *sameDayArr = _dayArr[section];
        return sameDayArr.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == TAG_TAB_TEAM){
        SP_TeamCell *cell = [SP_TeamCell cellWithTableView:tableView];
        
        SP_TeamModel *team = _teamsArr[indexPath.row];
        
        cell.team = team;
        
        
        if(team.playerData.count == 1){
            NSDictionary *leaderOneDict = team.playerData[0];
            
            SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
            
            cell.leaderOneBtn.tag = [leaderOne.uid integerValue];
            
            [cell.leaderOneBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];

        
        }else if (team.playerData.count == 2){
            
            NSDictionary *leaderOneDict = team.playerData[0];
            NSDictionary *leaderTwoDict = team.playerData[1];
            
            SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
            SP_TeamMateModel *leaderTwo = [SP_TeamMateModel objectWithKeyValues:leaderTwoDict];
            
            cell.leaderOneBtn.tag = [leaderOne.uid integerValue];
            cell.leaderTwoBtn.tag = [leaderTwo.uid integerValue];
            
            [cell.leaderOneBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];
            [cell.leaderTwoBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];

        
        }
        return cell;
    }else if (tableView.tag == TAG_TAB_HISTORY){
        SP_RaceDetailsCell *cell = [SP_RaceDetailsCell cellWithTableView:tableView];
        
        NSMutableArray *sameDayArr = _dayArr[indexPath.section];
        
        SP_MatchList *matchList = sameDayArr[indexPath.row];
        cell.matchList = matchList;
        
        
        return cell;

    }
    
    return nil;

}


- (void)pushToUserHomeVC:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == TAG_TAB_TEAM){
        return 150;
    }else{
        return 80;
    }
    
    return 0;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag == TAG_TAB_TEAM){
        SP_TeamModel *team = _teamsArr[indexPath.row];
        SP_TeamInfoViewController *teamVC = [[SP_TeamInfoViewController alloc]init];
        teamVC.teamName = team.name;
        teamVC.teamID = team.teamid;
        [[AppDelegate sharedAppDelegate].nav pushViewController:teamVC animated:YES];
    }else if (tableView.tag == TAG_TAB_HISTORY){
        NSMutableArray *sameDayArr = _dayArr[indexPath.section];
        SP_MatchList *matchList = sameDayArr[indexPath.row];
        
        
        
        if([matchList.ateamscore isEqualToString:@"-"]){
            SP_BeforeDetailsViewController *vc = [[SP_BeforeDetailsViewController alloc]init];
            vc.matchList = matchList;
//            vc.listModel = self.listModel;
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        }else{
            SP_AfterDetailsViewController *vc = [[SP_AfterDetailsViewController alloc]init];
            vc.matchList = matchList;
//            vc.listModel = self.listModel;
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        }
    
    }
    
}



#pragma mark 自定义SectionView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == TAG_TAB_TEAM){
        return 0;
    }
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == TAG_TAB_HISTORY){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
        view.backgroundColor = UIColorFromRGB(0xffffff);
        
        UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth, 1)];
        bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
        [view addSubview:bottomLine];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, CGRectGetHeight(view.bounds))];
        label.textColor = UIColorFromRGB(0x696969);
        label.font = [UIFont systemFontOfSize:15];
        label.text = @"2015-05-16 星期八";
        [view addSubview:label];
        
        
        NSMutableArray *sameDayArr = _dayArr[section];
        SP_MatchList *matchList = sameDayArr[0];
        label.text = matchList.date;
        
        NSString *day = [self dateWithTimeIntervalSince1970:matchList.date];
        
        int timeStamp = [matchList.date intValue];
        NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeStamp];
        NSString *week = [self weekdayStringFromDate:confromTimesp];
        
        NSString *labelText = [NSString stringWithFormat:@"%@ %@",day,week];
        label.text = labelText;
        return view;
    }
    return nil;
}

- (void) getTeamTableViewHeight
{
    //重新计算tableView、创建战队按钮和返回资料按钮的Frame
    _teamTableView.frame = CGRectMake(0, CGRectGetMaxY(_teamGreenLine.frame) + 15, ScreenWidth, 150 * _teamsArr.count);
    _historyTableView.frame = CGRectMake(0, CGRectGetMaxY(_teamGreenLine.frame) + 15, ScreenWidth, 120 * _historyArr.count);

    if(_teamTableView.superview != nil){
        _createTeamBtn.frame = CGRectMake(ScreenWidth/2 -125, CGRectGetMaxY(_teamTableView.frame) + 50, 250, 40);
        _createTeamBtn.hidden = NO;
    }else{
        _createTeamBtn.frame = CGRectMake(ScreenWidth/2 -125, CGRectGetMaxY(_historyTableView.frame) + 50, 250, 40);
        _createTeamBtn.hidden = YES;
    }
    
    _bottomGreenLine.frame = CGRectMake(0, CGRectGetMaxY(_createTeamBtn.frame) + 40, ScreenWidth, 15);
    _bottomSwitchBtn.frame = CGRectMake(ScreenWidth / 2 - 17.25, CGRectGetMaxY(_bottomGreenLine.frame) - 30, 34.5, 30);
    self.view.frame = CGRectMake(0, 0, ScreenWidth, CGRectGetMaxY(_bottomGreenLine.frame) + 64 * 2);
    
    //                CGFloat scrollHeight = CGRectGetMaxY(_teamGreenLine.frame) + 15 + _teamsArr.count * 150 + 200;
    CGFloat scrollHeight = CGRectGetMaxY(_bottomGreenLine.frame) + 64;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(getScrollHeightToHomeView:)]){
        [self.delegate getScrollHeightToHomeView:scrollHeight];
    }

}



/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (NSString*)weekdayStringFromDate:(NSDate*)inputDate
{
    
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
