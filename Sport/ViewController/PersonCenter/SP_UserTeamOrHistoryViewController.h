//
//  SP_UserTeamOrHistoryViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/29.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"



@class SP_UserInfo;

@interface SP_UserTeamOrHistoryViewController : BaseViewController

@property (nonatomic,strong) SP_UserInfo *userInfoMsg;
@property (nonatomic,strong) NSString *touid;


@end


