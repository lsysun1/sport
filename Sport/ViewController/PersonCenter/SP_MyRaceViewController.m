//
//  SP_MyRaceViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_MyRaceViewController.h"
#import "SP_RaceDetailsCell.h"
#import "SP_MatchList.h"
#import "SP_MatchLeagueList.h"
#import "SP_AfterDetailsViewController.h"
#import "SP_BeforeDetailsViewController.h"

@interface SP_MyRaceViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_arr;
    NSMutableArray *_dayArr;
    
    
}
@end

@implementation SP_MyRaceViewController


- (id) init
{
    self = [super init];
    if(self){
        
        _arr = [[NSMutableArray alloc]init];    // 所有比赛的数组
        _dayArr = [[NSMutableArray alloc]init]; // 比赛有多少组(不同的日期),里面再装数组(同一天比赛的微博)
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"我的比赛"];
    [self addBackButton];
    
    [self setUpTableView];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETMYMATCH(GETUSERID, @"0") Tag:TAG_HTTP_GETMYMATCH];
    
    
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
//        NSLog(@"MATCHLIST --- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETMYMATCH){
            if (HTTP_SUCCESS(code))
            {
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_MatchList *matchList = [SP_MatchList objectWithKeyValues:dict];
                    [_arr addObject:matchList];
                    
                }
            }
            NSLog(@"_arr -- %ld",_arr.count);
            
            [self chuliData];
            [_tableView reloadData];
            [_tableView.header endRefreshing];
        }
    }
}


#pragma marl - 处理数组,相同的日期放在同一个数组
- (void) chuliData
{
    for (int i = 0; i < _arr.count; i ++) {
        
        SP_MatchList *match = _arr[i];
        NSString *string = [self dateWithTimeIntervalSince1970:match.date];
        
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        for (int j = 0; j < _arr.count; j ++) {
            
            SP_MatchList *match = _arr[j];
            NSString *jstring = [self dateWithTimeIntervalSince1970:match.date];
            
            if([string isEqualToString:jstring]){
                [tempArray addObject:match];
            }
        }
        
        [_dayArr addObject:tempArray];
        
        
        for (int i = 0; i < [tempArray count] - 1; i++) {
            [_arr removeObject:[tempArray objectAtIndex:i]];
        }
    }
    
    
    
}






#pragma mark - SetUpRaceTableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.tag = 1;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 80;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(locdNewData)];
    
    
    [self.view addSubview:_tableView];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"_dayArr.count -- %ld",_dayArr.count);
    return _dayArr.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *sameDayArr = _dayArr[section];
    return sameDayArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_RaceDetailsCell *cell = [SP_RaceDetailsCell cellWithTableView:tableView];
    
    NSMutableArray *sameDayArr = _dayArr[indexPath.section];
    
    SP_MatchList *matchList = sameDayArr[indexPath.row];
    cell.matchList = matchList;
    
    
    return cell;
    
}

#pragma mark 自定义SectionView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    view.backgroundColor = UIColorFromRGB(0xffffff);
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 39, ScreenWidth, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [view addSubview:bottomLine];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 200, CGRectGetHeight(view.bounds))];
    label.textColor = UIColorFromRGB(0x696969);
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"2015-05-16 星期八";
    [view addSubview:label];
    
    
    NSMutableArray *sameDayArr = _dayArr[section];
    SP_MatchList *matchList = sameDayArr[0];
    label.text = matchList.date;
    
    NSString *day = [self dateWithTimeIntervalSince1970:matchList.date];
    
    int timeStamp = [matchList.date intValue];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    NSString *week = [self weekdayStringFromDate:confromTimesp];
    
    NSString *labelText = [NSString stringWithFormat:@"%@ %@",day,week];
    label.text = labelText;
    return view;
}




- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableArray *sameDayArr = _dayArr[indexPath.section];
    SP_MatchList *matchList = sameDayArr[indexPath.row];
    
    
    
    if([matchList.ateamscore isEqualToString:@"-"]){
        SP_BeforeDetailsViewController *vc = [[SP_BeforeDetailsViewController alloc]init];
        vc.matchList = matchList;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }else{
        SP_AfterDetailsViewController *vc = [[SP_AfterDetailsViewController alloc]init];
        vc.matchList = matchList;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}



#pragma mark 下拉重新加载数据
- (void) locdNewData
{
    [_arr removeAllObjects];
    [_dayArr removeAllObjects];

    
    
    
}


/**
 *  时间戳转时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (NSString*)weekdayStringFromDate:(NSDate*)inputDate
{
    
    NSArray *weekdays = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSTimeZone *timeZone = [[NSTimeZone alloc] initWithName:@"Asia/Shanghai"];
    
    [calendar setTimeZone: timeZone];
    
    NSCalendarUnit calendarUnit = NSWeekdayCalendarUnit;
    
    NSDateComponents *theComponents = [calendar components:calendarUnit fromDate:inputDate];
    
    return [weekdays objectAtIndex:theComponents.weekday];
    
}

@end
