//
//  SP_AboutViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AboutViewController.h"
#import "SP_GroundMsgViewController.h"
#import "SP_IntegrationViewController.h"
#import "RIButtonItem.h"
#import "UIAlertView+Blocks.h"


@interface SP_AboutViewController ()

@property (nonatomic,strong) UIImageView *topBgView;
@property (nonatomic,strong) UIImageView *topIconView;




@end


CGFloat orignY;

@implementation SP_AboutViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"关于运动GO"];
    [self addBackButton];
    self.view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    [self setUpTopView];
    [self setUpMenuView];
    
    
    

}


- (void) setUpTopView
{
    self.topBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 150)];
    self.topBgView.image = [UIImage imageNamed:@"赛事详情-背景图.jpg"];
    [self.view addSubview:self.topBgView];
    
    self.topIconView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 30, 64 + 150/2 - 30, 60, 60)];
    self.topIconView.image = [UIImage imageNamed:@"图标"];
    [self.view addSubview:self.topIconView];

}



-(void) setUpMenuView
{
    NSArray *title = @[@"帮助与反馈",@"积分规则",@"退出"];

    orignY = 64 + 150;
    for(int i = 0 ;i <title.count ; i++){
        NSString *name = title[i];
        
        [self createCellWithName:name btnTag:i];
        orignY += 45;
    }
}

- (void)createCellWithName:(NSString *)name btnTag:(NSInteger) tag
{
    UIView *cellBgView = [[UIView alloc]initWithFrame:CGRectMake(0, orignY, ScreenWidth, 45)];

    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 45)];
    nameLabel.font = [UIFont systemFontOfSize:14];
    nameLabel.text = name;
    
    [cellBgView addSubview:nameLabel];
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, 44, ScreenWidth, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [cellBgView addSubview:bottomLine];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 45)];
    btn.tag = tag;
    [btn addTarget:self action:@selector(btnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cellBgView addSubview:btn];

    [self.view addSubview:cellBgView];
    

}



- (void) btnDidClick:(UIButton *)btn
{
    NSLog(@"123");
    
    switch (btn.tag) {
        case 0:
        {
            SP_GroundMsgViewController *msgVC = [[SP_GroundMsgViewController alloc]init];
            msgVC.isFadeBack = YES;
            [[AppDelegate sharedAppDelegate].nav presentViewController:msgVC animated:YES completion:nil];
        }
            break;
        case 1:
        {
            SP_IntegrationViewController *vc = [[SP_IntegrationViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
        }
            break;
        case 2:{
            RIButtonItem *makeSureItem = [RIButtonItem itemWithLabel:@"确定" action:^{
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERID];
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERTEL];
                [[AppDelegate sharedAppDelegate].nav popToRootViewControllerAnimated:YES];
            }];
            RIButtonItem *cancleItem= [RIButtonItem itemWithLabel:@"取消" action:^{
            }];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"确定退出账号？" cancelButtonItem:cancleItem otherButtonItems:makeSureItem,nil];
            [alertView show];

        }
            break;
        default:
            break;
    }

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
