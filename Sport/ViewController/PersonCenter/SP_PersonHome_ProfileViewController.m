//
//  SP_PersonHome_ProfileViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_PersonHome_ProfileViewController.h"
#import "SP_UserInfo.h"
#import "SP_ProfileOne_Cell.h"
#import "SP_ProfileTwoCell.h"
#import "SP_ProfileThreeCell.h"
#import "SP_MyActivityInvitedViewController.h"
#import "SP_MyMessageViewController.h"
#import "SP_IntegrationViewController.h"
#import "SP_FocusTeamViewController.h"
#import "SP_MyMarkViewController.h"
#import "SP_AddFriendViewController.h"
#import "SP_IntegrationViewController.h"
#import "SP_MyRaceViewController.h"
#import "SP_QuitCell.h"
#import "SP_AboutViewController.h"
#import "SP_TagModel.h"

#import "fansListViewController.h"

#define TAG_TAB_PROFILE     11


@interface SP_PersonHome_ProfileViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    //顶部的图片View
    UIImageView *_topImgView;
    UIImageView *_headBoardView;
    UIImageView *_headImgView;
    UILabel *_nameLabel;
    UILabel *_foucesLabel;
    UILabel *_fansLabel;
    UIView *_topGreenLine;
    UIButton *_topSwitchBtn;


    //个人资料的tableView
    UITableView *_profileTableView;
    NSArray *_imgArr;
    NSArray *_titleArr;
    NSArray *_arrowArr;
    
    SP_UserInfo *_userMsgInfo;
    
    NSMutableArray *_signArr;
    
    
    UIButton *_foucsBtn;
    UIButton *_fansBtn;

    
    
}
@end

@implementation SP_PersonHome_ProfileViewController


- (id)init
{
    self = [super init];
    if(self){
        _signArr = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];


    
    [self setUpTopView];
    [self setUpProfileTableView];
    [self setUpMidLine];

    [self setHTTPRequest:GETUSERDATA(GETUSERID, GETUSERID) Tag:TAG_HTTP_GETUSERDATA];
    [self setHTTPRequest:GETUSERTAG(GETUSERID) Tag:TAG_HTTP_GETUSERTAG];


    

}





#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
//        NSLog(@"GETUSERDATA -- 个人资料resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETUSERDATA){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msgDict = dictionary[@"msg"];
                SP_UserInfo *info = [SP_UserInfo objectWithKeyValues:msgDict];
                _userMsgInfo = info;
                [self setUpData];
                [_profileTableView reloadData];
            }
        }else if(request.tag == TAG_HTTP_GETUSERTAG){
            if(HTTP_SUCCESS(code)){
                
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                    SP_TagModel *model = [SP_TagModel objectWithKeyValues:dict];
                    [_signArr addObject:model];
                }
                
                [_profileTableView reloadData];
            }
        }
    }
    
}



- (void) setUpData
{
    if([_userMsgInfo.sex isEqualToString:@"0"]){
        _headBoardView.layer.borderColor = UIColorFromRGB(0xe84c3d).CGColor;
    }else{
        _headBoardView.layer.borderColor = UIColorFromRGB(0x599ffff).CGColor;
    }

    NSURL *imgURL = [NSURL URLWithString:_userMsgInfo.headimg];
    [_headImgView sd_setImageWithURL:imgURL];


    if(![_userMsgInfo.postion isEqualToString:@""]){
        _nameLabel.text = [NSString stringWithFormat:@"%@(%@)",_userMsgInfo.nickname,_userMsgInfo.postion];
    }else{
        _nameLabel.text = [NSString stringWithFormat:@"%@",_userMsgInfo.nickname];
    }

    _fansLabel.text = [NSString stringWithFormat:@"粉丝:%@",_userMsgInfo.fansCount];
    _foucesLabel.text = [NSString stringWithFormat:@"关注:%@",_userMsgInfo.attentionCount];

}



#pragma mark - 设置顶部View
- (void) setUpTopView
{
    
    self.view.frame=  CGRectMake(0, 0, ScreenWidth, ScreenHeight * 2);
    _topImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 200)];
    _topImgView.image = [UIImage imageNamed:@"上图.jpg"];
    
    _headBoardView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 40,20 , 80, 80)];
    _headBoardView.layer.borderWidth = 1;
    _headBoardView.layer.cornerRadius = 40;
    
    
    _headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 37.5, 22.5, 75, 75)];
    _headImgView.image = [UIImage imageNamed:@"default_head_img"];
    _headImgView.layer.cornerRadius = 37.5;
    _headImgView.layer.masksToBounds = YES;
    
    
    
    
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2 - 100, CGRectGetMaxY(_headImgView.frame) + 20, 200, 15)];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.font = [UIFont systemFontOfSize:15];
    
    _foucesLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth / 2 -70, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _foucesLabel.textColor = [UIColor whiteColor];
    _foucesLabel.textAlignment = NSTextAlignmentLeft;
    _foucesLabel.text = [NSString stringWithFormat:@"关注:0"];
    _foucesLabel.font = [UIFont systemFontOfSize:14];
    
    
    
    _fansLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth / 2 + 28, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _fansLabel.textColor = [UIColor whiteColor];
    _fansLabel.textAlignment = NSTextAlignmentLeft;
    _fansLabel.font = [UIFont systemFontOfSize:14];
    
    
    
    _foucsBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 -70, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _foucsBtn.tag = 1111;
    [_foucsBtn addTarget:self action:@selector(fansOrFocusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    _fansBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 + 28, CGRectGetMaxY(_nameLabel.frame) + 15, 50, 14)];
    _fansBtn.tag = 2222;
    [_fansBtn addTarget:self action:@selector(fansOrFocusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.view  addSubview:_foucsBtn];
    [self.view  addSubview:_fansBtn];

    
    [self.view  addSubview:_topImgView];
    [self.view  addSubview:_headBoardView];
    [self.view  addSubview:_headImgView];
    [self.view  addSubview:_nameLabel];
    [self.view  addSubview:_foucesLabel];
    [self.view  addSubview:_fansLabel];

}


-(void)fansOrFocusBtnDidClick:(UIButton *)btn
{
    if(btn.tag == 1111){
        NSLog(@"关注");
        fansListViewController *vc = [[fansListViewController alloc]init];
        vc.type = @"1";
        vc.userInfoMsg = _userMsgInfo;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        
    }else if (btn.tag == 2222){
        NSLog(@"粉丝");
        fansListViewController *vc = [[fansListViewController alloc]init];
        vc.type = @"2";
        vc.userInfoMsg = _userMsgInfo;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}

#pragma mark - 设置中间绿条以及按钮
- (void) setUpMidLine
{
    _topGreenLine = [[UIView alloc]initWithFrame:CGRectMake(0, 200, ScreenWidth, 15)];
    _topGreenLine.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    _topSwitchBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth / 2 - 17.5, CGRectGetMaxY(_topGreenLine.frame) - 32, 35, 43)];
    [_topSwitchBtn setBackgroundImage:[UIImage imageNamed:@"person_teanm"] forState:UIControlStateNormal];
    [_topSwitchBtn addTarget:self action:@selector(topSwitchBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_topGreenLine];
    [self.view addSubview:_topSwitchBtn];
    
}


#pragma mark - 设置个人菜单TableView
- (void)setUpProfileTableView
{
    _profileTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topImgView.frame) + 15, ScreenWidth, 525 - 50)];
    _profileTableView.tag = TAG_TAB_PROFILE;
    _profileTableView.delegate = self;
    _profileTableView.dataSource = self;
    _profileTableView.separatorStyle = 0;
    _profileTableView.scrollEnabled = NO;
    
    _imgArr = @[@"",@"",@"我关注的球队图标.png",@"icon_invite.png",@"icon_message.png",@"icon_collect.png",@"积分图标.png",@"关于图标.png"];
    
    _titleArr = @[@"",@"",@"我关注的球队",@"我的活动邀请",@"我的消息",@"我的收藏",@"我的积分",@"关于运动GO"];
    _arrowArr = @[@"",@"",@"arrow_attenteam.png",@"arro_invite.png",@"arrow_meaasge.png",@"arrow_collect.png",@"arrow_collect.png",@"arrow_collect.png"];
    
    [self.view addSubview:_profileTableView];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        SP_ProfileOne_Cell *cell = [SP_ProfileOne_Cell cellWithTableView:tableView];
        cell.signArr = _signArr;
        cell.userInfoMsg = _userMsgInfo;
        return cell;
    }else if (indexPath.row == 1){
        SP_ProfileTwoCell *cell = [SP_ProfileTwoCell cellWithTableView:tableView];
        return cell;
    }else if(indexPath.row > 1 && indexPath.row < 8){
        SP_ProfileThreeCell *cell = [SP_ProfileThreeCell cellWithTableView:tableView];
        
        NSString *imgStr = _imgArr[indexPath.row];
        NSString *titleStr = _titleArr[indexPath.row];
        NSString *arrowStr = _arrowArr[indexPath.row];
        
        cell.imgView.image = [UIImage imageNamed:imgStr];
        cell.titleLabel.text = titleStr;
        cell.arrowImg.image = [UIImage imageNamed:arrowStr];
        if(indexPath.row == 5){
            cell.desLabel.hidden = NO;
        }
        return cell;
    }else{
        SP_QuitCell *cell = [SP_QuitCell cellWithTableView:tableView];
        return cell;
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 130;
    }else{
        return 50;
    }

    return 0;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:
        {
                            NSLog(@"我的比赛");

            SP_MyRaceViewController *vc = [[SP_MyRaceViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        }
            break;
        case 2:{
                            NSLog(@"我关注的球队");
            SP_FocusTeamViewController *VC = [[SP_FocusTeamViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:VC animated:YES];
        }
            break;
        case 3:{
                            NSLog(@"我的活动邀请");
            SP_MyActivityInvitedViewController *VC = [[SP_MyActivityInvitedViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:VC animated:YES];
        }
            break;
        case 4:{
                            NSLog(@"我的消息");
            SP_MyMessageViewController *VC = [[SP_MyMessageViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:VC animated:YES];
        }
            break;
        case 5:{
            NSLog(@"我的收藏");
            SP_MyMarkViewController *VC = [[SP_MyMarkViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:VC animated:YES];
        }
            break;
        case 6:{
                            NSLog(@"我的积分");
            SP_IntegrationViewController *VC = [[SP_IntegrationViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:VC animated:YES];
        }
            break;
        case 7:{
            NSLog(@"关于运动GO");
            SP_AboutViewController *vc = [[SP_AboutViewController alloc]init];
            [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
        }
            break;
        case 8:{
            NSLog(@"退出登录");
//            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:USERID];
//            [[AppDelegate sharedAppDelegate].nav popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
    
    
}


- (void) setUserInfoMsg:(SP_UserInfo *)userInfoMsg
{
    _userInfoMsg = userInfoMsg;
}



- (void) topSwitchBtnDidClick
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(bottomBtnDidClick)]){
        [self.delegate bottomBtnDidClick];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
