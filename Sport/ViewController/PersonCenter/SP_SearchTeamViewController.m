//
//  SP_SearchTeamViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_SearchTeamViewController.h"
#import "SP_TeamCell.h"
#import "SP_TeamModel.h"
#import "SP_TeamMateModel.h"
#import "SP_UerHomeViewController.h"
#import "SP_TeamInfoViewController.h"
#import "addteamfoucsCell.h"
#import "SP_TeamInfoViewController.h"
#import "AddfriendReusableView.h"
#import "AddfriendCollectionCell.h"
#import "SP_FriendModel.h"
#import "SP_UserInfo.h"
#import "AddCollectionFooterView.h"
#import "SP_UerHomeViewController.h"


#define TEAMCOLLECTIONVIEW 10
#define COLLECTIONVIEW     20

@interface SP_SearchTeamViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITextField * _textfInput;
    UITableView *_tableView;
    NSMutableArray *_arr;
    UIButton *_currentBtn;
    
    
    UICollectionView * _collectView;
    UICollectionView * _userCollectionView;
    NSDictionary *_teamDict;
    NSMutableArray *_userAttentionArr;
    
    
    NSMutableArray * _firIntroArr;
    
    NSMutableArray * _secIntroARR;
    
    UIButton *_currentFocusBtn;

}
@end

@implementation SP_SearchTeamViewController


- (id)init
{
    self = [super init];
    if(self){
        _arr = [NSMutableArray array];
        
        _firIntroArr = [[NSMutableArray alloc] init];
        
        _secIntroARR = [[NSMutableArray alloc] init];

    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"关注球队"];
    [self addBackButton];
    [self _initSearchView];
    [self setUpTableView];
    // Do any additional setup after loading the view from its nib.
    
    
    
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setItemSize:CGSizeMake(ScreenWidth/2-10, 50)];//设置cell的尺寸
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//设置其布局方向
    flowLayout.sectionInset = UIEdgeInsetsMake(8, 0, 0, 5);//设置其边界
    _collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, [self viewTop]+54, ScreenWidth, ScreenHeight-[self viewTop]-54) collectionViewLayout:flowLayout];
    UINib* nib = [UINib nibWithNibName: @"AddfriendCollectionCell" bundle: nil];
    
    [_collectView registerNib:nib forCellWithReuseIdentifier:@"AddfriendCollectionCell"];
    
    [self.view addSubview:_collectView];
    [_collectView registerNib:[UINib nibWithNibName:@"AddfriendReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"AddfriendReusableView"];
    [_collectView registerNib:[UINib nibWithNibName:@"AddCollectionFooterView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"AddCollectionFooterView"];
    
    _collectView.tag = COLLECTIONVIEW;
    _collectView.dataSource = self;
    _collectView.delegate = self;
    
    _collectView.backgroundColor = [UIColor whiteColor];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETRECTEAM(GETUSERID) Tag:TAG_HTTP_GETRECTEAM];
    [self setHTTPRequest:GETRECTEAM(GETUSERID) Tag:TAG_HTTP_GETSAMETEAM];


}


- (void)_initSearchView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, [self viewTop]-1, ScreenWidth+2, 55)];
    
    [self.view addSubview:view];
    
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView * inputView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-20*2, 34)];
    [view addSubview:inputView];
    inputView.backgroundColor = [UIColor whiteColor];
    inputView.layer.borderWidth = 1;
    
    inputView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    inputView.layer.cornerRadius = 2;
    
    _textfInput = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, inputView.frame.size.width-60, 24)];
    [inputView addSubview:_textfInput];
    _textfInput.delegate = self;
    _textfInput.placeholder = @"请输入球队名称";
    _textfInput.returnKeyType = UIReturnKeySearch;
    
    UIView * viewVerline = [[UIView alloc] initWithFrame:CGRectMake(inputView.frame.size.width - 50, 5, 1, 24)];
    [inputView addSubview:viewVerline];
    viewVerline.backgroundColor = UIColorFromRGB(0xe5e5e5);
    
    UIButton * btnSearch = GET_BUTTON(CGRectMake(inputView.frame.size.width-50, 0, 40, 34), 14, NO, nil);
    [inputView addSubview:btnSearch];
    [btnSearch setImage:[UIImage imageNamed:@"person_search"] forState:UIControlStateNormal];
    [btnSearch setImageEdgeInsets:UIEdgeInsetsMake(7, 10, 7, 10)];
    [btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
}



#pragma -mark Btn Action
- (void)btnSearchClick{
    
    NSString *name = [self urlEncodeValue:_textfInput.text];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:SEARCHTEAM(GETUSERID,name) Tag:9999];
}

#pragma -mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    [_collectView removeFromSuperview];
    
    //直接请求接口
    [self btnSearchClick];
    
    return YES;
}

#pragma mark - 初始化比赛列表TableView
- (void) setUpTableView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, [self viewTop]-1 + 55, ScreenWidth, ScreenHeight -[self viewTop] + 1 - 55)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 150;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_tableView];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == 9999){
                        NSLog(@"res -- %@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                [_arr removeAllObjects];
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_TeamModel *team = [SP_TeamModel objectWithKeyValues:dict];
                    [_arr addObject:team];
                }
                [_tableView reloadData];
            }
            
            [_tableView reloadData];
            
        }else if (request.tag == TAG_HTTP_DISTEAMATTENTION){
            if (HTTP_SUCCESS(code))
            {
                [self showAlertStr:@"取消关注成功"];
                NSString *name = [self urlEncodeValue:_textfInput.text];
                
                _currentFocusBtn.selected = NO;
                [self asyshowHUDView:WAITTING CurrentView:self];
                [self setHTTPRequest:SEARCHTEAM(GETUSERID,name) Tag:9999];
            }else{
                [self showAlertStr:@"取消关注失败"];

            }
            
        }else if (request.tag == TAG_HTTP_TEAMATTENTION){
            if (HTTP_SUCCESS(code))
            {
                [self showAlertStr:@"关注成功"];
                _currentFocusBtn.selected = YES;
                NSString *name = [self urlEncodeValue:_textfInput.text];
                
                
                [self asyshowHUDView:WAITTING CurrentView:self];
                [self setHTTPRequest:SEARCHTEAM(GETUSERID,name) Tag:9999];

            }else{
                [self showAlertStr:@"关注失败"];
                
            }
            
        }else if (request.tag == TAG_HTTP_GETRECTEAM){
            NSLog(@"TAG_HTTP_GETRECTEAM -- %@",request.responseString);

            if (HTTP_SUCCESS(code))
            {
                NSLog(@"TAG_HTTP_GETSAMEIDOLFRIEND - resopnse:%@",request.responseString);
                
                NSArray *msgarr = dictionary[@"msg"];

                if(msgarr.count != 0){
                    
                    [_firIntroArr removeAllObjects];
                    for(NSDictionary *dict in msgarr){
                        SP_TeamModel *model = [SP_TeamModel objectWithKeyValues:dict];
                        [_firIntroArr addObject:model];
                        
                        
                    }
                    
                    //                NSLog(@"_firIntroArr.count -- %ld",_firIntroArr.count);
                    NSIndexSet *sec = [[NSIndexSet alloc]initWithIndex:1];
                    [_collectView reloadSections:sec];

                
            }

        
        }else if (request.tag == TAG_HTTP_GETSAMETEAM){
            NSLog(@"TAG_HTTP_GETRECTEAM -- %@",request.responseString);
            
            if (HTTP_SUCCESS(code))
            {
                NSLog(@"TAG_HTTP_GETRECFRIEND - resopnse:%@",request.responseString);
                
                NSArray *msgarr = dictionary[@"msg"];
                if (HTTP_SUCCESS(code))
                {
                    [_secIntroARR removeAllObjects];
                    for(NSDictionary *dict in msgarr){
                        SP_TeamModel *model = [SP_TeamModel objectWithKeyValues:dict];
                        [_secIntroARR addObject:model];
                        
                    }
                    
                    
                    NSIndexSet *sec = [[NSIndexSet alloc]initWithIndex:0];
                    [_collectView reloadSections:sec];

                
            }
            }
        }
        }
    }
}
#pragma mark - TableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    addteamfoucsCell *cell = [addteamfoucsCell cellWithTableView:tableView];
    
    
    SP_TeamModel *team = _arr[indexPath.row];
    
    cell.team = team;
    
    cell.foucsbtn.tag = [team.teamid integerValue];
    
    if(cell.foucsbtn.selected == YES){
        [cell.foucsbtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.foucsbtn addTarget:self action:@selector(add:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    return cell;


}


- (void)cancel:(UIButton *)btn
{
    _currentBtn = btn;
    NSString *teamid = [NSString stringWithFormat:@"%ld",btn.tag];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:DISTEAMATTENTION(GETUSERID, teamid) Tag:TAG_HTTP_DISTEAMATTENTION];

}

- (void)add:(UIButton *)btn
{
    _currentBtn = btn;
    NSString *teamid = [NSString stringWithFormat:@"%ld",btn.tag];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:TEAMATTENTION(GETUSERID, teamid) Tag:TAG_HTTP_TEAMATTENTION];

}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_TeamModel *team = _arr[indexPath.row];

    SP_TeamInfoViewController *vc = [[SP_TeamInfoViewController alloc]init];
    vc.teamID = team.teamid;
    vc.teamName = team.name;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}




#pragma -mark CollectView datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView.tag == COLLECTIONVIEW){
        if (1 == section) {
            
            return _firIntroArr.count < 6 ? _firIntroArr.count:6;
            
        }else if (0 == section){
            
            return _secIntroARR.count < 6 ? _secIntroARR.count:6;
        }
    }
    
    return 0;
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if(collectionView.tag == COLLECTIONVIEW){
        return 2;
    }
    return 0;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    static NSString * myidentify  = @"AddfriendCollectionCell";
    
    AddfriendCollectionCell *cell = nil;
    
    cell = (AddfriendCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
    [cell.focusBtn addTarget:self action:@selector(foucsFrinedBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.headbtn addTarget:self action:@selector(headbtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if(collectionView.tag == COLLECTIONVIEW){
        if(indexPath.section == 0){
            if(_secIntroARR.count != 0){
                //                NSLog(@"_secIntroARR.count == %ld",_secIntroARR.count);
                SP_TeamModel *model = _secIntroARR[indexPath.row];
                cell.teammodel = model;
                cell.focusBtn.tag = 100 + [cell.teammodel.teamid intValue];
                cell.headbtn.tag = 200 + [cell.teammodel.teamid intValue];
                
            }
        }
        else if(indexPath.section == 1)
        {
            if(_firIntroArr.count != 0){
                //                NSLog(@"_firIntroArr == %ld",_secIntroARR.count);
                SP_TeamModel *model = _firIntroArr[indexPath.row];
                cell.teammodel = model;
                cell.focusBtn.tag = 100 + [cell.teammodel.teamid intValue];
                cell.headbtn.tag = 200 + [cell.teammodel.teamid intValue];
                
            }
            
        }
    }
    
    //  cell.backgroundColor = [UIColor grayColor];
    return cell;
}


- (void) headbtnDidClick:(UIButton *)btn
{
    
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 200];
    SP_TeamInfoViewController *vc = [[SP_TeamInfoViewController alloc]init];
    vc.teamID = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}




- (void) foucsFrinedBtnDidClick:(UIButton *)btn
{
    _currentFocusBtn = btn;
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag - 100];
    if(btn.selected == NO){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:TEAMATTENTION(GETUSERID,touid) Tag:TAG_HTTP_TEAMATTENTION];
    }else if (btn.selected == YES){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:DISTEAMATTENTION(GETUSERID,touid) Tag:TAG_HTTP_DISTEAMATTENTION];
        
    }
}


//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size={ScreenWidth,35};
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    
    if(collectionView.tag == COLLECTIONVIEW){
        if(section == 0){
            if(_secIntroARR.count == 6){
                CGSize size={ScreenWidth,60};
                return size;
            }else{
                CGSize size={0,0};
                return size;
            }
        }else if(section == 1){
            if(_firIntroArr.count == 6){
                CGSize size={ScreenWidth,60};
                return size;
            }else{
                CGSize size={0,0};
                return size;
                
            }
        }
    }
    CGSize size={ScreenWidth,60};
    return size;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    NSString *reuseIdentifier;
    if ([kind isEqualToString: UICollectionElementKindSectionFooter ]){
        reuseIdentifier = @"AddCollectionFooterView";
    }else{
        reuseIdentifier = @"AddfriendReusableView";
    }
    
    AddfriendReusableView *view =  (AddfriendReusableView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
    
    
    if(collectionView.tag == COLLECTIONVIEW){
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
            
            UILabel *label = (UILabel *)[view viewWithTag:1];
            if (0 == indexPath.section) {
                label.text = [NSString stringWithFormat:@"同校球队推荐"];
            }else{
                label.text = [NSString stringWithFormat:@"随便看看"];
            }
            return view;
            
        }
        else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
            AddCollectionFooterView *footView = (AddCollectionFooterView *)[collectionView dequeueReusableSupplementaryViewOfKind :kind   withReuseIdentifier:reuseIdentifier   forIndexPath:indexPath];
            
            if(indexPath.section == 0){
                [footView.changeGroupBtn addTarget:self action:@selector(schoolFriendChange:) forControlEvents:UIControlEventTouchUpInside];
            }else if (indexPath.section == 1){
                [footView.changeGroupBtn addTarget:self action:@selector(idolFrinedChange:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            return footView;
            
        }
    }
    
    return view;
}


- (void) schoolFriendChange:(UIButton *)btn
{
    //    NSLog(@"同学");
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETRECFRIEND(GETUSERID) Tag:TAG_HTTP_GETRECFRIEND];
    
    
}

- (void) idolFrinedChange:(UIButton *)btn
{
    //    NSLog(@"偶像");
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETSAMEIDOLFRIEND(GETUSERID) Tag:TAG_HTTP_GETSAMEIDOLFRIEND];
}

- (void)userFriendChange:(UIButton *)btn
{
    NSLog(@"用户关注的好友");
}


@end
