//
//  SP_FocusTeamViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_FocusTeamViewController.h"
#import "SP_TeamCell.h"
#import "SP_TeamModel.h"
#import "SP_TeamMateModel.h"
#import "SP_UerHomeViewController.h"
#import "SP_TeamInfoViewController.h"
#import "SP_SearchTeamViewController.h"



@interface SP_FocusTeamViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    NSMutableArray *_teamsArr;

}
@end

@implementation SP_FocusTeamViewController

- (id)init
{
    self = [super init];
    if(self){
        _teamsArr = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"我关注的球队"];
    [self addBackButton];
    
    UIButton * btneAdd = GET_BUTTON(CGRectMake(ScreenWidth-45, ios7_height, 44, 44), 14, NO, [UIColor whiteColor]);
    
    [self.topView addSubview:btneAdd];
    
    [btneAdd setImage:[UIImage imageNamed:@"关注球队1"] forState:UIControlStateNormal];
    [btneAdd setImage:[UIImage imageNamed:@"关注球队2"] forState:UIControlStateHighlighted];

    [btneAdd setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btneAdd addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    btneAdd.tag = 2;
    
    
    [self setUpTableView];
    
    [self setHTTPRequest:GETATTENTIONTEAM(GETUSERID) Tag:TAG_HTTP_GETATTENTIONTEAM];
}

- (void)btnNavRightClick
{
    SP_SearchTeamViewController *vc = [[SP_SearchTeamViewController alloc]init];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate].drawerController closeDrawerAnimated:NO completion:nil];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
//    [self openSliderMenu];
//    [[AppDelegate sharedAppDelegate].drawerController closeDrawerAnimated:NO completion:nil];
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = 0;
    _tableView.rowHeight = 150;
    
    [self.view addSubview:_tableView];
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_GETJOINTEAM -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_GETATTENTIONTEAM){
            if (HTTP_SUCCESS(code))
            {
                [_teamsArr removeAllObjects];
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_TeamModel *team = [SP_TeamModel objectWithKeyValues:dict];
                    [_teamsArr addObject:team];
                }
                [_tableView reloadData];
            }
        }else if (request.tag == TAG_HTTP_DISTEAMATTENTION){
            if (HTTP_SUCCESS(code))
            {
                [self showAlertStr:@"取消关注成功"];
                [_teamsArr removeAllObjects];
                [self setHTTPRequest:GETATTENTIONTEAM(GETUSERID) Tag:TAG_HTTP_GETATTENTIONTEAM];
            }
        
        }
    }
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _teamsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_TeamCell *cell = [SP_TeamCell cellWithTableView:tableView];
    
    
    SP_TeamModel *team = _teamsArr[indexPath.row];
    
    cell.team = team;
    
    
    cell.cancelFocus.hidden = NO;
    cell.cancelFocus.tag = [team.teamid integerValue];
    [cell.cancelFocus addTarget:self action:@selector(cancelFocusBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(team.playerData.count == 1){
        NSDictionary *leaderOneDict = team.playerData[0];
        
        SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
        
        cell.leaderOneBtn.tag = [leaderOne.uid integerValue];
        
        [cell.leaderOneBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }else if (team.playerData.count == 2){
        
        NSDictionary *leaderOneDict = team.playerData[0];
        NSDictionary *leaderTwoDict = team.playerData[1];
        
        SP_TeamMateModel *leaderOne = [SP_TeamMateModel objectWithKeyValues:leaderOneDict];
        SP_TeamMateModel *leaderTwo = [SP_TeamMateModel objectWithKeyValues:leaderTwoDict];
        
        cell.leaderOneBtn.tag = [leaderOne.uid integerValue];
        cell.leaderTwoBtn.tag = [leaderTwo.uid integerValue];
        
        [cell.leaderOneBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];
        [cell.leaderTwoBtn addTarget:self action:@selector(pushToUserHomeVC:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    return cell;
}


- (void)cancelFocusBtnDidClick:(UIButton*)btn
{
    NSString *teamid = [NSString stringWithFormat:@"%ld",btn.tag];
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:DISTEAMATTENTION(GETUSERID, teamid) Tag:TAG_HTTP_DISTEAMATTENTION];

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_TeamModel *team = _teamsArr[indexPath.row];

    SP_TeamInfoViewController *vc = [[SP_TeamInfoViewController alloc]init];
    vc.teamID = team.teamid;
    vc.teamName = team.name;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}


- (void)pushToUserHomeVC:(UIButton *)btn
{
    NSString *touid = [NSString stringWithFormat:@"%ld",btn.tag];
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = touid;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
