//
//  SP_AddTagViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/20.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_AddTagViewController.h"
#import "SP_TagModel.h"

@interface SP_AddTagViewController ()<UITextFieldDelegate>
{
    UITextField * _textfInput;
    NSMutableArray *_tagArr;
    
    __weak IBOutlet UIButton *tag1;
    __weak IBOutlet UIButton *tag2;
    __weak IBOutlet UIButton *tag3;
    __weak IBOutlet UIButton *tag4;
    __weak IBOutlet UIButton *tag5;
    __weak IBOutlet UIButton *tag6;
    __weak IBOutlet UIButton *tag7;
    __weak IBOutlet UIButton *tag8;
    __weak IBOutlet UIButton *tag9;
    __weak IBOutlet UIButton *tag10;
    __weak IBOutlet UIButton *tag11;
    __weak IBOutlet UIImageView *bgImgView;
    
    UIView *_voteView;
    UIButton *_zanBtn;
    UIButton *_caiBtn;
    
    
    int currentBtnTag;
    
    
    
    

}
@end

@implementation SP_AddTagViewController

- (id)init
{
    self = [super init];
    if(self){
        _tagArr = [NSMutableArray array];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavigationBar:@"添加标签"];
    [self addBackButton];
    self.view.backgroundColor = UIColorFromRGB(0xfafafa);
    [self _initSearchView];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:GETUSERTAG(self.touid) Tag:TAG_HTTP_GETUSERTAG];
    
    
    
    NSLog(@"sex -- %@",self.sex);
    
    if([self.sex isEqualToString:@"1"]){
        bgImgView.image = [UIImage imageNamed:@"男"];
    }else{
        bgImgView.image = [UIImage imageNamed:@"女"];
    }
    
    
}



- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        //        NSLog(@"战术板 --- resopnse:%@",request.responseString);
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETUSERTAG){
            if(HTTP_SUCCESS(code)){
//            NSLog(@"TAG_HTTP_GETUSERTAG --- resopnse:%@",request.responseString);

                [_tagArr removeAllObjects];
                
                NSArray *arr = dictionary[@"msg"];
                for(NSDictionary *dict in arr){
                
                    SP_TagModel *model = [SP_TagModel objectWithKeyValues:dict];
                    [_tagArr addObject:model];
                
                }
                
                [self refreshView];
            }
        }else if (request.tag == TAG_HTTP_ADDTAG){
            if(HTTP_SUCCESS(code)){
//                NSLog(@"TAG_HTTP_GETUSERTAG --- resopnse:%@",request.responseString);
                
            
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
                
                [self setHTTPRequest:GETUSERTAG(self.touid) Tag:TAG_HTTP_GETUSERTAG];

                
            }else{
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
                

            }
        }else if (request.tag == TAG_HTTP_OPUSERTAG){
            NSLog(@"TAG_HTTP_OPUSERTAG --- resopnse:%@",request.responseString);

            if(HTTP_SUCCESS(code)){

                [self setHTTPRequest:GETUSERTAG(self.touid) Tag:TAG_HTTP_GETUSERTAG];

            
            }
        
        
        }
    }
}


- (void)_initSearchView{
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(-1, [self viewTop]-1, ScreenWidth+2, 55)];
    
    [self.view addSubview:view];
    
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    
    view.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView * inputView = [[UIView alloc] initWithFrame:CGRectMake(20, 10, ScreenWidth-20*2, 34)];
    [view addSubview:inputView];
    inputView.backgroundColor = [UIColor whiteColor];
    inputView.layer.borderWidth = 1;
    
    inputView.layer.borderColor = UIColorFromRGB(0xe5e5e5).CGColor;
    inputView.layer.cornerRadius = 2;
    
    _textfInput = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, inputView.frame.size.width-60, 24)];
    [inputView addSubview:_textfInput];
    _textfInput.delegate = self;
    _textfInput.placeholder = @"手动添加（五个字以内）";
    _textfInput.returnKeyType = UIReturnKeySearch;
    
    UIView * viewVerline = [[UIView alloc] initWithFrame:CGRectMake(inputView.frame.size.width - 50, 5, 1, 24)];
    [inputView addSubview:viewVerline];
    viewVerline.backgroundColor = UIColorFromRGB(0xe5e5e5);
    
    UIButton * btnSearch = GET_BUTTON(CGRectMake(inputView.frame.size.width-50, 0, 40, 34), 14, NO, nil);
    [btnSearch setTitle:@"提交" forState:UIControlStateNormal];
    btnSearch.titleLabel.font = [UIFont systemFontOfSize:15];
    btnSearch.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnSearch setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
    [inputView addSubview:btnSearch];
//    [btnSearch setImage:[UIImage imageNamed:@"person_search"] forState:UIControlStateNormal];
    [btnSearch setImageEdgeInsets:UIEdgeInsetsMake(7, 10, 7, 10)];
    [btnSearch addTarget:self action:@selector(btnSearchClick) forControlEvents:UIControlEventTouchUpInside];
}



#pragma -mark Btn Action
- (void)btnSearchClick{
    
//    NSString *tagName = [self urlEncodeValue:_textfInput.text];
    
    
    if(_textfInput.text.length > 5){
        [self showAlertStr:@"标签字数不能大于5"];
        return ;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:self.touid forKey:@"uid"];
    [dict setObject:_textfInput.text forKey:@"tag"];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:ADDTAG Dictionary:dict Tag:TAG_HTTP_ADDTAG];
    
}

#pragma -mark UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    //直接请求接口
    [self btnSearchClick];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) refreshView
{
    for(int i = 0;i<_tagArr.count;i++){
        
        SP_TagModel *model = _tagArr[i];
        
        UIButton *btn = (UIButton *)[self.view viewWithTag:i+1];
        [btn setTitle:model.tag forState:UIControlStateNormal];
        btn.hidden = NO;
        
        btn.tag = 100 +i;
        
        
        [btn addTarget:self action:@selector(tagBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }


}

- (void)tagBtnDidClick:(UIButton *)btn
{
    
    if(currentBtnTag == btn.tag && _voteView.superview != nil){
        [_voteView removeFromSuperview];
        return;
    }
    
    
    if(_voteView.superview != nil){
        [_voteView removeFromSuperview];
    }

    
    SP_TagModel *model = _tagArr[btn.tag - 100];
    

    
    _voteView = [[UIView alloc]initWithFrame:CGRectMake(btn.frame.origin.x, CGRectGetMaxY(btn.frame) + 5, 90, 35)];
    _voteView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_voteView];
    
    UIImageView *voteBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 90, 35)];
    voteBgView.image = [UIImage imageNamed:@"弹框"];
    [_voteView addSubview:voteBgView];
    
    
    _zanBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 2, 40, 35)];
    _zanBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    _zanBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _zanBtn.tag = 200 + [model.usertagid integerValue];
    [_zanBtn setTitle:[NSString stringWithFormat:@"赞(%@)",model.zhan] forState:UIControlStateNormal];
    [_zanBtn setTitleColor:UIColorFromRGB(GREEN_COLOR_VALUE) forState:UIControlStateNormal];
    [_zanBtn addTarget:self action:@selector(zanBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_voteView addSubview:_zanBtn];
    
    _caiBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_zanBtn.frame) , 2, 40, 35)];
    _caiBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    _caiBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _caiBtn.tag = 200 + [model.usertagid integerValue];
    [_caiBtn setTitle:[NSString stringWithFormat:@"踩(%@)",model.cai] forState:UIControlStateNormal];
    [_caiBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_caiBtn addTarget:self action:@selector(caiBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_voteView addSubview:_caiBtn];

    
    
    currentBtnTag = (int)btn.tag;
}



- (void) zanBtnDidClick:(UIButton *)btn
{
    [_voteView removeFromSuperview];
    
    NSString *usertagid = [NSString stringWithFormat:@"%ld",btn.tag-200];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:OPUSERTAG(usertagid, @"1") Tag:TAG_HTTP_OPUSERTAG];
}

- (void) caiBtnDidClick:(UIButton *)btn
{
    [_voteView removeFromSuperview];

    
    NSString *usertagid = [NSString stringWithFormat:@"%ld",btn.tag-200];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:OPUSERTAG(usertagid, @"0") Tag:TAG_HTTP_OPUSERTAG];

    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
