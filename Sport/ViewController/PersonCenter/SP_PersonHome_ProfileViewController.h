//
//  SP_PersonHome_ProfileViewController.h
//  Sport
//
//  Created by 李松玉 on 15/5/31.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"


@class SP_UserInfo;

@protocol SP_PersonHome_ProfileDelegate <NSObject>

- (void) bottomBtnDidClick;

- (void) upUserData;

@end


@interface SP_PersonHome_ProfileViewController : BaseViewController
@property (nonatomic,strong) SP_UserInfo *userInfoMsg;

@property (nonatomic,assign) id<SP_PersonHome_ProfileDelegate> delegate;

@end
