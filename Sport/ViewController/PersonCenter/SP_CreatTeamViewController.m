//
//  SP_CreatTeamViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/30.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_CreatTeamViewController.h"
#import "MyTextView.h"
#import "SP_UserInfo.h"
#import "SP_AddTeamMateViewController.h"
#import "SP_FriendModel.h"
#import "SP_AddTeamMateCollectionCell.h"
#import "SP_AddMateBtnCollectionCell.h"
#import "PhotoChoiceView.h"

#define Btn_Tag_Foot    101
#define Btn_Tag_Basket  102
#define Btn_Tag_School  103
#define btn_Tag_Company 104

#define Tag_TextFiled_leaderOneNum      55
#define Tag_TextFiled_leaderTwoNum      66



@interface SP_CreatTeamViewController () <SP_AddTeamMateDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,PhotoChoiceViewDelegate,UITextViewDelegate>
{
    UIScrollView *_scrollView;

    //顶部球队图片选择View
    UIView *_topView;
    UIView *_imgBgView;
    UIImageView *_addImg;
    UILabel *_addLabel;
    UILabel *_topNoticeLabel;
    UIImageView *_teamImgView;
    UIButton *_addTeamImgBtn;
    UIImageView *_demoImgView;
    UIImageView *_arrowImgView;
    
    //球队信息View
    UIView *_teamDesView;
    UILabel *_teamNameLabel;
    UITextField *_teamNameText;
    UIButton *_typeBtn;
    UIButton *_schCoBtn;
    MyTextView *_schCoNameText;
    BOOL schCoNameTextEdit;
    UILabel *_teamDesLabel;
    UITextField *_teamDesText;
    
    //队长和副队长选择
    UIView *_leaderView;
    UIImageView *_leaderOneImg;
    UILabel *_leaderOneName;
    UILabel *_leaderOneTel;
    UITextField *_leaderOneNum;
    SP_FriendModel *_leaderOneInfo;
    UIImageView *_leaderTwoImg;
    UILabel *_leaderTwoName;
    UILabel *_leaderTwoTel;
    UITextField *_leaderTwoNum;
    UIButton *_leaderTwoBtn;
    UIImageView *_towBgImg;
    SP_FriendModel *_leaderTwoInfo;
    UIView *_imgBgViewTwo;
    
    //添加队员View
    UIView *_addTeamMateView;
    UIButton *_addTeamMateBtn;
    UICollectionView *_collectView;
    
    NSMutableArray *_teamMatesArr;
    
    //球类选择View
    UIView *_ballTypeView;
    NSString *_ballType;   //0 篮球 ， 1 足球
    
    //公司学校选择View
    UIView *_coSchView;

    
    
}
@end

@implementation SP_CreatTeamViewController

- (id)init
{
    self = [super init];
    if(self){
        _teamMatesArr = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"建立球队"];
    [self addBackButton];
    [self addNavRightView];

    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64)];
    _scrollView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    [self.view addSubview:_scrollView];
    
    [self setUpTopView];
    [self setUpTeamDesView];
    [self setUpLeaderView];
    [self setUpAddTeamMateView];
    
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil] ;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil] ;


}

- (void)addNavRightView{
    UIButton * btnedit = GET_BUTTON(CGRectMake(ScreenWidth-60, ios7_height, 44, 44), 17, NO, [UIColor whiteColor]);
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"完成"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick:) forControlEvents:UIControlEventTouchUpInside];
    
    btnedit.tag = 1;
    
}

#pragma mark - 确定按钮点击方法
- (void) btnNavRightClick:(UIButton *)btn
{

    NSMutableArray *teamPlayers = [[NSMutableArray alloc]init];
    
    //添加队长
    NSMutableDictionary *leaderOne = [[NSMutableDictionary alloc]init];
    [leaderOne setValue:GETUSERID forKey:@"uid"];
    [leaderOne setValue:_leaderOneInfo.num forKey:@"playernum"];
    [leaderOne setValue:@"2" forKey:@"role"];
    [teamPlayers addObject:leaderOne];
    
    //添加副队长
    if(_leaderTwoInfo != nil){
        NSMutableDictionary *leaderTwo = [[NSMutableDictionary alloc]init];
        [leaderTwo setValue:_leaderTwoInfo.touid forKey:@"uid"];
        [leaderTwo setValue:_leaderTwoInfo.num forKey:@"playernum"];
        [leaderTwo setValue:@"1" forKey:@"role"];
        [teamPlayers addObject:leaderTwo];

    }
    
    //添加小队成员
    for(SP_FriendModel *model in _teamMatesArr){
        NSMutableDictionary *mate = [[NSMutableDictionary alloc]init];
        [mate setValue:model.touid forKey:@"uid"];
        [mate setValue:model.num forKey:@"playernum"];
        [mate setValue:@"0" forKey:@"role"];
        [teamPlayers addObject:mate];
    
    
    }
    
    
    
    NSString *teamName = _teamNameText.text;
    NSString *teamDesc = _teamDesText.text;
    UIImage *teamImg = _teamImgView.image;
    NSString *teamType = _ballType;
    NSString *playerStr = [teamPlayers JSONString];

    [self createTeamWithName:teamName Desc:teamDesc TeamImg:teamImg Type:teamType Player:playerStr];
}


//uid
//name   球队名称
//desc    球队简介
//img     球队头像(base64处理)
//type     (0篮球队    1足球队)
//player   json  例：
//[{"uid":"7","playernum":"8","role":"2"},{"uid":"8","playernum":"9","role":"1"}]
//说明：playernum 球员号码
//role  0普通 1副队长 2队长
#pragma mark - 创建球队数据请求
- (void) createTeamWithName:(NSString *)name
                       Desc:(NSString *)desc
                    TeamImg:(UIImage  *)teamImg
                       Type:(NSString *)type
                     Player:(NSString *)player
{
    
    
    NSString *iconStr;

    if(teamImg != nil){
        UIImage *img = [GT_Tool scaleToSize:teamImg size:CGSizeMake(90, 90)];
        NSData *iconImg = UIImageJPEGRepresentation(img,0.5);
        iconStr = [GTMBase64 stringByEncodingData:iconImg];
    }else{
        UIImage *demoImg = [UIImage imageNamed:@"cup_deault.png"];
        UIImage *img = [GT_Tool scaleToSize:demoImg size:CGSizeMake(90, 90)];
        NSData *iconImg = UIImageJPEGRepresentation(img,0.5);
        iconStr = [GTMBase64 stringByEncodingData:iconImg];
    }
    
    
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];
    [dictionary setObject:name      forKey:@"name"];
    [dictionary setObject:desc      forKey:@"desc"];
    [dictionary setObject:iconStr   forKey:@"img"];
    [dictionary setObject:player    forKey:@"player"];
    [dictionary setObject:type      forKey:@"type"];

    
    NSLog(@"dictionary -- %@",dictionary);

    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHttpPostData:CREATETEAM Dictionary:dictionary Tag:TAG_HTTP_CREATETEAM];
    
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_CREATETEAM -- resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_CREATETEAM){
            if (HTTP_SUCCESS(code))
            {
                NSString *msg = dictionary[@"msg"];
                [self showMessage:msg];
                [self.navigationController popViewControllerAnimated:YES];

            }
        }
    }
}



#pragma mark - 创建顶部的球队图片选择View
- (void) setUpTopView
{
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 90)];
    _topView.backgroundColor = [UIColor whiteColor];
    
    _imgBgView = [[UIView alloc]initWithFrame:CGRectMake(15, 10, 70, 70)];
    _imgBgView.backgroundColor = UIColorFromRGB(0xe0e0e0);
    
    _addImg = [[UIImageView alloc]initWithFrame:CGRectMake(32, 20, 34, 34)];
    _addImg.image = [UIImage imageNamed:@"add_assert.png"];
    
    _addLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_addImg.frame) + 10, 60, 10)];
    _addLabel.text = @"添加球队图标";
    _addLabel.font = [UIFont systemFontOfSize:10];
    _addLabel.textColor = [UIColor darkGrayColor];
    

    _topNoticeLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imgBgView.frame) + 10, 28, 84, 30)];
    _topNoticeLabel.text = @"如果未上传图标\r则使用默认图标";
    _topNoticeLabel.numberOfLines = 0;
    _topNoticeLabel.font = [UIFont systemFontOfSize:12];
    _topNoticeLabel.textColor = UIColorFromRGB(0x696969);
    _topNoticeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    _teamImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 70, 70)];
    
    _addTeamImgBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 10, 70, 70)];
    _addTeamImgBtn.backgroundColor = [UIColor clearColor];
    [_addTeamImgBtn addTarget:self action:@selector(addTeamImgBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    _arrowImgView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_topNoticeLabel.frame) + 20, 40, 22, 14)];
    _arrowImgView.image = [UIImage imageNamed:@"arrow_deaudt.png"];
    
    
    
    _demoImgView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_arrowImgView.frame) + 10, 26, 36, 40)];
    _demoImgView.image = [UIImage imageNamed:@"cup_deault.png"];
    
    
    
    
    
    
    [_scrollView addSubview:_topView];
    [_topView addSubview:_imgBgView];
    [_topView addSubview:_addImg];
    [_topView addSubview:_addLabel];
    [_topView addSubview:_topNoticeLabel];
    [_topView addSubview:_teamImgView];
    [_topView addSubview:_addTeamImgBtn];
    [_topView addSubview:_arrowImgView];
    [_topView addSubview:_demoImgView];


}


/**
 *  添加战队图片方法
 */
- (void) addTeamImgBtnDidClick
{
//    NSLog(@"添加战队图片");
    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:self.view.frame];
    under.delegate = self;
    [self.view addSubview:under];

}

- (void)photoChoice:(UIImage *)image
{
    _teamImgView.image = image;
}


/**
 *  设置球队信息View
 */
- (void) setUpTeamDesView
{
    _teamDesView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topView.frame) + 8, ScreenWidth, 140)];
    _teamDesView.backgroundColor = [UIColor whiteColor];
    
    _teamNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 70, 21)];
    _teamNameLabel.font = [UIFont systemFontOfSize:14];
    _teamNameLabel.text = @"球队昵称:";
    _teamNameLabel.textColor = [UIColor blackColor];
    
    _teamNameText = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_teamNameLabel.frame), 15, 200, 21)];
    _teamNameText.delegate = self;
    _teamNameText.placeholder = @"请输入球队昵称";
    _teamNameText.font = [UIFont systemFontOfSize:14];
    _teamNameText.textColor = UIColorFromRGB(0x696969);
    
    _typeBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 75, 10, 60, 30)];
    _typeBtn.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _typeBtn.layer.borderWidth = 1;
    _typeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_typeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_typeBtn setTitle:@"足球" forState:UIControlStateNormal];
    [_typeBtn setImage:[UIImage imageNamed:@"peredit_arrow.png"] forState:UIControlStateNormal];
    [_typeBtn addTarget:self action:@selector(typeBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [_typeBtn setImageEdgeInsets:UIEdgeInsetsMake(9, 9, 8, 46)];
    _ballType = @"1";
    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_typeBtn.frame) + 5, ScreenWidth - 20, 1)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    _schCoBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) +5, 60, 30)];
    _schCoBtn.layer.borderColor = UIColorFromRGB(GREEN_COLOR_VALUE).CGColor;
    _schCoBtn.layer.borderWidth = 1;
    _schCoBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_schCoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_schCoBtn setTitle:@"学校" forState:UIControlStateNormal];
    [_schCoBtn setImage:[UIImage imageNamed:@"peredit_arrow.png"] forState:UIControlStateNormal];
    [_schCoBtn setImageEdgeInsets:UIEdgeInsetsMake(9, 9, 8, 46)];
    [_schCoBtn addTarget:self action:@selector(schCoBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    _schCoNameText = [[MyTextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_schCoBtn.frame) + 5, CGRectGetMaxY(midLine.frame) +5, ScreenWidth -  CGRectGetMaxX(_schCoBtn.frame) - 20, 40)];
    _schCoNameText.placehoder = @"请填写你所在的学校和公司方便信息匹配";
    _schCoNameText.delegate = self;
    _schCoNameText.font = [UIFont systemFontOfSize:15];
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_schCoNameText.frame) + 5, ScreenWidth - 20, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    _teamDesLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(bottomLine.frame) + 5, 70, 21)];
    _teamDesLabel.font = [UIFont systemFontOfSize:14];
    _teamDesLabel.text = @"球队简介:";
    _teamDesLabel.textColor = [UIColor blackColor];
    
    _teamDesText = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_teamDesLabel.frame),  CGRectGetMaxY(bottomLine.frame) + 5, ScreenWidth - CGRectGetMaxX(_teamDesLabel.frame) - 20, 21)];
    _teamDesText.placeholder = @"请输入球队简介";
    _teamDesText.delegate = self;
    _teamDesText.font = [UIFont systemFontOfSize:14];
    _teamDesText.textColor = UIColorFromRGB(0x696969);
    
    
    
    [_scrollView addSubview:_teamDesView];
    [_teamDesView addSubview:_teamNameLabel];
    [_teamDesView addSubview:_teamNameText];
    [_teamDesView addSubview:_typeBtn];
    [_teamDesView addSubview:midLine];
    [_teamDesView addSubview:_schCoBtn];
    [_teamDesView addSubview:_schCoNameText];
    [_teamDesView addSubview:bottomLine];
    [_teamDesView addSubview:_teamDesLabel];
    [_teamDesView addSubview:_teamDesText];

    
}


- (void) setUpLeaderView
{
    _leaderView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_teamDesView.frame) + 8, ScreenWidth, 170)];
    _leaderView.backgroundColor = [UIColor whiteColor];
    
    _leaderOneImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 45, 45)];
    _leaderOneImg.layer.cornerRadius = 10;
    _leaderOneImg.layer.masksToBounds = YES;
    NSURL *imgUrl = [NSURL URLWithString:self.userInfoMsg.headimg];
    [_leaderOneImg sd_setImageWithURL:imgUrl];
    
    
    UILabel *oneLogo = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderOneImg.frame) + 5, 20, 24, 12)];
    oneLogo.text = @"队长";
    oneLogo.font = [UIFont systemFontOfSize:10];
    oneLogo.textAlignment = NSTextAlignmentCenter;
    oneLogo.backgroundColor = UIColorFromRGB(0xe67f22);
    oneLogo.textColor = [UIColor whiteColor];
    oneLogo.layer.cornerRadius = 5;
    oneLogo.layer.masksToBounds = YES;
    
    UILabel *oneName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderOneImg.frame) + 5, CGRectGetMaxY(oneLogo.frame) + 5, 35,14)];
    oneName.text = @"昵称:";
    oneName.font = [UIFont systemFontOfSize:14];
    oneName.textColor = [UIColor blackColor];
    
    _leaderOneName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(oneName.frame),  CGRectGetMaxY(oneLogo.frame) + 5, 150, 14)];
    _leaderOneName.font = [UIFont systemFontOfSize:14];
    _leaderOneName.text = self.userInfoMsg.nickname;
    _leaderOneName.textColor = UIColorFromRGB(0x696969);
    
    UILabel *oneTel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 140,  CGRectGetMaxY(oneLogo.frame) + 5, 35, 14)];
    oneTel.text = @"电话:";
    oneTel.font = [UIFont systemFontOfSize:14];
    oneTel.textColor = [UIColor blackColor];
    
    _leaderOneTel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(oneTel.frame), CGRectGetMaxY(oneLogo.frame) + 5, 100, 14)];
    _leaderOneTel.font = [UIFont systemFontOfSize:14];
    _leaderOneTel.text = self.userInfoMsg.tel;
    _leaderOneTel.textColor = UIColorFromRGB(0x696969);
    
    
    UILabel *oneNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderOneImg.frame) + 5, CGRectGetMaxY(_leaderOneTel.frame) + 5, 50, 21)];
    oneNum.text = @"球衣号:";
    oneNum.font = [UIFont systemFontOfSize:14];
    oneNum.textColor = [UIColor blackColor];

    _leaderOneNum = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(oneName.frame) + 15, CGRectGetMaxY(_leaderOneTel.frame)+ 10, 100, 14)];
    _leaderOneNum.delegate = self;
    _leaderOneNum.placeholder = @"请输入球衣号";
    _leaderOneNum.font = [UIFont systemFontOfSize:14];
    _leaderOneNum.textColor = UIColorFromRGB(0x696969);
    _leaderOneNum.tag = Tag_TextFiled_leaderOneNum;
    
    
    
    
    UIView *midLine = [[UIView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_leaderOneNum.frame) + 10, ScreenWidth - 20, 1)];
    midLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    _imgBgViewTwo = [[UIView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 5, 45, 45)];
    _imgBgViewTwo.backgroundColor = UIColorFromRGB(0xe0e0e0);
    _imgBgViewTwo.layer.cornerRadius = 8;
    _imgBgViewTwo.layer.masksToBounds = YES;

    
    _towBgImg = [[UIImageView alloc]initWithFrame:CGRectMake(21, CGRectGetMaxY(midLine.frame) + 11, 34 , 34)];
    _towBgImg.image = [UIImage imageNamed:@"add_assert.png"];
    _towBgImg.layer.cornerRadius = 8;
    _towBgImg.layer.masksToBounds = YES;
    
    
    
    _leaderTwoImg = [[UIImageView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 5, 45, 45)];
    _leaderTwoImg.layer.cornerRadius = 10;
    _leaderTwoImg.layer.masksToBounds = YES;

    _leaderTwoBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(midLine.frame) + 5, 45, 45)];
    _leaderTwoBtn.backgroundColor = [UIColor clearColor];
    [_leaderTwoBtn addTarget:self action:@selector(addleaderTwoBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *twoLogo = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderTwoImg.frame) + 5, CGRectGetMaxY(midLine.frame) + 20, 24, 12)];
    twoLogo.text = @"副";
    twoLogo.font = [UIFont systemFontOfSize:10];
    twoLogo.textAlignment = NSTextAlignmentCenter;
    twoLogo.backgroundColor = UIColorFromRGB(0xe67f22);
    twoLogo.textColor = [UIColor whiteColor];
    twoLogo.layer.cornerRadius = 5;
    twoLogo.layer.masksToBounds = YES;
    
    UILabel *twoName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderTwoImg.frame) + 5, CGRectGetMaxY(twoLogo.frame) + 5, 35,14)];
    twoName.text = @"昵称:";
    twoName.font = [UIFont systemFontOfSize:14];
    twoName.textColor = [UIColor blackColor];
    
    _leaderTwoName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(twoName.frame),  CGRectGetMaxY(twoLogo.frame) + 5, 150, 14)];
    _leaderTwoName.font = [UIFont systemFontOfSize:14];
    _leaderTwoName.textColor = UIColorFromRGB(0x696969);
    
    UILabel *twoTel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth - 140,  CGRectGetMaxY(twoLogo.frame) + 5, 35, 14)];
    twoTel.text = @"电话:";
    twoTel.font = [UIFont systemFontOfSize:14];
    twoTel.textColor = [UIColor blackColor];
    
    _leaderTwoTel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(twoTel.frame), CGRectGetMaxY(twoLogo.frame) + 5, 100, 14)];
    _leaderTwoTel.font = [UIFont systemFontOfSize:14];
    _leaderTwoTel.textColor = UIColorFromRGB(0x696969);
    
    
    UILabel *twoNum = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leaderTwoImg.frame) + 5, CGRectGetMaxY(_leaderTwoTel.frame) + 5, 50, 21)];
    twoNum.text = @"球衣号:";
    twoNum.font = [UIFont systemFontOfSize:14];
    twoNum.textColor = [UIColor blackColor];
    
    _leaderTwoNum = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(twoName.frame) + 15, CGRectGetMaxY(_leaderTwoTel.frame)+ 10, 100, 14)];
    _leaderTwoNum.delegate = self;
    _leaderTwoNum.placeholder = @"请输入球衣号";
    _leaderTwoNum.font = [UIFont systemFontOfSize:14];
    _leaderTwoNum.textColor = UIColorFromRGB(0x696969);
    _leaderTwoNum.tag = Tag_TextFiled_leaderTwoNum;
    
    
    
    
    
    [_scrollView addSubview:_leaderView];
    [_leaderView addSubview:_leaderOneImg];
    [_leaderView addSubview:oneLogo];
    [_leaderView addSubview:oneName];
    [_leaderView addSubview:_leaderOneName];
    [_leaderView addSubview:oneTel];
    [_leaderView addSubview:_leaderOneTel];
    [_leaderView addSubview:oneNum];
    [_leaderView addSubview:_leaderOneNum];
    [_leaderView addSubview:midLine];
    [_leaderView addSubview:twoLogo];
    [_leaderView addSubview:twoName];
    [_leaderView addSubview:_leaderTwoName];
    [_leaderView addSubview:twoTel];
    [_leaderView addSubview:_leaderTwoTel];
    [_leaderView addSubview:twoNum];
    [_leaderView addSubview:_leaderTwoNum];
    [_leaderView addSubview:_imgBgViewTwo];
    [_leaderView addSubview:_towBgImg];
    [_leaderView addSubview:_leaderTwoImg];
    [_leaderView addSubview:_leaderTwoBtn];
    
    
}


#pragma mark - 球类选择按钮点击方法
- (void)typeBtnDidClick
{
    if(_ballTypeView.superview == nil){
        _ballTypeView = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth - 110,40,100, 40)];
        
        UIImageView *coschBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 40)];
        coschBgView.image = [UIImage imageNamed:@"学校，公司选择框"];

        
        UIButton *footBallBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 10, 45, 30)];
        footBallBtn.tag = Btn_Tag_Foot;
        [footBallBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        footBallBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [footBallBtn setTitle:@"足球" forState:UIControlStateNormal];
        [footBallBtn addTarget:self action:@selector(footBallOrBasketBallBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *basketBallBtn = [[UIButton alloc]initWithFrame:CGRectMake(55, 10, 45, 30)];
        basketBallBtn.tag = Btn_Tag_Basket;
        [basketBallBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        basketBallBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [basketBallBtn setTitle:@"篮球" forState:UIControlStateNormal];
        [basketBallBtn addTarget:self action:@selector(footBallOrBasketBallBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

        
        
        
        
        [_teamDesView addSubview:_ballTypeView];
        [_ballTypeView addSubview:coschBgView];
        [_ballTypeView addSubview:footBallBtn];
        [_ballTypeView addSubview:basketBallBtn];
    }

}

#pragma mark 足球-篮球 按钮点击方法
- (void) footBallOrBasketBallBtnDidClick:(UIButton *)btn
{
    if(btn.tag == Btn_Tag_Basket){
        _ballType = @"0";
        [_typeBtn setTitle:@"篮球" forState:UIControlStateNormal];
    }else if (btn.tag == Btn_Tag_Foot){
        _ballType = @"1";
        [_typeBtn setTitle:@"足球" forState:UIControlStateNormal];
    }
    [_ballTypeView removeFromSuperview];
    NSLog(@"_ballType -- %@",_ballType);
}


#pragma mark - 公司/学校选择按钮点击方法
- (void) schCoBtnDidClick
{
    if(_coSchView.superview == nil){
        _coSchView = [[UIView alloc]initWithFrame:CGRectMake(15,82,100, 40)];
        
        UIImageView *coschBgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 40)];
        coschBgView.image = [UIImage imageNamed:@"学校，公司选择框"];
        
        
        UIButton *schoolBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 10, 45, 30)];
        schoolBtn.tag = Btn_Tag_School;
        [schoolBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        schoolBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [schoolBtn setTitle:@"学校" forState:UIControlStateNormal];
        [schoolBtn addTarget:self action:@selector(schoolOrCoBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *companyBtn = [[UIButton alloc]initWithFrame:CGRectMake(55, 10, 45, 30)];
        companyBtn.tag = btn_Tag_Company;
        [companyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        companyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [companyBtn setTitle:@"公司" forState:UIControlStateNormal];
        [companyBtn addTarget:self action:@selector(schoolOrCoBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
        
        [_teamDesView addSubview:_coSchView];
        [_coSchView addSubview:coschBgView];
        [_coSchView addSubview:schoolBtn];
        [_coSchView addSubview:companyBtn];
    }
}

- (void) schoolOrCoBtnDidClick:(UIButton *)btn
{
    if(btn.tag == Btn_Tag_School){
        [_schCoBtn setTitle:@"学校" forState:UIControlStateNormal];
    }else if (btn.tag == btn_Tag_Company){
        [_schCoBtn setTitle:@"公司" forState:UIControlStateNormal];
    }
    [_coSchView removeFromSuperview];
}



#pragma mark 副队长头像点击方法
- (void)addleaderTwoBtnDidClick
{
    SP_AddTeamMateViewController *vc = [[SP_AddTeamMateViewController alloc]init];
    vc.addType = @"0";
    vc.delegate = self;
    vc.type = @"0";
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
}


#pragma mark - SP_AddTeamMateDelegate Method
- (void) backLeaderTwoInfo:(SP_FriendModel *)leaderTwo
{
    _leaderTwoInfo = leaderTwo;
    
    _leaderTwoName.text = _leaderTwoInfo.nickname;
    _leaderTwoTel.text = _leaderTwoInfo.tel;
    
    NSURL *url = [NSURL URLWithString:_leaderTwoInfo.headimg];
    [_leaderTwoImg sd_setImageWithURL:url];
    _towBgImg.hidden = YES;
    _imgBgViewTwo.hidden = YES;
    
    
    
}




#pragma mark - 设置添加队友View
- (void) setUpAddTeamMateView
{
    _addTeamMateView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_leaderView.frame), ScreenWidth , 200)];
    _addTeamMateView.backgroundColor = [UIColor whiteColor];
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    UILabel *teamMate = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, ScreenWidth, 15)];
    teamMate.font = [UIFont systemFontOfSize:15];
    teamMate.text = @"添加球员";
    
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(teamMate.frame) + 10, ScreenWidth, 1)];
    bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    _addTeamMateBtn = [[UIButton alloc]init];
    [_addTeamMateBtn setImage:[UIImage imageNamed:@"add_assert.png"] forState:UIControlStateNormal];
    _addTeamMateBtn.backgroundColor = [UIColor lightGrayColor];
    _addTeamMateBtn.layer.cornerRadius = 8;
    _addTeamMateBtn.layer.masksToBounds = YES;
    [_addTeamMateBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    

    
    
    // 1.流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 2.每个cell的尺寸
    layout.itemSize = CGSizeMake(90, 110);
    // 3.设置cell之间的水平间距
    layout.minimumInteritemSpacing = 0;
    // 4.设置cell之间的垂直间距
    layout.minimumLineSpacing = 10;
    // 5.设置四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(layout.minimumLineSpacing, 20, 0, 20);
    
    _collectView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(bottomLine.frame) + 10   , ScreenWidth, ScreenHeight * 2) collectionViewLayout:layout];
    
    
    _collectView.backgroundColor = [UIColor whiteColor];
    UINib *nib = [UINib nibWithNibName: @"SP_AddTeamMateCollectionCell" bundle: nil];
    UINib *addBtnNib = [UINib nibWithNibName:@"SP_AddMateBtnCollectionCell" bundle:nil];
    [_collectView registerNib:nib       forCellWithReuseIdentifier:@"SP_AddTeamMateCollectionCell"];
    [_collectView registerNib:addBtnNib forCellWithReuseIdentifier:@"SP_AddMateBtnCollectionCell"];
    
    
    _collectView.dataSource = self;
    _collectView.delegate = self;
    
    _collectView.backgroundColor = [UIColor whiteColor];
    
    _scrollView.contentSize = CGSizeMake(ScreenWidth, CGRectGetMaxY(_leaderView.frame) + 200 );

    [_scrollView addSubview:_addTeamMateView];
    [_addTeamMateView addSubview:teamMate];
    [_addTeamMateView addSubview:topLine];
    [_addTeamMateView addSubview:bottomLine];
    [_addTeamMateView addSubview:_collectView];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _teamMatesArr.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.row == _teamMatesArr.count){
        static NSString * myidentify  = @"SP_AddMateBtnCollectionCell";
        SP_AddMateBtnCollectionCell *cell = nil;
        cell = (SP_AddMateBtnCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
        [cell.addBtn addTarget:self action:@selector(addMatesBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
        static NSString * myidentify  = @"SP_AddTeamMateCollectionCell";
        
        SP_AddTeamMateCollectionCell *cell = nil;
        cell = (SP_AddTeamMateCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:myidentify forIndexPath:indexPath];
        SP_FriendModel *model = _teamMatesArr[indexPath.row];
        cell.model = model;
        
        cell.delMateBtn.tag = indexPath.row;
        [cell.delMateBtn addTarget:self action:@selector(delMateBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.numTextField.delegate = self;
        cell.numTextField.tag = 200 + indexPath.row;
        
        return cell;
    }
    
    return nil;
}

- (void) delMateBtnDidClick:(UIButton *)btn
{
    [_teamMatesArr removeObjectAtIndex:btn.tag];
    [_collectView reloadData];
}


- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == _teamMatesArr.count - 1){
        SP_AddTeamMateViewController *vc = [[SP_AddTeamMateViewController alloc]init];
        vc.addType = @"1";
        vc.delegate = self;
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

        
    }
}


- (void) addMatesBtnDidClick
{
    SP_AddTeamMateViewController *vc = [[SP_AddTeamMateViewController alloc]init];
    vc.addType = @"1";
    vc.delegate = self;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (void) backTeamMateInfo:(SP_FriendModel *) teamMate
{
    
    for(SP_FriendModel *model in _teamMatesArr){
        NSLog(@"num -- %@",model.num);
    }
    
    
    
    [_teamMatesArr addObject:teamMate];
    [_collectView reloadData];
    
    
    NSInteger hBeiShu = _teamMatesArr.count / 3;
    NSLog(@"hBeiShu --- %ld",hBeiShu);
    _addTeamMateView.frame = CGRectMake(0, CGRectGetMaxY(_leaderView.frame), ScreenWidth ,200 + (200 * hBeiShu));
    //    _collectView.frame = CGRectMake(0, 46 , ScreenWidth, 150 + (150 * hBeiShu));
    _scrollView.contentSize = CGSizeMake(ScreenWidth, CGRectGetMaxY(_leaderView.frame) + 200 + (150 * hBeiShu));
    _collectView.scrollEnabled = NO;
    
    
}



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_coSchView.superview != nil){
        [_coSchView removeFromSuperview];
    }
    
    if(_ballTypeView.superview != nil){
        [_ballTypeView removeFromSuperview];
    }
    
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(_coSchView.superview != nil){
        [_coSchView removeFromSuperview];
    }
    
    if(_ballTypeView.superview != nil){
        [_ballTypeView removeFromSuperview];
    }
    
//    if(textField.tag  > 200){
//        SP_FriendModel *model = _teamMatesArr[textField.tag - 200];
//        model.num = textField.text;
//    }else if(textField.tag == Tag_TextFiled_leaderOneNum){
//        _leaderOneInfo.num = textField.text;
//    }else if (textField.tag == Tag_TextFiled_leaderTwoNum) {
//        _leaderTwoInfo.num = textField.text;
//    }

}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(_coSchView.superview != nil){
        [_coSchView removeFromSuperview];
    }
    
    if(_ballTypeView.superview != nil){
        [_ballTypeView removeFromSuperview];
    }
    schCoNameTextEdit = YES;

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    schCoNameTextEdit = NO;
}



- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag  > 200){
        SP_FriendModel *model = _teamMatesArr[textField.tag - 200];
        model.num = textField.text;
    }else if(textField.tag == Tag_TextFiled_leaderOneNum){
        _leaderOneInfo.num = textField.text;
    }else if (textField.tag == Tag_TextFiled_leaderTwoNum) {
        _leaderTwoInfo.num = textField.text;
    }
    
}

- (void) setUserInfoMsg:(SP_UserInfo *)userInfoMsg
{
    _userInfoMsg = userInfoMsg;
    
    _leaderOneInfo = [[SP_FriendModel alloc]init];
    _leaderOneInfo.uid = userInfoMsg.uid;
    _leaderOneInfo.role = @"2";
}


/**
 *  当键盘出现的时候，移动View的位置
 */
- (void)KeyBoardWillShow:(NSNotification *)note
{

    
    if(_teamNameText.editing == YES){
        return;
    }
    
    if(_teamDesText.editing == YES){
        return;
    }
    
    if(schCoNameTextEdit == YES){
        return;
    }
    
    CGRect keyBoardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //  2.计算控制器view需要平移的距离
    CGFloat transformY = keyBoardFrame.origin.y - (self.view.frame.size.height);
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, transformY);
        
    }];
    
}

- (void)KeyBoardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}






- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //让当前view结束所有控件的编辑状态
    [self.view endEditing:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
