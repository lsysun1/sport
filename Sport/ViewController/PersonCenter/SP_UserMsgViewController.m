//
//  SP_UserMsgViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/28.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_UserMsgViewController.h"
#import "SP_TextView.h"
@interface SP_UserMsgViewController ()<UITextViewDelegate>
@property (nonatomic,strong) SP_TextView *msgTextView;
@end

@implementation SP_UserMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"发布留言"];
    [self addBackButton];
    [self addNavRightView];
    [self setUpTextView];
    
}


- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"发送"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(btnNavRightClick) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)setUpTextView
{
    self.msgTextView = [[SP_TextView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 150)];
    if([self.type isEqualToString:@"board"]){
        self.msgTextView.placehoder = @"  说点什么吧";
    }else{
        self.msgTextView.placehoder = @"  这一刻的想法";
    }
    self.msgTextView.delegate = self;
    [self.view addSubview:self.msgTextView];
}

- (void) btnNavRightClick
{
    if(self.msgTextView.text.length == 0 ){
        [self showMessage:@"留言不能为空!"];
        return;
    }
    
    if([self.type isEqualToString:@"board"]){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.teamID forKey:@"teamid"];
        [dict setObject:self.msgTextView.text forKey:@"content"];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:PUBLISHZSBMSG Dictionary:dict Tag:TAG_HTTP_PUBLISHZSBMSG];

    
    }else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.touid forKey:@"touid"];
        [dict setObject:self.msgTextView.text forKey:@"content"];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:USERCOMMENT Dictionary:dict Tag:TAG_HTTP_USERCOMMENT];
    }

}


- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if(request.responseString != nil){
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
            if(HTTP_SUCCESS(code)){
                
                [self showMessage:@"留言成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [self showMessage:@"留言失败"];
            }
    }
}




@end
