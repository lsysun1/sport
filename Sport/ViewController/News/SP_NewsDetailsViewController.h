//
//  SP_NewsDetailsViewController.h
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"


@class SP_NewsList;
@interface SP_NewsDetailsViewController : BaseViewController
@property (nonatomic,strong) SP_NewsList *newsList;
@end
