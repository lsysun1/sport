//
//  SP_NewsDetailsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/19.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsDetailsViewController.h"
#import "SP_NewsList.h"
#import "SP_NewsDetailsHeadView.h"
#import "SP_NewsCommentModel.h"
#import "SP_NewsCommentFrame.h"
#import "SP_NewsCommentCell.h"
#import "MJRefresh.h"
#import <ShareSDK/ShareSDK.h>
#import "SP_UerHomeViewController.h"


@interface SP_NewsDetailsViewController ()<UITableViewDataSource,UITableViewDelegate,SP_NewsDetailsHeadViewDelegate,UITextFieldDelegate,SP_NewsCommentCellDelegate>
{
    UITableView *_tableView;
    SP_NewsDetailsHeadView *_headView;
    NSString *_newscontentStr;
    NSMutableArray *_commentArr;
    
    //底部的评论View
    UIView *_bottomeView;
    UITextField *_msgTextView;
    UIButton *_msgSendBtn;

    UIButton *_currentPairseBtn;
    
}
@end

@implementation SP_NewsDetailsViewController

- (id) init
{
    if(self = [super init]){
        _commentArr = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"运动GO"];
    [self addBackButton];
    [self setUpCommentView];
    [self addRightButton:nil Target:self Action:@selector(shareBtnDidClick) ImageName:@"新闻分享" Frame:CGRectMake(ScreenWidth - 40, 29, 24, 23)];
    [self setHTTPRequest:GETNEWSINFO(self.newsList.newsid) Tag:TAG_HTTP_GETNEWSINFO];
    [self setHTTPRequest:GETNEWSCOMMENTLIST(@"0", @"10", self.newsList.newsid,GETUSERID) Tag:TAG_HTTP_GETNEWSCOMMENTLIST];
    
    
}



#pragma mark - 分享按钮点击方法
- (void)shareBtnDidClick
{
    
    
    NSString *shareUrl = self.newsList.shareurl;
    NSString *shareContent = self.newsList.newsdesc;
    NSString *shareTitle = self.newsList.newstitle;
    NSString *shareImgUrl = self.newsList.newsimg;
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:shareContent
                                       defaultContent:@"运动GO"
                                                image:[ShareSDK imageWithUrl:shareImgUrl]
                                                title:shareTitle
                                                  url:shareUrl
                                          description:shareContent
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:self.view arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETNEWSINFO){    //获取新闻详情
            NSLog(@"TAG_HTTP_GETNEWSINFO -- resopnse:%@",request.responseString);
            if (HTTP_SUCCESS(code))
            {
                
                NSDictionary *msgDict = dictionary[@"msg"];
                
                NSLog(@"新闻msg -- %@",msgDict);
                
                _newscontentStr = msgDict[@"newscontent"];
                NSString *videoURL = msgDict[@"videourl"];

                if(videoURL.length > 0 ){
                    _newscontentStr = [NSString stringWithFormat:@"<video src = '%@'></video>%@",videoURL,_newscontentStr];
                }
                
                [self setUpTableView];

                _headView.newscontentStr = _newscontentStr;
                
                [_tableView reloadData];
            }
        }
            else if (request.tag == TAG_HTTP_GETNEWSCOMMENTLIST)
        {  //获取新闻评论
            {
                NSLog(@"TAG_HTTP_GETNEWSCOMMENTLIST -- resopnse:%@",request.responseString);

                if (HTTP_SUCCESS(code))
                {
                    NSArray *msgArr = dictionary[@"msg"];
                    for(NSDictionary *dict in msgArr)
                    {
                        SP_NewsCommentFrame *frame = [[SP_NewsCommentFrame alloc]init];
                        SP_NewsCommentModel *model = [SP_NewsCommentModel objectWithKeyValues:dict];
                        frame.msgModel = model;
                        [_commentArr addObject:frame];
                }
                    
                    if(_commentArr.count == 20){
                        [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(refreshComment)];                        
                    }
                    
                    [_tableView reloadData];
                    [_tableView.footer endRefreshing];
                    [_tableView.header endRefreshing];

            }
            }
        }
        else if (request.tag == TAG_HTTP_PUBLISHNEWSCOMMENT){  //评论

            if (HTTP_SUCCESS(code)){
                [self showMessage:@"评论成功"];
                _msgTextView.text = @"";
                [_commentArr removeAllObjects];
                [self setHTTPRequest:GETNEWSCOMMENTLIST(@"0", @"20", self.newsList.newsid,GETUSERID) Tag:TAG_HTTP_GETNEWSCOMMENTLIST];
            
            }
        }else if (request.tag == TAG_HTTP_NEWSCOMMENTPRAISE){
            if(HTTP_SUCCESS(code)){
//                _currentPairseBtn.selected = YES;
            }else{
                [self showAlertStr:dictionary[@"msg"]];
            }
        }
        
    }
    
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 64 - 40)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = 0;
    
    _headView = [[SP_NewsDetailsHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    _headView.newsListModel = self.newsList;
    _headView.delegate = self;
    _tableView.tableHeaderView = _headView;
    
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadComment)];
    
    [self.view addSubview:_tableView];

}

- (void) reloadComment
{
    [self setHTTPRequest:GETNEWSCOMMENTLIST(@"0", @"10", self.newsList.newsid,GETUSERID) Tag:TAG_HTTP_GETNEWSCOMMENTLIST];
}


- (void) refreshComment
{
    SP_NewsCommentFrame *frame = [_commentArr lastObject];
    SP_NewsCommentModel *model = frame.msgModel;
    [self setHTTPRequest:GETNEWSCOMMENTLIST(model.newsid, @"10", self.newsList.newsid,GETUSERID) Tag:TAG_HTTP_GETNEWSCOMMENTLIST];
}



- (void)webviewDidFinished:(CGFloat)height
{
    _headView.frame = CGRectMake(0, 0, ScreenWidth, height);
    _tableView.tableHeaderView = _headView;
}

#pragma mark - _tableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _commentArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsCommentCell *cell = [SP_NewsCommentCell cellWithTableView:tableView];
    
    
    
    cell.commentFrame = _commentArr[indexPath.row];
    
    SP_NewsCommentFrame *frame = _commentArr[indexPath.row];
    SP_NewsCommentModel *model = frame.msgModel;

    cell.praiseBtn.tag = [model.newscommentid integerValue];
    cell.delegate = self;
    
    [cell.headButton addTarget:self action:@selector(headButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.headButton.tag = [model.uid integerValue];
    
    return cell;
}

//- (void)praiseBtnDidClick:(UIButton *)btn
//{
//    _currentPairseBtn = btn;
//    NSString *newscommentid = [NSString stringWithFormat:@"%ld",btn.tag];
//    [self setHTTPRequest:NEWSCOMMENTPRAISE(GETUSERID, newscommentid) Tag:TAG_HTTP_NEWSCOMMENTPRAISE];
//}

- (void) praiseBtnIsClick:(NSString *)msgID
{
    [self setHTTPRequest:NEWSCOMMENTPRAISE(GETUSERID, msgID) Tag:TAG_HTTP_NEWSCOMMENTPRAISE];
}
- (void) headButtonDidClick:(UIButton *)btn
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsCommentFrame *frame = _commentArr[indexPath.row];
    return frame.cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 45)];
    view.backgroundColor = UIColorFromRGB(0xffffff);
    
    UIView *topVoew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 5)];
    topVoew.backgroundColor = UIColorFromRGB(0xfafafa);
    [view addSubview:topVoew];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 40, CGRectGetHeight(view.bounds))];
    label.textColor = UIColorFromRGB(0xe74c38);
    label.font = [UIFont systemFontOfSize:15];
    label.text = @"评论";
    [view addSubview:label];
    
    UILabel *commentNum = [[UILabel alloc]initWithFrame:CGRectMake(58, 0, 40, CGRectGetHeight(view.bounds))];
    commentNum.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)_commentArr.count];
    commentNum.font = [UIFont systemFontOfSize:15];
    commentNum.textColor = UIColorFromRGB(0x696969);
    [view addSubview:commentNum];
    return view;

}


#pragma mark - 设置底部的评论视图
- (void) setUpCommentView
{
    _bottomeView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 44, ScreenWidth, 44)];
    _bottomeView.backgroundColor = UIColorFromRGB(0xfafafa);
    
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    
    
    _msgSendBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 70, 8, 54, 30)];
    UIImage *btnImg = [GT_Tool createImageWithColor:UIColorFromRGB(GREEN_COLOR_VALUE)];
    [_msgSendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [_msgSendBtn setBackgroundImage:btnImg forState:UIControlStateNormal];
    [_msgSendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _msgSendBtn.layer.cornerRadius = 5;
    _msgSendBtn.layer.masksToBounds = YES;
    [_msgSendBtn addTarget:self action:@selector(sendBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    _msgTextView = [[UITextField alloc]initWithFrame:CGRectMake(8, 7, CGRectGetMinX(_msgSendBtn.frame) - 10, 30)];
    _msgTextView.placeholder = @"我想说两句";
    _msgTextView.font = [UIFont systemFontOfSize:13];
    _msgTextView.borderStyle = UITextBorderStyleRoundedRect;
    _msgTextView.delegate = self;
    
    
    
    
    [self.view addSubview:_bottomeView];
    [_bottomeView addSubview:topLine];
    [_bottomeView addSubview:_msgSendBtn];
    [_bottomeView addSubview:_msgTextView];
    
    
}

- (void) sendBtnDidClick
{
    NSString *msgStr = [self urlEncodeValue:_msgTextView.text];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:msgStr forKey:@"content"];
    [dict setObject:GETUSERID forKey:@"uid"];
    [dict setObject:self.newsList.newsid forKey:@"newsid"];


    
    [self setHttpPostData:PUBLISHNEWSCOMMENT Dictionary:dict Tag:TAG_HTTP_PUBLISHNEWSCOMMENT];
    
    
    
    NSLog(@"123  -- %@",self.newsList.newsid);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
