//
//  SP_NewsViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_NewsViewController.h"
#import "SP_NewsListCell.h"
#import "SP_PersonHomeViewController.h"
#import "SP_NewsHeadView.h"
#import "SP_NewsList.h"
#import "SP_NewsDetailsViewController.h"
#import "SP_BannerEntity.h"
#import "SP_LoginViewController.h"

@interface SP_NewsViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    SP_NewsHeadView *_headView;
    NSMutableArray *_newsListArr;
    NSString *_type;
    int _currentType;
    NSMutableArray *_bannerArr;
}
@end

@implementation SP_NewsViewController


- (id)init
{
    self = [super init];
    if(self){
        _newsListArr = [[NSMutableArray alloc]init];
        _bannerArr = [[NSMutableArray alloc]init];
        _currentType = 1;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setNavigationBar:@"运动GO"];
    [self addRightButton:nil Target:self Action:@selector(personHomeBtnDidClick) ImageName:@"ProfileIcon" Frame:CGRectMake(20, 29, 24, 23)];

    [self setHTTPRequest:GETBANNER Tag:TAG_HTTP_GETBANNER];
    
    if(GETUSERID){
        [self setHTTPRequest:GETUSERDATA(GETUSERID, GETUSERID) Tag:TAG_HTTP_GETUSERDATA];
    }
    

}


- (void)addRightButton:(NSString *)title Target:(id)target Action:(SEL)action ImageName:(NSString *)imgname Frame:(CGRect)frame
{
    NSString *norStr = [NSString stringWithFormat:@"homepage_news_left_icon.png"];
    NSString *clickStr = [NSString stringWithFormat:@"homepage_news_left_pressed_icon.png"];
    //构造返回item的按钮视图
    //UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    btn.frame = frame;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:clickStr
                                                              ofType:nil]]
                   forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:norStr
                                                              ofType:nil]]
                   forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:btn];
    // [btn release];
}

#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
//        NSLog(@"TAG_HTTP_MATCHLEAGUELIST -- resopnse:%@",request.responseString);
        if(request.tag == TAG_HTTP_GETNEWSLIST){
            
            NSLog(@"TAG_HTTP_GETNEWSLIST -- resopnse:%@",request.responseString);

            if (HTTP_SUCCESS(code))
            {
                NSArray *listArr = dictionary[@"msg"];
                for(NSDictionary *dict in listArr){
                    SP_NewsList *model = [SP_NewsList objectWithKeyValues:dict];
                    [_newsListArr addObject:model];
                }
                
                
                if(listArr.count == 20){
                    [_tableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(addMoreInfo)];
                }
                
                if(listArr.count == 0){
                    [self showMessage:@"没有更多的新闻了"];
                }
                
                [_tableView reloadData];
                [_tableView.header endRefreshing];
                [_tableView.footer endRefreshing];
                
                if(_currentType == 1){
                    _headView.footBtn.selected = YES;
                    _headView.baskBtn.selected = NO;
                }else{
                    _headView.footBtn.selected = NO;
                    _headView.baskBtn.selected = YES;
                }
                
            }
        }else if(request.tag == TAG_HTTP_GETBANNER){
            if(HTTP_SUCCESS(code)){
                NSArray *bannerArr = dictionary[@"msg"];
                
                for(NSDictionary *dict in bannerArr){
                    SP_BannerEntity *model = [SP_BannerEntity objectWithKeyValues:dict];
                    [_bannerArr addObject:model];
                }
                
                
                [self setUpTableView];
                
                
                NSString *type = [NSString stringWithFormat:@"%d",_currentType];
                
                [self asyshowHUDView:WAITTING CurrentView:self];
                [self setHTTPRequest:GETNEWSLIST(@"0", @"20", type) Tag:TAG_HTTP_GETNEWSLIST];
            }
        }else if (request.tag == TAG_HTTP_GETUSERDATA){
            
            if(HTTP_SUCCESS(code)){
//                        NSLog(@"TAG_HTTP_GETUSERDATA -- resopnse:%@",request.responseString);

                NSDictionary *dict = dictionary[@"msg"];
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"nickname"] forKey:USERNAME];
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"headimg"] forKey:USERIMGURL];
                [[NSUserDefaults standardUserDefaults] setObject:dict[@"sex"] forKey:USERSEX];
                
            }
        
        }
    }
}


- (void) personHomeBtnDidClick
{
    if([GETUSERID isEqualToString:@"0"]){
        SP_LoginViewController *logVC = [[SP_LoginViewController alloc]init];
        [[AppDelegate sharedAppDelegate].nav pushViewController:logVC animated:YES];
    }else{
        SP_PersonHomeViewController *vc = [[SP_PersonHomeViewController alloc]init];
        [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
    [self openSliderMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //关闭侧滑菜单
    [self closeSliderMenu];
}


- (void) setUpTableView
{
    _headView = [[SP_NewsHeadView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth, 240)];
    _headView.bannerArr = _bannerArr;
    [_headView.footBtn addTarget:self action:@selector(typeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_headView.baskBtn addTarget:self action:@selector(typeBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, ScreenHeight - 112)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableHeaderView = _headView;
    _tableView.rowHeight = 83;
    _tableView.separatorStyle = 0;
    [_tableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadNewInfo)];

    [self.view addSubview:_tableView];

}

- (void) addMoreInfo
{
    SP_NewsList *model = [_newsListArr lastObject];
    NSString *newsid = model.newsid;
    NSString *type = [NSString stringWithFormat:@"%d",_currentType];
    
    [self setHTTPRequest:GETNEWSLIST(newsid, @"0", type) Tag:TAG_HTTP_GETNEWSLIST];

}

- (void) reloadNewInfo
{
    _headView = nil;
    _tableView = nil;
    [_newsListArr removeAllObjects];
    [_bannerArr removeAllObjects];
    
    [self setHTTPRequest:GETBANNER Tag:TAG_HTTP_GETBANNER];
}




- (void) typeBtnDidClick:(UIButton *)btn
{
    if(btn.tag == 1){
        // 1 足球
        _currentType = 1;
        _headView.footBtn.selected = YES;
        _headView.baskBtn.selected = NO;
        [_newsListArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETNEWSLIST(@"0", @"0", @"1") Tag:TAG_HTTP_GETNEWSLIST];

        
    
    }else if (btn.tag == 0){
        // 0 篮球
        _currentType = 0;
        _headView.footBtn.selected = NO;
        _headView.baskBtn.selected = YES;
        [_newsListArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETNEWSLIST(@"0", @"0", @"0") Tag:TAG_HTTP_GETNEWSLIST];

    }

}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _newsListArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsListCell *cell = [SP_NewsListCell cellWithTableView:tableView];
    SP_NewsList *model = _newsListArr[indexPath.row];
    cell.model = model;
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_NewsDetailsViewController *vc = [[SP_NewsDetailsViewController alloc]init];
    SP_NewsList *model = _newsListArr[indexPath.row];
    vc.newsList = model;
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}



@end
