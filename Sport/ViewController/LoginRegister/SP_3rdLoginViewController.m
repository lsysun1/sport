//
//  SP_3rdLoginViewController.m
//  Sport
//
//  Created by 李松玉 on 15/8/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_3rdLoginViewController.h"
#import "SP_RegisterViewController.h"
#import "SP_TabBarViewController.h"


//主界面
#import "MMDrawerController.h"
#import "SP_TabBarViewController.h"
#import "SP_RightViewController.h"


@interface SP_3rdLoginViewController ()

@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UITextField *tel;

@property (weak, nonatomic) IBOutlet UITextField *pwd;

@property (weak, nonatomic) IBOutlet UIButton *regBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UIButton *bindBtn;

@end

@implementation SP_3rdLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     UIColor *color = [UIColor whiteColor];
    self.tel.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"手机号" attributes:@{NSForegroundColorAttributeName: color}];
    self.pwd.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"密 码" attributes:@{NSForegroundColorAttributeName: color}];

    [self.backBtn addTarget:self action:@selector(backBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.regBtn addTarget:self action:@selector(regBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bindBtn addTarget:self action:@selector(bindBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    if([self.type isEqualToString:@"qq"]){
        [dict setObject:self.uid forKey:@"qqopenid"];
        self.typeLabel.text = @"QQ登录成功";
    }else{
        self.typeLabel.text = @"微博登录成功";
        [dict setObject:self.uid forKey:@"weiboopenid"];
    }
    [self setHttpPostData:OTHERLOGIN Dictionary:dict Tag:TAG_HTTP_OTHERLOGIN];

    NSURL *url = [NSURL URLWithString:self.imgUrl];
    [self.headImg sd_setImageWithURL:url];
    
    self.headImg.layer.cornerRadius = 30;
    self.headImg.layer.masksToBounds = YES;
    
}

#pragma mark 绑定按钮
- (void)bindBtnDidClick
{
    if (![GT_Tool Isphonenumber:self.tel.text]) {
        
        //  [self showMessage:@"请输入正确的手机号"];
        
        [self showAlertStr:@"请输入正确的手机号"];
        
        return;
    }
    
    if(self.pwd.text.length == 0){
        [self showAlertStr:@"密码不能为空"];
        return;
    }
    
    
    NSString *userName = self.tel.text;
    NSString *passWord = self.pwd.text;
    NSString *udid = [GT_Tool uuid];
    
    
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:LOGIN(userName, passWord,udid) Tag:TAG_HTTP_LOGIN];

}


- (void)requestFinished:(ASIHTTPRequest *)request{
    
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSLog(@"TAG_HTTP_OTHERLOGIN:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if (request.tag == TAG_HTTP_OTHERLOGIN){
            if(HTTP_SUCCESS(code)){
            
//                NSLog(@"TAG_HTTP_OTHERLOGIN:%@",request.responseString);

            
            }
            
        }else if (request.tag == TAG_HTTP_LOGIN){
            if(HTTP_SUCCESS(code)){
                                NSLog(@"TAG_HTTP_OTHERLOGIN:%@",request.responseString);
                NSDictionary *dict = dictionary[@"msg"];
                NSString *uid = dict[@"uid"];
                
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
                [postDict setObject:uid forKey:@"uid"];
                [[NSUserDefaults standardUserDefaults] setObject:uid forKey:USERID];
                [[NSUserDefaults standardUserDefaults] setObject:self.tel.text forKey:USERTEL];

                if([self.type isEqualToString:@"qq"]){
                    [postDict setObject:self.uid forKey:@"qqopenid"];
                }else{
                    [postDict setObject:self.uid forKey:@"weiboopenid"];
                }
                
                [self setHttpPostData:OTHERLOGINBIND Dictionary:postDict Tag:TAG_HTTP_OTHERLOGINBIND];
                
            }
        
        }else if (request.tag == TAG_HTTP_OTHERLOGINBIND){
            if(HTTP_SUCCESS(code)){

                                
                [self pushToHomeViewController];

            
            }
        
        }
        
    }
}



- (void) backBtnDidClick
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) regBtnDidClick
{
    SP_RegisterViewController *vc = [[SP_RegisterViewController alloc]init];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/**
 *  跳转到主界面
 */
- (void) pushToHomeViewController
{
    SP_TabBarViewController *tabBar = [[SP_TabBarViewController alloc]init];
    [AppDelegate sharedAppDelegate].nav = [[UINavigationController alloc]initWithRootViewController:tabBar];
    SP_RightViewController * rightVC = [[SP_RightViewController alloc] init];
    
    
    [AppDelegate sharedAppDelegate].drawerController = [[MMDrawerController alloc]
                                                        initWithCenterViewController:[AppDelegate sharedAppDelegate].nav
                                                        leftDrawerViewController:nil
                                                        rightDrawerViewController:rightVC];
    [[AppDelegate sharedAppDelegate].drawerController setShowsShadow:YES];
    [[AppDelegate sharedAppDelegate].drawerController setRestorationIdentifier:@"MMDrawer"];
    [[AppDelegate sharedAppDelegate].drawerController setMaximumRightDrawerWidth:130.0];
    [[AppDelegate sharedAppDelegate].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [[AppDelegate sharedAppDelegate].drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [[AppDelegate sharedAppDelegate].drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         UIViewController * sideDrawerViewController;
         if(drawerSide == MMDrawerSideLeft){
             sideDrawerViewController = drawerController.leftDrawerViewController;
         }
         else if(drawerSide == MMDrawerSideRight){
             sideDrawerViewController = drawerController.rightDrawerViewController;
         }
         [sideDrawerViewController.view setAlpha:percentVisible];
     }];
    
    [self.navigationController pushViewController:[AppDelegate sharedAppDelegate].drawerController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
