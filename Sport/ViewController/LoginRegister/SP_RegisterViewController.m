//
//  SP_RegisterViewController.m
//  Sport
//
//  Created by WT_lyy on 15/4/22.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_RegisterViewController.h"



@interface SP_RegisterViewController ()<UITextFieldDelegate>{

    __weak IBOutlet UIView *_viewInput;
    
    __weak IBOutlet UITextField *_textfTel;
    __weak IBOutlet UITextField *_textPwd;
    __weak IBOutlet UITextField *_textfCode;
    __weak IBOutlet UIButton *_authBtn;
}

@end

@implementation SP_RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackButton];
    
    if (iPhone4) {
        
        _viewInput.frame = CGRectMake(0, 120, ScreenWidth, 284);
    }
    
    [_textfTel setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_textPwd setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_textfCode setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];

    
}
#pragma -mark UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (iPhone4) {
        _viewInput.frame = CGRectMake(0, 60, ScreenWidth, 284);
    }
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma -mark UI Action
- (IBAction)backClick:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)getVerifyClick:(id)sender {
    
    if(_textfTel.text.length == 11){
        [self setDynamicCountButton:_authBtn Total:60 Gap:1];
        [self asyshowHUDView:WAITTING CurrentView:self];

        [self setHTTPRequest:VELIDATECODE(_textfTel.text) Tag:TAG_HTTP_CODE];
    
    }else{
        [self showMessage:@"手机号码格式不正确"];
    }
}


- (IBAction)registerClick:(id)sender {
    
    if(_textfTel.text.length != 11){
        [self showAlertStr:@"手机号码格式不正确"];
        return;
    }
    
    if(_textPwd.text.length < 6){
        [self showAlertStr:@"密码不能小于六位"];
        return;
    }

    
    if(_textfCode.text.length != 6){
        [self showAlertStr:@"验证码格式不正确"];
        return;
    }
    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:REGISTER(_textfTel.text, _textPwd.text, _textfCode.text) Tag:TAG_HTTP_REGISTER];

   
}




- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
//        NSLog(@"resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_CODE){
            if (HTTP_SUCCESS(code))
            {
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
            }
        }else if(request.tag == TAG_HTTP_REGISTER){
            if (HTTP_SUCCESS(code))
            {
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}


/**
 *  重置发送验证码按钮
 */
- (void) setDynamicCountButton:(UIButton *)button Total:(NSInteger)total Gap:(NSTimeInterval)gap
{
    button.enabled = !button.enabled;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 1; i <= total; i++) {
            [NSThread sleepForTimeInterval:gap];
            long j = total - i;
            if (j > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [button setTitle:[NSString stringWithFormat:@"%ld秒后重发", j] forState:UIControlStateNormal];
                });
            }
            else {
                button.enabled = !button.enabled;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_authBtn setTitleColor:UIColorFromRGB(0x2d8bfe) forState:UIControlStateNormal];
                    [button setTitle:@"重新发送" forState:UIControlStateNormal];
                });
            }
        }
    });
}


/**
 *  点击当前视图上的空白部分,关闭键盘
 */
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}





@end
