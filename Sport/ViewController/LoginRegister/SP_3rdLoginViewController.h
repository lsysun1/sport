//
//  SP_3rdLoginViewController.h
//  Sport
//
//  Created by 李松玉 on 15/8/14.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@interface SP_3rdLoginViewController : BaseViewController

@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *imgUrl;
@property (nonatomic,copy) NSString *type;


@end
