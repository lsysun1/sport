//
//  SP_LoginViewController.m
//  Sport
//
//  Created by WT_lyy on 15/4/22.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_LoginViewController.h"
#import "SP_RegisterViewController.h"

//主界面
#import "MMDrawerController.h"
#import "SP_TabBarViewController.h"
#import "SP_RightViewController.h"


//test
#import "SP_PersonHomeViewController.h"
#import "SP_CreatTeamViewController.h"
#import "SP_EditPersonInfoViewController.h"
#import "SP_AddFriendViewController.h"
#import "SP_3rdLoginViewController.h"

#import <ShareSDK/ShareSDK.h>
#import "WeiboSDK.h"


@interface SP_LoginViewController ()<UITextFieldDelegate>{

    __weak IBOutlet UITextField *_textfUsername;
    
    __weak IBOutlet UITextField *_textPassw;
    __weak IBOutlet UIButton *backBtn;
    
    NSMutableDictionary *myInfo;
    
    BOOL _isStart;
    
}

@end

@implementation SP_LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackButton];
    


}
- (IBAction)backBtnDidClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [_textfUsername resignFirstResponder];
    
    [_textPassw resignFirstResponder];
}

#pragma -mark UI Action
- (IBAction)loginClick:(id)sender {
    
    if (![GT_Tool Isphonenumber:_textfUsername.text]) {
        
      //  [self showMessage:@"请输入正确的手机号"];
        
        [self showAlertStr:@"请输入正确的手机号"];
        
        return;
    }
    
    if(_textPassw.text.length == 0){
        [self showAlertStr:@"密码不能为空"];
        return;
    }
    
    
    NSString *userName = _textfUsername.text;
    NSString *passWord = _textPassw.text;
    NSString *udid = [GT_Tool uuid];
    

    [self asyshowHUDView:WAITTING CurrentView:self];
    [self setHTTPRequest:LOGIN(userName, passWord,udid) Tag:TAG_HTTP_LOGIN];

    
    
    
    
//    SP_PersonHomeViewController * resVctrl = [[SP_PersonHomeViewController
//                                              alloc] init];
//    
//    [self.navigationController pushViewController:resVctrl animated:YES];


    
//    //写信历史记录
//    NSMutableDictionary *httpdic = [[NSMutableDictionary alloc] init];
//    [httpdic setObject:[NSString stringWithInteger:self.famousperId] forKey:@"famouspersonid"];
//    [httpdic setObject:userid forKey:@"userid"];
//    NSString *jsontext = [UFO_Tool sendJson:httpdic];
//    NSString *address = [NSString stringWithFormat:@"%@itemlist/famouschat",URL_BASE];
//    [self sethttpRequestFormat:address Tag:0 Data:[jsontext dataUsingEncoding:NSUTF8StringEncoding]];
}

- (IBAction)forgetPsClick:(id)sender {
}


- (IBAction)registerClick:(id)sender {
    
    SP_RegisterViewController * resVctrl = [[SP_RegisterViewController alloc] init];
    
    [self.navigationController pushViewController:resVctrl animated:YES];
    
}

- (IBAction)thirdLoginClick:(UIButton *)sender {
    
    if (1 == sender.tag) {//QQ 登录
        [ShareSDK getUserInfoWithType:ShareTypeQQSpace authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
            if (result) {
                NSLog(@"uid = %@",[userInfo uid]);
                NSLog(@"name = %@",[userInfo nickname]);
                NSLog(@"icon = %@",[userInfo profileImage]);
                
                NSString *oid = [userInfo uid];
                NSString *thirdParty = @"qq";
                if (nil == myInfo) {
                    myInfo = [[NSMutableDictionary alloc] init];
                }
                [myInfo setObject:oid forKey:@"openid"];
                [myInfo setObject:[userInfo profileImage] forKey:@"img"];

                [myInfo setObject:thirdParty forKey:@"thirdparty"];
                
                NSLog(@"123123");
                
                 NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[userInfo uid] forKey:@"qqopenid"];
                [self setHttpPostData:OTHERLOGIN Dictionary:dict Tag:TAG_HTTP_OTHERLOGIN];
            }
            else {
                NSLog(@"error code: %ld", (long)[error errorCode]);
            }
        }];

    
    }else if (2 == sender.tag){//weixin 登录
    
        
    }else if (3 == sender.tag){//sina 登录
        

        
        [ShareSDK getUserInfoWithType:ShareTypeSinaWeibo authOptions:nil result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
            if (result) {
                NSLog(@"uid = %@",[userInfo uid]);
                NSLog(@"name = %@",[userInfo nickname]);
                NSLog(@"icon = %@",[userInfo profileImage]);
                
                NSString *oid = [userInfo uid];
                NSString *thirdParty = @"weibo";
                if (nil == myInfo) {
                    myInfo = [[NSMutableDictionary alloc] init];
                }
                [myInfo setObject:oid forKey:@"openid"];
                [myInfo setObject:[userInfo profileImage] forKey:@"img"];
                [myInfo setObject:thirdParty forKey:@"thirdparty"];
                
                
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setObject:[userInfo uid] forKey:@"weiboopenid"];
                [self setHttpPostData:OTHERLOGIN Dictionary:dict Tag:TAG_HTTP_OTHERLOGIN];
            }
            else {
                NSLog(@"error code: %ld", (long)[error errorCode]);
            }
        }];
    }
}


- (void)requestFinished:(ASIHTTPRequest *)request{
    
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
//        NSLog(@"resopnse:%@",request.responseString);
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_LOGIN){
            if (HTTP_SUCCESS(code))
            {
                NSDictionary *msg = dictionary[@"msg"];
                NSString *uid = msg[@"uid"];
                NSLog(@"uid -- %@",uid);
                
                [[NSUserDefaults standardUserDefaults] setObject:uid forKey:USERID];
                [[NSUserDefaults standardUserDefaults] setObject:_textfUsername.text forKey:USERTEL];

                
                
                [self pushToHomeViewController];
            }
        }else if (request.tag == TAG_HTTP_OTHERLOGIN){

            if(HTTP_SUCCESS(code)){
                NSDictionary *dict = dictionary[@"msg"];
                NSString *uid = dict[@"uid"];
                
                NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
                [postDict setObject:uid forKey:@"uid"];
                [[NSUserDefaults standardUserDefaults] setObject:uid forKey:USERID];
                [[NSUserDefaults standardUserDefaults] setObject:_textfUsername.text forKey:USERTEL];


                [self pushToHomeViewController];

            }else if (code == 300){
                SP_3rdLoginViewController *vc = [[SP_3rdLoginViewController alloc]init];
                vc.uid = myInfo[@"openid"];
                vc.imgUrl = myInfo[@"img"];
                vc.type =  myInfo[@"thirdparty"];
                [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];

            }
        
        }
    
    }
}






/**
 *  跳转到主界面
 */
- (void) pushToHomeViewController
{
    SP_TabBarViewController *tabBar = [[SP_TabBarViewController alloc]init];
    [AppDelegate sharedAppDelegate].nav = [[UINavigationController alloc]initWithRootViewController:tabBar];
    SP_RightViewController * rightVC = [[SP_RightViewController alloc] init];
    
    
    [AppDelegate sharedAppDelegate].drawerController = [[MMDrawerController alloc]
                                                        initWithCenterViewController:[AppDelegate sharedAppDelegate].nav
                                                        leftDrawerViewController:nil
                                                        rightDrawerViewController:rightVC];
    [[AppDelegate sharedAppDelegate].drawerController setShowsShadow:YES];
    [[AppDelegate sharedAppDelegate].drawerController setRestorationIdentifier:@"MMDrawer"];
    [[AppDelegate sharedAppDelegate].drawerController setMaximumRightDrawerWidth:130.0];
    [[AppDelegate sharedAppDelegate].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [[AppDelegate sharedAppDelegate].drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [[AppDelegate sharedAppDelegate].drawerController
     setDrawerVisualStateBlock:^(MMDrawerController *drawerController, MMDrawerSide drawerSide, CGFloat percentVisible) {
         UIViewController * sideDrawerViewController;
         if(drawerSide == MMDrawerSideLeft){
             sideDrawerViewController = drawerController.leftDrawerViewController;
         }
         else if(drawerSide == MMDrawerSideRight){
             sideDrawerViewController = drawerController.rightDrawerViewController;
         }
         [sideDrawerViewController.view setAlpha:percentVisible];
     }];
    
    [self.navigationController pushViewController:[AppDelegate sharedAppDelegate].drawerController animated:YES];
}


@end
