//
//  SP_ZoneViewController.m
//  Sport
//
//  Created by 李松玉 on 15/5/11.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZoneViewController.h"
#import "SP_ZoneContent.h"
#import "SP_ZoneContentFrame.h"
#import "SP_ZoneMainCell.h"
#import "SP_ZoneCommentCell.h"
#import "SP_ZoneCommentFrame.h"
#import "SP_CommentBtn.h"
#import "PhotoChoiceView.h"
#import "SP_ZonePostViewController.h"
#import "SP_ZoneCommentEntiry.h"
#import "SP_CommentTextSpecial.h"
#import "SP_SendMsgViewController.h"
#import "SP_UerHomeViewController.h"
#import "SP_LoginViewController.h"
#import "SP_ZanCell.h"
#import "UIImageView+WebCache.h"
#import <ShareSDK/ShareSDK.h>



@interface SP_ZoneViewController () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,PhotoChoiceViewDelegate,SP_CommentTextViewDelegate>
{
    
    
    __weak IBOutlet UIView *_topHeadView;
    
    
    UIView *_topSwitchView;
    __weak IBOutlet UIView *_itemView;
    __weak IBOutlet UIButton *_focusBtn;
    __weak IBOutlet UIButton *_cityBtn;
    __weak IBOutlet UIButton *_radomBtn;
    
    __weak IBOutlet UIImageView *topBgView;
    __weak IBOutlet UIImageView *headImgView;
    __weak IBOutlet UILabel *userLabel;
    
    
    UIImageView *_itemLine;
    UIButton *_selectedBtn;
    
    
    
    UITableView *_mainTableView;
    NSMutableArray *_contentArr;
    NSMutableArray *_commentArr;
    NSMutableArray *_hfArr;
    
    int _currentType;       // 0 是关注  1 是同城  2 是随便看看
    
    int _refresh;
    
    CGFloat _offsetY;
    
    NSString *_currentPyqID;
    NSString *_currentPyqplid;
    NSIndexPath *_currentIndex;
    
    SP_CommentBtn *_currentZanBtn;
    
}
@end

@implementation SP_ZoneViewController

- (id) init
{
    self = [super init];
    if(self){
        _contentArr = [[NSMutableArray alloc]init];
        _hfArr = [[NSMutableArray alloc]init];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar:@"圈子"];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setUpTopHeadView];
    [self setUpItemView];
    [self setUpMainTableView];
    _refresh = 0;
    [self setHTTPRequest:GETPYQLIST(GETUSERID, @"0", @"10") Tag:TAG_HTTP_GETPYQLIST];
    [self addRightButton:nil Target:self Action:@selector(postShuoShuoBtnDidClick) ImageName:@"发布图标" Frame:CGRectMake(ScreenWidth - 40, 29, 24, 23)];
    _currentType = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iAmBackHaHa) name:@"iAmBack" object:nil];
    
    
}


- (void)iAmBackHaHa
{
    [self setHTTPRequest:GETCOMMENTLIST(_currentPyqID, @"0", @"10") Tag:666];
 
}


#pragma mark - 发布说说按钮点击方法
- (void) postShuoShuoBtnDidClick
{
//    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 48)];
//    under.delegate = self;
//    [self.view addSubview:under];
    SP_ZonePostViewController *vc = [[SP_ZonePostViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)photoChoice:(UIImage *)image
{
//    _teamImgView.image = image;
}


#pragma mark - 数据请求
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        if(request.tag == TAG_HTTP_GETPYQLIST || request.tag == TAG_HTTP_GETRANDPYQ || request.tag == TAG_HTTP_GETCITYPYQ){
            if (HTTP_SUCCESS(code))
            {
//                NSLog(@"TAG_HTTP_GETPYQLIST -- resopnse:%@",request.responseString);

                if(_refresh == 0){
                    [_contentArr removeAllObjects];
                }
                
                NSArray *msgArr = dictionary[@"msg"];
                for(NSDictionary *dict in msgArr){
                    SP_ZoneContentFrame *frame = [[SP_ZoneContentFrame alloc]init];
                    SP_ZoneContent *model = [SP_ZoneContent objectWithKeyValues:dict];
                    frame.zoneContent = model;
                    [_contentArr addObject:frame];
                    
                    NSLog(@"model.pyqid -- %@  --- %@",model.pyqid,model.content);
                    [self setHTTPRequest:GETCOMMENTLIST(model.pyqid, @"0", @"999") Tag:TAG_HTTP_GETCOMMENTLIST];
                }
                
                if(_contentArr.count == 10){
                    [_mainTableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                }
                
                [_mainTableView.header endRefreshing];
                [_mainTableView.footer endRefreshing];
                
                [_mainTableView reloadData];
            }
        }else if (request.tag == TAG_HTTP_GETCOMMENTLIST){  //评论列表
            if(HTTP_SUCCESS(code)){
            
                NSArray *commentArr = dictionary[@"msg"];
                
                NSMutableArray *allComment = [NSMutableArray array];    //每条圈子信息下面的所有评论数组

                for(NSDictionary *dict in commentArr){
                    
                    //请求中每条评论数据
                    SP_ZoneCommentFrame *commentFrame = [[SP_ZoneCommentFrame alloc]init];
                    SP_ZoneCommentEntiry *commentEntiry = [SP_ZoneCommentEntiry objectWithKeyValues:dict];
                    commentFrame.commentEntiry = commentEntiry;
                    
                    //将评论数据添加到allComment所有评论数组
                    [allComment addObject:commentFrame];
                    
                    //每条请求数据当中包含了一个hfdata数组,是回复评论的数组
                    if(commentEntiry.hfdata.count > 0){
                        
                        for(NSDictionary * hfDict in commentEntiry.hfdata){
                            SP_ZoneCommentFrame *commentFrame = [[SP_ZoneCommentFrame alloc]init];
                            SP_ZoneCommentEntiry *commentEntiry = [SP_ZoneCommentEntiry objectWithKeyValues:hfDict];
                            commentFrame.commentEntiry = commentEntiry;
                            
                            //将回复数据添加到allComment所有评论数组
                            [allComment addObject:commentFrame];
                        }
                    }
                }
                
                //经过上面的步骤，已经获得一个同时包含评论数据和回复数据的数组allComment
                //现在需要将这个数组赋给pyqid对应的朋友圈信息
            
                if(allComment.count > 0) {   //当allComment中的数据个数大于0时，表示这个朋友圈信息有评论,则将这个数组赋给对应的朋友圈信息模型
                    
                    //取到回复的pyqid
                    SP_ZoneCommentFrame *commentFrame = allComment[0];
                    SP_ZoneCommentEntiry *commentEntiry = commentFrame.commentEntiry;
                    NSString *pyqid = commentEntiry.pyqid;

                    
                    for(SP_ZoneContentFrame *frame in _contentArr){
                        SP_ZoneContent *content = frame.zoneContent;
                        
                        if([content.pyqid isEqualToString:pyqid]){
                            
                            frame.commentArr = allComment;
                        }
                        
                    }
                    
                }

                [_mainTableView reloadData];

            }
        }else if (request.tag == TAG_HTTP_PUBLISHCOMMENT){  //发布评论
            if(HTTP_SUCCESS(code)){
                
                [self setHTTPRequest:GETCOMMENTLIST(_currentPyqID, @"0", @"999") Tag:666];
                
                
            }
        }else if (request.tag == 666){  //发布完后重新刷新评论
            
            SP_ZoneContentFrame *frameModel = _contentArr[_currentIndex.section];
            frameModel.commentArr = nil;
            
            if(HTTP_SUCCESS(code)){
                
                NSArray *commentArr = dictionary[@"msg"];
                
                NSMutableArray *allComment = [NSMutableArray array];    //每条圈子信息下面的所有评论数组
                for(NSDictionary *dict in commentArr){
                    
                    //请求中每条评论数据
                    SP_ZoneCommentFrame *commentFrame = [[SP_ZoneCommentFrame alloc]init];
                    SP_ZoneCommentEntiry *commentEntiry = [SP_ZoneCommentEntiry objectWithKeyValues:dict];
                    commentFrame.commentEntiry = commentEntiry;
                    
                    //将评论数据添加到allComment所有评论数组
                    [allComment addObject:commentFrame];
                    
                    //每条请求数据当中包含了一个hfdata数组,是回复评论的数组
                    if(commentEntiry.hfdata.count > 0){
                        
                        for(NSDictionary * hfDict in commentEntiry.hfdata){
                            SP_ZoneCommentFrame *commentFrame = [[SP_ZoneCommentFrame alloc]init];
                            SP_ZoneCommentEntiry *commentEntiry = [SP_ZoneCommentEntiry objectWithKeyValues:hfDict];
                            commentFrame.commentEntiry = commentEntiry;
                            
                            //将回复数据添加到allComment所有评论数组
                            [allComment addObject:commentFrame];
                        }
                    }
                }
                
                //经过上面的步骤，已经获得一个同时包含评论数据和回复数据的数组allComment
                //现在需要将这个数组赋给pyqid对应的朋友圈信息
                
                if(allComment.count > 0) {   //当allComment中的数据个数大于0时，表示这个朋友圈信息有评论,则将这个数组赋给对应的朋友圈信息模型
                    
                    frameModel.commentArr = allComment;

                }
                frameModel.commentArr = allComment;
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:_currentIndex.section];
                [_mainTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];

//                [_mainTableView reloadData];

            }
        }else if (request.tag == TAG_HTTP_PYQZHAN){
            NSLog(@"TAG_HTTP_PYQZHAN -- resopnse:%@",request.responseString);

            if(HTTP_SUCCESS(code)){
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
                switch (_selectedBtn.tag) {
                    case 0:
                    {
                        _currentType = 0;
                        [_contentArr removeAllObjects];
                        [self asyshowHUDView:WAITTING CurrentView:self];
                        [self setHTTPRequest:GETPYQLIST(GETUSERID, @"0", @"10") Tag:TAG_HTTP_GETPYQLIST];
                    }
                        break;
                    case 1:
                    {
                        _currentType = 1;
                        [_contentArr removeAllObjects];
                        
                        NSString *location = [self urlEncodeValue:GETSELECTEDCITYNAME];
                        [self asyshowHUDView:WAITTING CurrentView:self];
                        [self setHTTPRequest:GETCITYPYQ(GETUSERID, @"0", @"10", location) Tag:TAG_HTTP_GETCITYPYQ];
                        
                    }
                        break;
                    case 2:
                    {
                        _currentType = 2;
                        [_contentArr removeAllObjects];
                        [self asyshowHUDView:WAITTING CurrentView:self];
                        [self setHTTPRequest:GETRANDPYQ(GETUSERID, @"20") Tag:TAG_HTTP_GETRANDPYQ];
                    }
                        break;
                    default:
                        break;
                }

            }else{
                NSString *msg = dictionary[@"msg"];
                [self showAlertStr:msg];
            }
        }
    }
    
}



#pragma mark - 顶部头像View
- (void) setUpTopHeadView
{
    _topHeadView.frame = CGRectMake(0, 64, ScreenWidth, 120);
    topBgView.frame = CGRectMake(0, 0, ScreenWidth, 120);;
    headImgView.frame = CGRectMake(ScreenWidth - 100, 31, 70, 70);
    headImgView.layer.cornerRadius = 35;
    headImgView.layer.masksToBounds = YES;
    headImgView.layer.borderWidth = 2;
    userLabel.frame = CGRectMake(CGRectGetMinX(headImgView.frame)-220, 71, 200, 30);
    
    
    NSURL *imgUrl = [NSURL URLWithString:GETUSERIMGURL];
    [headImgView sd_setImageWithURL:imgUrl];
    
    
    if ([GETUSERSEX isEqualToString:@"0"]) {
        headImgView.layer.borderColor = [UIColorFromRGB(0xff5999)CGColor];
    }else{
        headImgView.layer.borderColor = [UIColor blueColor].CGColor;
    }
    
    userLabel.text = GETUSERNAME;

}




#pragma mark - 顶部选择菜单
- (void) setUpItemView
{
    _itemView.frame = CGRectMake(0, 64, ScreenWidth, 38);
    _itemView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    
    _focusBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _cityBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _radomBtn.titleLabel.textAlignment = NSTextAlignmentCenter;

    
    _cityBtn.frame = CGRectMake(ScreenWidth/2 -35 , 4, 70, 30);
    CGFloat raceMinX = CGRectGetMinX(_cityBtn.frame);
    CGFloat raceMaxX = CGRectGetMaxX(_cityBtn.frame);
    
    _focusBtn.frame = CGRectMake(raceMinX - 110, 4, 70, 30);
    _radomBtn.frame = CGRectMake(raceMaxX + 40, 4, 70, 30);
    
    [_focusBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_cityBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_radomBtn addTarget:self action:@selector(menuBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _focusBtn.selected = YES;
    _selectedBtn = _focusBtn;
    
    UIImage *lineImg = [GT_Tool createImageWithColor:UIColorFromRGB(0xfd7f45)];
    CGFloat activeMinX = CGRectGetMinX(_focusBtn.frame);
    CGFloat activeMaxY = CGRectGetMaxY(_focusBtn.frame);
    _itemLine = [[UIImageView alloc]init];
    _itemLine.frame = CGRectMake(activeMinX +1, activeMaxY , 70, 4);
    _itemLine.image = lineImg;
    [_itemView addSubview:_itemLine];
}

#pragma mark 顶部选择菜单按钮点击方法
- (void) menuBtnDidClick:(UIButton *)btn
{
    //关注Btn的Tag是0
    //同城Btn的Tag是1
    //随便Btn的Tag是2
    
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
    
    
    
    
    
    CGFloat activeMinX = CGRectGetMinX(_focusBtn.frame);
    CGFloat activeMaxY = CGRectGetMaxY(_focusBtn.frame);
    CGFloat raceMinX = CGRectGetMinX(_cityBtn.frame);
    CGFloat groundMinX = CGRectGetMinX(_radomBtn.frame);
    
    CGRect lineOneRect = CGRectMake(activeMinX +1, activeMaxY , 70, 4);
    CGRect lineTwoRect = CGRectMake(raceMinX +1, activeMaxY , 70, 4);
    CGRect lineThreeRect = CGRectMake(groundMinX +1, activeMaxY , 70, 4);
    
    switch (btn.tag) {
        case 0:
        {
            _currentType = 0;
            _itemLine.frame = lineOneRect;
            [_contentArr removeAllObjects];
            [self asyshowHUDView:WAITTING CurrentView:self];
            [self setHTTPRequest:GETPYQLIST(GETUSERID, @"0", @"10") Tag:TAG_HTTP_GETPYQLIST];
        }
            break;
        case 1:
        {
            _currentType = 1;
            _itemLine.frame = lineTwoRect;
            [_contentArr removeAllObjects];
            
            NSString *location = [self urlEncodeValue:GETCITYNAME];
            [self asyshowHUDView:WAITTING CurrentView:self];
            [self setHTTPRequest:GETCITYPYQ(GETUSERID, @"0", @"10", location) Tag:TAG_HTTP_GETCITYPYQ];
            
        }
            break;
        case 2:
        {
            _currentType = 2;
            _itemLine.frame = lineThreeRect;
            [_contentArr removeAllObjects];
            [self asyshowHUDView:WAITTING CurrentView:self];
            [self setHTTPRequest:GETRANDPYQ(GETUSERID, @"20") Tag:TAG_HTTP_GETRANDPYQ];
        }
            break;
        default:
            break;
    }
    
}


- (void) setUpMainTableView
{
    _mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_itemView.frame), ScreenWidth, ScreenHeight - CGRectGetMaxY(_itemView.frame) - 48)];
    _mainTableView.delegate = self;
    _mainTableView.dataSource = self;
    _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_mainTableView addLegendHeaderWithRefreshingTarget:self refreshingAction:@selector(reloadNewData)];
//    [_mainTableView addLegendFooterWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    
    
    [self.view addSubview:_mainTableView];

}


#pragma mark 下拉重新刷新数据
- (void) reloadNewData
{
    _refresh = 0;
    if(_currentType == 0){

        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETPYQLIST(GETUSERID, @"0", @"10") Tag:TAG_HTTP_GETPYQLIST];
    }else if (_currentType == 1){

        NSString *location = [self urlEncodeValue:GETSELECTEDCITYNAME];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETCITYPYQ(GETUSERID, @"0", @"10", location) Tag:TAG_HTTP_GETCITYPYQ];
    }else if (_currentType == 2){

        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETRANDPYQ(GETUSERID, @"20") Tag:TAG_HTTP_GETRANDPYQ];
    }

}


#pragma mark 上拉加载更多数据
- (void) loadMoreData
{
    _refresh = 1;
    SP_ZoneContentFrame *frameModel = [_contentArr lastObject];
    SP_ZoneContent *model = frameModel.zoneContent;
    NSString *pyqid = model.pyqid;
    
    if(_currentType == 0){
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETPYQLIST(GETUSERID, pyqid, @"10") Tag:TAG_HTTP_GETPYQLIST];
    }else if (_currentType == 1){
        NSString *location = [self urlEncodeValue:GETCITYNAME];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETCITYPYQ(GETUSERID, pyqid, @"10", location) Tag:TAG_HTTP_GETCITYPYQ];
    }else if (_currentType == 2){
        [_contentArr removeAllObjects];
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHTTPRequest:GETRANDPYQ(GETUSERID, @"20") Tag:TAG_HTTP_GETRANDPYQ];
    }


}




#pragma mark - _mainTableView Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _contentArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SP_ZoneContentFrame *frame = _contentArr[section];
    
    return frame.commentArr.count + 1;
;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_ZoneContentFrame *frameModel = _contentArr[indexPath.section];
    SP_ZoneContent *model = frameModel.zoneContent;

    
    if(indexPath.row == 0){
        SP_ZoneMainCell *cell = [SP_ZoneMainCell cellWithTableView:tableView];
        cell.vc = self;
        
        cell.headButton.tag = [model.uid integerValue];
        [cell.headButton addTarget:self action:@selector(headBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];

        cell.commentBtn.pyqid = model.pyqid;
        cell.commentBtn.userName = model.nickname;
        cell.commentBtn.index = indexPath;
        [cell.commentBtn addTarget:self action:@selector(commentBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    
        cell.shareBtn.tag = indexPath.row;
        [cell.shareBtn addTarget:self action:@selector(shareBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.zanBtn.pyqid = model.pyqid;
        cell.zanBtn.userName = model.nickname;
        cell.zanBtn.index = indexPath;
        [cell.zanBtn addTarget:self action:@selector(zanBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        cell.frameModel = frameModel;
        
        return cell;
    }else{
        SP_ZoneCommentCell *cell = [SP_ZoneCommentCell cellWithTableView:tableView];
        
        cell.indexNum = indexPath;
        cell.commentTextView.nameDelagate = self;
        
        SP_ZoneContentFrame *frameModel = _contentArr[indexPath.section];
        
        SP_ZoneCommentFrame *commentFrame = frameModel.commentArr[indexPath.row - 1];
        commentFrame.index = indexPath;
        
        SP_ZoneCommentEntiry *commentEntiry = commentFrame.commentEntiry;
        
        commentFrame.commentEntiry = commentEntiry;
        
        cell.frameModel = commentFrame;
        
        cell.commentTextView.backgroundColor = UIColorFromRGB(0xe5e5e5);
        
        return cell;

    }

    
    
    return nil;
}

- (void)shareBtnDidClick:(UIButton *)btn
{

    SP_ZoneContentFrame *frameModel = _contentArr[btn.tag];
    SP_ZoneContent *model = frameModel.zoneContent;
    
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:model.content
                                       defaultContent:@"运动GO"
                                                image:nil
                                                title:model.content
                                                  url:nil                                          description:model.content
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:self.view arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                }
                            }];


}


- (void)headBtnDidClick:(UIButton *)btn
{
    SP_UerHomeViewController *vc = [[SP_UerHomeViewController alloc]init];
    vc.touid = [NSString stringWithFormat:@"%ld",btn.tag];
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SP_ZoneContentFrame *frame = _contentArr[indexPath.section];

    if(indexPath.row == 0){
        return frame.cellHeight;
        
    }else{
        SP_ZoneContentFrame *frameModel = _contentArr[indexPath.section];
        
        SP_ZoneCommentFrame *commentFrame = frameModel.commentArr[indexPath.row - 1];
        
        if(commentFrame == frameModel.commentArr[0]){
            return commentFrame.cellHeight;
            
        }else{
            return commentFrame.cellHeight;
            
        }
    
    }
    return 0;
}


- (void) cellNameDidClick:(SP_CommentTextSpecial *)Special
{
    if(Special.hfName.length == 0){
        NSLog(@"名字: -- %@",Special.nickName);
        NSLog(@"点中了几个seciton -- %ld",(long)Special.indexNum.section);
        
        _currentIndex = Special.indexNum;
        _currentPyqplid = Special.pyqplid;
        _currentPyqID = Special.pyqid;
        
    }else{
        _currentIndex = Special.indexNum;
        _currentPyqplid = Special.pyqplid;
        _currentPyqID = Special.pyqid;
        
    }
}




#pragma mark - 评论、分享、赞 按钮点击方法

/**
 *   评论按钮点击方法
 */
- (void) commentBtnDidClick:(SP_CommentBtn *)btn
{
    
    _currentIndex = btn.index;
    _currentPyqID = btn.pyqid;
    
    
    SP_SendMsgViewController *vc = [[SP_SendMsgViewController alloc]init];
    vc.pyqid = btn.pyqid;
    vc.type = @"0";
    [[AppDelegate sharedAppDelegate].nav pushViewController:vc animated:YES];
    
    
}


- (void) zanBtnDidClick:(SP_CommentBtn *)btn
{
    _refresh = 0;

    _currentZanBtn = btn;
    _currentIndex = btn.index;
    _currentPyqID = btn.pyqid;
    
    [self setHTTPRequest:PYQZHAN(GETUSERID, btn.pyqid) Tag:TAG_HTTP_PYQZHAN];
    
}





- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //打开侧滑菜单
    [self openSliderMenu];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //关闭侧滑菜单
    [self closeSliderMenu];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
