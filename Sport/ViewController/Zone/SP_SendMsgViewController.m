//
//  SP_SendMsgViewController.m
//  Sport
//
//  Created by 李松玉 on 15/7/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_SendMsgViewController.h"
#import "FaceView.h"
#import "SP_TextView.h"

@interface SP_SendMsgViewController ()<FaceViewDelegate,UITextViewDelegate>
{
    UIView *_sendMsgView;
    
    SP_TextView *_msgTextView;
    UIButton *_msgSendBtn;
    FaceView *_faceView;

}
@end

@implementation SP_SendMsgViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromRGB(0xfafafa)];
    [self setNavigationBar:@"发表评论"];
    [self addBackButton];
    [self setUpMsgTextView];
    [self setUpSendMsgView];
    
    //监听键盘的通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil] ;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(KeyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil] ;




}



/**
 *  评论发送按钮点击方法
 */
- (void) sendBtnDidClick
{
    if(_msgTextView.text.length == 0){
        return;
    }
    
    
    
    if([self.type isEqualToString:@"0"]){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_msgTextView.text forKey:@"content"];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.pyqid forKey:@"pyqid"];
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:PUBLISHCOMMENT Dictionary:dict Tag:TAG_HTTP_PUBLISHCOMMENT];
        [_msgTextView resignFirstResponder];
    }else if ([self.type isEqualToString:@"1"]){
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_msgTextView.text forKey:@"content"];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.hfuid forKey:@"hfuid"];
        [dict setObject:self.pyqplid forKey:@"pyqplid"];

    
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:PUBLISHPYQMSGPLHF Dictionary:dict Tag:TAG_HTTP_PUBLISHPYQMSGPLHF];

    
    }else if ([self.type isEqualToString:@"3"]){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.hfuid forKey:@"hfuid"];
        [dict setObject:self.usermsgid forKey:@"usermsgid"];
        [dict setObject:_msgTextView.text forKey:@"content"];
        
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:USERREPLYCOMMENT Dictionary:dict Tag:TAG_HTTP_USERREPLYCOMMENT];
        
    }else if ([self.type isEqualToString:@"4"]){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:GETUSERID forKey:@"uid"];
        [dict setObject:self.hfuid forKey:@"hfuid"];
        [dict setObject:self.zsbplid forKey:@"zsbplid"];
        [dict setObject:self.teamID forKey:@"teamid"];
        [dict setObject:_msgTextView.text forKey:@"content"];
        
        NSLog(@"dict -- %@",dict);
        
        
        [self asyshowHUDView:WAITTING CurrentView:self];
        [self setHttpPostData:PUBLISHZSBMSGPLHF Dictionary:dict Tag:TAG_HTTP_PUBLISHZSBMSGPLHF];

    
    }
}

- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
            if (HTTP_SUCCESS(code))
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"iAmBack" object:nil];
                [self back];
                [self showMessage:@"评论发布成功"];
            }else
            {
                [self showMessage:@"评论发布失败"];
            }

    }
}





- (void) setUpMsgTextView
{
    _msgTextView = [[SP_TextView alloc]initWithFrame:CGRectMake(0, 64, ScreenWidth, 150)];
    _msgTextView.placehoder = @"  这一刻的想法";
    _msgTextView.delegate = self;
    [self.view addSubview:_msgTextView];
}



#pragma mark - 设置底部的聊天框
- (void) setUpSendMsgView
{
    _sendMsgView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 40, ScreenWidth, 40)];
    _sendMsgView.backgroundColor = UIColorFromRGB(0xf3f3f3);
    
    UIView *topLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 1)];
    topLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    
    UIButton *faceBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 8, 20, 20)];
    [faceBtn setBackgroundImage:[UIImage imageNamed:@"ico_face"] forState:UIControlStateNormal];
    [faceBtn setBackgroundImage:[UIImage imageNamed:@"ico_face_pressed"] forState:UIControlStateHighlighted];
    [faceBtn addTarget:self action:@selector(faceBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
    [_sendMsgView addSubview:faceBtn];
    
    
    _msgSendBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 50, 0, 40, 40)];
    [_msgSendBtn setTitleColor:UIColorFromRGB(0x696969) forState:UIControlStateNormal];
    [_msgSendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [_msgSendBtn addTarget:self action:@selector(sendBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    _msgSendBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    
    
    
    [self.view addSubview:_sendMsgView];
    [_sendMsgView addSubview:topLine];
    [_sendMsgView addSubview:_msgSendBtn];
    
}

- (void)faceBtnDidClick:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (btn.selected == YES) {
        //        [_textField resignFirstResponder];
        _faceView = [[FaceView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 216)];
        _faceView.delegate = self;
        _msgTextView.inputView = _faceView;
        [_msgTextView reloadInputViews];
        [_msgTextView becomeFirstResponder];
    }else
    {
        _msgTextView.inputView = nil;
        [_msgTextView reloadInputViews];
        [_msgTextView becomeFirstResponder];
    }
}


-(void)faceViewDelegate:(NSInteger)tag{
    // 获得光标所在的位置
    _msgTextView.placehoder=@"";
    NSInteger location = _msgTextView.selectedRange.location;
    // 将UITextView中的内容进行调整（主要是在光标所在的位置进行字符串截取，再拼接你需要插入的文字即可）
    NSString *content = _msgTextView.text;
    if (tag == 118) {
        if (location == 0) {
            return;
        }
        NSString *result = [NSString stringWithFormat:@"%@%@",[content substringToIndex:location - 1],[content substringFromIndex:location]];
        _msgTextView.text = result;
        NSRange range;
        range.location = location - 1;
        range.length = 0;
        _msgTextView.selectedRange = range;
        //        [self textViewDidChange:myTextView_content];
        return;
    }
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"emoji" ofType:nil];
    NSString *str= [NSString stringWithContentsOfFile:plistPath encoding:NSUTF8StringEncoding error:nil];
    NSArray *array = [str componentsSeparatedByString:@"\n"];
    
    NSString *string = array[tag];
    NSRange rang1 = [string rangeOfString:@"["];
    NSRange rang2 = [string rangeOfString:@"]"];
    
    NSString *result = [NSString stringWithFormat:@"%@%@%@",[content substringToIndex:location],[string substringWithRange:NSMakeRange(rang1.location, rang2.location - rang1.location + 1)],[content substringFromIndex:location]];
    // 将调整后的字符串添加到UITextView上面
    _msgTextView.text = result;
    NSRange range;
    range.location = location + [[string substringWithRange:NSMakeRange(rang1.location, rang2.location - rang1.location + 1)] length];
    range.length = 0;
    _msgTextView.selectedRange = range;
    //    [self textViewDidChange:myTextView_content];
}



/**
 *  当键盘出现的时候，移动View的位置
 */
- (void)KeyBoardWillShow:(NSNotification *)note
{
    //  1.取得键盘最后的frame
    CGRect keyBoardFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //  2.计算控制器view需要平移的距离
    CGFloat transformY = keyBoardFrame.origin.y - (self.view.frame.size.height);
    
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.transform = CGAffineTransformMakeTranslation(0, transformY);
        
    }];
}

- (void)KeyBoardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.25 animations:^{
        _sendMsgView.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}


#pragma mark -textview delegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([@"\n" isEqualToString:text] == YES)
    {
        [textView resignFirstResponder];
        
        
        return NO;
    }
    return YES;
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    
    
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.editing = NO;
    [_msgTextView resignFirstResponder];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
