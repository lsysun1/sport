//
//  SP_SendMsgViewController.h
//  Sport
//
//  Created by 李松玉 on 15/7/23.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "BaseViewController.h"

@class SP_CommentTextSpecial;
@interface SP_SendMsgViewController : BaseViewController

@property (nonatomic,copy) NSString *pyqid;

@property (nonatomic,copy) NSString *hfuid;
@property (nonatomic,copy) NSString *pyqplid;
@property (nonatomic,copy) NSString *name;      //所要回复人的名字

@property (nonatomic,copy) NSString *type;      // 0 表示朋友圈信息评论      //1 表示平朋友圈信息回复
                                                // 3 表示回复个人中心评论    //4  战术板评论回复



@property (nonatomic,copy) NSString *usermsgid;


@property (nonatomic,copy) NSString *zsbplid;
@property (nonatomic,copy) NSString *teamID;


@end
