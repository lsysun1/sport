//
//  SP_ZonePostViewController.m
//  Sport
//
//  Created by 李松玉 on 15/6/27.
//  Copyright (c) 2015年 sport. All rights reserved.
//

#import "SP_ZonePostViewController.h"
#import "SP_TextView.h"
#import "ZipArchive.h"
#import "PhotoChoiceView.h"



@interface SP_ZonePostViewController ()<UITextViewDelegate,PhotoChoiceViewDelegate>
{
    UIScrollView *_scrollView;

    SP_TextView *_msgTextView;

    UIView *_photosView;
    
    UIButton *_photoAddBtn;
    NSMutableArray *_photoViewArray;
    NSMutableArray *jpgPathArray;   //转换后的jpg数组
    ZipArchive *jpgZip;
    NSMutableArray *_imgArray;
    long int timeINT;


}
@end

@implementation SP_ZonePostViewController

- (id) init
{
    self = [super init];
    if(self){
        _photoViewArray = [NSMutableArray array];
        _imgArray = [NSMutableArray array];
        jpgPathArray = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xfafafa);
    [self setNavigationBar:@"圈子"];
    [self addNavRightView];
    [self addBackButton];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64,ScreenWidth, ScreenHeight - 64)];
    _scrollView.backgroundColor = UIColorFromRGB(0xfafafa);
    [self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singTapMethod)];
    [_scrollView addGestureRecognizer:singtap];
    
    [self setUpMsgTextView];
    [self createPhotosView];


}

- (void)addNavRightView{
    UIButton *btnedit = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth-60, ios7_height, 44, 44)];
    btnedit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnedit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.topView addSubview:btnedit];
    
    [btnedit setTitle:@"发送"  forState:UIControlStateNormal];
    
    [btnedit setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [btnedit addTarget:self action:@selector(sendZoneInfo) forControlEvents:UIControlEventTouchUpInside];
    
    
}


- (void) singTapMethod
{
    [_msgTextView resignFirstResponder];
}


- (void) setUpMsgTextView
{
    _msgTextView = [[SP_TextView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 150)];
    _msgTextView.placehoder = @"  这一刻的想法";
    _msgTextView.delegate = self;
    [_scrollView addSubview:_msgTextView];
}

- (void) createPhotosView
{
    
    _photoAddBtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, 90, 90)];
    [_photoAddBtn setBackgroundImage:[UIImage imageNamed:@"addBtn"] forState:UIControlStateNormal];
    [_photoAddBtn addTarget:self action:@selector(addPhotoBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    
    
    if(_imgArray.count == 0){
        [_imgArray addObject:_photoAddBtn];
    }else{
        if([_imgArray[0] isKindOfClass:[UIImage class]]){
            [_imgArray insertObject:_photoAddBtn atIndex:0];
        }
    }
    
    
    _photosView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_msgTextView.frame), ScreenWidth, ScreenHeight * 10)];
    _photosView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:_photosView];
    
    CGFloat originX = 15;
    CGFloat originY = 10;
    for(int i = 0; i <_imgArray.count; i ++){
        if(i == 0){
            UIButton *photoView = _imgArray[i];
            photoView.frame = CGRectMake(originX, originY, 90, 90);
            [_photosView addSubview:photoView];
        }else{
            UIImage *photoImg = _imgArray[i];
            
            UIImageView *photoView = [[UIImageView alloc]initWithFrame:CGRectMake(originX, originY, 90, 90)];
            photoView.image = photoImg;
            photoView.tag = i + 100;
            [_photosView addSubview:photoView];
            [_photoViewArray addObject:photoView];
            
            UIButton *delBtn = [[UIButton alloc]initWithFrame:CGRectMake(originX + 83, originY - 5, 15, 15)];
            delBtn.tag = i +100;
            [delBtn setBackgroundImage:[UIImage imageNamed:@"btn_deletepic"] forState:UIControlStateNormal];
            [delBtn addTarget:self action:@selector(delBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
            [_photosView addSubview:delBtn];
        }
        
        // 计算下个view的尺寸
        originX += 100;
        
        if((i+1)%3 == 0 && i != 0){
            originX = 16;
            originY += 100;
        }
    }
    
    CGFloat maxH = CGRectGetMaxY(_msgTextView.frame) + _imgArray.count / 3 * 110 + 100;
//    CGFloat photoViewMaxH = _imgArray.count / 3 * 110 + 100;
//    _photosView.frame = CGRectMake(0, CGRectGetMaxY(_msgTextView.frame), ScreenWidth, photoViewMaxH);
    _scrollView.contentSize = CGSizeMake(ScreenWidth, maxH);
    
    
}


#pragma mark - push到添加图片ViewController
- (void) addPhotoBtnDidClick
{
    PhotoChoiceView *under = [[PhotoChoiceView alloc] initWithFrame:self.view.frame];
    under.delegate = self;
    [self.view addSubview:under];
}


#pragma mark 删除图片
- (void) delBtnDidClick:(UIButton *)btn
{
    [_imgArray removeObjectAtIndex:btn.tag - 100];
    [_photosView removeFromSuperview];
    [self createPhotosView];
    
}


/**
 *  打包图片
 */
- (void) zipImgs
{
    
    
    //获取时间戳
    timeINT= (long)[[NSDate  date] timeIntervalSince1970];
    
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    
    
    jpgZip = [[ZipArchive alloc] init];
    NSString* zipfile = [NSString stringWithFormat:@"%@/%ld.zip",documentsDirectory,timeINT];
    
    [jpgZip CreateZipFile2:zipfile];
    
    for(int i = 1;i<_imgArray.count;i++){
        
        UIImage *img = _imgArray[i];
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/img%d.jpg",i]];
        
        
        [UIImageJPEGRepresentation(img, 0.5) writeToFile:jpgPath atomically:YES];
        
        [jpgZip addFileToZip:jpgPath newname:[NSString stringWithFormat:@"img%d.jpg",i]];
        
        [fileMgr removeItemAtPath:jpgPath error:nil];
    }
    
    if( ![jpgZip CloseZipFile2] )
    {
        zipfile = @"";
    }
    
}

#pragma mark 发送请求
/**
 *  发送数据请求
 */

//name
//upload   场馆图片（zip包）
//location
//tel
//shi
//xian
//lat
//lng
//type

- (void) sendZoneInfo
{
    
    [self zipImgs];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    NSString *picsPath = [NSString stringWithFormat:@"%@/%ld.zip",docDir,timeINT];
    NSString *picsName = [NSString stringWithFormat:@"%ld",timeINT];

    
    NSString *content = _msgTextView.text;
   
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:GETUSERID forKey:@"uid"];
    [dictionary setObject:content forKey:@"content"];
    [dictionary setObject:picsName forKey:@"upload"];
    [dictionary setObject:GETZONENAM forKey:@"location"];

    
    
    [self setHttpPostFile:PUBLISHMSG Params:dictionary File:picsPath Data:nil Tag:TAG_HTTP_PUBLISHMSG];
    
}


#pragma mark - 数据请求完成
- (void) requestFinished:(ASIHTTPRequest *)request
{
    [super requestFinished:request];
    [self removeHUDView:self];
    if (request.responseString != nil)
    {
        
        NSDictionary *dictionary = [request.responseData objectFromJSONData];
        int code = [dictionary[@"code"] intValue];
        
        if(request.tag == TAG_HTTP_PUBLISHMSG){
            if (HTTP_SUCCESS(code))
            {
                [self showMessage:@"发布消息成功!"];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}




#pragma mark - PhotoChoiceViewDelegate
- (void)photoChoice:(UIImage *)image
{
    [_imgArray addObject:image];
    [self createPhotosView];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
