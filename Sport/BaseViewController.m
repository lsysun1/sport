//
//  GT_BaseViewController.m
//  LH_ToolKit
//
//  Created by chou on 13-11-20.
//  Copyright (c) 2013年 chou. All rights reserved.
//

#import "BaseViewController.h"
#import "METoast.h"
#import "TKAlertCenter.h"

static int NAVIGATIONBACKBUTTONTAG  = 2234;
static int NAVIGATIONRIGHTBUTTONTAG = 2233;
static int kLABELTAG                = 101;
static int HUDVIEWTAG               = 2235;
static int TITLEVIEW_TAG            = 2236;
static int BUTTONVIEW_TAG           = 2237;

@interface BaseViewController ()
{
    NSTimer *myTimer;
    UIScrollView * scrollVi;
    NSInteger OFFSET_X;
}

@end

@implementation BaseViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //隐藏系统导航条
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)setNavigationBar:(NSString *)titleStr
{
    //自定义用户导航条
    self.topView = [[UIView alloc] init];
    self.topView.frame = CGRectMake(0, 0, ScreenWidth, IOS7_OR_LATER ? 64 : 44);
    self.topView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    
    [self.view addSubview:self.topView];
    [self.view bringSubviewToFront:self.topView];
    
    UILabel *titleLable = [[UILabel alloc] init];
    titleLable.frame = CGRectMake(60, IOS7_OR_LATER ? 30 : 10, ScreenWidth - 60*2, 21);
    titleLable.backgroundColor = [UIColor clearColor];
    titleLable.font = [UIFont boldSystemFontOfSize:17];
    
    titleLable.textColor = [UIColor whiteColor];
    titleLable.text = titleStr;
    titleLable.tag = TITLEVIEW_TAG;
    [self.topView addSubview:titleLable];
    titleLable.textAlignment = NSTextAlignmentCenter;
   // [titleLable release];
    
   // [self.topView release];
}

- (void)setNavigationBarWithButton:(NSString *)titleStr
{
    //自定义用户导航条
    self.topView = [[UIView alloc] init];
    self.topView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), IOS7_OR_LATER ? 64 : 44);
    self.topView.backgroundColor = UIColorFromRGB(GREEN_COLOR_VALUE);
    [self.view addSubview:self.topView];
    [self.view bringSubviewToFront:self.topView];
    
  //  UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    [btn setTitle:titleStr forState:UIControlStateNormal];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, -37, 0, 0);
    [btn setImage:[UIImage imageNamed:@"icon_about.png"] forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(1, 102, 8, 29);
    btn.frame = CGRectMake(88, IOS7_OR_LATER ? 31 : 11, 144, 22);
    btn.tag = BUTTONVIEW_TAG;
    [btn addTarget:self action:@selector(clickTopButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:btn];
   // [btn release];
    
   // [self.topView release];
}


- (void)setViewTitle:(NSString *)titleStr
{
    UILabel *lable = (UILabel *)[self.topView viewWithTag:TITLEVIEW_TAG];
    if (lable != nil)
    {
        lable.text = titleStr;
        return;
    }
    UIButton *btn = (UIButton *)[self.topView viewWithTag:BUTTONVIEW_TAG];
    if (btn != nil)
    {
        [btn setTitle:titleStr forState:UIControlStateNormal];
    }
}

- (void)addBackButton
{
    [self.navigationItem setHidesBackButton:YES];
    
    if ([self.navigationController.navigationBar viewWithTag:NAVIGATIONBACKBUTTONTAG])
    {
        return;
    }
    
    CGFloat orginY = ios7_height;
    
    //NSString *clickStr = @"bg_back_s.png";
    NSString *normalStr = @"back_btn.png";
    //构造返回item的按钮视图
    //UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = NAVIGATIONBACKBUTTONTAG;
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//btn.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 10, 10);
    btn.frame = CGRectMake(10, orginY, 39, 44);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:normalStr
                                                              ofType:nil]]
                   forState:UIControlStateNormal];
//    [btn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
//                                                              pathForResource:clickStr
//                                                              ofType:nil]]
//                   forState:UIControlStateHighlighted];
//    [clickStr release];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:btn];
  //  [btn release];
}
//
//- (void)addHomeButton
//{
//    NSString *clickStr = @"bg_home_s.png";
//    NSString *nomalStr = @"bg_home_n.png";
//    //构造返回item的按钮视图
//    UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
//    btn.tag = NAVIGATIONRIGHTBUTTONTAG;
////    [btn setTitle:title forState:UIControlStateNormal];
//    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
//    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
//    CGFloat orginY = ios7_height;
//    btn.frame = CGRectMake(271, orginY, 44, 44);
//    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
//                                                              pathForResource:clickStr
//                                                              ofType:nil]]
//                   forState:UIControlStateHighlighted];
//    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
//                                                              pathForResource:nomalStr
//                                                              ofType:nil]]
//                   forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(backHome) forControlEvents:UIControlEventTouchUpInside];
//    [self.topView addSubview:btn];
//    [btn release];
//}
//
- (void)addRightButton:(NSString *)image Action:(SEL)action Target:(id)target
{
    NSString *nomalStr = [NSString stringWithFormat:@"%@.png",image];
    //构造返回item的按钮视图
    //UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = NAVIGATIONRIGHTBUTTONTAG;
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    CGFloat orginY = ios7_height;
    btn.imageEdgeInsets = UIEdgeInsetsMake(2, 5, 16, 11);
    btn.frame = CGRectMake(275, orginY + 8, 44, 44);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:nomalStr
                                                              ofType:nil]]
                   forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:btn];
  //  [btn release];
}
//
//- (void)removeBackButton
//{
//    if ([self.navigationController.navigationBar viewWithTag:NAVIGATIONBACKBUTTONTAG])
//    {
//        [[self.navigationController.navigationBar viewWithTag:NAVIGATIONBACKBUTTONTAG] removeFromSuperview];
//    }
//}

- (void)addRightButton:(NSString *)title Target:(id)target Action:(SEL)action ImageName:(NSString *)imgname Frame:(CGRect)frame
{
    NSString *clickStr = [NSString stringWithFormat:@"%@_s.png",imgname];
    NSString *nomalStr = [NSString stringWithFormat:@"%@_n.png",imgname];
    //构造返回item的按钮视图
    //UIButton *btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = NAVIGATIONRIGHTBUTTONTAG;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    btn.frame = frame;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:clickStr
                                                              ofType:nil]]
                   forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]
                                                              pathForResource:nomalStr
                                                              ofType:nil]]
                   forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:btn];
   // [btn release];
}

- (void)asyshowHUDView:(NSString *)title CurrentView:(UIViewController *)viewController
{
    LH_MBProgressHUD *tagview = (LH_MBProgressHUD *)[self.view viewWithTag:HUDVIEWTAG];
    if (tagview)
    {
        tagview.labelText = title;
        return;
    }
    CGFloat hudOrgin = IOS7_OR_LATER ? 64 : 44;
    HUD = [[LH_MBProgressHUD alloc] initWithView:self.view];
    HUD.frame = CGRectMake(0, hudOrgin, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - hudOrgin);
    HUD.animationType = MBProgressHUDAnimationZoom;
    HUD.tag = HUDVIEWTAG;
    [self.view addSubview:HUD];
    HUD.labelText = title;
	
    [HUD show:YES];
}

- (void)removeHUDView:(UIViewController *)viewcontroller
{
    if (HUD != nil)
    {
        [HUD hide:YES];
        [HUD removeFromSuperview];
     //   [HUD release];
        HUD = nil;
    }
}

- (void)showMessage:(NSString *)message
{
    [METoast resetToastAttribute];
    [METoast toastWithMessage:message];
}

- (void)showAlertStr:(NSString *)msgString{

    [[TKAlertCenter defaultCenter] postAlertWithMessage:msgString];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alert:(NSString*)_alert
{
	if (!_alert)
	{
		_alert = @"出错啦!";
	}
    CGFloat orginY = ios7_height;
	//移除alert
//	UIImageView *aview = (UIImageView *)[self.view viewWithTag:666];
//	if (aview)
//	{
//		UILabel *l = (UILabel *)[aview viewWithTag:kLABELTAG];
//		if (l)
//		{
//			l.text = _alert;
//			_alert = nil;
//			//动画开始
//			[UIView beginAnimations:nil context:nil];
//			[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//			[UIView setAnimationDuration:4.0f];
//			aview.frame = CGRectMake(0, 44 + orginY, CGRectGetWidth(self.view.bounds), 50);
//			[UIView commitAnimations];
//			
//			[UIView beginAnimations:nil context:nil];
//			[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//			[UIView setAnimationDuration:2.0f];
//			aview.frame = CGRectMake(0, -64, CGRectGetWidth(self.view.bounds), 50);
//			[UIView commitAnimations];
//			
//			return;
//		}
//	}
//	//移除旧视图
//	UIImageView *view = (UIImageView*)[self.view viewWithTag:666];
//	if (view) {[view removeFromSuperview];}
//	//背景视图
//	UIImageView *myview = [[UIImageView alloc] init];
//	myview.tag = 666;
//	UIImage *image = [[UIImage alloc]initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bg_navigationbar.png" ofType:nil]];
//	myview.frame = CGRectMake(0, -64, CGRectGetWidth(self.view.bounds), 50);
//	myview.image = image;
//	[image release];
//	
//	//提示文字
//	UILabel *alertLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.view.bounds) - 20, 30)];
//	alertLab.backgroundColor = [UIColor clearColor];
//	alertLab.text = _alert;
//	alertLab.tag = kLABELTAG;
//	alertLab.font = [UIFont boldSystemFontOfSize:14];
//	alertLab.textColor = [UIColor whiteColor];
//	alertLab.textAlignment = UITextAlignmentCenter;
//	[myview addSubview:alertLab];
//	[alertLab release];
//
//	//动画开始
//	[UIView beginAnimations:nil context:nil];
//	[UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//	[UIView setAnimationDuration:4.0f];
//	myview.frame = CGRectMake(0, 44 + orginY, CGRectGetWidth(self.view.bounds), 50);
//	[self.view insertSubview:myview belowSubview:self.topView];
//	[UIView commitAnimations];
//	
//	[UIView beginAnimations:nil context:nil];
//	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//	[UIView setAnimationDuration:4.0f];
//	myview.frame = CGRectMake(0, -64, CGRectGetWidth(self.view.bounds), 50);
//	[UIView commitAnimations];
//	[myview release];
//	_alert = nil;
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, -(44 + orginY), CGRectGetWidth(self.view.bounds), 50);
    view.backgroundColor = [UIColor clearColor];
    view.tag = kLABELTAG;
    [self.view insertSubview:view belowSubview:self.topView];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 50);
    imageView.image = [UIImage imageNamed:@"bg_navigationbar.png"];
    [view addSubview:imageView];
   // [imageView release];
    
    UILabel *alertLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, CGRectGetWidth(self.view.bounds) - 20, 30)];
	alertLab.backgroundColor = [UIColor clearColor];
	alertLab.text = _alert;
	alertLab.font = [UIFont boldSystemFontOfSize:14];
	alertLab.textColor = [UIColor whiteColor];
	alertLab.textAlignment = UITextAlignmentCenter;
	[view addSubview:alertLab];
	//[alertLab release];
    
    [UIView animateWithDuration:0.4 animations:^{
        [view setFrame:CGRectMake(0, orginY + 43, CGRectGetWidth(self.view.bounds), 50)];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(hideAlertView) withObject:nil afterDelay:1.0f];
    }];
    //[view release];
}

- (void)hideAlertView
{
    CGFloat orginY = ios7_height;
    UIView *view = (UIView *)[self.view viewWithTag:kLABELTAG];
    if (view != nil)
    {
        [UIView animateWithDuration:0.4 animations:^{
            [view setFrame:CGRectMake(0, -(44 + orginY), CGRectGetWidth(self.view.bounds), 50)];
        } completion:^(BOOL finished) {
            [view removeFromSuperview];
        }];
    }
}

- (void)setHTTPRequest:(NSString *)address Tag:(int)tag
{
    if (![GT_Tool isExistenceNetwork])
    {
        [self removeHUDView:self];
        [self alert:@"未链接到网络,请检查网络设置"];
        return;
    }
    if (requestBase != nil)
    {
      //  [requestBase release];
        requestBase = nil;
    }
    requestBase = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:address]];
    requestBase.tag = tag;
    requestBase.delegate = self;
    requestBase.timeOutSeconds = TIMEOUT;
    [requestBase startAsynchronous];
}

- (void)sethttpRequestFormat:(NSString *)address Tag:(int)tag Data:(NSData *)data
{
    if (![GT_Tool isExistenceNetwork])
    {
        [self alert:@"未链接到网络,请检查网络设置"];
        return;
    }
    if (requestFormat != nil)
    {
        //[requestFormat release];
        requestFormat = nil;
    }
    requestFormat = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:address]];
    requestFormat.tag = tag;
    [requestFormat appendPostData:data];
    requestFormat.delegate = self;
    requestFormat.tag = tag;
    requestFormat.timeOutSeconds = TIMEOUT;
    [requestFormat startAsynchronous];
}



- (void)setHttpPostFile:(NSString *)address Params:(NSDictionary *)dictionary File:(NSString *)file Data:(NSData *)data Tag:(int)tag
{
    if (![GT_Tool isExistenceNetwork])
    {
        [self alert:@"当前网络不可用,请检查网络设置"];
        [self removeHUDView:self];
        return;
    }
    if (requestFormat != nil)
    {
        //[requestFormat release];
        requestFormat = nil;
    }
    
    NSLog(@"address:%@",address);
    requestFormat =[ASIFormDataRequest requestWithURL:[NSURL URLWithString:address]];
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF8);
    [requestFormat setStringEncoding:enc];
    [requestFormat setFile:file forKey:@"upload"];
    requestFormat.tag = tag;
    for (NSString *key in dictionary)
    {
        [requestFormat setPostValue:dictionary[key] forKey:key];
    }
    requestFormat.delegate = self;
    requestFormat.tag = tag;
    requestFormat.timeOutSeconds = TIMEOUT;
    [requestFormat buildRequestHeaders];
    [requestFormat startAsynchronous];
}




- (void)setHttpPostData:(NSString *)address Dictionary:(NSMutableDictionary *)dictionary Tag:(int)tag
{
    if (![GT_Tool isExistenceNetwork])
    {
        [self alert:@"未链接到网络,请检查网络设置"];
        return;
    }
    if (requestFormat != nil)
    {
        //[requestFormat release];
        requestFormat = nil;
    }
    
    requestFormat = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:address]];
    requestFormat.tag = tag;
    for (NSString *key in dictionary)
    {
        [requestFormat setPostValue:dictionary[key] forKey:key];
    }
    requestFormat.delegate = self;
    requestFormat.tag = tag;
    requestFormat.timeOutSeconds = TIMEOUT;
    [requestFormat startAsynchronous];
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self removeHUDView:self];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [self removeHUDView:self];
    [self alert:@"请检查网络设置"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (requestBase != nil)
    {
        [requestBase clearDelegatesAndCancel];
 //       [requestBase release];
        requestBase = nil;
    }
    if (requestFormat != nil)
    {
        [requestFormat clearDelegatesAndCancel];
  //      [requestFormat release];
        requestFormat = nil;
    }
}

- (UIViewController *)viewController
{
    for (UIView *next = (UIView *)[self.view superview];next;next = next.superview)
    {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	if (IOS7_OR_LATER)
    {
        //不让UISCrollerView自动调整。IOS7特有属性
        [self setAutomaticallyAdjustsScrollViewInsets:NO];
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return(toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int)viewTop
{
    return (self.topView.frame.origin.y + self.topView.frame.size.height);
}


#pragma mark - 关闭侧滑菜单

- (void) closeSliderMenu
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    [appDelegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    
    [appDelegate.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    
}

#pragma mark - 开启侧滑菜单
- (void) openSliderMenu
{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    [appDelegate.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    
    [appDelegate.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];

}



- (NSString *)urlEncodeValue:(NSString *)str
{
    NSString *result = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, NULL, kCFStringEncodingUTF8));
    return result;
}



- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM月dd日 hh:mm"];
    NSTimeInterval time= [dateline doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    return [formatter stringFromDate:detaildate];
}


- (void)dealloc
{
 //   [_topView release];
    if (myTimer)
    {
        [myTimer invalidate];
    //    [myTimer release];
        myTimer = nil;
    }
    if (scrollVi)
    {
    //    [scrollVi release];
        scrollVi = nil;
    }
   // [super dealloc];
}

@end
