//
//  GT_BaseViewController
//  LH_ToolKit
//  所有试图基类，实现试图基本方法
//  Created by chou on 13-11-20.
//  Copyright (c) 2013年 chou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "GT_Tool.h"
#import "LH_MBProgressHUD.h"
#import "JSONKit.h"
#import "AppDelegate.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "UIImageView+WebCache.h"
#import "GTMBase64.h"

#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>


@interface BaseViewController : UIViewController
{
    LH_MBProgressHUD *HUD;
    ASIHTTPRequest *requestBase;
    ASIFormDataRequest *requestFormat;
}

@property (nonatomic, retain) UIView *topView;

/**
 *  添加导航返回按钮
 */
- (void)addBackButton;

//
///**
// *  添加返回主页按钮
// */
//- (void)addHomeButton;

/**
 *  添加返回主页按钮
 */
- (void)addRightButton:(NSString *)image Action:(SEL)action Target:(id)target;
//
///**
// *  移除导航返回按钮
// */
//- (void)removeBackButton;
/**
 *  添加导航右边按钮
 *
 *  @param title   按钮标题
 *  @param target  按钮代理时间
 *  @param action  按钮执行方法
 *  @param imgname 按钮背景图片
 *  @param frame   按钮frame
 */
- (void)addRightButton:(NSString *)title Target:(id)target Action:(SEL)action ImageName:(NSString *)imgname Frame:(CGRect)frame;
///**
// *  移除导航右边按钮
// */
//- (void)removeRightButton;
//
/**
 *  显示等待框
 *
 *  @param title          显示文字
 *  @param viewController 在viewcontroller显示(self)
 */
- (void)asyshowHUDView:(NSString *)title CurrentView:(UIViewController *)viewController;

/**
 *  移除等待框
 *
 *  @param viewcontroller self
 */
- (void)removeHUDView:(UIViewController *)viewcontroller;

/**
 *  显示提示信息
 *
 *  @param message 显示的内容
 */
- (void)showMessage:(NSString *)message;

- (void)showAlertStr:(NSString *)msgString;

/**
 *  动画显示提醒框
 *
 *  @param _alert 显示的内容
 */
//- (void)alert:(NSString*)_alert;
//
/**
 *  基类数据请求方法
 *
 *  @param address 请求地址
 */
- (void)setHTTPRequest:(NSString *)address Tag:(int)tag;

/**
 *  基类数据请求方法（POST）
 *
 *  @param address 请求地址
 *  @param tag     当前请求标识
 *  @param data    post上传数据
 */
- (void)sethttpRequestFormat:(NSString *)address Tag:(int)tag Data:(NSData *)data;

- (void)setHttpPostData:(NSString *)address Dictionary:(NSMutableDictionary *)dictionary Tag:(int)tag;

//
///**
// *  请求数据方法回调(用于子类定义自己的)
// *
// *  @param request 请求对象
// */
- (void)requestFinished:(ASIHTTPRequest *)request;

/**
 *  设置导航条
 */
- (void)setNavigationBar:(NSString *)titleStr;

/**
 *  设置导航条
 */
- (void)setNavigationBarWithButton:(NSString *)titleStr;

/**
 *  设置背景图片
 */
- (void)setViewBackgroundColor:(UIView *)view;
//
/**
 *  修改title
 *
 *  @param titleStr title
 */
- (void)setViewTitle:(NSString *)titleStr;

/**
 *  获取当前view的ViewController
 *
 *  @return 返回ViewController
 */
- (UIViewController *)viewController;

/**
 *  获取当前topview的高度
 *
 *  @return 返回高度
 */
- (int)viewTop;


/**
 *  关闭侧滑菜单
 */
- (void) closeSliderMenu;


/**
 *  开启侧滑菜单
 */
- (void) openSliderMenu;

/**
 *  URL中的中文转码
 */

- (NSString *)urlEncodeValue:(NSString *)str;

/**
 *  时间戳转格式化的时间
 */
- (NSString *)dateWithTimeIntervalSince1970:(NSString *)dateline;


- (void)setHttpPostFile:(NSString *)address Params:(NSDictionary *)dictionary File:(NSString *)file Data:(NSData *)data Tag:(int)tag;

@end
